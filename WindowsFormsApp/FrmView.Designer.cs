﻿
namespace WindowsFormsApp
{
    partial class FrmView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmView));
            this.modbusView1 = new WindowsFormsApp.customizeWidget.ModbusView();
            this.modbusView2 = new WindowsFormsApp.customizeWidget.ModbusView();
            this.button1 = new System.Windows.Forms.Button();
            this.skinTabControl1 = new CCWin.SkinControl.SkinTabControl();
            this.skinTabPage1 = new CCWin.SkinControl.SkinTabPage();
            this.skinTabPage2 = new CCWin.SkinControl.SkinTabPage();
            this.modbusView4 = new WindowsFormsApp.customizeWidget.ModbusView();
            this.modbusView3 = new WindowsFormsApp.customizeWidget.ModbusView();
            this.skinTabControl2 = new CCWin.SkinControl.SkinTabControl();
            this.skinTabPage3 = new CCWin.SkinControl.SkinTabPage();
            this.skinTabPage4 = new CCWin.SkinControl.SkinTabPage();
            this.skinTabPage5 = new CCWin.SkinControl.SkinTabPage();
            this.modbusView5 = new WindowsFormsApp.customizeWidget.ModbusView();
            this.skinTabControl1.SuspendLayout();
            this.skinTabPage1.SuspendLayout();
            this.skinTabPage2.SuspendLayout();
            this.skinTabControl2.SuspendLayout();
            this.skinTabPage3.SuspendLayout();
            this.skinTabPage4.SuspendLayout();
            this.skinTabPage5.SuspendLayout();
            this.SuspendLayout();
            // 
            // modbusView1
            // 
            this.modbusView1.IsRegister = 1;
            this.modbusView1.Location = new System.Drawing.Point(18, 72);
            this.modbusView1.Name = "modbusView1";
            this.modbusView1.RemPath = null;
            this.modbusView1.Size = new System.Drawing.Size(693, 464);
            this.modbusView1.TabIndex = 0;
            // 
            // modbusView2
            // 
            this.modbusView2.IsRegister = 2;
            this.modbusView2.Location = new System.Drawing.Point(717, 72);
            this.modbusView2.Name = "modbusView2";
            this.modbusView2.RemPath = null;
            this.modbusView2.Size = new System.Drawing.Size(693, 464);
            this.modbusView2.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(138, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(155, 51);
            this.button1.TabIndex = 2;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // skinTabControl1
            // 
            this.skinTabControl1.AnimatorType = CCWin.SkinControl.AnimationType.HorizSlide;
            this.skinTabControl1.CloseRect = new System.Drawing.Rectangle(2, 2, 12, 12);
            this.skinTabControl1.Controls.Add(this.skinTabPage1);
            this.skinTabControl1.Controls.Add(this.skinTabPage2);
            this.skinTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabControl1.HeadBack = null;
            this.skinTabControl1.ImgTxtOffset = new System.Drawing.Point(0, 0);
            this.skinTabControl1.ItemSize = new System.Drawing.Size(70, 36);
            this.skinTabControl1.Location = new System.Drawing.Point(8, 39);
            this.skinTabControl1.Name = "skinTabControl1";
            this.skinTabControl1.PageArrowDown = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageArrowDown")));
            this.skinTabControl1.PageArrowHover = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageArrowHover")));
            this.skinTabControl1.PageCloseHover = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageCloseHover")));
            this.skinTabControl1.PageCloseNormal = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageCloseNormal")));
            this.skinTabControl1.PageDown = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageDown")));
            this.skinTabControl1.PageHover = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageHover")));
            this.skinTabControl1.PageImagePosition = CCWin.SkinControl.SkinTabControl.ePageImagePosition.Left;
            this.skinTabControl1.PageNorml = null;
            this.skinTabControl1.SelectedIndex = 1;
            this.skinTabControl1.Size = new System.Drawing.Size(1422, 627);
            this.skinTabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.skinTabControl1.TabIndex = 3;
            // 
            // skinTabPage1
            // 
            this.skinTabPage1.BackColor = System.Drawing.Color.White;
            this.skinTabPage1.Controls.Add(this.button1);
            this.skinTabPage1.Controls.Add(this.modbusView2);
            this.skinTabPage1.Controls.Add(this.modbusView1);
            this.skinTabPage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage1.Location = new System.Drawing.Point(0, 36);
            this.skinTabPage1.Name = "skinTabPage1";
            this.skinTabPage1.Size = new System.Drawing.Size(1422, 591);
            this.skinTabPage1.TabIndex = 0;
            this.skinTabPage1.TabItemImage = null;
            this.skinTabPage1.Text = "控制器";
            // 
            // skinTabPage2
            // 
            this.skinTabPage2.BackColor = System.Drawing.Color.White;
            this.skinTabPage2.Controls.Add(this.skinTabControl2);
            this.skinTabPage2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage2.Location = new System.Drawing.Point(0, 36);
            this.skinTabPage2.Name = "skinTabPage2";
            this.skinTabPage2.Size = new System.Drawing.Size(1422, 591);
            this.skinTabPage2.TabIndex = 1;
            this.skinTabPage2.TabItemImage = null;
            this.skinTabPage2.Text = "机械臂";
            // 
            // modbusView4
            // 
            this.modbusView4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.modbusView4.IsRegister = 4;
            this.modbusView4.Location = new System.Drawing.Point(0, 0);
            this.modbusView4.Name = "modbusView4";
            this.modbusView4.RemPath = null;
            this.modbusView4.Size = new System.Drawing.Size(1422, 555);
            this.modbusView4.TabIndex = 1;
            // 
            // modbusView3
            // 
            this.modbusView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.modbusView3.IsRegister = 3;
            this.modbusView3.Location = new System.Drawing.Point(0, 0);
            this.modbusView3.Name = "modbusView3";
            this.modbusView3.RemPath = null;
            this.modbusView3.Size = new System.Drawing.Size(1422, 555);
            this.modbusView3.TabIndex = 0;
            // 
            // skinTabControl2
            // 
            this.skinTabControl2.AnimatorType = CCWin.SkinControl.AnimationType.HorizSlide;
            this.skinTabControl2.CloseRect = new System.Drawing.Rectangle(2, 2, 12, 12);
            this.skinTabControl2.Controls.Add(this.skinTabPage3);
            this.skinTabControl2.Controls.Add(this.skinTabPage4);
            this.skinTabControl2.Controls.Add(this.skinTabPage5);
            this.skinTabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabControl2.HeadBack = null;
            this.skinTabControl2.ImgTxtOffset = new System.Drawing.Point(0, 0);
            this.skinTabControl2.ItemSize = new System.Drawing.Size(70, 36);
            this.skinTabControl2.Location = new System.Drawing.Point(0, 0);
            this.skinTabControl2.Name = "skinTabControl2";
            this.skinTabControl2.PageArrowDown = ((System.Drawing.Image)(resources.GetObject("skinTabControl2.PageArrowDown")));
            this.skinTabControl2.PageArrowHover = ((System.Drawing.Image)(resources.GetObject("skinTabControl2.PageArrowHover")));
            this.skinTabControl2.PageCloseHover = ((System.Drawing.Image)(resources.GetObject("skinTabControl2.PageCloseHover")));
            this.skinTabControl2.PageCloseNormal = ((System.Drawing.Image)(resources.GetObject("skinTabControl2.PageCloseNormal")));
            this.skinTabControl2.PageDown = ((System.Drawing.Image)(resources.GetObject("skinTabControl2.PageDown")));
            this.skinTabControl2.PageHover = ((System.Drawing.Image)(resources.GetObject("skinTabControl2.PageHover")));
            this.skinTabControl2.PageImagePosition = CCWin.SkinControl.SkinTabControl.ePageImagePosition.Left;
            this.skinTabControl2.PageNorml = null;
            this.skinTabControl2.SelectedIndex = 1;
            this.skinTabControl2.Size = new System.Drawing.Size(1422, 591);
            this.skinTabControl2.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.skinTabControl2.TabIndex = 2;
            // 
            // skinTabPage3
            // 
            this.skinTabPage3.BackColor = System.Drawing.Color.White;
            this.skinTabPage3.Controls.Add(this.modbusView3);
            this.skinTabPage3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage3.Location = new System.Drawing.Point(0, 36);
            this.skinTabPage3.Name = "skinTabPage3";
            this.skinTabPage3.Size = new System.Drawing.Size(1422, 555);
            this.skinTabPage3.TabIndex = 0;
            this.skinTabPage3.TabItemImage = null;
            this.skinTabPage3.Text = "输入";
            // 
            // skinTabPage4
            // 
            this.skinTabPage4.BackColor = System.Drawing.Color.White;
            this.skinTabPage4.Controls.Add(this.modbusView5);
            this.skinTabPage4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage4.Location = new System.Drawing.Point(0, 36);
            this.skinTabPage4.Name = "skinTabPage4";
            this.skinTabPage4.Size = new System.Drawing.Size(1422, 555);
            this.skinTabPage4.TabIndex = 1;
            this.skinTabPage4.TabItemImage = null;
            this.skinTabPage4.Text = "输出";
            // 
            // skinTabPage5
            // 
            this.skinTabPage5.BackColor = System.Drawing.Color.White;
            this.skinTabPage5.Controls.Add(this.modbusView4);
            this.skinTabPage5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage5.Location = new System.Drawing.Point(0, 36);
            this.skinTabPage5.Name = "skinTabPage5";
            this.skinTabPage5.Size = new System.Drawing.Size(1422, 555);
            this.skinTabPage5.TabIndex = 2;
            this.skinTabPage5.TabItemImage = null;
            this.skinTabPage5.Text = "R寄存器";
            // 
            // modbusView5
            // 
            this.modbusView5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.modbusView5.IsRegister = 5;
            this.modbusView5.Location = new System.Drawing.Point(0, 0);
            this.modbusView5.Name = "modbusView5";
            this.modbusView5.RemPath = null;
            this.modbusView5.Size = new System.Drawing.Size(1422, 555);
            this.modbusView5.TabIndex = 0;
            // 
            // FrmView
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1438, 674);
            this.Controls.Add(this.skinTabControl1);
            this.Name = "FrmView";
            this.Text = "设备状态检测";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmView_FormClosing);
            this.Load += new System.EventHandler(this.FrmView_Load);
            this.skinTabControl1.ResumeLayout(false);
            this.skinTabPage1.ResumeLayout(false);
            this.skinTabPage2.ResumeLayout(false);
            this.skinTabControl2.ResumeLayout(false);
            this.skinTabPage3.ResumeLayout(false);
            this.skinTabPage4.ResumeLayout(false);
            this.skinTabPage5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private customizeWidget.ModbusView modbusView1;
        private customizeWidget.ModbusView modbusView2;
        private System.Windows.Forms.Button button1;
        private CCWin.SkinControl.SkinTabControl skinTabControl1;
        private CCWin.SkinControl.SkinTabPage skinTabPage1;
        private CCWin.SkinControl.SkinTabPage skinTabPage2;
        private customizeWidget.ModbusView modbusView3;
        private customizeWidget.ModbusView modbusView4;
        private CCWin.SkinControl.SkinTabControl skinTabControl2;
        private CCWin.SkinControl.SkinTabPage skinTabPage3;
        private CCWin.SkinControl.SkinTabPage skinTabPage4;
        private CCWin.SkinControl.SkinTabPage skinTabPage5;
        private customizeWidget.ModbusView modbusView5;
    }
}