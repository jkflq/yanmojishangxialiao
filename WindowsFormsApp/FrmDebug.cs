﻿using System;
using System.Collections.Generic;
using WindowsFormsApp.deviceConnect;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinFormsBase;
using System.Threading;

namespace WindowsFormsApp
{
    public partial class FrmDebug : CCWin.Skin_Color
    {

        public FrmDebug()
        {
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true); // 禁止擦除背景.
            SetStyle(ControlStyles.DoubleBuffer, true); // 双缓冲


            this.UpdateStyles();
            InitializeComponent();
            InitUpdate();
        }

        private void FrmDebug_Load(object sender, EventArgs e)
        {
            this.skinTabControl3.SelectedIndex = 0;
           
        }


        public void InitUpdate()
        {
            //将实例化的真空发生器对象赋值给控件里的真空发生器开关
            FirstRobotVacuumSwitch.vacuum = ConnectConfig.FirstRobotVacuum;
            SecondRobotVacuumSwitch.vacuum = ConnectConfig.SecondRobotVacuum;
            ThirdRobotVacuumSwitch.vacuum = ConnectConfig.ThirdRobotVacuum;

            //实例化夹爪
            grip_up.grip = ConnectConfig.Up_Grip;
            grip_down.grip = ConnectConfig.Down_Grip;

            //将实例化的气缸对象赋值给控件里的气缸开关
            qgSwitch1.qg = ConnectConfig.QG1;
            qgSwitch2.qg = ConnectConfig.QG2;
            qgSwitch3.qg = ConnectConfig.QG3;
            qgSwitch4.qg = ConnectConfig.QG4;
            qgSwitch5.qg = ConnectConfig.QG5;
            qgSwitch6.qg = ConnectConfig.QG6;
            qgSwitch7.qg = ConnectConfig.QG7;
            qgSwitch8.qg = ConnectConfig.QG8;
            qgSwitch9.qg = ConnectConfig.QG9;
            qgSwitch10.qg = ConnectConfig.QG10;
            qgSwitch11.qg = ConnectConfig.QG11;

            //将实例化的上料电缸对象赋值给空间里的电机对象 并对控件进行标号便于区分
            Up_Position1.emotor = ConnectConfig.UPDisksEMotor;
            Up_Position1.PositionIdex = 1;
            Up_Position2.emotor = ConnectConfig.UPDisksEMotor;
            Up_Position2.PositionIdex = 2;
            Up_Position3.emotor = ConnectConfig.UPDisksEMotor;
            Up_Position3.PositionIdex = 3;
            Up_Position4.emotor = ConnectConfig.UPDisksEMotor;
            Up_Position4.PositionIdex = 4;
            Up_Position5.emotor = ConnectConfig.UPDisksEMotor;
            Up_Position5.PositionIdex = 5;
            Up_Position6.emotor = ConnectConfig.UPDisksEMotor;
            Up_Position6.PositionIdex = 6;
            Up_Position7.emotor = ConnectConfig.UPDisksEMotor;
            Up_Position7.PositionIdex = 7;
            Up_Position8.emotor = ConnectConfig.UPDisksEMotor;
            Up_Position8.PositionIdex = 8;
            Up_PositionTG1.emotor = ConnectConfig.UPDisksEMotor;
            Up_PositionTG1.PositionIdex = 11;
            Up_PositionTG2.emotor = ConnectConfig.UPDisksEMotor;
            Up_PositionTG2.PositionIdex = 12;
            Up_PositionTG3.emotor = ConnectConfig.UPDisksEMotor;
            Up_PositionTG3.PositionIdex = 13;
            Up_PositionTG4.emotor = ConnectConfig.UPDisksEMotor;
            Up_PositionTG4.PositionIdex = 14;
            Up_PositionTG5.emotor = ConnectConfig.UPDisksEMotor;
            Up_PositionTG5.PositionIdex = 15;
            Up_PositionTG6.emotor = ConnectConfig.UPDisksEMotor;
            Up_PositionTG6.PositionIdex = 16;
            Up_PositionTG7.emotor = ConnectConfig.UPDisksEMotor;
            Up_PositionTG7.PositionIdex = 17;
            Up_PositionTG8.emotor = ConnectConfig.UPDisksEMotor;
            Up_PositionTG8.PositionIdex = 18;


            Down_Position1.emotor = ConnectConfig.DOWNDisksEMotor;
            Down_Position1.PositionIdex = 1;
            Down_Position2.emotor = ConnectConfig.DOWNDisksEMotor;
            Down_Position2.PositionIdex = 2;
            Down_Position3.emotor = ConnectConfig.DOWNDisksEMotor;
            Down_Position3.PositionIdex = 3;
            Down_Position4.emotor = ConnectConfig.DOWNDisksEMotor;
            Down_Position4.PositionIdex = 4;
            Down_PositionTG1.emotor = ConnectConfig.DOWNDisksEMotor;
            Down_PositionTG1.PositionIdex = 11;
            Down_PositionTG2.emotor = ConnectConfig.DOWNDisksEMotor;
            Down_PositionTG2.PositionIdex = 12;
            Down_PositionTG3.emotor = ConnectConfig.DOWNDisksEMotor;
            Down_PositionTG3.PositionIdex = 13;
            Down_PositionTG4.emotor = ConnectConfig.DOWNDisksEMotor;
            Down_PositionTG4.PositionIdex = 14;

            Take_Position1.emotor = ConnectConfig.TAKEDisksEMotor;
            Take_Position1.PositionIdex = 1;
            Take_Position2.emotor = ConnectConfig.TAKEDisksEMotor;
            Take_Position2.PositionIdex = 2;
            Take_Position3.emotor = ConnectConfig.TAKEDisksEMotor;
            Take_Position3.PositionIdex = 3;
            Take_Position4.emotor = ConnectConfig.TAKEDisksEMotor;
            Take_Position4.PositionIdex = 4;

            //将实例化的电缸对象赋值给控件的电缸对象，用于对其的控制
            Up_EmotorControl.emotor = ConnectConfig.UPDisksEMotor;
            Down_EmotorControl.emotor = ConnectConfig.DOWNDisksEMotor;
            Take_EmotorControl.emotor = ConnectConfig.TAKEDisksEMotor;
            //实例化电机
            Motor_Control.motor = ConnectConfig.Motor;

        }

        private void FrmDebug_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }

        private void skinTabControl3_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (skinTabControl3.SelectedIndex)
            {
                case 2:
                    {
                        Motor_Control.read();
                    }
                    break;
                case 3:
                    {
                        Up_Position1.Read();
                        Up_Position2.Read();
                        Up_Position3.Read();
                        Up_Position4.Read();
                        Up_Position5.Read();
                        Up_Position6.Read();
                        Up_Position7.Read();
                        Up_Position8.Read();
                        Up_PositionTG1.Read();
                        Up_PositionTG2.Read();
                        Up_PositionTG3.Read();
                        Up_PositionTG4.Read();
                        Up_PositionTG5.Read();
                        Up_PositionTG6.Read();
                        Up_PositionTG7.Read();
                        Up_PositionTG8.Read();
                    }
                    break;
                case 4:
                    {
                        Down_Position1.Read();
                        Down_Position2.Read();
                        Down_Position3.Read();
                        Down_Position4.Read();
                        Down_PositionTG1.Read();
                        Down_PositionTG2.Read();
                        Down_PositionTG3.Read();
                        Down_PositionTG4.Read();

                    }
                    break;
                case 5:
                    {
                        Take_Position1.Read();
                        Take_Position2.Read();
                        Take_Position3.Read();
                        Take_Position4.Read();
                    }
                    break;


            }

        }
    }

}
