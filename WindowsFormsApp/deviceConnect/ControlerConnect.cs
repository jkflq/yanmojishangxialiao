﻿using System;
using System.Collections.Generic;
using NModbus;
using WinFormsBase;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Windows.Forms;

namespace WindowsFormsApp.deviceConnect
{
    class ControlerConnect
    {
        //创建Modbus连接控制器
        private static ModbusFactory modbusFactory;
        private static IModbusMaster master;

        /// <summary>
        /// 开启控制器连接
        /// </summary>
        public static bool Connect()
        {

            return ConnectConfig.ControlUnit.ConnectServer();
                
        }


        /// <summary>
        /// 关闭PLC连接
        /// </summary>
        public static void Disconnect()
        {
            ConnectConfig.ControlUnit.stop();
        }


        //气嘴吸气
        



        /// <summary>
        /// 写保持寄存器
        /// </summary>
        /// <param name="address"></param>保持寄存器的地址
        /// <param name="value"></param>写入的值
        public async static void WriteMulR(ushort address, string value)
        {
            //参数(分别为站号,起始地址,长度)
            master = modbusFactory.CreateMaster(new TcpClient(ConnectConfig.ControlerIP, ConnectConfig.ControlerPort));
            byte slaveAddress;
            ushort startAddress;
            ushort[] registerBuffer;
            slaveAddress = 1;
            startAddress = address;
            string[] strarr = { value };
            registerBuffer = new ushort[strarr.Length];
            for (int i = 0; i < strarr.Length; i++)
            {
                registerBuffer[i] = ushort.Parse(strarr[i]);
            }
            await master.WriteMultipleRegistersAsync(slaveAddress, startAddress, registerBuffer);
        }


        /// <summary>
        /// 写单个线圈
        /// </summary>
        /// <param name="address"></param>线圈的地址
        /// <param name="value"></param>写入的值
        public async static void WriteMulC(ushort address, String value)
        {
            //参数(分别为站号,起始地址,长度)
            master = modbusFactory.CreateMaster(new TcpClient(ConnectConfig.ControlerIP, ConnectConfig.ControlerPort));
            byte slaveAddress;
            ushort startAddress;
            bool[] coilsBuffer;
            slaveAddress = 1;
            startAddress = address;
            string[] strarr = { value };
            coilsBuffer = new bool[strarr.Length];
            //转化为bool数组
            for (int i = 0; i < strarr.Length; i++)
            {
                // strarr[i] == "0" ? coilsBuffer[i] = true : coilsBuffer[i] = false;
                if (strarr[i] == "0")
                {
                    coilsBuffer[i] = false;
                }
                else
                {
                    coilsBuffer[i] = true;
                }
                await master.WriteMultipleCoilsAsync(slaveAddress, startAddress, coilsBuffer);
            }
        }
    }
}
