﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using WindowsFormsApp.utils.XML;

namespace WindowsFormsApp.deviceConnect
{
    class ParamConfig
    {


        public enum ProductionType
        {
            Pro_97R = 0,
            Pro_97U,
        }


        public static ProductionType DiskType;
        //暂停标志位（暂时先放到这里）
        public volatile static bool Pause = false;

        //报警标志
        public volatile static bool _warnflag = false;

        //错误标志
        public volatile static bool _errorflag = false;

        //当需要进行目标检测时，再利用此标志位开启监测
        public volatile static bool camera_run = false;

        //上料时出现真空报警时  下一次抓取时只上料不下料
        public static bool shangliao_erro = false;

        //手动状态下 使用自动流程时需要使用该标志位
        public static bool runflag = false;
        public static bool WarnFlag
        {
            set
            {
                _warnflag = value;
                if (value)
                {
                    //报警灯亮
                    RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_WARN_LIGHT, true);
                }
                else
                {
                    //报警灯灭
                    RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_WARN_LIGHT, false);

                }
            }
            get
            {
                return _warnflag;

            }
        }

        public static bool ErrorFlag
        {
            set
            {
                _errorflag = value;
                if (value)
                {
                    //错误灯亮
                    //RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_WARN_LIGHT, true);
                }
                else
                {
                    //错误灯灭
                   // RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_WARN_LIGHT, false);

                }
            }
            get
            {
                return _errorflag;

            }
        }


        /// <summary>
        /// 调整角度速度设定
        /// </summary>
        public enum ADJUST_ANGEL_SPEED
        {
            FAST = 100,//100
            SLIGHT = 50,//50
            SLOW = 5
        };


        #region 一号机械臂相关寄存器定义
        public static int START_POINT_LR_REG = 109;    //原点存储寄存器



        //切换姿态
        public static int point0_JR_REG = 0;
        public static int point1_JR_REG = 1;



        //盘数存储寄存器
        public static int PanNum_R_REG = 103;

        //磨盘内吸取延时
        public static int ProductionSuckDelay_R_REG = 100;

        //上料点吸取延时
        public static int SuckDelay_R_REG = 101;


        //首盘磨盘寄存器
        public static int FirstOrEndPan_R_REG = 13;


        //自动运行速度寄存器
        public static int AutoRun_R_REG = 106;

        //完成标志
        public static int SuccessFlag_R_REG = 105;


        List<int> FirstRobot_R_REG = new List<int> { AutoRun_R_REG, ProductionSuckDelay_R_REG, SuckDelay_R_REG };


        //用于每盘磨完后更新高度
        public static List<int> LR_REG = new List<int> { FANGLIAO_LR_REG, QULIAO_LR_REG, START_POINT_LR_REG };

        public static List<int> JR_REG = new List<int> { point0_JR_REG, point1_JR_REG };





        //offset添加LR寄存器  


        //上料时  各个放置点  
        public static int FirstPanOffset_LR_REG = 128;
        public static int FirstPanReleaseHeightOffset_LR_REG = 123;  //带有高度信息


        public static int SecondPanOffset_LR_REG = 129;
        public static int SecondPanReleaseHeightOffset_LR_REG = 124;

        public static int ThirdPanOffset_LR_REG = 130;
        public static int ThirdPanReleaseHeightOffset_LR_REG = 125;

        public static int FourthPanOffset_LR_REG = 131;
        public static int FourthPanReleaseHeightOffset_LR_REG = 126;

        public static int FifthPanOffset_LR_REG = 132;
        public static int FifthPanReleaseHeightOffset_LR_REG = 127;

        //取料点offset添加
        public static int QULIAO_LR_REG = 106;
        //放料点offset添加
        public static int FANGLIAO_LR_REG = 104;

        //上料点offset添加
        public static int SHANGLIAO_POINT_LR_REG = 100;
        //下料点offset添加
        public static int XIALIAO_POINT_LR_REG = 102;

        public static List<int> Offset_LR_REG = new List<int> {FirstPanOffset_LR_REG, SecondPanOffset_LR_REG, ThirdPanOffset_LR_REG, FourthPanOffset_LR_REG, FifthPanOffset_LR_REG, QULIAO_LR_REG, FANGLIAO_LR_REG,
        SHANGLIAO_POINT_LR_REG,XIALIAO_POINT_LR_REG};


        //上料点下降高度LR寄存器
        public static int ShangLiaoHeight_LR_REG = 120;


        //与偏移量有关 ，也与产品的参数有关
        public static List<int> Offset_Height_LR_REG = new List<int> { FirstPanReleaseHeightOffset_LR_REG, SecondPanReleaseHeightOffset_LR_REG, 
            ThirdPanReleaseHeightOffset_LR_REG, FourthPanReleaseHeightOffset_LR_REG, FifthPanReleaseHeightOffset_LR_REG};




        //下料点下降高度LR寄存器
        public static int XiaLiaoHeight_LR_REG = 121;


        ////磨盘内上料时时吸取高度 offset  
        public static int DropHeight_LR_REG = 122;


        #endregion

        #region  二号机械臂相关寄存器定义
        public static int CALIBRATE_POS_IO = 50;  //校准点
        public static int LEFT_UP_POS_IO = 100;  //左上位置点
        public static int RIGHT_UP_POS_IO = 101; //右上位置点
        public static int LEFT_DOWN_POS_IO = 102;//左下位置点
        public static int RIGHT_DOWN_POS_IO = 103;//右下位置点
        public static int CENTER_POS_IO = 104;//中心点


        public static int GRIP_DIR = 14;  //夹爪方向
        public static int GRIP_PUL = 51;  //夹爪脉冲
        public static int GRIP_FRE = 52;  //夹爪频率

        #endregion


        /// <summary>
        /// 普通参数类  
        /// </summary>
        public class ParamData
        {
            public string ParamName { get; set; }
            public double ParamValue { get; set; }

            public string ParamUnit { get; set; }

            //隐式转换
            public static implicit operator ParamData(XmlElement node)
            {
                return new ParamData()
                {
                    ParamName = node.Attributes["ParamName"].Value,
                    ParamValue = double.Parse(node.InnerText),
                    ParamUnit = node.Attributes["ParamUnit"].Value,

                };
            }
        }

        #region 相机相关参数
        //相机模板参数
        public volatile static ParamData TemplateX = new ParamData();
        public volatile static ParamData TemplateY = new ParamData();
        //相机识别高度
        public volatile static ParamData CameraRecHeight = new ParamData();
        //像素精度
        public volatile static ParamData XPixToMm = new ParamData();
        //Mark点扫描角度
        public volatile static ParamData ScanAngel = new ParamData();

        public static List<ParamData> CameraParam = new List<ParamData> { TemplateX, TemplateY, CameraRecHeight, XPixToMm, ScanAngel };

        public static void UpdateCameraParam()
        {

            List<ParamData> list = myXML.LoadPCParams("CameraParamData");
            for (int i = 0; i < CameraParam.Count(); i++)
            {
                CameraParam[i].ParamValue = list[i].ParamValue;
            }

        }




        #endregion

        #region 产品相关参数
        //97R产品吸取时机械臂下降高度
        public volatile static ParamData Production_97R_DropHeight = new ParamData();
        //97R产品上料点机械臂下降高度
        public volatile static ParamData Production_97R_ShangLiaoDropHeight = new ParamData();
        //97R产品释放时机械臂下降高度
        public volatile static ParamData Production_97R_ReleaseHeight = new ParamData();
        //吸取延时
        public volatile static ParamData Production_97R_AbsorbDelay = new ParamData();


        //97U产品下料吸取时机械臂下降高度
        public volatile static ParamData Production_97U_DropHeight = new ParamData();
        //97U产品上料点机械臂下降高度
        public volatile static ParamData Production_97U_ShangLiaoDropHeight = new ParamData();
        //97U产品上料释放时机械臂下降高度
        public volatile static ParamData Production_97U_ReleaseHeight = new ParamData();
        //吸取延时
        public volatile static ParamData Production_97U_AbsorbDelay = new ParamData();


        public static List<ParamData> ProductParam = new List<ParamData> { Production_97R_DropHeight, Production_97R_ShangLiaoDropHeight, Production_97R_ReleaseHeight, Production_97R_AbsorbDelay, Production_97U_DropHeight, Production_97U_ShangLiaoDropHeight, Production_97U_ReleaseHeight, Production_97U_AbsorbDelay };


        public static void UpdateProductParam()
        {

            List<ParamData> list = myXML.LoadPCParams("ProductionParamData");
            for (int i = 0, j = 0; i < ProductParam.Count(); i++, j++)
            {
                if (list[j].ParamName.Contains("-")) j++;
                ProductParam[i].ParamValue = list[j].ParamValue;
            }

            //Hsc3.Comm.GeneralPos curpos;
            //if (ParamConfig.DiskType == ProductionType.Pro_97R)
            //{
            //    //改变五个点上料时  释放高度
            //    curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.FirstPanDropHeightOffset_LR_REG);
            //    curpos.vecPos[2] = -ParamConfig.Production_97R_ReleaseHeight.ParamValue;
            //    RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.FirstPanDropHeightOffset_LR_REG);

            //    curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.SecondPanDropHeightOffset_LR_REG);
            //    curpos.vecPos[2] = -ParamConfig.Production_97R_ReleaseHeight.ParamValue;
            //    RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.SecondPanDropHeightOffset_LR_REG);


            //    curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.ThirdPanDropHeightOffset_LR_REG);
            //    curpos.vecPos[2] = -ParamConfig.Production_97R_ReleaseHeight.ParamValue;
            //    RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.ThirdPanDropHeightOffset_LR_REG);


            //    curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.FourthPanDropHeightOffset_LR_REG);
            //    curpos.vecPos[2] = -ParamConfig.Production_97R_ReleaseHeight.ParamValue;
            //    RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.FourthPanDropHeightOffset_LR_REG);

            //    curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.FifthPanDropHeightOffset_LR_REG);
            //    curpos.vecPos[2] = -ParamConfig.Production_97R_ReleaseHeight.ParamValue;
            //    RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.FifthPanDropHeightOffset_LR_REG);


            //    //改变上料点的下降高度
            //    curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.ShangLiaoHeight_LR_REG);
            //    curpos.vecPos[2] = -ParamConfig.Production_97R_ShangLiaoDropHeight.ParamValue;
            //    RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.ShangLiaoHeight_LR_REG);


            //    //改变 下料时 磨盘内下降高度
            //    curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.SuckHeight_LR_REG);
            //    curpos.vecPos[2] = -ParamConfig.Production_97R_DropHeight.ParamValue;
            //    RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.SuckHeight_LR_REG);

            //    //改变吸取延时
            //    RobotConnect.SetR(ConnectConfig.FirstRobot, ParamConfig.ProductionSuckDelay_R_REG, ParamConfig.Production_97R_AbsorbDelay.ParamValue);

            //}

            //else if(ParamConfig.DiskType == ProductionType.Pro_97U)
            //{
            //    curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.FirstPanDropHeightOffset_LR_REG);
            //    curpos.vecPos[2] = -ParamConfig.Production_97U_ReleaseHeight.ParamValue;
            //    RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.FirstPanDropHeightOffset_LR_REG);


            //    curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.SecondPanDropHeightOffset_LR_REG);
            //    curpos.vecPos[2] = -ParamConfig.Production_97U_ReleaseHeight.ParamValue;
            //    RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.SecondPanDropHeightOffset_LR_REG);


            //    curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.ThirdPanDropHeightOffset_LR_REG);
            //    curpos.vecPos[2] = -ParamConfig.Production_97U_ReleaseHeight.ParamValue;
            //    RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.ThirdPanDropHeightOffset_LR_REG);


            //    curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.FourthPanDropHeightOffset_LR_REG);
            //    curpos.vecPos[2] = -ParamConfig.Production_97U_ReleaseHeight.ParamValue;
            //    RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.FourthPanDropHeightOffset_LR_REG);

            //    curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.FifthPanDropHeightOffset_LR_REG);
            //    curpos.vecPos[2] = -ParamConfig.Production_97U_ReleaseHeight.ParamValue;
            //    RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.FifthPanDropHeightOffset_LR_REG);


            //    //改变上料点的下降高度
            //    curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.ShangLiaoHeight_LR_REG);
            //    curpos.vecPos[2] = -ParamConfig.Production_97U_ShangLiaoDropHeight.ParamValue;
            //    RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.ShangLiaoHeight_LR_REG);



            //    //改变 下料时 磨盘内下降高度
            //    curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.SuckHeight_LR_REG);
            //    curpos.vecPos[2] = -ParamConfig.Production_97U_DropHeight.ParamValue;
            //    RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.SuckHeight_LR_REG);


            //    //改变吸取延时
            //    RobotConnect.SetR(ConnectConfig.FirstRobot, ParamConfig.ProductionSuckDelay_R_REG, ParamConfig.Production_97U_AbsorbDelay.ParamValue);
            //}

        }




        #endregion

        #region 一号机械臂相关参数

        ////上升下降速度
        //public volatile static ParamData ElevatorSpeed = new ParamData();
        //自动运行速度
        public volatile static ParamData AutoMoveSpeed = new ParamData();
        //手动运行速度
        public volatile static ParamData ManualMoveSpeed = new ParamData();

        //机械手距磨石高度
        public volatile static ParamData Height = new ParamData();
        //落地旋转角度
        public volatile static ParamData RotateAngel = new ParamData();

        //下料点吸取释放高度
        public volatile static ParamData XiaLiaoHeight = new ParamData();
        //上料点吸取延时
        public volatile static ParamData ShangLiaoSuckDelay = new ParamData();

        //高度校准预下降距离
        public volatile static ParamData HeightCabPreDecDistance = new ParamData();

        public static List<ParamData> FirstRobotParam = new List<ParamData> {
            AutoMoveSpeed,ManualMoveSpeed ,Height, XiaLiaoHeight,ShangLiaoSuckDelay, RotateAngel, HeightCabPreDecDistance };

        public static void UpdateFirstRobotParam()
        {
            List<ParamData> list = myXML.LoadPCParams("FirstRobotParamData");
            for (int i = 0, j = 0; i < FirstRobotParam.Count(); i++, j++)
            {
                if (list[j].ParamName.Contains("-")) j++;
                FirstRobotParam[i].ParamValue = list[j].ParamValue;
            }

            ////更新自动速度参数R寄存器
            //RobotConnect.SetR(ConnectConfig.FirstRobot, AutoRun_R_REG, AutoMoveSpeed.ParamValue);
            ////更新上料点吸取延时R寄存器
            //RobotConnect.SetR(ConnectConfig.FirstRobot, SuckDelay_R_REG, ShangLiaoSuckDelay.ParamValue);


            //Hsc3.Comm.GeneralPos curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.XiaLiaoHeight_LR_REG);
            //curpos.vecPos[2] = -ParamConfig.XiaLiaoHeight.ParamValue;
            ////更新下料点下降高度LR寄存器
            //RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.XiaLiaoHeight_LR_REG);
            //RobotConnect.SaveData(ConnectConfig.FirstRobot, "LR");
        }

        #endregion


        #region  Offset补偿参数

        //补偿值类
        public class OffsetData
        {
            public string Section { get; set; }
            public double Xoffset { get; set; } = 0;

            public double Yoffset { get; set; } = 0;

        //    public double Zoffset { get; set; } = 0;
            public double Angeloffset { get; set; } = 0;
            //隐式转换
            public static implicit operator OffsetData(XmlElement node)
            {
                    return new OffsetData()
                    {
                        Section = node.Attributes["Section"].Value,
                        Xoffset = double.Parse(node.Attributes["Xoffset"].Value),
                        Yoffset = double.Parse(node.Attributes["Yoffset"].Value),
                        Angeloffset = double.Parse(node.Attributes["Angeloffset"].Value),
               //       Zoffset = double.Parse(node.Attributes["Zoffset"].Value),
                    };
                
            }
               
            
        }

        /// <summary>
        /// 补偿值
        /// </summary>
        public volatile static OffsetData FirstPanOffset = new OffsetData();
        public volatile static OffsetData SecondPanOffset = new OffsetData();
        public volatile static OffsetData ThirdPanOffset = new OffsetData();
        public volatile static OffsetData FourthPanOffset = new OffsetData();
        public volatile static OffsetData FifthPanOffset = new OffsetData();


        public volatile static OffsetData QuLiaoPointOffset = new OffsetData();
        public volatile static OffsetData FangLiaoPointOffset = new OffsetData();
        public volatile static OffsetData ShangLiaoPointOffset = new OffsetData();
        public volatile static OffsetData XiaLiaoPointOffset = new OffsetData();


        public static List<OffsetData> offsetDatas = new List<OffsetData> { FirstPanOffset, SecondPanOffset, ThirdPanOffset, FourthPanOffset, FifthPanOffset, QuLiaoPointOffset, FangLiaoPointOffset, ShangLiaoPointOffset, XiaLiaoPointOffset };
        



        //更新offset参数值 ，需要的话写进机械臂中
        public static  void UpdateOffsetParam()
        {
            //从文件中获取最新数据
            List<OffsetData> list = myXML.LoadOffsetConfig("FirstOffsetData");

            for (int i = 0; i < offsetDatas.Count; i++)
            {
                offsetDatas[i].Xoffset = list[i].Xoffset;
                offsetDatas[i].Yoffset = list[i].Yoffset;
                offsetDatas[i].Angeloffset = list[i].Angeloffset;
            }



        }






    #endregion



    }
}
