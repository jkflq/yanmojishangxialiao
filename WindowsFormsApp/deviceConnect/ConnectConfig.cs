﻿
#define DEVICE_CONTROLER

using System;
using System.Collections.Generic;
using System.Net.Sockets;
using WindowsFormsApp.components;
using WindowsFormsApp.components.QGUnit;
using WindowsFormsApp.components.grip;
using WindowsFormsApp.components.GDUnit;
using WindowsFormsApp.components.robot;
using WindowsFormsApp.components.tray;
using WindowsFormsApp.components.vacuum;
using WindowsFormsApp.utils.modbus;
using WindowsFormsApp.components.EMotor;
using WindowsFormsApp.components.Motor;

namespace WindowsFormsApp.deviceConnect
{
    public static class ConnectConfig
    {
 

        //创建时间对象，用于更新下位机状态
        public static System.Timers.Timer timer;

        //基于EventHandler委托定义相机连接与断开事件
        public static event EventHandler Connecting;   //连接事件

        public static event EventHandler DisConnecting;//断开连接事件

        //用于判断是否是正常手动断开
        public static bool Error_Connect_Flag = false;

        //用于判别初始化是否完成
        public static bool Task_Init_Kill = false;



        //建立状态自动更新列表
        public static List<IAutoUpdatable> updateList = new List<IAutoUpdatable>();

        //创建调试窗体
  //      public static FrmDebug frmDebug = new FrmDebug();


        #region 无用内容



        //创建相机Socket
        public static Socket CameraSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); 
        //创建机械臂Socket
        public static Socket RobotSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        //创建相机IP和端口号
        public static string CameraIP
        {
            get;
            set;

        }="192.168.250.60";
        public static int CameraPort = 8999;


       #endregion


        //创建控制器的IP和端口号
        public static string ControlerIP
        {
            get;
            set;
        } = "192.168.1.92";//"192.168.250.62"
        public static int ControlerPort = 502;


        //创建间接控制机械臂的IP和端口号
        public static string FirstRobotIP
        {
            set;
            get;
        } = "192.168.1.93";
        public static int FirstRobotSocketPort = 9000;
        //创建直接机械臂控制柜端口号
        public static ushort FirstRobotCommPort = 23234;

        public static string SecondRobotIP
        {
            set;
            get;
        } = "192.168.1.94";
        public static int SecondRobotSocketPort = 9000;
        //创建直接机械臂控制柜端口号
        public static ushort SecondRobotCommPort = 23234;


        public static string ThirdRobotIP
        {
            set;
            get;
        } = "192.168.1.95";
        public static int ThirdRobotSocketPort = 9000;
        //创建直接机械臂控制柜端口号
        public static ushort ThirdRobotCommPort = 23234;






        


        //用于判断设备连接状态（默认为未连接）

        public static bool CameraConnected = false;


        public static bool ControlerConnected = false;//控制器


        public static bool Connected = false;  //总连接标志位








        //创建托盘
        public static Tray tray2;


        //光电1-A
        public static GD GD1 = new GD()
        {
            busTcpClient = ControlUnit,
            Address = ControlerPortConfig.GD1
        };

        //实例化机械臂
        public static Robot FirstRobot = new Robot(ConnectConfig.FirstRobotIP, ConnectConfig.FirstRobotCommPort, 1, "上下料机械臂");
        public static Robot SecondRobot = new Robot(ConnectConfig.SecondRobotIP, ConnectConfig.FirstRobotCommPort, 2, "上料机械臂");
        public static Robot ThirdRobot = new Robot(ConnectConfig.ThirdRobotIP, ConnectConfig.FirstRobotCommPort, 3, "下料机械臂");


        //存储所创建的机械臂监测列表
        public static List<Robot> robots = new List<Robot> { FirstRobot, SecondRobot, ThirdRobot };
        //实例化控制器
        public static ModbusListener ControlUnit = new ModbusListener(ConnectConfig.ControlerIP);

        //实例化夹爪
        public static Grip Up_Grip = new Grip()
        {
            busTcpClient = ControlUnit,
            robot = ConnectConfig.SecondRobot,
            First_move = RobotPortConfig.OUTPUT_UP_Grip_First,
            Second_move = RobotPortConfig.OUTPUT_UP_Grip_Second,
            Third_move = RobotPortConfig.OUTPUT_UP_Grip_Third
        };
        public static Grip Down_Grip = new Grip()
        {
            busTcpClient = ControlUnit,
            robot = ConnectConfig.ThirdRobot,
            First_move = RobotPortConfig.OUTPUT_DOWN_Grip_First,
            Second_move = RobotPortConfig.OUTPUT_DOWN_Grip_Second,
            Third_move = RobotPortConfig.OUTPUT_DOWN_Grip_Third
        };
        //实例化上下料真空发生器
        public static Vacuum FirstRobotVacuum = new Vacuum()
        {
                robot = ConnectConfig.FirstRobot,
                InspiratoryTriggerIO = RobotPortConfig.FirstRobot_OUTPUT_Vacuum_IN,
                ExpiratoryTriggerIO = RobotPortConfig.FirstRobot_OUTPUT_Vacuum_EX,
                isVacuumDegreeIO = RobotPortConfig.FirstRobot_INPUT_VacuumDegree
            };
        //实例化上料机械臂真空发生器
        public static Vacuum SecondRobotVacuum = new Vacuum()
        {
            robot = ConnectConfig.SecondRobot,
            InspiratoryTriggerIO = RobotPortConfig.SecondRobot_OUTPUT_Vacuum_IN,
            ExpiratoryTriggerIO = RobotPortConfig.SecondRobot_OUTPUT_Vacuum_EX,
             isVacuumDegreeIO = RobotPortConfig.SecondRobot_INPUT_VacuumDegree
        };
        //实例化下料机械臂真空发生器
        public static Vacuum ThirdRobotVacuum = new Vacuum()
        {
            robot = ConnectConfig.ThirdRobot,
            InspiratoryTriggerIO = RobotPortConfig.ThirdRobot_OUTPUT_Vacuum_IN,
            ExpiratoryTriggerIO = RobotPortConfig.ThirdRobot_OUTPUT_Vacuum_EX,
            isVacuumDegreeIO = RobotPortConfig.ThirdRobot_INPUT_VacuumDegree
        };
        //实例化气缸
        public static QG QG1 = new QG()
        {
            qgType = QGType.QG_90,
                busTcpClient = ConnectConfig.ControlUnit,
                LevelTriggerCoil = ControlerPortConfig.QG1_LevelTriggerCoil,
                VerticalTriggerCoil = ControlerPortConfig.QG1_VerticalTriggerCoil,
                LevelStatusCoil = ControlerPortConfig.QG1_LevelCoil,
                VerticalStatusCoil = ControlerPortConfig.QG1_VerticalCoil
            };
        public static QG QG2 = new QG()
        {
            qgType = QGType.QG_90,
                busTcpClient = ConnectConfig.ControlUnit,
                LevelTriggerCoil = ControlerPortConfig.QG2_LevelTriggerCoil,
                VerticalTriggerCoil = ControlerPortConfig.QG2_VerticalTriggerCoil,
                LevelStatusCoil = ControlerPortConfig.QG2_LevelCoil,
                VerticalStatusCoil = ControlerPortConfig.QG2_VerticalCoil
            };
        public static QG QG3 = new QG()
        {
            qgType = QGType.QG_90,
                busTcpClient = ConnectConfig.ControlUnit,
                LevelTriggerCoil = ControlerPortConfig.QG3_LevelTriggerCoil,
                VerticalTriggerCoil = ControlerPortConfig.QG3_VerticalTriggerCoil,
                LevelStatusCoil = ControlerPortConfig.QG3_LevelCoil,
                VerticalStatusCoil = ControlerPortConfig.QG3_VerticalCoil
            };
        public static QG QG4 = new QG()
        {
            qgType = QGType.QG_90,
                busTcpClient = ConnectConfig.ControlUnit,
                LevelTriggerCoil = ControlerPortConfig.QG4_LevelTriggerCoil,
                VerticalTriggerCoil = ControlerPortConfig.QG4_VerticalTriggerCoil,
                LevelStatusCoil = ControlerPortConfig.QG4_LevelCoil,
                VerticalStatusCoil = ControlerPortConfig.QG4_VerticalCoil
            };
        public static QG QG5 = new QG()
        {
            qgType = QGType.QG_90,
                busTcpClient = ConnectConfig.ControlUnit,
                LevelTriggerCoil = ControlerPortConfig.QG5_LevelTriggerCoil,
                VerticalTriggerCoil = ControlerPortConfig.QG5_VerticalTriggerCoil,
                LevelStatusCoil = ControlerPortConfig.QG5_LevelCoil,
                VerticalStatusCoil = ControlerPortConfig.QG5_VerticalCoil
            };
        public static QG QG6 = new QG()
        {
            qgType = QGType.QG_90,
                busTcpClient = ConnectConfig.ControlUnit,
                LevelTriggerCoil = ControlerPortConfig.QG6_LevelTriggerCoil,
                VerticalTriggerCoil = ControlerPortConfig.QG6_VerticalTriggerCoil,
                LevelStatusCoil = ControlerPortConfig.QG6_LevelCoil,
                VerticalStatusCoil = ControlerPortConfig.QG6_VerticalCoil
            };
        public static QG QG7 = new QG()
        {
            qgType = QGType.QG_90,
                busTcpClient = ConnectConfig.ControlUnit,
                LevelTriggerCoil = ControlerPortConfig.QG7_LevelTriggerCoil,
                VerticalTriggerCoil = ControlerPortConfig.QG7_VerticalTriggerCoil,
                LevelStatusCoil = ControlerPortConfig.QG7_LevelCoil,
                VerticalStatusCoil = ControlerPortConfig.QG7_VerticalCoil
            };

        public static QG QG8 = new QG()
        {
            qgType = QGType.QG_180,
                busTcpClient = ConnectConfig.ControlUnit,
                LevelTriggerCoil = ControlerPortConfig.QG8_LevelTriggerCoil,
                VerticalTriggerCoil = ControlerPortConfig.QG8_VerticalTriggerCoil,
                LevelStatusCoil = ControlerPortConfig.QG8_LevelCoil,
                VerticalStatusCoil = ControlerPortConfig.QG8_VerticalCoil
            };
        public static QG QG9 = new QG()
        {
            qgType = QGType.QG_Line,
                busTcpClient = ConnectConfig.ControlUnit,
                BackTriggerCoil = ControlerPortConfig.QG9_StraightTriggerCoil,
                SendTriggerCoil = ControlerPortConfig.QG9_BendTriggerCoil,
                StraightStatuCoil = ControlerPortConfig.QG9_StraightStatuCoil,
                BendingStatusCoil = ControlerPortConfig.QG9_BendingStatusCoil
            };
        public static QG QG10 = new QG()
        {
            qgType = QGType.QG_180,
                busTcpClient = ConnectConfig.ControlUnit,
                LevelTriggerCoil = ControlerPortConfig.QG10_LevelTriggerCoil,
                VerticalTriggerCoil = ControlerPortConfig.QG10_VerticalTriggerCoil,
                LevelStatusCoil = ControlerPortConfig.QG10_LevelCoil,
                VerticalStatusCoil = ControlerPortConfig.QG10_VerticalCoil
            };
        public static QG QG11 = new QG()
        {
            qgType = QGType.QG_Line,
                busTcpClient = ConnectConfig.ControlUnit,
                BackTriggerCoil = ControlerPortConfig.QG11_StraightTriggerCoil,
                SendTriggerCoil = ControlerPortConfig.QG11_BendTriggerCoil,
                StraightStatuCoil = ControlerPortConfig.QG11_StraightStatuCoil,
                BendingStatusCoil = ControlerPortConfig.QG11_BendingStatusCoil
            };

        //实例化电缸
        public static EMotor UPDisksEMotor = new EMotor()
        {
            ID = 0,
                busTcpClient = ConnectConfig.ControlUnit,
                FirstPositionReg = ControlerPortConfig.UPDisksEMotor_FirstPositionReg,
                FirstTGPositionReg = ControlerPortConfig.UPDisksEMotor_FirstTGPositionReg,

                SecondPositionReg = ControlerPortConfig.UPDisksEMotor_SecondPositionReg,
                SecondTGPositionReg = ControlerPortConfig.UPDisksEMotor_SecondTGPositionReg,

                ThirdPositionReg = ControlerPortConfig.UPDisksEMotor_ThirdPositionReg,
                ThirdTGPositionReg = ControlerPortConfig.UPDisksEMotor_ThirdTGPositionReg,


                FourthPositionReg = ControlerPortConfig.UPDisksEMotor_FourthPositionReg,
                FourthTGPositionReg = ControlerPortConfig.UPDisksEMotor_FourthTGPositionReg,

                FifthPositionReg = ControlerPortConfig.UPDisksEMotor_FifthPositionReg,
                FifthTGPositionReg = ControlerPortConfig.UPDisksEMotor_FifthTGPositionReg,

                SixthPositionReg = ControlerPortConfig.UPDisksEMotor_SixthPositionReg,
                SixthTGPositionReg = ControlerPortConfig.UPDisksEMotor_SixthTGPositionReg,

                SeventhPositionReg = ControlerPortConfig.UPDisksEMotor_SeventhPositionReg,
                SeventhTGPositionReg = ControlerPortConfig.UPDisksEMotor_SeventhTGPositionReg,

                EighthPositionReg = ControlerPortConfig.UPDisksEMotor_EighthPositionReg,
                EighthTGPositionReg = ControlerPortConfig.UPDisksEMotor_EighthTGPositionReg,

                CurrentPositionReg = ControlerPortConfig.UPDiskEMotor_CurrentPositionReg,
                TargetPositionReg = ControlerPortConfig.UPDiskEMotor_TargetPositionReg,
                SpeedReg = ControlerPortConfig.UPDiskEMotor_SpeedReg,
                AccReg = ControlerPortConfig.UPDiskEMotor_AccReg,
                EnableCoil = ControlerPortConfig.UPDiskEMotor_Enable,
                Moving_Flag = ControlerPortConfig.UPDiskEMotor_Moving,
                CancelWanningCoil = ControlerPortConfig.UPDiskEMotor_Cancel_Wanning,
                Wanning_Flag = ControlerPortConfig.UPDiskEMotor_WanningCoil,
                AlalmCodeReg = ControlerPortConfig.UPDiskEMotor_AlalmCode,
                Running = ControlerPortConfig.UPDiskEMotor_Run

            };
        public static EMotor DOWNDisksEMotor = new EMotor()
        {
                ID = 1,
                busTcpClient = ConnectConfig.ControlUnit,
                FirstPositionReg = ControlerPortConfig.DOWNDisksEMotor_FirstPositionReg,
                FirstTGPositionReg = ControlerPortConfig.DOWNDisksEMotor_FirstTGPositionReg,

                SecondPositionReg = ControlerPortConfig.DOWNDisksEMotor_SecondPositionReg,
                SecondTGPositionReg = ControlerPortConfig.DOWNDisksEMotor_SecondTGPositionReg,

                ThirdPositionReg = ControlerPortConfig.DOWNDisksEMotor_ThirdPositionReg,
                ThirdTGPositionReg = ControlerPortConfig.DOWNDisksEMotor_ThirdTGPositionReg,


                FourthPositionReg = ControlerPortConfig.DOWNDisksEMotor_FourthPositionReg,
                FourthTGPositionReg = ControlerPortConfig.DOWNDisksEMotor_FourthTGPositionReg,

                CurrentPositionReg = ControlerPortConfig.DOWNDiskEMotor_CurrentPositionReg,
                TargetPositionReg = ControlerPortConfig.DOWNDiskEMotor_TargetPositionReg,
                SpeedReg = ControlerPortConfig.DOWNDiskEMotor_SpeedReg,
                AccReg = ControlerPortConfig.DOWNDiskEMotor_AccReg,
                EnableCoil = ControlerPortConfig.DOWNDiskEMotor_Enable,
                Moving_Flag = ControlerPortConfig.DOWNDiskEMotor_Moving,
                CancelWanningCoil = ControlerPortConfig.DOWNDiskEMotor_Cancel_Wanning,
                Wanning_Flag = ControlerPortConfig.DOWNDiskEMotor_WanningCoil,
                Running = ControlerPortConfig.DOWNDiskEMotor_Run,
                AlalmCodeReg = ControlerPortConfig.DOWNDiskEMotor_AlalmCode
        };
        public static EMotor TAKEDisksEMotor = new EMotor()
        {
                ID = 2,
                busTcpClient = ConnectConfig.ControlUnit,
                FirstPositionReg = ControlerPortConfig.TAKEDisksEMotor_FirstPositionReg,

                SecondPositionReg = ControlerPortConfig.TAKEDisksEMotor_SecondPositionReg,

                ThirdPositionReg = ControlerPortConfig.TAKEDisksEMotor_ThirdPositionReg,

                FourthPositionReg = ControlerPortConfig.TAKEDisksEMotor_FourthPositionReg,

                CurrentPositionReg = ControlerPortConfig.TAKEDiskEMotor_CurrentPositionReg,
                TargetPositionReg = ControlerPortConfig.TAKEDiskEMotor_TargetPositionReg,
                SpeedReg = ControlerPortConfig.TAKEDiskEMotor_SpeedReg,
                AccReg = ControlerPortConfig.TAKEDiskEMotor_AccReg,
                EnableCoil = ControlerPortConfig.TAKEDiskEMotor_Enable,
                Moving_Flag = ControlerPortConfig.TAKEDiskEMotor_Moving,
                CancelWanningCoil = ControlerPortConfig.TAKEDiskEMotor_Cancel_Wanning,
                Wanning_Flag = ControlerPortConfig.TAKEDiskEMotor_WanningCoil,
                Running = ControlerPortConfig.TAKEDiskEMotor_Run,
                AlalmCodeReg = ControlerPortConfig.TAKEDiskEMotor_AlalmCode
        };

        //实例化电机
        public static Motor Motor = new Motor()
        {
            busTcpClient = ConnectConfig.ControlUnit,
            Current_PositionReg = ControlerPortConfig.Motor_Current_AngleReg,
            WarningCoil = ControlerPortConfig.Motor_WarningTriggerCoil,
            Move_OriginCoil = ControlerPortConfig.Motor_Move_OriginTriggerCoil,
            SpeedReg = ControlerPortConfig.Motor_SpeedReg,
            AccReg = ControlerPortConfig.Motor_AccReg,
            Target_AngleReg = ControlerPortConfig.Motor_Target_AngleReg,
            EnableCoil = ControlerPortConfig.Motor_Enable,
            Run = ControlerPortConfig.Motor_Run

         };

        /// <summary>
        /// 建立与设备的通讯连接
        /// </summary>
        public static void Connect()
        {
            //建立通讯
            try
            {
                //有一个没连接上就执行再次连接事件
                if (!Connected)
                {
                    //触发连接事件
                    Connecting?.Invoke(null, new EventArgs());

                }
                StartUpdate();
            }
            catch(Exception ex)
            {
                //触发断开连接事件
                //DisConnecting?.Invoke(null, new EventArgs());
                StopUpdate();
            }

        }

        public static void DisConnect()
        {
            DisConnecting?.Invoke(null, new EventArgs());
            StopUpdate();

        }

        public static void StopUpdate()
        {
            if (timer != null)
                timer.Stop();
        }



        public static void StartUpdate()
        {
            if(timer == null)
            {
                //实例化timer，设置间隔时间为200ms
                timer = new System.Timers.Timer(500);
                //到达时间后执行的事件
                timer.Elapsed += (sender, e) =>
                {
                    foreach(var u in updateList)
                    {
                        u.ReFreshState();
                    }
                   // CtrlChange.Ctrl_Working.Subscriber.ReFreshState();
                };
            }
            timer.Start();
        }

        

    }




}
