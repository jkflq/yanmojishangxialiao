﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp.deviceConnect
{
    class ControlerPortConfig
    {
        public static string GD1 = "21"; 
        public static string GD2 = "22"; 
        public static string GD3 = "21"; 
        public static string GD4 = "28"; 
        public static string GD5 = "24"; 
        public static string GD6 = "26"; 
        public static string GD7 = "27"; 
        public static string GD8 = "30"; 
        public static string GD9 = "32"; 
        public static string GD10 = "30";
        public static string GD11 = "23";

        //机械臂夹爪行程
        public static string UP_Grip_FirstCoil = "16436";
        public static string UP_Grip_SecondCoil = "16437";
        public static string UP_Grip_ThirdCoil = "16438";
        public static string DOWN_Grip_FirstCoil = "16439";
        public static string DOWN_Grip_SecondCoil = "16440";
        public static string DOWN_Grip_ThirdCoil = "16441";

        //90°气缸磁性开关状态
        public static string QG1_LevelCoil = "16425";
        public static string QG1_VerticalCoil = "16426";
        public static string QG2_LevelCoil = "16423";
        public static string QG2_VerticalCoil = "16424";
        public static string QG3_LevelCoil = "16421";
        public static string QG3_VerticalCoil = "16422";
        public static string QG4_LevelCoil = "16419";
        public static string QG4_VerticalCoil = "16420";
        public static string QG5_LevelCoil = "16417";
        public static string QG5_VerticalCoil = "16418";
        public static string QG6_LevelCoil = "16415";
        public static string QG6_VerticalCoil = "16416";
        public static string QG7_LevelCoil = "16413";
        public static string QG7_VerticalCoil = "16414";
        
        //180°气缸磁性开关状态
        public static string QG8_LevelCoil = "16458";
        public static string QG8_VerticalCoil = "16459";

        //直线气缸磁性开关状态
        public static string QG9_StraightStatuCoil = "16428";
        public static string QG9_BendingStatusCoil = "16427";
        //推拉杆90°气缸磁性开关状态
        public static string QG10_LevelCoil = "16429";
        public static string QG10_VerticalCoil = "16430";

        //顶盒气缸磁性开关状态
        public static string QG11_StraightStatuCoil = "16461";
        public static string QG11_BendingStatusCoil = "16460";

        //90°气缸触发线圈地址
        public static string QG1_LevelTriggerCoil = "16396";
        public static string QG1_VerticalTriggerCoil = "16397";
        public static string QG2_LevelTriggerCoil = "16394";
        public static string QG2_VerticalTriggerCoil = "16395";
        public static string QG3_LevelTriggerCoil = "16392";
        public static string QG3_VerticalTriggerCoil = "16393";
        public static string QG4_LevelTriggerCoil = "16390";
        public static string QG4_VerticalTriggerCoil = "16391";
        public static string QG5_LevelTriggerCoil = "16388";
        public static string QG5_VerticalTriggerCoil = "16389";
        public static string QG6_LevelTriggerCoil = "16386";
        public static string QG6_VerticalTriggerCoil = "16387";
        public static string QG7_LevelTriggerCoil = "16384";
        public static string QG7_VerticalTriggerCoil = "16385";

        //180°气缸触发线圈地址
        public static string QG8_LevelTriggerCoil = "16402";
        public static string QG8_VerticalTriggerCoil = "16403";
        //直线气缸触发线圈地址
        public static string QG9_StraightTriggerCoil = "16398";
        public static string QG9_BendTriggerCoil = "16399";

        //推拉杆90°气缸触发线圈地址
        public static string QG10_LevelTriggerCoil = "16400";
        public static string QG10_VerticalTriggerCoil = "16401";

        //顶盒气缸触发线圈地址
        public static string QG11_StraightTriggerCoil = "16411";
        public static string QG11_BendTriggerCoil = "16412";

        //上料电缸寄存器地址与线圈地址
        public static string UPDisksEMotor_FirstPositionReg = "16517";
        public static string UPDisksEMotor_FirstTGPositionReg = "16533";
        public static string UPDisksEMotor_SecondPositionReg = "16519";
        public static string UPDisksEMotor_SecondTGPositionReg = "16535";
        public static string UPDisksEMotor_ThirdPositionReg = "16521";
        public static string UPDisksEMotor_ThirdTGPositionReg = "16537";
        public static string UPDisksEMotor_FourthPositionReg = "16523";
        public static string UPDisksEMotor_FourthTGPositionReg = "16539";
        public static string UPDisksEMotor_FifthPositionReg = "16525";
        public static string UPDisksEMotor_FifthTGPositionReg = "16541";
        public static string UPDisksEMotor_SixthPositionReg = "16527";
        public static string UPDisksEMotor_SixthTGPositionReg = "16543";
        public static string UPDisksEMotor_SeventhPositionReg = "16529";
        public static string UPDisksEMotor_SeventhTGPositionReg = "16545";
        public static string UPDisksEMotor_EighthPositionReg = "16531";
        public static string UPDisksEMotor_EighthTGPositionReg = "16547";


        //下料电缸寄存器地址与线圈地址
        public static string DOWNDisksEMotor_FirstPositionReg = "16493";
        public static string DOWNDisksEMotor_FirstTGPositionReg = "16501";
        public static string DOWNDisksEMotor_SecondPositionReg = "16495";
        public static string DOWNDisksEMotor_SecondTGPositionReg = "16503";
        public static string DOWNDisksEMotor_ThirdPositionReg = "16497";
        public static string DOWNDisksEMotor_ThirdTGPositionReg = "16505";
        public static string DOWNDisksEMotor_FourthPositionReg = "16499";
        public static string DOWNDisksEMotor_FourthTGPositionReg = "16507";

        //取料电钢寄存器地址与线圈地址
        public static string TAKEDisksEMotor_FirstPositionReg = "16509";
        public static string TAKEDisksEMotor_SecondPositionReg = "16511";
        public static string TAKEDisksEMotor_ThirdPositionReg = "16513";
        public static string TAKEDisksEMotor_FourthPositionReg = "16515";

        //电缸的控制寄存器地址
        public static string UPDiskEMotor_AutoButton = "16454";
        public static string UPDiskEMotor_CurrentPositionReg = "16441";
        public static string UPDiskEMotor_TargetPositionReg = "16555";
        public static string UPDiskEMotor_SpeedReg = "16557";
        public static string UPDiskEMotor_AccReg = "16440";
        public static string UPDiskEMotor_Enable = "16493";
        public static string UPDiskEMotor_Moving = "16501";                          //上料电缸移动中
        public static string UPDiskEMotor_WanningCoil = "16502";                         //上料电缸报警
        public static string UPDiskEMotor_Cancel_Wanning = "16497";                     //上料电缸复位取消报警
        public static string UPDiskEMotor_AlalmCode = "16447";                       //报警代码，用于解析
        public static string UPDiskEMotor_Run = "16494";                             //启动，运动到目标位置


        public static string DOWNDiskEMotor_AutoButton = "16449";
        public static string DOWNDiskEMotor_CurrentPositionReg = "16425";
        public static string DOWNDiskEMotor_TargetPositionReg = "16549";
        public static string DOWNDiskEMotor_SpeedReg = "16551";
        public static string DOWNDiskEMotor_AccReg = "16424";
        public static string DOWNDiskEMotor_Enable = "16462";
        public static string DOWNDiskEMotor_Moving = "16471";                          //下料电缸移动中
        public static string DOWNDiskEMotor_WanningCoil = "16472";                         //下料电缸报警
        public static string DOWNDiskEMotor_Cancel_Wanning = "16466";                     //下料电缸复位取消报警
        public static string DOWNDiskEMotor_AlalmCode = "16431";                       //报警代码，用于解析
        public static string DOWNDiskEMotor_Run = "16463";                             //启动，运动到目标位置


        public static string TAKEDiskEMotor_CurrentPositionReg = "16433";
        public static string TAKEDiskEMotor_TargetPositionReg = "16552";
        public static string TAKEDiskEMotor_SpeedReg = "16554";
        public static string TAKEDiskEMotor_AccReg = "16432";
        public static string TAKEDiskEMotor_Enable = "16478";                          
        public static string TAKEDiskEMotor_Moving = "16486";                          //取料电缸移动中
        public static string TAKEDiskEMotor_WanningCoil = "16487";                         //取料电缸报警
        public static string TAKEDiskEMotor_Cancel_Wanning = "16482";                     //取料电缸复位取消报警
        public static string TAKEDiskEMotor_AlalmCode = "16439";                       //报警代码，用于解析
        public static string TAKEDiskEMotor_Run = "16479";                             //启动，运动到目标位置

        //旋转电机的控制寄存器地址
        //电机为4位
        public static string Motor_Current_AngleReg = "16468";
        public static string Motor_WarningTriggerCoil = "26";
        public static string Motor_SpeedReg = "16452";
        public static string Motor_Target_AngleReg = "16448";
        public static string Motor_AccReg = "16456";
        public static string Motor_Enable = "16524";
        public static string Motor_Move_OriginTriggerCoil = "16527";
        public static string Motor_Run = "16525";



        //检测当前状态的地址

        public static string CurrentPlateIndex = "55";              //当前隔离盘编号
        public static string CurrentPassMillis = "55";                   //当前流程时间


        public static string CurrentFinishPlate = "55";                     //当前完成盘片数

        //控制器初始化标志位
        public static string All_InitCoil = "16543";                 //总初始化
        public static string Motor_InitCoil = "16540";               //电机初始化
        public static string UpEmotor_InitCoil = "16542";            //上料电缸初始化
        public static string DownEmotor_InitCoil = "16544";          //下料电缸初始化
        public static string Robot_InitCoil = "16541";               //机械爪回零初始化                                                             //
        public static string All_Init_Flag = "16547";                //总初始化完成标志位
//        public static string Motor_Init_Flag = "16540";            //电机初始化标志位
        public static string UpEmotor_Init_Flag = "16546";           //上料电缸初始化标志位
        public static string DownEmotor_Init_Flag = "16548";         //下料电缸初始化标志位
        public static string Robot_Init_Flag = "16545";              //机械爪回零初始化标志位  


        //上下料每层是否有料检测标志位
        public static string Up_Disk_Finish_1 = "16515";
        public static string Up_Disk_Finish_2 = "16514";
        public static string Up_Disk_Finish_3 = "16513";
        public static string Up_Disk_Finish_4 = "16512";
        public static string Up_Disk_Finish_5 = "16511";
        public static string Up_Disk_Finish_6 = "16510";
        public static string Up_Disk_Finish_7 = "16509";
        public static string Up_Disk_Finish_8 = "16508";
        public static string Down_Disk_Finish_1 = "16516";
        public static string Down_Disk_Finish_2 = "16517";
        public static string Down_Disk_Finish_3 = "16518";
        public static string Down_Disk_Finish_4 = "16519";
        public static string Down_Disk_Finish_5 = "16520";
        public static string Down_Disk_Finish_6 = "16521";
        public static string Down_Disk_Finish_7=  "16522";
        public static string Down_Disk_Finish_8 = "16523";


    }
}
