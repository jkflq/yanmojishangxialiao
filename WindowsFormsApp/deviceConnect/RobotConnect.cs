﻿using Hsc3.Comm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WindowsFormsApp.components;
using WindowsFormsApp.components.robot;
using WindowsFormsApp.utils.log;
using WinFormsBase;

namespace WindowsFormsApp.deviceConnect
{

    /// <summary>
    /// 此类用于实现机械臂的一些方法
    /// </summary>
    public class RobotConnect
    {

        internal static void GetLocData(object firstRobot)
        {
            throw new NotImplementedException();
        }

        public static string curProgram = "";
  
        //连接
        public static bool connectIPC(Hsc3.Comm.CommApi cmApi, string strIP, ushort uPort)
        {
            ulong conn;
            //自动重连模式：使用该接口返回0x0表示成功触发自动重连功能（不能多次触发），不代表通信已连接。
            //非自动重连模式：使用该接口会阻塞调用线程，直至连接成功/超时（约3s）失败/其它原因导致的失败。 
            conn = cmApi.connect(strIP, uPort);
            cmApi.setUseHeartBeat(true);//开启心跳
            string backStr = "";
            conn = cmApi.execCmd("mot.getRobTypeName(0)", ref backStr, 0);
            if (conn != 0)
            {
                //Console.WriteLine("CommApi::connect() : ret = " + conn);
            }
            if (cmApi.isConnected())
            {
                //Console.WriteLine("机械臂控制柜连接成功");
                return true;
            }
            else
            {
                // Console.WriteLine("机械臂控制柜连接失败");
               

                return false;
            }
        }
        //断开连接
        public static void disconnectIPC(Hsc3.Comm.CommApi cmApi)
        {
            //timer.Stop();
            bool connect = cmApi.isConnected();
            while(connect)
            {
                cmApi.disconnect();
                connect = cmApi.isConnected();
            }

            cmApi.Dispose();

        }

        //加载机械臂内部程序
        public static void AutoLoadProgram(Robot robot, string path,string filename,int speed)
        {
            if (!robot.Connected)
                return;
            //关闭一下手动模式，防止上次手动模式示教完没有关闭
            //使能机械臂
            robot.proMot.setManualMode(Hsc3.Comm.ManualMode.MANUAL_CONTINUE);
            RobotConnect.setGpEn(robot,0, true);
            //ConnectConfig.proMot.setGpEn(0, true);
            //将机器人设置为自动运行模式
            robot.proMot.setOpMode(Hsc3.Comm.OpMode.OP_AUT);
            //设置自动运行模式（目前不能使用）
            //ConnectConfig.proMot.setAutoMode(Hsc3.Comm.AutoMode.AUTO_MODE_CONTINUE);
            //设置自动运行倍率
            robot.proMot.setVord(speed);
            //加载程序
            robot.proVm.load(path, filename);

            Console.WriteLine("加载程序完成");

        }

        /// <summary>
        /// 外部输入模式下加载程序
        /// </summary>
        /// <param name="robot"></param>
        public static void AutoLoadProgram(Robot robot,int prgindex)
        {
            if (!robot.Connected)
                return;

            //切换到外部输入模式
            SwitchRobotMode(robot,3);
            //给加载程序一个虚拟IO
            RobotConnect.SetR(robot, 20, prgindex);
            //加载程序
            RobotConnect.GiveDinPluse(robot, RobotPortConfig.INPUT_PRG_LOAD);

            while (!GetDout(robot, RobotPortConfig.OUTOUT_PRG_READY)) ;

            Console.WriteLine($"{robot.Name}初始化程序加载成功");

            //RobotConnect.setGpEn(robot, 0, true);

            LogHelper.loginfo.Info("程序加载完成");
        }

        //启动程序
        public static void RunProgram(Robot robot, string filename)
        {
            if (!robot.Connected)
                return;

            List<string> name = new List<string>();
            //获取加载程序列表
            string PRG;
            bool isExist = false;
            robot.proVm.mainProgNames(name);
            foreach(var prg in name)
            {
                PRG = prg.Substring(1, prg.Length - 2);
                if (filename == PRG)
                    isExist = true;
            }
            string commend = "";
            string str = "";
            if (isExist)
            {
                Console.WriteLine("程序已加载，开始运行");
                commend = $"vm.start({filename})";
                //"vm.start(" + filename + ")"
                //执行命令，启动程序
                robot.CommMot.execCmd(commend, ref str, 0);
      
                //Thread.Sleep(500);
                robot.CommMot.execCmd("?group[0].status", ref str, 0);

                while (str != "\"4\"")
                {
                    robot.CommMot.execCmd(commend, ref str, 0);
                    robot.CommMot.execCmd("?group[0].status", ref str, 0);
                }
                Console.WriteLine("运行成功");
            }
            else
            {
                Console.WriteLine("未加载该程序，运行失败");
            }



        }




        /// <summary>
        /// 此函数用于外部触发模式卸载机械臂程序
        /// </summary>
        public static void unload(Robot robot)
        {
            if (!robot.Connected)
                return;
            //切换到外部输入模式
            SwitchRobotMode(robot, 3);
            if (GetDout(robot, RobotPortConfig.OUTOUT_PRG_READY) || GetDout(robot, RobotPortConfig.OUTPUT_PRG_PAUSE) || GetDout(robot, RobotPortConfig.OUTPUT_PRG_RUNNING))
            {
                //给加载程序一个虚拟IO
                //停止程序的运行
                if(GetDout(robot, RobotPortConfig.OUTPUT_PRG_RUNNING))
                     RobotConnect.GiveDinPluse(robot, RobotPortConfig.INPUT_PRG_STOP);

                //掉使能
                RobotConnect.setGpEn(robot, 0, false);

                //卸载程序
                RobotConnect.GiveDinPluse(robot, RobotPortConfig.INPUT_PRG_UNLOAD);

                Console.WriteLine($"{robot.Name}卸载程序完成");
            }
            else
            {
                Console.WriteLine($"{robot.Name}已无加载程序，无需卸载");
            }




        }
        
        /// <summary>
        /// 外部触发模式暂停机械臂已加载的程序
        /// </summary>
        /// <param name="robot"></param>
        public static void PauseOrStart(Robot robot)
        {
            if (!robot.Connected)
                return;

            //如果有程序就暂停  
            if (GetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_PRG_RUNNING))
            {
                SwitchRobotMode(ConnectConfig.FirstRobot, 3);
                RobotConnect.GiveDinPluse(robot, RobotPortConfig.INPUT_PRG_PAUSE);
                Console.WriteLine("程序已暂停");
            }
            else if(GetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_PRG_PAUSE))
            {
                SwitchRobotMode(ConnectConfig.FirstRobot, 3);
                RobotConnect.GiveDinPluse(robot, RobotPortConfig.INPUT_PRG_START);
                Console.WriteLine("程序继续运行");
            }

        }
        /// <summary>
        /// 外部触发模式开始运行机械臂已加载的程序
        /// </summary>
        /// <param name="robot"></param>
        public static void Start(Robot robot)
        {
            if (!robot.Connected && Status(robot) == "\"4\"")
                return;

            if (GetDout(robot, RobotPortConfig.OUTOUT_PRG_READY))
            {
                SwitchRobotMode(robot, 3);
                //上使能
                RobotConnect.setGpEn(robot, 0, true);
                //Thread.Sleep(500);
                RobotConnect.GiveDinPluse(robot, RobotPortConfig.INPUT_PRG_START);

                Console.WriteLine($"{robot.Name}程序已启动");
            }
            else
            {
                Console.WriteLine($"{robot.Name}未加载程序，不能启动");
            }
        }

        /// <summary>
        /// 使能机械臂
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="gpid"></param>
        /// <param name="en"></param>
        public static void setGpEn(Robot robot,sbyte gpid,bool en)
        {
            if (!robot.Connected)
                return;
            bool is_en = false;

            switch (GetRobotMode(robot))
            {
                case Hsc3.Comm.OpMode.OP_EXT:
                    robot.proIO.getDout(RobotPortConfig.OUTOUT_ENABLE_STATE, ref is_en);
                    if (is_en)
                    {
                        Console.WriteLine($"{robot.Name}已使能，无需上使能");
                    }
                    if (!is_en && en)
                    {

                        //现在的使能
                        robot.proIO.setDin(RobotPortConfig.INPUT_ENABLE, false);
                        Thread.Sleep(10);
                        robot.proIO.setDin(RobotPortConfig.INPUT_ENABLE, true);
                        Thread.Sleep(300);
                        Console.WriteLine($"{robot.Name}已重新上使能");

                    }

                    else if (is_en && !en)
                    {
                        robot.proIO.setDin(RobotPortConfig.INPUT_ENABLE, false);
                        Thread.Sleep(10);
                        Console.WriteLine($"{robot.Name}已掉使能");
                    }
                    break;
                default:
                    robot.proMot.getGpEn(0, ref is_en);
                    if (!is_en && en)
                    {

                        //以前的使能
                        robot.proMot.setGpEn(0, true);
                        robot.proMot.getGpEn(0, ref is_en);
                        while (!is_en)
                        {
                            robot.proMot.setGpEn(0, true);
                            robot.proMot.getGpEn(0, ref is_en);
                        }


                    }

                    else if (is_en && !en)
                    {
                          robot.proMot.setGpEn(0, false);

                    }


                    break;
            }
                
        }

        /// <summary>
        /// 初始化机械臂
        /// </summary>
        /// <param name="robot"></param>
        public static void InitRobot(Robot robot)
        {
            if (!robot.Connected)
                return;
            //程序清空
            unload(robot);

            AutoLoadProgram(robot,1);

            Start(robot);

            Console.WriteLine($"{robot.Name}开始初始化");
        }
        //获取当前轴坐标位置
        public static List<double> GetJntData(Robot robot)
        {

            robot.GetJntData();
            return robot.jntData;
        }

        //获取机械臂笛卡尔坐标位置
        public static List<double> GetLocData(Robot robot)
        {

            robot.GetLocData();
            return robot.locData;
        }

        /// <summary>
        /// 立即停止机械臂
        /// </summary>
        /// <param name="robot"></param>
        public static void StopRobot(Robot robot)
        {
            if (!robot.Connected)
                return;
            Hsc3.Comm.OpMode mode = new Hsc3.Comm.OpMode();
            robot.proMot.getOpMode(ref mode);
            if (mode == Hsc3.Comm.OpMode.OP_T2 || mode == Hsc3.Comm.OpMode.OP_T1)
            {
                robot.proMot.stopJog(0);
            }
            else if (mode == Hsc3.Comm.OpMode.OP_AUT)
            {
                RobotConnect.unload(robot);
            }

        }


        //
        /// <summary>
        /// 切换操作模式（0 手动1，1 手动2，外部，2 自动运行）
        /// </summary>
        /// <param name="flag">0:T1模式 1：自动模式</param>
        public static void SwitchRobotMode(Robot robot,int flag)
        {
           
            switch (flag)
            {
                case 0:
                    robot.proMot.setOpMode(Hsc3.Comm.OpMode.OP_T1);
                    ConnectConfig.FirstRobot.Pattern = MotorPattern.T1;
                    break;
                case 1:
                    robot.proMot.setOpMode(Hsc3.Comm.OpMode.OP_T2);
                    ConnectConfig.FirstRobot.Pattern = MotorPattern.T2;
                    break;
                case 2:
                    robot.proMot.setOpMode(Hsc3.Comm.OpMode.OP_AUT);
                    ConnectConfig.FirstRobot.Pattern = MotorPattern.AUTO;
                    break;
                case 3:
                    robot.proMot.setOpMode(Hsc3.Comm.OpMode.OP_EXT);
                    ConnectConfig.FirstRobot.Pattern = MotorPattern.EXT;
                    break;
            }

        }



        public static Hsc3.Comm.OpMode GetRobotMode(Robot robot)
        {
            Hsc3.Comm.OpMode opMode = new Hsc3.Comm.OpMode();
            robot.proMot.getOpMode(ref opMode);
            return opMode;
        }

        //笛卡尔坐标运动到点
        /// <summary>
        /// 不允许打断
        /// </summary>
        /// <param name="point"></param>
        /// <param name="speed"></param>
        /// <param name="isLinear"></param>
        public static void MoveTo(Robot robot,ref Hsc3.Comm.GeneralPos point,int speed,bool isLinear)
        {
            if (!robot.Connected || robot.Error)
                return;
            //"mot.moveTo(0,false,-1,-1,0,\"{ 97,426.5,-56.6,90,0.0,0.0,0.0,0.0,0.0}\",true)"
            string commend = "";
            string str_isJoint = point.isJoint.ToString();
            string str_utNum = point.utNum.ToString();
            string str_ufNum = point.ufNum.ToString();
            string str_config = point.config.ToString();
            string str_point = "";
            for(int i = 0;i<6;i++)
            {
                str_point += point.vecPos[i].ToString() + ",";
            }
            str_point += "0.0,0.0,0.0";
            str_point = "\"{" + str_point + "}\"";
            commend = "mot.moveTo(0," + str_isJoint + "," + str_utNum + "," + str_ufNum + "," + str_config + "," + str_point + "," + isLinear.ToString()+")";

            string str = "";
            //使能
            RobotConnect.setGpEn(robot,0, true);
            //手动模式
            robot.proMot.setOpMode(Hsc3.Comm.OpMode.OP_T2);
            robot.proMot.setJogVord(speed);
            //运动到点
            robot.CommMot.execCmd(commend, ref str, 0);
            //ConnectConfig.proMot.moveTo(0, point, isLinear);
            Thread.Sleep(20);
            robot.CommMot.execCmd("?group[0].status", ref str, 0);
            //Console.WriteLine("移动到点轴状态：" + str);
            //等到机械臂手动走完
            //确保移动完成
            while (str == "\"4\"")
            {
                robot.CommMot.execCmd("?group[0].status", ref str, 0);
            }
            robot.proMot.stopJog(0);
        }
        /// <summary>
        /// 允许打断
        /// </summary>
        /// <param name="point"></param>
        /// <param name="speed"></param>
        /// <param name="isLinear"></param>

        public static void MoveTo2(Robot robot,ref Hsc3.Comm.GeneralPos point, int speed, bool isLinear)
        {
            if (!robot.Connected || robot.Error)
                return;
            //"mot.moveTo(0,false,-1,-1,0,\"{ 97,426.5,-56.6,90,0.0,0.0,0.0,0.0,0.0}\",true)"
            string commend = "";
            string str_isJoint = point.isJoint.ToString();
            string str_utNum = point.utNum.ToString();
            string str_ufNum = point.ufNum.ToString();
            string str_config = point.config.ToString();
            string str_point = "";
            for (int i = 0; i < 4; i++)
            {
                str_point += point.vecPos[i].ToString() + ",";
            }
            str_point += "0.0,0.0,0.0,0.0,0.0";
            str_point = "\"{" + str_point + "}\"";
            commend = "mot.moveTo(0," + str_isJoint + "," + str_utNum + "," + str_ufNum + "," + str_config + "," + str_point + "," + isLinear.ToString() + ")";

            string str = "";
            //使能
            RobotConnect.setGpEn(robot,0, true);
            //Thread.Sleep(500);
            //手动模式
            robot.proMot.setOpMode(Hsc3.Comm.OpMode.OP_T2);
            robot.proMot.setJogVord(speed);
            //运动到点
            robot.CommMot.execCmd(commend, ref str, 0);
            //ConnectConfig.proMot.moveTo(0, point, isLinear);
            //Thread.Sleep(20);
            //robot.CommMot.execCmd("?group[0].status", ref str, 0);
           // Console.WriteLine("移动到点轴状态：" + str);
            //等到机械臂手动走完
            //确保移动完成
            //while (str == "\"4\"")
            //{
            //    ConnectConfig.CmJixiebi.execCmd("?group[0].status", ref str, 0);
            //}

        }



        /// <summary>
        /// 关节增量状态移动
        /// </summary>
        /// <param name="robot">对象</param>
        /// <param name="axid">轴号</param>
        /// <param name="speed">速度</param>
        /// <param name="length">增量距离</param>
        /// <param name="dir">方向</param>
        public static void JointManualIncMove(Robot robot,sbyte axid,int speed,double length,Hsc3.Comm.DirectType dir)
        {
            if (!robot.Connected || robot.Error)
                return;
            string str = "";
            //使能
            RobotConnect.setGpEn(robot,0, true);
            robot.proMot.setOpMode(Hsc3.Comm.OpMode.OP_T2);
            //设置手动模式为增量模式
            robot.proMot.setManualMode(Hsc3.Comm.ManualMode.MANUAL_INCREMENT);
            //设置手动增量距离
            robot.proMot.setInchLen(length);
            //设置坐标系
            robot.proMot.setWorkFrame(0,Hsc3.Comm.FrameType.FRAME_JOINT);
            //设置手动运行倍率
            robot.proMot.setJogVord(speed);
            string commend = "";
            if (dir == Hsc3.Comm.DirectType.NEGATIVE)
                commend = "mot.startJog(0," + axid.ToString() + "," + "1" + ")";
            else
                commend = "mot.startJog(0," + axid.ToString() + "," + "0" + ")";
            //单轴手动运动
            robot.CommMot.execCmd(commend, ref str, 0);
            //这块必须延迟，保证机械手运动起来
            Thread.Sleep(20);
            //ConnectConfig.proMot.startJog(0,axid,dir);
            robot.CommMot.execCmd("?group[0].status", ref str, 0);
            //Console.WriteLine("调整坐标系时轴的状态：" + str);
            //保证运动操作执行完毕
            while (str == "\"4\"")
            {
                robot.CommMot.execCmd("?group[0].status", ref str, 0);
            }
            robot.proMot.stopJog(0);

        }

        /// <summary>
        /// 笛卡尔增量移动
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="axid"></param>
        /// <param name="speed"></param>
        /// <param name="length"></param>
        /// <param name="dir"></param>
        public static void DescartManualIncMove(Robot robot, sbyte axid, int speed, double length, Hsc3.Comm.DirectType dir)
        {
            if (!robot.Connected || robot.Error)
                return;
            string str = "";
            //使能
            RobotConnect.setGpEn(robot, 0, true);
            robot.proMot.setOpMode(Hsc3.Comm.OpMode.OP_T2);
            //设置手动模式为增量模式
            robot.proMot.setManualMode(Hsc3.Comm.ManualMode.MANUAL_INCREMENT);
            //设置手动增量距离
            robot.proMot.setInchLen(length);
            //设置坐标系
            robot.proMot.setWorkFrame(0, Hsc3.Comm.FrameType.FRAME_WORLD);
            //设置手动运行倍率
            robot.proMot.setJogVord(speed);
            string commend = "";
            if (dir == Hsc3.Comm.DirectType.NEGATIVE)
                commend = "mot.startJog(0," + axid.ToString() + "," + "1" + ")";
            else
                commend = "mot.startJog(0," + axid.ToString() + "," + "0" + ")";
            //单轴手动运动
            robot.CommMot.execCmd(commend, ref str, 0);
            //这块必须延迟，保证机械手运动起来
            Thread.Sleep(20);
            //ConnectConfig.proMot.startJog(0,axid,dir);
            robot.CommMot.execCmd("?group[0].status", ref str, 0);
            //Console.WriteLine("调整坐标系时轴的状态：" + str);
            //保证运动操作执行完毕
            while (str == "\"4\"")
            {
                robot.CommMot.execCmd("?group[0].status", ref str, 0);
            }
            robot.proMot.stopJog(0);

        }
        /// <summary>
        /// 关节持续状态移动
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="axid"></param>
        /// <param name="speed"></param>
        /// <param name="dir"></param>
        public static void JointManualConMove(Robot robot,sbyte axid, int speed, Hsc3.Comm.DirectType dir)
        {
            if (!robot.Connected || robot.Error)
                return;
            string str = "";
            //使能
            robot.proMot.setGpEn(0, true);

            robot.proMot.setOpMode(Hsc3.Comm.OpMode.OP_T1);
            //设置手动模式为持续模式
            robot.proMot.setManualMode(Hsc3.Comm.ManualMode.MANUAL_CONTINUE);
  
            //设置坐标系
            robot.proMot.setWorkFrame(0, Hsc3.Comm.FrameType.FRAME_JOINT);

            robot.proMot.setJogVord(speed);
            string commend = "";
            if (dir == Hsc3.Comm.DirectType.NEGATIVE)
                commend = "mot.startJog(0," + axid.ToString() + "," + "1"  + ")";
            else
                commend = "mot.startJog(0," + axid.ToString() + "," + "0"  + ")";
            //单轴手动运动
            ulong flag = robot.CommMot.execCmd(commend, ref str, 0);
            //Console.WriteLine(flag);
            Thread.Sleep(20);
            //ConnectConfig.proMot.startJog(0,axid,dir);
            flag = robot.CommMot.execCmd("?group[0].status", ref str, 0);
           
            while (str != "\"4\"")
            {
                robot.CommMot.execCmd(commend, ref str, 0);
                robot.CommMot.execCmd("?group[0].status", ref str, 0);
            }
            //robot.proMot.stopJog(0);

        }

        public static void DescartManualConMove(Robot robot, sbyte axid, int speed, Hsc3.Comm.DirectType dir)
        {
            if (!robot.Connected || robot.Error)
                return;
            string str = "";
            //使能
            robot.proMot.setGpEn(0, true);
            robot.proMot.setOpMode(Hsc3.Comm.OpMode.OP_T1);
            //设置手动模式为增量模式
            robot.proMot.setManualMode(Hsc3.Comm.ManualMode.MANUAL_CONTINUE);

            //设置坐标系
            robot.proMot.setWorkFrame(0, Hsc3.Comm.FrameType.FRAME_WORLD);

            robot.proMot.setJogVord(speed);
            string commend = "";
            if (dir == Hsc3.Comm.DirectType.NEGATIVE)
                commend = "mot.startJog(0," + axid.ToString() + "," + "1" + ")";
            else
                commend = "mot.startJog(0," + axid.ToString() + "," + "0" + ")";
            //单轴手动运动
            ulong flag = robot.CommMot.execCmd(commend, ref str, 0);
            //Console.WriteLine(flag);
            Thread.Sleep(20);
            //ConnectConfig.proMot.startJog(0,axid,dir);
            flag = robot.CommMot.execCmd("?group[0].status", ref str, 0);

            while (str != "\"4\"")
            {
                robot.CommMot.execCmd(commend, ref str, 0);
                robot.CommMot.execCmd("?group[0].status", ref str, 0);
            }
            //robot.proMot.stopJog(0);

        }


        /// <summary>
        /// 获取存储点
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="JRorLR"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static Hsc3.Comm.GeneralPos GetStorePoint(Robot robot,string JRorLR,int index)
        {
            if (!robot.Connected)
                return null;

            List<double> data = new List<double>();

            Hsc3.Comm.LocPos locPos = new Hsc3.Comm.LocPos();


            Hsc3.Comm.GroupData config = new Hsc3.Comm.GroupData();
            robot.proMot.getGroupData(0, ref config);
            Hsc3.Comm.GeneralPos dirPos = new Hsc3.Comm.GeneralPos();

            switch (JRorLR)
            {
                case "JR":
                    dirPos.isJoint = true;
                    robot.proVar.getJR(0, index, ref data);
                    dirPos.vecPos = data;
                    break;
                case "LR":
                    dirPos.isJoint = false;
                    robot.proVar.getLR(0, index, locPos);
                    dirPos.vecPos = locPos.vecPos;
                    break;
            }
            dirPos.ufNum = config.ufNum;
            dirPos.utNum = config.utNum;
            dirPos.config = config.config;
            
            return dirPos;
        }

        /// <summary>
        /// 获取当前位置信息
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="JRorLR"></param>
        /// <returns></returns>
        public static Hsc3.Comm.GeneralPos GetCurPoint(Robot robot, string JRorLR)
        {
            if (!robot.Connected)
                return null;
            List<double> data = new List<double>();
            Hsc3.Comm.LocPos locPos = new Hsc3.Comm.LocPos();
            Hsc3.Comm.GroupData config = new Hsc3.Comm.GroupData();
            robot.proMot.getGroupData(0, ref config);
            Hsc3.Comm.GeneralPos dirPos = new Hsc3.Comm.GeneralPos();
            switch (JRorLR)
            {
                case "JR":
                    dirPos.isJoint = true;
                    data = robot.GetJntData();
                    dirPos.vecPos = data;
                    break;
                case "LR":
                    dirPos.isJoint = false;
                    //获取笛卡尔坐标数据
                    data = robot.GetLocData();
                    dirPos.vecPos = data;
                    break;
            }
            dirPos.ufNum = config.ufNum;
            dirPos.utNum = config.utNum;
            dirPos.config = config.config;

            return dirPos;
        }

        /// <summary>
        /// 保存当前位置点
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="JRorLR"></param>
        /// <param name="index"></param>
        /// <param name="data"></param>
        public static void SaveCurPoint(Robot robot,string JRorLR,int index,double Xoffset=0, double Yoffset = 0,double angel = 0)
        {
            if (!robot.Connected)
                return;
            //用于存储笛卡尔点的数据
            Hsc3.Comm.LocPos locPos = new Hsc3.Comm.LocPos();
            List<double> data;
            Hsc3.Comm.GroupData config = new Hsc3.Comm.GroupData();
            robot.proMot.getGroupData(0, ref config);
            //Hsc3.Comm.GeneralPos dirPos = new Hsc3.Comm.GeneralPos();
            switch (JRorLR)
            {
                case "JR":
                    //获取关节点坐标数据
                    data = robot.GetJntData();
                    robot.proVar.setJR(0, index, ref data);
                    break;
                case "LR":
                    //获取笛卡尔坐标数据
                    data = robot.GetLocData();

                    //补偿值
                    //由于机械臂齿轮间隙，不同姿态的同一个笛卡尔点会有微小差别
                    data[0] += Xoffset;//x补偿值
                    data[1] += Yoffset;//y补偿值
                    data[3] += angel;//角度补偿值
                    //存储到寄存器
                    locPos.vecPos = data;
                    locPos.ufNum = config.ufNum;
                    locPos.utNum = config.utNum;
                    locPos.config =  config.config ;
                    robot.proVar.setLR(0, index, locPos);
                    break;
            }
        }


        /// <summary>
        /// 保存已知点
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="pos"></param>
        /// <param name="index"></param>
        public static void SaveKnowPoint(Robot robot,Hsc3.Comm.GeneralPos pos,int index)
        {
            if (!robot.Connected)
                return;
                //用于存储笛卡尔点的数据
                Hsc3.Comm.LocPos locPos = new Hsc3.Comm.LocPos();
            List<double> data;
            Hsc3.Comm.GroupData config = new Hsc3.Comm.GroupData();
            robot.proMot.getGroupData(0, ref config);
            //Hsc3.Comm.GeneralPos dirPos = new Hsc3.Comm.GeneralPos();
            if(pos.isJoint)
            {
                data = pos.vecPos;
                robot.proVar.setJR(0, index, ref data);
                
            }
            else
            {
                //存储到寄存器
                locPos.vecPos = pos.vecPos;
                locPos.ufNum = config.ufNum;
                locPos.utNum = config.utNum;
                locPos.config = config.config;
                robot.proVar.setLR(0, index, locPos);
            }

        }
        /// <summary>
        /// 设置机械臂IO输出状态
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="portindex"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool SetDout(Robot robot,int portindex,bool value)
        {
            if (robot == null ||!robot.Connected)
                return false;
            bool status = false;
            robot.proIO.setDoutMaskBit(Convert.ToUInt32(portindex), false);
            robot.proIO.setDout(portindex, value);
            robot.proIO.getDout(portindex, ref status);
            if (status == value)
                return true;
            else
                return false;
        }



        public static bool GetDout(Robot robot,int portindex)
        {
            bool value = false; 
            robot.proIO.getDout(portindex, ref value);
            return value;
        }
        /// <summary>
        /// 获取单个输入IO状态
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="portindex"></param>
        /// <returns></returns>
        public static bool GetDin(Robot robot, int portindex)
        {
            if (!robot.Connected)
                return false;

            string ret = "";
            robot.CommMot.execCmd($"io.getDin({portindex})",ref ret,0);
            return ret == "\"false\"" ? false:true ;

        }

        /// <summary>
        /// 获取输入组
        /// 
        /// /// </summary>
        /// <param name="robot"></param>
        /// <returns></returns>
        public static bool[] GetDinGrp(Robot robot)
        {
            bool[] value = new bool[32];
            if (!robot.Connected)
                return value;
            uint _value = 0;
            robot.proIO.getDinGrp(0, ref _value);
            for (int i = 0; _value > 0; i++)
            {
                value[i] = (_value % 2) == 0 ? false : true;
                _value = _value / 2;
            }
            return value;
        }


        public static bool[] GetDoutGrp(Robot robot)
        {
            bool[] value = new bool[32];
            if (!robot.Connected)
                return value;
            uint _value = 0;
            robot.proIO.getDoutGrp(0, ref _value);
            for (int i = 0; _value > 0; i++)
            {
                value[i] = (_value % 2) == 0 ? false : true;
                _value = _value / 2;
            }
            return value;
        }

        /// <summary>
        /// 获取LR寄存器组
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="index"></param>
        /// <param name="len"></param>
        /// <returns></returns>
        public static List<LocPos> GetLRGrp(Robot robot,int index,int len)
        {
            List<LocPos> listData = new List<LocPos>();
            robot.proVar.getLR(0 ,index,len, ref listData);
            LocPos locPos;
            return listData;
        }

        /// <summary>
        /// 给模拟输入一个下降沿脉冲
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="portindex"></param>
        public static void GiveDinPluse(Robot robot,int portindex)
        {
            robot.proIO.setDin(portindex, true);
            Thread.Sleep(20);
            robot.proIO.setDin(portindex, false);
            Thread.Sleep(20);
        }
        /// <summary>
        /// 设置模拟输入电平
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="portindex"></param>
        /// <param name="value"></param>
        public static void SetDin(Robot robot,int portindex,bool value)
        {
            robot.proIO.setDin(portindex, value);
        }

        /// <summary>
        /// 设置R寄存器的值
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="index"></param>
        /// <param name="value"></param>
        public static void SetR(Robot robot,int index,double value)
        {
            if (!robot.Connected)
                return;
            robot.proVar.setR(index, value);
            Thread.Sleep(10);
        }
        /// <summary>
        /// 获取R寄存器值
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static double GetR(Robot robot, int index)
        {
            if (!robot.Connected)
                return 0;
            double value = -1;
            robot.proVar.getR(index, ref value);
            return value;
        }

        public static double[] GetRGrp(Robot robot)
        {
            List<double> data = new List<double>();
            double[] reg = new double[32];
            if (!robot.Connected)
               return reg;
            robot.proVar.getR(0, 32,ref data);
            if(data.Count!= 0)
             reg = data.ToArray();
            return reg;
        }





        /// <summary>
        /// 获取机械臂状态
        /// 0 初值 1:未使能 2:准备完成 3:回零点中 4:运动中 5：停止中  6：错误停止中
        /// </summary>
        /// <param name="robot"></param>
        /// <returns></returns>
        public static string Status(Robot robot)
        {
            if (!robot.Connected)
                return null;
            string ret = "";
            robot.CommMot.execCmd("?group[0].status", ref ret, 0);
            return ret;
        }

        /// <summary>
        /// 保存寄存器值 ，type 可选 "JR" "LR" 
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="type"></param>
        public static void SaveData(Robot robot,string type)
        {
            if (!robot.Connected)
                return;
            robot.proVar.saveGp(0, type);
        }


 

        /// <summary>
        /// 机械臂复位
        /// </summary>
        /// <param name="robot"></param>
        public static void Reset(Robot robot)
        {
            if (!robot.Connected)
                return;
            robot.Error = false;
            robot.proSys.reset();

        }





        public static float MeasureHeight()
        {
            float height;
            ushort high16 = (ushort)GetR(ConnectConfig.FirstRobot, 510);
            ushort low16 = (ushort)GetR(ConnectConfig.FirstRobot, 511);
            int value = (int)(high16 << 16 | low16);
            height = BitConverter.ToSingle(BitConverter.GetBytes(value), 0);

            return height;
        }


    }



}
