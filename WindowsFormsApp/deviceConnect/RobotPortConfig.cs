﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp.deviceConnect
{
    class RobotPortConfig
    {

        #region 上下料机械臂 真实输入输出I/O
        //继电器状态
        public enum EVC_STATUS
        {
            INSPIRATE,
            EXPIRATE,
            CLOSE
        }
        //机械臂相关IO
        //研磨机点动
        public static int OUTPUT_YANMOJI_DIANDONG = 9;
        //研磨机研磨液
        public static int OUTPUT_YANMOJI_YANMOYE = 5;

        //抓取完成信号（输出）
        public static int OUTPUT_DINGWEI_ZHUAQUWANCHENG = 14;

        //启动定位系统信号（输出）
        public static int OUTPUT_DINGWEI_STARTSYS = 15;


        //研磨机升起就绪(输入)
        public static int INPUT_YANMOJI_READY = 5;
        //上料机就绪信号(输入)
        public static int INPUT_SHANGLIAO_READY = 6;
        //下料机就绪信号(输入)
        public static int INPUT_XIALIAO_READY = 4;
        //定位系统工作状态信号(输入)
        public static int INPUT_DINGWEI_READY = 7;
        //定位系统到位信号(输入)
        public static int INPUT_DINGWEI_ARRIVAL = 0;


        //取料完成信号
        public static int OUTPUT_SHANGLIAO_SUCCESS = 8;
        //下料触发信号
        public static int OUTPUT_XIALIAO_SUCCESS = 4;

        //机械臂在研磨机里的信号
        public static int OUTPUT_ROBOT_IN_YANMOJI = 9;
        //上料完成指示灯
        public static int OUTPUT_SHANGLIAO_SEUUESS_LIGHT = 2;
        //运行指示灯
        public static int OUTPUT_RUN_LIGHT = 0;
        //暂停按键
        public static int INPIT_PAUSE_KEY = 1;
        //暂停指示灯
        public static int OUTPUT_PAUSE_LIGHT = 1;
        //启动按键
        public static int INPUT_START_KEY = 2;
        //报警指示灯
        public static int OUTPUT_WARN_LIGHT = 3;

        //吸嘴真空度是否达到

        public static int FirstRobot_OUTPUT_Vacuum_IN = 3;
        public static int FirstRobot_OUTPUT_Vacuum_EX = 2;
        public static int FirstRobot_INPUT_VacuumDegree = 3;

        public static int SecondRobot_OUTPUT_Vacuum_IN = 1;
        public static int SecondRobot_OUTPUT_Vacuum_EX = 6;
        public static int SecondRobot_INPUT_VacuumDegree = 0;

        public static int ThirdRobot_OUTPUT_Vacuum_IN = 1;
        public static int ThirdRobot_OUTPUT_Vacuum_EX = 6;
        public static int ThirdRobot_INPUT_VacuumDegree = 0;


        //继电器1 吸嘴吸
        public static int OUTPUT_EVC1_OP = 11;
        public static int OUTPUT_EVC1_ED = 10;
        private static volatile EVC_STATUS evc1_status = EVC_STATUS.CLOSE;
        public static EVC_STATUS EVC1_STATUS
        {
            get
            {
                return evc1_status;
            }
            set
            {
                evc1_status = value;
                if (value == EVC_STATUS.INSPIRATE)
                {
                    RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_EVC1_OP, true);
                    RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_EVC1_ED, false);
                }
                else
                {
                    RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_EVC1_OP, false);
                    RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_EVC1_ED, true);
                }
            }
        }




        //夹爪行程
        public static int OUTPUT_UP_Grip_First = 3;
        public static int OUTPUT_UP_Grip_Second = 4;
        public static int OUTPUT_UP_Grip_Third = 5;
        public static int OUTPUT_DOWN_Grip_First = 3;
        public static int OUTPUT_DOWN_Grip_Second = 4;
        public static int OUTPUT_DOWN_Grip_Third= 5;


        //吸嘴吹
        private static int OUTPUT_EVC2_OP = 12;
        private static int OUTPUT_EVC2_ED = 13;
        private static volatile EVC_STATUS evc3_status = EVC_STATUS.CLOSE;
        public static EVC_STATUS EVC2_STATUS
        {
            get
            {
                return evc3_status;
            }
            set
            {
                evc3_status = value;
                if (value == EVC_STATUS.EXPIRATE)
                {

                    RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_EVC2_OP, true);
                    RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_EVC2_ED, false);
                }
                else
                {

                    RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_EVC2_OP, false);
                    RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_EVC2_ED, true);
                }
            }
        }


        #endregion



        #region 上下料机械臂 虚拟输入输出I/O

        public static int OUTPUT_ROBOT_READY = 50;
        public static int OUTPUT_ROBOT_FAULTS = 51;
        public static int OUTOUT_ENABLE_STATE = 52;
        public static int OUTPUT_PRG_UNLOAD = 53;
        public static int OUTOUT_PRG_READY = 54;
        public static int OUTPUT_PRG_RUNNING = 55;
        public static int OUTPUT_PRG_ERR = 56;
        public static int OUTPUT_PRG_PAUSE = 57;

        public static int OUTPUT_PRG_RUNSUCCESS = 20;




        public static int INPUT_PRG_START = 50;
        public static int INPUT_PRG_PAUSE = 51;
        public static int INPUT_PRG_STOP = 52;
        public static int INPUT_PRG_LOAD = 53;
        public static int INPUT_PRG_UNLOAD = 54;
        public static int INPUT_ENABLE = 55;
        public static int INPUT_CLEAR_FAULTS = 56;


        public static int INPUT_SUCCESS = 56;


        #endregion

    }
}
