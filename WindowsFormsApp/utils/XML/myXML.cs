﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using WindowsFormsApp.deviceConnect;
using static WindowsFormsApp.deviceConnect.ParamConfig;
using static WindowsFormsApp.FrmOffsetConfig;

namespace WindowsFormsApp.utils.XML
{
    class myXML
    {
        //文件对象
        XmlDocument xmlDoc;
        //文件跟节点
        XmlElement xmlRoot;
        //文件名字
        string filename;

        public myXML(string name)
        {
            //实例化
            this.xmlDoc = new XmlDocument();
            this.filename = name;
            //若文件存在则会加载文件
            try 
            {
                xmlDoc.Load(filename);
                this.xmlRoot = xmlDoc.DocumentElement;
            }
            catch(Exception ex)
            {
                this.CreeatXML("root");
            }

        }

        public void CreeatXML(string rootName)
        {
            XmlDeclaration xmlDec = this.xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            this.xmlDoc.AppendChild(xmlDec);
            //创建根节点
            this.xmlRoot = xmlDoc.CreateElement(rootName);
            //添加根节点到xml文件中去
            xmlDoc.AppendChild(xmlRoot);
            //创建保存文件
            this.xmlDoc.Save(filename);
        }

        //添加子节点
        public void AddNode(string parentNode, string nodeName)
        {
            //创建子节点
            XmlElement element = xmlDoc.CreateElement(nodeName);
            if (parentNode == "root")
                xmlRoot.AppendChild(element);
            //找到父节点
            else
            {
                XmlElement parentelement = this.xmlRoot[parentNode];
                parentelement.AppendChild(element);
            }
            this.xmlDoc.Save(filename);

        }

        /// <summary>
        /// 给节点添加属性
        /// </summary>
        /// <param name="nodeName"></param>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public void AddAttributeion(string nodeName,string name,string value)
        {
            xmlRoot[nodeName].SetAttribute(name,value);
            this.xmlDoc.Save(filename);
        }
        /// <summary>
        /// 获取节点的值
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public string getNodeValue(string node)
        {
            return this.xmlRoot[node].InnerText;
        }
        /// <summary>
        /// 设置节点的值
        /// </summary>
        /// <param name="node"></param>
        /// <param name="value"></param>
        public void setNodeValue(string node ,string value)
        {
            xmlRoot[node].InnerText = value;
            this.xmlDoc.Save(filename);
        }
        /// <summary>
        /// 加载参数文件中的offset数据
        /// </summary>
        /// <param name="path"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public static List<OffsetData> LoadOffsetConfig(string str,string path = "ParamConfig.xml", string field = "OffsetParams")
        {
            XmlDocument xmldom = new XmlDocument();
            xmldom.Load(path);
            XmlElement xmlRoot = xmldom.DocumentElement;
            var xNodes = xmlRoot[field].SelectNodes(str);
            List<OffsetData> list = new List<OffsetData>();
            foreach (XmlElement node in xNodes)
            {
                list.Add(node);
            }
            return list;
        }

        /// <summary>
        /// 加载PC参数
        /// </summary>
        /// <param name="str"></param>
        /// <param name="path"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public static List<ParamData> LoadPCParams(string str,string path = "ParamConfig.xml", string field = "ParamData")
        {
            XmlDocument xmldom = new XmlDocument();
            xmldom.Load(path);
            XmlElement xmlRoot = xmldom.DocumentElement;
            var xNodes = xmlRoot[field].SelectNodes(str);
            List<ParamData> list = new List<ParamData>();
            foreach (XmlElement node in xNodes)
            {
                list.Add(node);
            }
            return list;
        }



        //保存offset数据到参数文件中
        public static  void SaveOffsetConfig(List<OffsetData> configDatas, string field = "OffsetParams", string path = "ParamConfig.xml")
        {

            XmlDocument xmldom = new XmlDocument();
            xmldom.Load(path);
            XmlElement xmlRoot = xmldom.DocumentElement;
            xmlRoot[field].RemoveAll();

            foreach (var configdata in configDatas)
            {

                XmlElement element = xmldom.CreateElement("OffsetData");
                if (configdata.Section == "上料点偏移量" || configdata.Section == "下料点偏移量" || configdata.Section == "下料抓取点偏移量" || configdata.Section == "上料放置点偏移量")
                {
            
                    ////如果机械臂已连接，则更改机械臂LR存储点
                    //if(ConnectConfig.RobotConnected)
                    //{
                       
                    //    switch (configdata.Section)
                    //    {
                    //        case "取料点偏移量":
                    //            Hsc3.Comm.GeneralPos dirpos1 = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.QULIAO_LR_REG);
                    //            dirpos1.vecPos[0] += configdata.Xoffset;
                    //            dirpos1.vecPos[1] += configdata.Yoffset;
                    //            dirpos1.vecPos[3] += configdata.Angeloffset;
                    //            RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, dirpos1, QULIAO_LR_REG);
                    //            break;
                    //        case "放料点偏移量":
                    //            Hsc3.Comm.GeneralPos dirpos2 = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.FANGLIAO_LR_REG);
                    //            dirpos2.vecPos[0] += configdata.Xoffset;
                    //            dirpos2.vecPos[1] += configdata.Yoffset;
                    //            dirpos2.vecPos[3] += configdata.Angeloffset;
                    //            RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, dirpos2, FANGLIAO_LR_REG);
                    //            break;
                    //        case "上料点偏移量":
                    //            Hsc3.Comm.GeneralPos dirpos3 = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.SHANGLIAO_POINT_LR_REG);
                    //            dirpos3.vecPos[0] += configdata.Xoffset;
                    //            dirpos3.vecPos[1] += configdata.Yoffset;
                    //            dirpos3.vecPos[3] += configdata.Angeloffset;
                    //            RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, dirpos3, SHANGLIAO_POINT_LR_REG);
                    //            break;
                    //        case "下料点偏移量":
                    //            Hsc3.Comm.GeneralPos dirpos4 = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.XIALIAO_POINT_LR_REG);
                    //            dirpos4.vecPos[0] += configdata.Xoffset;
                    //            dirpos4.vecPos[1] += configdata.Yoffset;
                    //            dirpos4.vecPos[3] += configdata.Angeloffset;
                    //            RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, dirpos4, XIALIAO_POINT_LR_REG);
                    //            break;
                    //    }

                    //    RobotConnect.SaveData(ConnectConfig.FirstRobot, "LR");
                    //}
                    configdata.Xoffset = 0;
                    configdata.Yoffset = 0;
                    configdata.Angeloffset = 0;

                }
                element.SetAttribute("Section", configdata.Section);
                element.SetAttribute("Xoffset", configdata.Xoffset.ToString());
                element.SetAttribute("Yoffset", configdata.Yoffset.ToString());
                element.SetAttribute("Angeloffset", configdata.Angeloffset.ToString());
                xmlRoot[field].AppendChild(element);
            }
            
            xmldom.Save(path);
        }
        /// <summary>
        /// 保存PC参数
        /// </summary>
        /// <param name="configDatas"></param>
        /// <param name="str"></param>
        /// <param name="field"></param>
        /// <param name="path"></param>
        public static void SavePCConfig(List<ParamData> configDatas, string str,string field = "ParamData" , string path = "ParamConfig.xml")
        {
            try {
                XmlDocument xmldom = new XmlDocument();
                xmldom.Load(path);
                XmlElement xmlRoot = xmldom.DocumentElement;

                var xNodes = xmlRoot[field].SelectNodes(str);
                foreach (XmlNode XMLNode in xNodes)
                {
                    xmlRoot[field].RemoveChild(XMLNode);
                }

                foreach (var configdata in configDatas)
                {

                    XmlElement element = xmldom.CreateElement(str);
                    element.SetAttribute("ParamName", configdata.ParamName);
                    element.SetAttribute("ParamUnit", configdata.ParamUnit);
                    element.InnerText = configdata.ParamValue.ToString();
                    xmlRoot[field].AppendChild(element);

                }
                xmldom.Save(path);


            }
            catch
            {

            }


        }





    }
}
