﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp.utils.XML;

namespace WindowsFormsApp.utils.database
{
    /// <summary>
    /// 主要用于创建数据库连接及关闭打开的数据库连接
    /// </summary>
    class ConMethod
    {

        string conStr = "";
        //创建数据库连接对象
        public  MySqlConnection _mySql;

        public  string constr;

        //public SqlConnection _mySql;

        /// <summary>
        /// 数据库连接
        /// </summary>
        /// 
        public void ConDatabase()
        {
            try
            {
                myXML xml = new myXML("config.xml");
                constr = xml.getNodeValue("db_ConnectStr");
                _mySql = new MySqlConnection(constr);
                _mySql.Open();

            }
            catch
            {
                MessageBox.Show("数据库连接失败");            
            }


        }
        /// <summary>
        /// 关闭数据库
        /// </summary>
        /// <returns></returns>
        public bool closeCon()
        {
            try
            {
                //若数据连接处于打开状态
                if (_mySql.State == ConnectionState.Open)
                {
                    _mySql.Close();//关闭连接
                }
                return true;
            }
            catch
            {
                return false;//若产生异常，返回false
            }
        }
    }
}
