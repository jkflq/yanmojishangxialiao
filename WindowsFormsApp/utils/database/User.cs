﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp.utils.database
{
    /// <summary>
    /// 用于标识用户信息
    /// </summary>
    public class User
    {
        public string UserName;
        public string UserPasswd;
        public UserType UserType;
    }
    /// <summary>
    /// 用户权限
    /// </summary>
    public enum UserType
    {
        Super,
        Normal,
        Debug
    }
}
