﻿using NModbus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp.deviceConnect;
using WindowsFormsApp.utils.modbus;

namespace WindowsFormsApp.utils.modbus.NModbus
{
    public class ModbusTcpNet { 


        //端口
        public int port = 502;
        //IP
        public string ipAddress = "192.168.250.50";//"192.168.250.62"
        //是否为调试状态
        public bool DebugMode = false;

        public virtual event EventHandler<string> ConnectError;
        public virtual event EventHandler<string> ConnectSuccess;


        private ModbusFactory modbusFactory;
        private IModbusMaster master;  
        
        public TcpClient tcp;

        public ModbusTcpNet(string ipaddress)
        {
            this.ipAddress = ipaddress;
        }
        public ModbusTcpNet(string ipaddress, int port)
        {
            this.ipAddress = ipaddress;
            this.port = port;
        }

        public  bool ReadCoil(string address)
        {
            Thread.Sleep(2);
            lock (this)
            {
                bool[] result;

                try
                {
                    result = master.ReadCoils(1, ushort.Parse(address), 1);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    ConnectError?.Invoke(this, ex.ToString());
                    return false;
                }
                return result[0];
            }
        }

        public  bool[] ReadCoils(string address, ushort len)
        {
            Thread.Sleep(2);
            lock (this)
            {
                bool[] result;
        
                    result = master.ReadCoils(1, ushort.Parse(address), len);
                //catch (Exception ex)
                //{
                //    Console.WriteLine(ex);
                //    ConnectError?.Invoke(this, ex.ToString());
                //    return new bool[len];
                //}
                return result;
            }
        }

        public  void Write(string address, bool flag)
        {
            lock (this)
            {
                try
                {
                    master.WriteSingleCoil(1, ushort.Parse(address), flag);
                }
                catch(Exception ex) {
                    Console.WriteLine(ex);
                    ConnectError?.Invoke(this, ex.ToString());
                }
            }
        }

        public  void Write(string address, bool[] flag)
        {
            lock (this)
            {
                Thread.Sleep(2);
                try
                {
                    master.WriteMultipleCoils(1, ushort.Parse(address), flag);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    ConnectError?.Invoke(this, ex.ToString());
                }
            }
        }

        public  void Write(string address, int num)
        {
            lock(this)
            {
                if (DebugMode)
                {
                    master.WriteSingleRegister(1, ushort.Parse(address), ushort.Parse(num.ToString()));
                    return;
                }
                try
                {
                    master.WriteSingleRegister(1, ushort.Parse(address), ushort.Parse(num.ToString()));
                    return;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    ConnectError?.Invoke(this, ex.ToString());
                }
            }
        }

        public  void Write(string address, int[] nums)
        {

            try
            {
                master.WriteMultipleRegisters(1, ushort.Parse(address), nums.Cast<ushort>().ToArray());
                return;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                ConnectError?.Invoke(this,ex.ToString());
            }

        }



        /// <summary>
        /// 控制器连接方法
        /// 
        /// </summary>
        public  bool ConnectServer()
        {
            Thread.Sleep(2);
            try
            {
                if (tcp == null || !ConnectConfig.ControlerConnected)
                {
                    tcp = new TcpClient();
                    tcp.Connect(ipAddress, port);
                    tcp.ReceiveTimeout = 3000;
                    tcp.SendTimeout = 3000;
                    modbusFactory = new ModbusFactory();
                    master = modbusFactory.CreateMaster(tcp);
                    ConnectConfig.ControlerConnected = true;
                    Console.WriteLine("控制器连接成功");
                    return true;
                }
                return true;
            }
            catch
            {
                MessageBox.Show("控制器连接失败");
                return false;
            }


        }
        /// <summary>
        /// 控制器断开连接方法
        /// </summary>
        public void DisConnect()
        {
            try
            {
                if (ConnectConfig.ControlerConnected)
                {
                    ConnectConfig.ControlerConnected = false;
                    master.Dispose();
                    tcp.Close();
                    Console.WriteLine("控制器断开连接");
                }
            }
            catch
            {
                Console.WriteLine("控制器断开连接异常");
            }
        }

        public  int[] ReadInt16(string address, int len)
        {
            lock (this)
            {
                Thread.Sleep(2);
                int[] result = new int[len];
                if (DebugMode)
                {
                    Array.Copy(master.ReadHoldingRegisters(1, ushort.Parse(address), ushort.Parse(len.ToString())), result, len);
                }
                try
                {
                    Array.Copy(master.ReadHoldingRegisters(1, ushort.Parse(address), ushort.Parse(len.ToString())), result, len);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    ConnectError?.Invoke(this, ex.ToString());
                }
                return result;
            }
        }


        public  ushort ReadInt16(string address)
        {
            lock (this)
            {
                Thread.Sleep(2);

                ushort[] result;
                try
                {
                    result = master.ReadHoldingRegisters(1, ushort.Parse(address), 1);
                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    ConnectError?.Invoke(this, ex.ToString());

                    return 0;
                }
                return result[0];
            }
        }
        public  int ReadDoubleWord(string address)
        {
            lock (this)
            {
                Thread.Sleep(2);
                ushort[] result;
                try
                {
                    result = master.ReadHoldingRegisters(1, ushort.Parse(address), 2);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    ConnectError?.Invoke(this, ex.ToString());
                    return 0;
                }
                return result[1] * 65536 + result[0];
            }
        }
        public  void WriteDoubleWord(string address, int val)
        {
            lock (this)
            {
                Thread.Sleep(2);
                int low = val % 65536;   //底16
                int high = val / 65536;     //高16
                int _address = ushort.Parse(address);
                try
                {
                    master.WriteMultipleRegisters(1, Convert.ToUInt16(_address), new ushort[]
                    {
                        ushort.Parse(low.ToString()),
                        ushort.Parse(high.ToString())
                    });
                    //master.WriteSingleRegister(1, Convert.ToUInt16(_address), ushort.Parse(low.ToString()));
                    //master.WriteSingleRegister(1, Convert.ToUInt16(_address + 1), ushort.Parse(high.ToString()));
                    return;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    ConnectError?.Invoke(this, ex.ToString());
                }
            }
        }
        public  ushort[] ReadUInt16(string address, ushort num)
        {
            lock (this)
            {
                Thread.Sleep(2);
                ushort[] result = new ushort[num];
               
                Array.Copy(master.ReadHoldingRegisters(1, ushort.Parse(address), num), result, num);
                
                //catch (Exception ex)
                //{
                //    Console.WriteLine(ex);
                //    ConnectError?.Invoke(this, ex.ToString());
                //}
                return result;
            }
        }



    }


}
