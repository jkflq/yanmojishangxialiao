﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp.deviceConnect;
using WindowsFormsApp.utils.modbus.NModbus;

namespace WindowsFormsApp.utils.modbus
{
    public class ModbusListener
    {
        //用于统计并发数量
        private int _coCurrentCount = 0;

        private ModbusTcpNet _busClient;    //控制器


        //获取并发数量
        public int getCoCurrentCount()
        {
            return _coCurrentCount;
        }
        public ModbusTcpNet busClient
        {
            get 
            {
                return _busClient;
            }
            set {
                _busClient = value;
                init();
            }
        }

        //定义定时器，通过Modbus循环读控制器里的数据
        public UInt16 len = 520;
        public UInt16 regLen_Front = 120;
        public UInt16 renLen_Back = 50;


        private System.Timers.Timer timer;


        public bool[] coils;
        public ushort[] regs_Front;
        public ushort[] regs_Back;

        public bool ReadyStatus { get; set; }

        public ModbusSubscriber Subscriber;

        public ModbusListener()
        {
            this.Subscriber = new ModbusSubscriber()
            {
                listener = this
            };

        }

        public ModbusListener(IPAddress iP) : this()
        {
            this.busClient = new ModbusTcpNet(iP.ToString());

        }
        public ModbusListener(string address, int port) : this()
        {
            this.busClient = new ModbusTcpNet(address, port);
        }

        public ModbusListener(string address) : this()
        {
            this.busClient = new ModbusTcpNet(address);
        }

        public IEnumerator<Object> GetEnumerator(List<Object> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                yield return list[i];
            }
        }

        public event EventHandler<string> ListenerError;
        public event EventHandler<string> ListenerOpen;
        public event EventHandler<string> ListenerClose;

        private void init()
        {
            this.timer = new System.Timers.Timer(500);
            this.timer.Elapsed += Timer_Elapsed;
            this.coils = new bool[len];
            this.regs_Front = new ushort[regLen_Front];
            this.regs_Back = new ushort[renLen_Back];
            busClient.ConnectError += (o, e) => ListenerError?.Invoke(o, e);
         

        }
        private async void Timer_Elapsed(object sendet,System.Timers.ElapsedEventArgs e)
        {
              await fun();
        }
        public int i = 0;
        private Task fun()
        {
            return Task.Run(() =>
            {
                try
                { 
                    Array.Copy(busClient.ReadCoils("16384", len), coils, len);
                    Thread.Sleep(100);
                    Array.Copy(busClient.ReadUInt16("16424", regLen_Front), regs_Front, regLen_Front);
                    Thread.Sleep(100);
                    Array.Copy(busClient.ReadUInt16("16544", renLen_Back), regs_Back, renLen_Back);

                    i = 0;
                }
                catch
                {
                    if (i == 0)
                    {
                        i++;
                        timer.Stop();
                        Console.WriteLine("Modbus连接断开");
                        ConnectConfig.DisConnect();

                        if(ConnectConfig.Error_Connect_Flag)
                         MessageBox.Show("控制器断开连接，请重新连接");
                    
                    }
                }

            });
        }
        

        public bool ConnectServer()
        {
            bool flag = busClient.ConnectServer();
            
            if(flag)
            {
                ReadyStatus = true;
                ConnectConfig.ControlerConnected = true;
                ListenerOpen?.Invoke(this, "控制器连接成功！");
                
                //打开读控制器寄存器的定时器，并将其读到数组中
                timer?.Start();

                //打开读数据数组的定时器
               // ConnectConfig.StartUpdate();
            }
            else
            {
                ReadyStatus = false;
                ConnectConfig.ControlerConnected = false;
                //关闭读数组的定时器
              //  ConnectConfig.StopUpdate();
                //关闭读控制器寄存的定时器
                timer?.Stop();

            }
            return flag;

        }

        public void stop()
        {
            timer.Stop();
            //Subscriber.Offline();
            //ReadyStatus = false;
            //while(Busy)
            //{
            //    Thread.Sleep(20);
            //}
            busClient.DisConnect();
            ListenerClose?.Invoke(this, "控制器连接断开！");
        }
        public bool ReadCoil(string address)
        {
            //return busClient.ReadCoil(address);
            int _address = Convert.ToInt32(address) - 16384;
            return coils[_address];
        }

        public bool[] ReadCoils(string address, int len)
        {
            //bool[] res = new bool[len];
            //res = busClient.ReadCoils(address, ushort.Parse(len.ToString()));
            //return res;
            bool[] res = new bool[len];
            Array.Copy(coils, res, len);
            return res;
        }

        public int ReadUInt16(string address)
        {
            //int res;
            //res = busClient.ReadInt16(address);
            //return (res + 65536) % 65536;
            int _address = Convert.ToInt32(address) - 16424;
            int res = regs_Front[_address];
            return (res + 65536) % 65536;
        }
        public int ReadInt16(string address)
        {
            //int res;
            //res = busClient.ReadInt16(address);
            //return (res + 65536) % 65536;
            int _address = Convert.ToInt32(address)- 16424;
            if (_address >= regs_Front.Length)
            {
               return regs_Back[_address- regs_Front.Length ];
            }
            return regs_Front[_address];
        }

        public int ReadDoubleWord(string address)
        {
            int a, b;
            int _address = Convert.ToInt32(address) - 16424;
            if (_address >= regs_Front.Length)
            {
                a = regs_Back[_address - regs_Front.Length];
                b = regs_Back[_address + 1 - regs_Front.Length];
            }
            else
            {
                a = regs_Front[_address];
                if (_address+1 >= regs_Front.Length)
                    b = regs_Back[_address + 1 - regs_Front.Length];
                else
                    b = regs_Front[_address + 1];

            }
            if (b > 3)
                b = 0;
            return b * 65536 + (a + 65536) % 65536;
        }
        public int ReadIDoubleWord(string address)
        {
            int _address = Convert.ToInt32(address) - 16424;
            int a = regs_Back[_address];
            int b = regs_Back[_address + 1];
            return b * 65536 + (a + 65536) % 65536;
        }
        public void Write(string address, bool val)
        {
            busClient.Write(address, val);
        }

        public void Write(string address, int val)
        {
            busClient.Write(address, val);
        }

        public void Write(string address, int[] arr)
        {
            busClient.Write(address, arr);
        }
        public void WriteDoubleWord(string address, int val)
        {
            busClient.WriteDoubleWord(address, val);
        }


    }
}
