﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp.utils.modbus
{
    public abstract class  IModbusTcpNet
    {
        //端口
        public int port = 502;
        //IP
        public string ipAddress = "192.168.250.62";
        //是否为调试状态
        public bool DebugMode = false;

        public abstract bool ReadCoil(string address);

        public abstract bool[] ReadCoil(string address, ushort len);

        public abstract void Write(string address, bool flag);

        public abstract void Write(string address, bool[] flag);

        public abstract void Write(string address, int num);

        public abstract void ConnectServer();

        public abstract int[] ReadInt16(string address, int len);

        public abstract ushort ReadInt16(string address);

        public abstract int ReadDoubleWord(string address);
        public abstract void WriteDoubleWord(string address, int val);
        public abstract int[] ReadUInt16(string address, ushort num);
        public abstract void Dispose();
        public abstract void Write(string address, int[] nums);


        public virtual event EventHandler<string> ConnectError;
        public virtual event EventHandler<string> ConnectSuccess;


    }
}
