﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp.components;
using WindowsFormsApp.deviceConnect;

namespace WindowsFormsApp.utils.modbus
{
    public class ModbusSubscriber:IAutoUpdatable
    {
        public ModbusListener listener;
        public List<BEffect> bEffects;
        public List<VEffect> vEffects;
        public ModbusSubscriber()
        {
            bEffects = new List<BEffect>();
            vEffects = new List<VEffect>();
        }

        public void ReFreshState()
        {
            bEffects.ForEach(effect => effect.UpdateMethod?.Invoke(ConnectConfig.ControlUnit.ReadCoil(effect.address)));
            vEffects.ForEach(effect => {
                if (effect.isDword)
                {
                    effect.UpdateMethod?.Invoke(ConnectConfig.ControlUnit.ReadDoubleWord(effect.address));
                }
                else
                {
                    effect.UpdateMethod?.Invoke(ConnectConfig.ControlUnit.ReadInt16(effect.address));
                }
            }
            );
        }

        public void Subscribe(string address,Action<bool> updateMethod)
        {
            bEffects.Add(new BEffect()
            {
                address = address,
                UpdateMethod = updateMethod
            }) ;
           
        }
        public void Subscribe(string address, Action<int> updateMethod)
        {
            vEffects.Add(new VEffect()
            {
                address = address,
                UpdateMethod = updateMethod
            });
        }
        public void Subscribe(string address, Action<long> updateMethod)
        {
            vEffects.Add(new VEffect()
            {
                address = address,
                UpdateMethod = (v) => updateMethod(v),
                isDword = true
            });
        }

        public void Offline()
        {
            bEffects.ForEach(effect => effect.UpdateMethod?.Invoke(false));
            vEffects.ForEach(effect => effect.UpdateMethod?.Invoke(0));
        }


    }

    public class BEffect
    {
        public string address;
        public Action<bool> UpdateMethod;
    }
    public class VEffect
    {
        public string address;
        public Action<int> UpdateMethod;
        public bool isDword = false;
    }

}
