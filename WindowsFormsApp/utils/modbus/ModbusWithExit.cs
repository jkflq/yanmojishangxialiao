﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp.utils.modbus
{
    public class ModbusWithExit<T>:IModbusTcpNet
    {
        private TaskCompletionSource<T> promise;
        private IModbusTcpNet modbus;
        public ModbusWithExit(IModbusTcpNet modbus,TaskCompletionSource<T> source)
        {
            this.promise = promise;
            this.modbus = modbus;
        }
        public override void ConnectServer()
        {
            throw new NotImplementedException();
        }
        public override void Dispose()
        {
            throw new NotImplementedException();
        }
        public override bool ReadCoil(string address)
        {
            if (promise.Task.IsCanceled) throw new TaskCanceledException();
            return modbus.ReadCoil(address);
        }
        public override bool[] ReadCoil(string address, ushort len)
        {
            if (promise.Task.IsCanceled) throw new TaskCanceledException();
            return modbus.ReadCoil(address, len);
        }

        public override int ReadDoubleWord(string address)
        {
            if (promise.Task.IsCanceled) throw new TaskCanceledException();
            return modbus.ReadDoubleWord(address);
        }

        public override int[] ReadInt16(string address, int len)
        {
            if (promise.Task.IsCanceled) throw new TaskCanceledException();
            return modbus.ReadInt16(address, len);
        }
        public override ushort ReadInt16(string address)
        {
            if (promise.Task.IsCanceled) throw new TaskCanceledException();
            return modbus.ReadInt16(address);
        }

        public override int[] ReadUInt16(string address, ushort num)
        {
            if (promise.Task.IsCanceled) throw new TaskCanceledException();
            return modbus.ReadUInt16(address, num);
        }


        public override void Write(string address, bool flag)
        {
            if (promise.Task.IsCanceled) throw new TaskCanceledException();
            modbus.Write(address, flag);
        }
        public override void Write(string address, bool[] flag)
        {
            if (promise.Task.IsCanceled) throw new TaskCanceledException();
            modbus.Write(address, flag);
        }
        public override void Write(string address, int num)
        {
            if (promise.Task.IsCanceled) throw new TaskCanceledException();
            modbus.Write(address, num);
        }

        public override void Write(string address, int[] nums)
        {
            if (promise.Task.IsCanceled) throw new TaskCanceledException();
            modbus.Write(address, nums);
        }

        public override void WriteDoubleWord(string address, int val)
        {
            if (promise.Task.IsCanceled) throw new TaskCanceledException();
            modbus.WriteDoubleWord(address, val);
        }


    }
}
