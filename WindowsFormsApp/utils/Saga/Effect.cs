﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp.utils.Saga
{
     public class Effect<T>
    {
        public int id;
        public EffectType effectType;  //效果类型
        public object param;   //结束任务
        public Func<T, bool> cb;   //处理数据
        public Msg msg;
    }

    public enum EffectType
    {
        TAKE,//提取有效信息
        COMM,//通信效果
        FORK,//异步效果
        CALL,//调用
        OVERLOAD//回调效果
    }
}
