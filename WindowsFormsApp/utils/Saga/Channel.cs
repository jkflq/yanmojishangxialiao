﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WindowsFormsApp.deviceConnect;
using WinFormsBase;

namespace WindowsFormsApp.utils.Saga
{
    public class Channel<T>
    {
        public Saga<T> saga;
        //第一次检测到Mark点标志位
        public bool flag = false;

        public void DealCarmeraData(T data)
        {
            for(int i = saga.listQue.Count-1;i>=0;i--)
            {
                //Console.WriteLine("我在处理数据");
                var effect = saga.listQue[i];
                if (saga.IsNeed(effect, data) && ! ParamConfig.Pause)
                {
                     saga.listQue.Remove(effect);
                     //将相机识别标志置位false 表示不会再处理数据
                     ParamConfig.camera_run = false;
                     effect.cb(data); //线程返回,返回结果为data
                     break;
                }
            }
        }


        public void SagaComm(Saga<T> fromsaga,Saga<T> tosaga, Effect<T> effect, TaskCompletionSource<T> promise)
        {
            //如果消息类型为发布类型（作为主机发布给从机的消息）
            if(effect.msg.msgtype == MsgType.PUBLIC)
            {
                Saga<T>.MsgQue[tosaga].Add(effect.msg);
                effect.cb(default);  //结束线程
            }
            //如果消息类型为订阅类型（作为从机接收主机的消息）
            else if(effect.msg.msgtype == MsgType.SUBSCRIBE)
            {
                //获取别人给自己发布的消息
                bool flag = true;
                while(flag && !promise.Task.IsCanceled)  //循环检查wait消息，不检查到不罢休
                {
                    foreach (var i in Saga<T>.MsgQue[fromsaga] )
                    {
                        if (i.msgcontent == MsgContent.WAIT)
                        {
                            Console.WriteLine("查找到有等待消息,等待继续消息");
                            foreach (var j in Saga<T>.MsgQue[fromsaga])
                            {

                                if (j.msgcontent == MsgContent.CONTINUE)
                                {
                                    Console.WriteLine("查找到有继续消息,继续执行");
                                    Console.WriteLine("继续工作，哈哈哈");
                                    Saga<T>.MsgQue[fromsaga].Remove(j);//移除消息
                                    Saga<T>.MsgQue[fromsaga].Remove(i); // 移除等待消息
                                    flag = false;  //跳出循环
                                    effect.cb(default(T));
                                }
                            }


                        }
                    }
                    Thread.Sleep(100);
                }

                
            }
        }





    }
}
