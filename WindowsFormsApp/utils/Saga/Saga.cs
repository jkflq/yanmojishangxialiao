﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WindowsFormsApp.components.robot;
using WindowsFormsApp.deviceConnect;
using WinFormsBase;

namespace WindowsFormsApp.utils.Saga
{
     public class Saga<T>
    {
        
        public static Dictionary<Saga<T>, List<Msg>> MsgQue = new Dictionary<Saga<T>, List<Msg>>();
        
        
        //存放数据效果的队列
        public List<Effect<T>> listQue;
        //消息队列，用来存放与此saga通信的消息
        public List<Msg> msgsQue;
        //存放任务
        public List<TaskCompletionSource<T>> listTask;
        //处理数据的通道
        public Channel<T> channel;
        //用于过滤信息的函数
        public Func<Effect<T>, T, bool> IsNeed;
        //存放take效果执行后的结果
        public Dictionary<Effect<T>, T> res;
        //存放处理效果的策略
        public Dictionary<EffectType, Action<Effect<T>, TaskCompletionSource<T>>> strategy;
        //保存机械臂检测线程
        public Dictionary<Task, CancellationTokenSource> checkRobotTask;

        //存放报警信息
        public List<string> warnMsg;


   



        public Saga()
        {
            res = new Dictionary<Effect<T>, T>();
            listQue = new List<Effect<T>>();
            msgsQue = new List<Msg>();
            listTask = new List<TaskCompletionSource<T>>();
            checkRobotTask = new Dictionary<Task, CancellationTokenSource>();
            channel = new Channel<T>();
            channel.saga = this;

            warnMsg = new List<string>();
            

            //将本saga的消息队列添加近公用的消息字典中
            Saga<T>.MsgQue.Add(this, msgsQue);

            //定义effect处理方法
            strategy = new Dictionary<EffectType, Action<Effect<T>, TaskCompletionSource<T>>>();
            strategy.Add(EffectType.TAKE, runTaskEffect);
            strategy.Add(EffectType.CALL, runCallEffect);
            strategy.Add(EffectType.FORK, runForkEffect);
            strategy.Add(EffectType.COMM, runCommEffect);
            strategy.Add(EffectType.OVERLOAD, runOverloadEffect);

        }

        public Saga(Func<Effect<T>,T,bool> matchFunction):this()
        {
            this.IsNeed += matchFunction;
        }


        public T GetRes(Effect<T> token)
        {
            var _res = this.res[token];
            this.res.Remove(token);
            return _res;
        }   
        /// <summary>
        /// 创建Task效果，用于处理有效数据，目前只有相机数据
        /// </summary>
        /// <param name="pattern"></param>
        /// <returns></returns>
        public Effect<T> Take(string pattern)
        {
            return new Effect<T>()
            {
                effectType = EffectType.TAKE,
                param = pattern
            };
        }

        /// <summary>
        /// 返回调用效果，用于调用普通函数
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public Effect<T> Call(Action<Func<T,bool>,TaskCompletionSource<T>> action)
        {
            return new Effect<T>()
            {
                effectType = EffectType.CALL,
                param = action
            };
        }

        /// <summary>
        /// 创建Call效果，用于调用函数，函数里可以返回效果
        /// </summary>
        /// <param name="generator"></param>
        /// <returns></returns>
        public Effect<T> Call(Func<IEnumerable<Effect<T>>> generator)
        {
            return new Effect<T>()
            {
                effectType = EffectType.OVERLOAD,
                param = generator
             };
        }

        /// <summary>
        /// 创建通信效果，用于与其他saga进行通讯
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public Effect<T> Comm(Saga<T> saga,Msg msg)
        {
            return new Effect<T>()
            {
                effectType = EffectType.COMM,
                msg = msg,
                param = saga
            };
        }
        
        /// <summary>
        /// 创建线程套线程效果
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public Effect<T> Fork(Action<Func<T, bool>, TaskCompletionSource<T>> action)
        {
            return new Effect<T>()
            {
                effectType = EffectType.FORK,
                param = action
            };
        }

        public void Cancel(TaskCompletionSource<T> promise)
        {
            //取消任务线程
            for(int i = listTask.Count-1;i>=0;i--)
            {
                listTask[i]?.TrySetCanceled();
            }

            //清空消息队列
            //取消机械臂监测线程
            foreach(var i in checkRobotTask)
            {
                i.Value.Cancel();
            }

            Task.Run(() =>
            {

                //Thread.Sleep(100);  //原来是3000 不知道为什么
                listTask.Clear();
                checkRobotTask.Clear();
                Console.WriteLine("任务线程剩余{0}个", listTask.Count);
                Console.WriteLine("检测线程剩余{0}个", checkRobotTask.Count);
            });

            //取消主线程
            promise?.TrySetCanceled();
            //Console.WriteLine("Task length :" + listTask.Count);
        }


        /// <summary>
        /// 自动执行saga异步流程
        /// async和await关键字同时存在。
        //控制流程：
        //调用异步方法，调用后立即返回一个Task类型的对象。
        //调用异步方法后执行到await表达式后返回
        //继续执行调用者后续代码。
        //当需要使用异步方法执行结果时，若异步方法仍未返回。
        //将生成一个continue委托，当操作完成的时候调用continue委托。
        //这个continue委托将控制权重新返回到”async”方法对应的await唤醒点处。
        /// </summary>
        /// <param name="ienum"></param>
        /// <param name="_promise"></param>
        /// <returns></returns>
        public async Task AutoRun(Func<IEnumerable<Effect<T>>> ienum,TaskCompletionSource<T> _promise = null)
        {
            //首先执行ienum函数并获取效果集合
            var enumrator = ienum().GetEnumerator();
            //效果遍历及其处理
            while (enumrator.MoveNext()&&!_promise.Task.IsCompleted&&!ParamConfig.WarnFlag)
            {
                //暂停功能实现
                var effect = (Effect<T>)enumrator.Current;
                //await Task.Run(() =>
                //{
                //    //暂停按钮可以取消
                //    ParamConfig.Pause = RobotConnect.GetDin(ConnectConfig.FirstRobot, PortConfig.INPIT_PAUSE_KEY);
                //    while (ParamConfig.Pause)  //暂停
                //    {
                //        //暂停指示灯亮
                //        RobotConnect.SetDout(ConnectConfig.FirstRobot, PortConfig.OUTPUT_PAUSE_LIGHT, true);
                //        ParamConfig.Pause = RobotConnect.GetDin(ConnectConfig.FirstRobot, PortConfig.INPIT_PAUSE_KEY);
                //    }
                //    //暂停指示灯灭
                //    RobotConnect.SetDout(ConnectConfig.FirstRobot, PortConfig.OUTPUT_PAUSE_LIGHT, false);
                //});
                
                //如果此时切换成手动模式，略过此次效果
                if (!ParamConfig.runflag)
                {
                    break;
                }

                //为单个任务建立异步线程
                var promise = new TaskCompletionSource<T>();
                //将此任务加入任务列表
                listTask.Add(promise);
                //使用该线程执行该效果对应的策略
                strategy[effect.effectType](effect, promise);

                var res = default(T);
                try
                {
                     //等待该任务执行完成
                    res = await promise.Task;
                    //Console.WriteLine("一个任务完成");
                    if (effect.effectType == EffectType.TAKE)
                    {
                        this.res.Add(effect, res);
                    }   
                }
                catch(Exception ex)
                {
                    //任务执行失败
                    //记录日志
                }
                listTask.Remove(promise);
                //任何一个任务被取消，则终止整个主线程
                if(promise.Task.IsCanceled)
                {
          
                    _promise.TrySetCanceled();//将线程取消
                    break;

                }

            }

            _promise?.TrySetResult(default(T));
        }



        /// <summary>
        /// 提取相机信息效果
        /// </summary>
        /// <param name="effect"></param>
        /// <param name="promise"></param>
        private void runTaskEffect(Effect<T> effect, TaskCompletionSource<T> promise)
        {
            //相机运行标志位开启
            ParamConfig.camera_run = true;
            effect.cb = promise.TrySetResult;
            listQue.Add(effect);
        }
        /// <summary>
        /// 效果
        /// </summary>
        /// <param name="effect"></param>
        /// <param name="promise"></param>
        private void runCallEffect(Effect<T> effect,TaskCompletionSource<T> promise)
        {
            var action = (Action<Func<T, bool>, TaskCompletionSource<T>>)effect.param;
            Task.Run(()=>
            {
                try
                {
                    action(promise.TrySetResult, promise);
                }
                catch
                {
                    promise.TrySetCanceled();
                }
            });

        }
        private async void runForkEffect(Effect<T> effect,TaskCompletionSource<T> promise)
        {
            promise.TrySetResult(default(T));
            var action = (Action<Func<T, bool>, TaskCompletionSource<T>>)effect.param;
            //创建另一个线程
            TaskCompletionSource<T> forkPromise = new TaskCompletionSource<T>();
            listTask.Add(forkPromise);
            Task.Run(() =>
            {
                try
                {
                    action(forkPromise.TrySetResult, forkPromise);
                }
                catch
                {
                    forkPromise.TrySetCanceled();
                }
            });
            //await forkPromise.Task;
        }

        private void runCommEffect(Effect<T> effect,TaskCompletionSource<T> promise)
        {
            //获取传入的Saga
            var saga = (Saga<T>)effect.param;
            //设置线程处理函数
            effect.cb = promise.TrySetResult;
            //Console.WriteLine("发布了一个消息");
            //处理效果
            Task.Run(() =>
            {
                try
                {
                    this.channel.SagaComm(this, saga, effect,promise);
                }
                catch
                {
                    promise.TrySetCanceled();
                }
            });
            
        }
        /// <summary>
        /// 处理调用效果
        /// </summary>
        /// <param name="effect"></param>
        /// <param name="promise"></param>
        private void runOverloadEffect(Effect<T> effect,TaskCompletionSource<T> promise)
        {
            var action = (Func<IEnumerable<Effect<T>>>)effect.param;
            Task.Run(() =>
            {
                AutoRun(action, promise);
            });
        }


    }
}
