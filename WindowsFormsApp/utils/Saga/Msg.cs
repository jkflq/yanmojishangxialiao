﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp.utils.Saga
{

    public enum MsgType
    {
        PUBLIC,
        SUBSCRIBE
    }
    public enum MsgContent
    {
        WAIT,
        CONTINUE
    }
    public class Msg
    {
        public MsgType msgtype;
        public MsgContent msgcontent;

        public Msg(MsgType type,MsgContent content)
        {
            msgtype = type;
            msgcontent = content;
        }
    }
}
