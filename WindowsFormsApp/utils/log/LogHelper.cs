﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp.utils.log
{
    public class LogHelper
    {
        public static readonly log4net.ILog loginfo = log4net.LogManager.GetLogger("loginfo");
        public static readonly log4net.ILog logerror = log4net.LogManager.GetLogger("logerror");

        public static void LogErr(string info)
        {
            logerror.Error(info);
        }

        public static void LogTrace(string info) 
        {
            loginfo.Debug("trace: " + info);

        }
        public static void WriteLog(string info)
        {
            if (loginfo.IsInfoEnabled)
            {
                loginfo.Info(info);
            }
        }

        public static void SetConfig()
        {
            var path = new FileInfo("log4net.config");
            //Console.Write(path);
            log4net.Config.XmlConfigurator.Configure(path);
            logerror.Info("Enter application.？？？？？？？？？？？");
            loginfo.Info("Enter application.？？？？？？？？？？？");
        }




        public static void WriteLog(string info, Exception ex)
        {
            if (logerror.IsErrorEnabled)
            {
                logerror.Error(info, ex);
            }
        }
    }

}
