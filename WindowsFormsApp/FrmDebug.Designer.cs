﻿using WebKit;


namespace WindowsFormsApp
{


    partial class FrmDebug
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDebug));
            this.skinTabControl3 = new CCWin.SkinControl.SkinTabControl();
            this.skinTabPage8 = new CCWin.SkinControl.SkinTabPage();
            this.grip_down = new WindowsFormsApp.components.grip.GripSwitch();
            this.grip_up = new WindowsFormsApp.components.grip.GripSwitch();
            this.SecondRobotVacuumSwitch = new WindowsFormsApp.components.vacuum.VacuumSwitch();
            this.ThirdRobotVacuumSwitch = new WindowsFormsApp.components.vacuum.VacuumSwitch();
            this.FirstRobotVacuumSwitch = new WindowsFormsApp.components.vacuum.VacuumSwitch();
            this.robotOption1 = new WindowsFormsApp.components.robot.RobotOption();
            this.skinTabPage9 = new CCWin.SkinControl.SkinTabPage();
            this.skinGroupBox4 = new CCWin.SkinControl.SkinGroupBox();
            this.qgSwitch11 = new WindowsFormsApp.components.QGUnit.QGSwitch();
            this.skinGroupBox2 = new CCWin.SkinControl.SkinGroupBox();
            this.qgSwitch8 = new WindowsFormsApp.components.QGUnit.QGSwitch();
            this.skinGroupBox3 = new CCWin.SkinControl.SkinGroupBox();
            this.qgSwitch9 = new WindowsFormsApp.components.QGUnit.QGSwitch();
            this.skinGroupBox1 = new CCWin.SkinControl.SkinGroupBox();
            this.qgSwitch10 = new WindowsFormsApp.components.QGUnit.QGSwitch();
            this.skinGroupBox5 = new CCWin.SkinControl.SkinGroupBox();
            this.qgSwitch7 = new WindowsFormsApp.components.QGUnit.QGSwitch();
            this.qgSwitch6 = new WindowsFormsApp.components.QGUnit.QGSwitch();
            this.qgSwitch5 = new WindowsFormsApp.components.QGUnit.QGSwitch();
            this.qgSwitch4 = new WindowsFormsApp.components.QGUnit.QGSwitch();
            this.qgSwitch3 = new WindowsFormsApp.components.QGUnit.QGSwitch();
            this.qgSwitch2 = new WindowsFormsApp.components.QGUnit.QGSwitch();
            this.qgSwitch1 = new WindowsFormsApp.components.QGUnit.QGSwitch();
            this.skinTabPage11 = new CCWin.SkinControl.SkinTabPage();
            this.Motor_Control = new WindowsFormsApp.components.Motor.MotorControl();
            this.skinTabPage1 = new CCWin.SkinControl.SkinTabPage();
            this.Up_EmotorControl = new WindowsFormsApp.components.EMotor.EmotorControl();
            this.Up_PositionTG5 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.Up_PositionTG8 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.Up_Position5 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.Up_Position8 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.Up_PositionTG4 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.Up_PositionTG3 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.Up_Position4 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.Up_Position3 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.Up_PositionTG7 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.Up_PositionTG6 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.Up_Position7 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.Up_Position6 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.Up_PositionTG2 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.Up_PositionTG1 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.Up_Position2 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.Up_Position1 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.skinTabPage2 = new CCWin.SkinControl.SkinTabPage();
            this.Down_EmotorControl = new WindowsFormsApp.components.EMotor.EmotorControl();
            this.Down_PositionTG4 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.Down_PositionTG3 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.Down_Position4 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.Down_Position3 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.Down_PositionTG2 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.Down_PositionTG1 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.Down_Position2 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.Down_Position1 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.skinTabPage3 = new CCWin.SkinControl.SkinTabPage();
            this.Take_Position4 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.Take_EmotorControl = new WindowsFormsApp.components.EMotor.EmotorControl();
            this.Take_Position2 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.Take_Position3 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.Take_Position1 = new WindowsFormsApp.components.EMotor.PositionBlock();
            this.skinTabControl3.SuspendLayout();
            this.skinTabPage8.SuspendLayout();
            this.skinTabPage9.SuspendLayout();
            this.skinGroupBox4.SuspendLayout();
            this.skinGroupBox2.SuspendLayout();
            this.skinGroupBox3.SuspendLayout();
            this.skinGroupBox1.SuspendLayout();
            this.skinGroupBox5.SuspendLayout();
            this.skinTabPage11.SuspendLayout();
            this.skinTabPage1.SuspendLayout();
            this.skinTabPage2.SuspendLayout();
            this.skinTabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // skinTabControl3
            // 
            this.skinTabControl3.AnimatorType = CCWin.SkinControl.AnimationType.HorizSlide;
            this.skinTabControl3.BackColor = System.Drawing.Color.White;
            this.skinTabControl3.CloseRect = new System.Drawing.Rectangle(2, 2, 12, 12);
            this.skinTabControl3.Controls.Add(this.skinTabPage8);
            this.skinTabControl3.Controls.Add(this.skinTabPage9);
            this.skinTabControl3.Controls.Add(this.skinTabPage11);
            this.skinTabControl3.Controls.Add(this.skinTabPage1);
            this.skinTabControl3.Controls.Add(this.skinTabPage2);
            this.skinTabControl3.Controls.Add(this.skinTabPage3);
            this.skinTabControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabControl3.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinTabControl3.HeadBack = null;
            this.skinTabControl3.ImgTxtOffset = new System.Drawing.Point(0, 0);
            this.skinTabControl3.ItemSize = new System.Drawing.Size(180, 50);
            this.skinTabControl3.Location = new System.Drawing.Point(8, 39);
            this.skinTabControl3.Name = "skinTabControl3";
            this.skinTabControl3.PageArrowDown = ((System.Drawing.Image)(resources.GetObject("skinTabControl3.PageArrowDown")));
            this.skinTabControl3.PageArrowHover = ((System.Drawing.Image)(resources.GetObject("skinTabControl3.PageArrowHover")));
            this.skinTabControl3.PageBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.skinTabControl3.PageBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.skinTabControl3.PageCloseHover = ((System.Drawing.Image)(resources.GetObject("skinTabControl3.PageCloseHover")));
            this.skinTabControl3.PageCloseNormal = ((System.Drawing.Image)(resources.GetObject("skinTabControl3.PageCloseNormal")));
            this.skinTabControl3.PageDown = ((System.Drawing.Image)(resources.GetObject("skinTabControl3.PageDown")));
            this.skinTabControl3.PageDownTxtColor = System.Drawing.Color.Lime;
            this.skinTabControl3.PageHover = ((System.Drawing.Image)(resources.GetObject("skinTabControl3.PageHover")));
            this.skinTabControl3.PageImagePosition = CCWin.SkinControl.SkinTabControl.ePageImagePosition.Left;
            this.skinTabControl3.PageNorml = null;
            this.skinTabControl3.SelectedIndex = 0;
            this.skinTabControl3.Size = new System.Drawing.Size(1502, 715);
            this.skinTabControl3.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.skinTabControl3.TabIndex = 1;
            this.skinTabControl3.SelectedIndexChanged += new System.EventHandler(this.skinTabControl3_SelectedIndexChanged);
            // 
            // skinTabPage8
            // 
            this.skinTabPage8.BackColor = System.Drawing.Color.Transparent;
            this.skinTabPage8.BorderColor = System.Drawing.Color.Black;
            this.skinTabPage8.Controls.Add(this.grip_down);
            this.skinTabPage8.Controls.Add(this.grip_up);
            this.skinTabPage8.Controls.Add(this.SecondRobotVacuumSwitch);
            this.skinTabPage8.Controls.Add(this.ThirdRobotVacuumSwitch);
            this.skinTabPage8.Controls.Add(this.FirstRobotVacuumSwitch);
            this.skinTabPage8.Controls.Add(this.robotOption1);
            this.skinTabPage8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage8.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinTabPage8.Location = new System.Drawing.Point(0, 50);
            this.skinTabPage8.Name = "skinTabPage8";
            this.skinTabPage8.Size = new System.Drawing.Size(1502, 665);
            this.skinTabPage8.TabIndex = 0;
            this.skinTabPage8.TabItemImage = null;
            this.skinTabPage8.Text = "机械臂相关传感器";
            // 
            // grip_down
            // 
            this.grip_down.BackColor = System.Drawing.Color.Transparent;
            this.grip_down.Font = new System.Drawing.Font("宋体", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.grip_down.HeaderContent = "下料夹爪";
            this.grip_down.Location = new System.Drawing.Point(1163, 501);
            this.grip_down.Margin = new System.Windows.Forms.Padding(4);
            this.grip_down.Name = "grip_down";
            this.grip_down.Size = new System.Drawing.Size(235, 127);
            this.grip_down.TabIndex = 17;
            // 
            // grip_up
            // 
            this.grip_up.BackColor = System.Drawing.Color.Transparent;
            this.grip_up.Font = new System.Drawing.Font("宋体", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.grip_up.HeaderContent = "上料夹爪";
            this.grip_up.Location = new System.Drawing.Point(855, 501);
            this.grip_up.Margin = new System.Windows.Forms.Padding(4);
            this.grip_up.Name = "grip_up";
            this.grip_up.Size = new System.Drawing.Size(239, 127);
            this.grip_up.TabIndex = 16;
            // 
            // SecondRobotVacuumSwitch
            // 
            this.SecondRobotVacuumSwitch.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.SecondRobotVacuumSwitch.HeaderContent = "上料真空发生器";
            this.SecondRobotVacuumSwitch.Location = new System.Drawing.Point(855, 37);
            this.SecondRobotVacuumSwitch.Margin = new System.Windows.Forms.Padding(4);
            this.SecondRobotVacuumSwitch.Name = "SecondRobotVacuumSwitch";
            this.SecondRobotVacuumSwitch.Size = new System.Drawing.Size(266, 218);
            this.SecondRobotVacuumSwitch.TabIndex = 14;
            // 
            // ThirdRobotVacuumSwitch
            // 
            this.ThirdRobotVacuumSwitch.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ThirdRobotVacuumSwitch.HeaderContent = "下料真空发生器";
            this.ThirdRobotVacuumSwitch.Location = new System.Drawing.Point(855, 263);
            this.ThirdRobotVacuumSwitch.Margin = new System.Windows.Forms.Padding(4);
            this.ThirdRobotVacuumSwitch.Name = "ThirdRobotVacuumSwitch";
            this.ThirdRobotVacuumSwitch.Size = new System.Drawing.Size(274, 212);
            this.ThirdRobotVacuumSwitch.TabIndex = 13;
            // 
            // FirstRobotVacuumSwitch
            // 
            this.FirstRobotVacuumSwitch.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FirstRobotVacuumSwitch.HeaderContent = "上下料真空发生器";
            this.FirstRobotVacuumSwitch.Location = new System.Drawing.Point(1163, 37);
            this.FirstRobotVacuumSwitch.Margin = new System.Windows.Forms.Padding(4);
            this.FirstRobotVacuumSwitch.Name = "FirstRobotVacuumSwitch";
            this.FirstRobotVacuumSwitch.Size = new System.Drawing.Size(257, 216);
            this.FirstRobotVacuumSwitch.TabIndex = 12;
            // 
            // robotOption1
            // 
            this.robotOption1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.robotOption1.AutoSize = true;
            this.robotOption1.BackColor = System.Drawing.Color.White;
            this.robotOption1.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.robotOption1.Location = new System.Drawing.Point(12, 43);
            this.robotOption1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.robotOption1.Name = "robotOption1";
            this.robotOption1.Size = new System.Drawing.Size(926, 698);
            this.robotOption1.TabIndex = 18;
            // 
            // skinTabPage9
            // 
            this.skinTabPage9.BackColor = System.Drawing.Color.Transparent;
            this.skinTabPage9.BorderColor = System.Drawing.Color.Black;
            this.skinTabPage9.Controls.Add(this.skinGroupBox4);
            this.skinTabPage9.Controls.Add(this.skinGroupBox2);
            this.skinTabPage9.Controls.Add(this.skinGroupBox3);
            this.skinTabPage9.Controls.Add(this.skinGroupBox1);
            this.skinTabPage9.Controls.Add(this.skinGroupBox5);
            this.skinTabPage9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage9.Location = new System.Drawing.Point(0, 50);
            this.skinTabPage9.Name = "skinTabPage9";
            this.skinTabPage9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.skinTabPage9.Size = new System.Drawing.Size(1502, 665);
            this.skinTabPage9.TabIndex = 1;
            this.skinTabPage9.TabItemImage = null;
            this.skinTabPage9.Text = "气缸";
            // 
            // skinGroupBox4
            // 
            this.skinGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.skinGroupBox4.BorderColor = System.Drawing.Color.Gray;
            this.skinGroupBox4.Controls.Add(this.qgSwitch11);
            this.skinGroupBox4.Font = new System.Drawing.Font("微软雅黑", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinGroupBox4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.skinGroupBox4.Location = new System.Drawing.Point(852, 285);
            this.skinGroupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.skinGroupBox4.Name = "skinGroupBox4";
            this.skinGroupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.skinGroupBox4.Radius = 8;
            this.skinGroupBox4.RectBackColor = System.Drawing.Color.Transparent;
            this.skinGroupBox4.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinGroupBox4.Size = new System.Drawing.Size(210, 210);
            this.skinGroupBox4.TabIndex = 103;
            this.skinGroupBox4.TabStop = false;
            this.skinGroupBox4.Text = "顶盒气缸";
            this.skinGroupBox4.TitleBorderColor = System.Drawing.Color.Transparent;
            this.skinGroupBox4.TitleRadius = 20;
            this.skinGroupBox4.TitleRectBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(198)))), ((int)(((byte)(203)))));
            this.skinGroupBox4.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // qgSwitch11
            // 
            this.qgSwitch11.ButtonText = new string[] {
        "缩",
        "伸"};
            this.qgSwitch11.CX_Left = "CX伸";
            this.qgSwitch11.CX_Right = "CX缩";
            this.qgSwitch11.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qgSwitch11.HeaderContent = "QG11";
            this.qgSwitch11.LeftTriggered = false;
            this.qgSwitch11.Location = new System.Drawing.Point(23, 52);
            this.qgSwitch11.Name = "qgSwitch11";
            this.qgSwitch11.RightTriggered = false;
            this.qgSwitch11.Size = new System.Drawing.Size(165, 139);
            this.qgSwitch11.TabIndex = 6;
            // 
            // skinGroupBox2
            // 
            this.skinGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.skinGroupBox2.BorderColor = System.Drawing.Color.Gray;
            this.skinGroupBox2.Controls.Add(this.qgSwitch8);
            this.skinGroupBox2.Font = new System.Drawing.Font("微软雅黑", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinGroupBox2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.skinGroupBox2.Location = new System.Drawing.Point(582, 37);
            this.skinGroupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.skinGroupBox2.Name = "skinGroupBox2";
            this.skinGroupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.skinGroupBox2.Radius = 8;
            this.skinGroupBox2.RectBackColor = System.Drawing.Color.Transparent;
            this.skinGroupBox2.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinGroupBox2.Size = new System.Drawing.Size(210, 210);
            this.skinGroupBox2.TabIndex = 103;
            this.skinGroupBox2.TabStop = false;
            this.skinGroupBox2.Text = "180°手爪气缸";
            this.skinGroupBox2.TitleBorderColor = System.Drawing.Color.Transparent;
            this.skinGroupBox2.TitleRadius = 20;
            this.skinGroupBox2.TitleRectBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(198)))), ((int)(((byte)(203)))));
            this.skinGroupBox2.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // qgSwitch8
            // 
            this.qgSwitch8.ButtonText = new string[] {
        "缩",
        "伸"};
            this.qgSwitch8.CX_Left = "CX左";
            this.qgSwitch8.CX_Right = "CX右";
            this.qgSwitch8.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qgSwitch8.HeaderContent = "QG8";
            this.qgSwitch8.LeftTriggered = false;
            this.qgSwitch8.Location = new System.Drawing.Point(23, 52);
            this.qgSwitch8.Name = "qgSwitch8";
            this.qgSwitch8.RightTriggered = false;
            this.qgSwitch8.Size = new System.Drawing.Size(165, 139);
            this.qgSwitch8.TabIndex = 6;
            // 
            // skinGroupBox3
            // 
            this.skinGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.skinGroupBox3.BorderColor = System.Drawing.Color.Gray;
            this.skinGroupBox3.Controls.Add(this.qgSwitch9);
            this.skinGroupBox3.Font = new System.Drawing.Font("微软雅黑", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinGroupBox3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.skinGroupBox3.Location = new System.Drawing.Point(582, 285);
            this.skinGroupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.skinGroupBox3.Name = "skinGroupBox3";
            this.skinGroupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.skinGroupBox3.Radius = 8;
            this.skinGroupBox3.RectBackColor = System.Drawing.Color.Transparent;
            this.skinGroupBox3.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinGroupBox3.Size = new System.Drawing.Size(210, 210);
            this.skinGroupBox3.TabIndex = 102;
            this.skinGroupBox3.TabStop = false;
            this.skinGroupBox3.Text = "直线气缸";
            this.skinGroupBox3.TitleBorderColor = System.Drawing.Color.Transparent;
            this.skinGroupBox3.TitleRadius = 20;
            this.skinGroupBox3.TitleRectBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(198)))), ((int)(((byte)(203)))));
            this.skinGroupBox3.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // qgSwitch9
            // 
            this.qgSwitch9.ButtonText = new string[] {
        "缩",
        "伸"};
            this.qgSwitch9.CX_Left = "CX伸";
            this.qgSwitch9.CX_Right = "CX缩";
            this.qgSwitch9.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qgSwitch9.HeaderContent = "QG9";
            this.qgSwitch9.LeftTriggered = false;
            this.qgSwitch9.Location = new System.Drawing.Point(23, 52);
            this.qgSwitch9.Name = "qgSwitch9";
            this.qgSwitch9.RightTriggered = false;
            this.qgSwitch9.Size = new System.Drawing.Size(165, 139);
            this.qgSwitch9.TabIndex = 6;
            // 
            // skinGroupBox1
            // 
            this.skinGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.skinGroupBox1.BorderColor = System.Drawing.Color.Gray;
            this.skinGroupBox1.Controls.Add(this.qgSwitch10);
            this.skinGroupBox1.Font = new System.Drawing.Font("微软雅黑", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinGroupBox1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.skinGroupBox1.Location = new System.Drawing.Point(852, 37);
            this.skinGroupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.skinGroupBox1.Name = "skinGroupBox1";
            this.skinGroupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.skinGroupBox1.Radius = 8;
            this.skinGroupBox1.RectBackColor = System.Drawing.Color.Transparent;
            this.skinGroupBox1.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinGroupBox1.Size = new System.Drawing.Size(210, 212);
            this.skinGroupBox1.TabIndex = 101;
            this.skinGroupBox1.TabStop = false;
            this.skinGroupBox1.Text = "推拉杆90度气缸";
            this.skinGroupBox1.TitleBorderColor = System.Drawing.Color.Transparent;
            this.skinGroupBox1.TitleRadius = 20;
            this.skinGroupBox1.TitleRectBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(198)))), ((int)(((byte)(203)))));
            this.skinGroupBox1.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // qgSwitch10
            // 
            this.qgSwitch10.ButtonText = new string[] {
        "垂直",
        "水平"};
            this.qgSwitch10.CX_Left = "CX水平";
            this.qgSwitch10.CX_Right = "CX垂直";
            this.qgSwitch10.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qgSwitch10.HeaderContent = "QG10";
            this.qgSwitch10.LeftTriggered = false;
            this.qgSwitch10.Location = new System.Drawing.Point(23, 52);
            this.qgSwitch10.Name = "qgSwitch10";
            this.qgSwitch10.RightTriggered = false;
            this.qgSwitch10.Size = new System.Drawing.Size(165, 139);
            this.qgSwitch10.TabIndex = 6;
            // 
            // skinGroupBox5
            // 
            this.skinGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.skinGroupBox5.BorderColor = System.Drawing.Color.Gray;
            this.skinGroupBox5.Controls.Add(this.qgSwitch7);
            this.skinGroupBox5.Controls.Add(this.qgSwitch6);
            this.skinGroupBox5.Controls.Add(this.qgSwitch5);
            this.skinGroupBox5.Controls.Add(this.qgSwitch4);
            this.skinGroupBox5.Controls.Add(this.qgSwitch3);
            this.skinGroupBox5.Controls.Add(this.qgSwitch2);
            this.skinGroupBox5.Controls.Add(this.qgSwitch1);
            this.skinGroupBox5.Font = new System.Drawing.Font("微软雅黑", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinGroupBox5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.skinGroupBox5.Location = new System.Drawing.Point(79, 14);
            this.skinGroupBox5.Margin = new System.Windows.Forms.Padding(4);
            this.skinGroupBox5.Name = "skinGroupBox5";
            this.skinGroupBox5.Padding = new System.Windows.Forms.Padding(4);
            this.skinGroupBox5.Radius = 8;
            this.skinGroupBox5.RectBackColor = System.Drawing.Color.Transparent;
            this.skinGroupBox5.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinGroupBox5.Size = new System.Drawing.Size(417, 639);
            this.skinGroupBox5.TabIndex = 100;
            this.skinGroupBox5.TabStop = false;
            this.skinGroupBox5.Text = "90度气缸";
            this.skinGroupBox5.TitleBorderColor = System.Drawing.Color.Transparent;
            this.skinGroupBox5.TitleRadius = 20;
            this.skinGroupBox5.TitleRectBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(198)))), ((int)(((byte)(203)))));
            this.skinGroupBox5.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // qgSwitch7
            // 
            this.qgSwitch7.ButtonText = new string[] {
        "水平",
        "垂直"};
            this.qgSwitch7.CX_Left = "CX垂直";
            this.qgSwitch7.CX_Right = "CX水平";
            this.qgSwitch7.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qgSwitch7.HeaderContent = "位置7气缸";
            this.qgSwitch7.LeftTriggered = false;
            this.qgSwitch7.Location = new System.Drawing.Point(22, 487);
            this.qgSwitch7.Name = "qgSwitch7";
            this.qgSwitch7.RightTriggered = false;
            this.qgSwitch7.Size = new System.Drawing.Size(158, 139);
            this.qgSwitch7.TabIndex = 6;
            // 
            // qgSwitch6
            // 
            this.qgSwitch6.ButtonText = new string[] {
        "水平",
        "垂直"};
            this.qgSwitch6.CX_Left = "CX垂直";
            this.qgSwitch6.CX_Right = "CX水平";
            this.qgSwitch6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qgSwitch6.HeaderContent = "位置6气缸";
            this.qgSwitch6.LeftTriggered = false;
            this.qgSwitch6.Location = new System.Drawing.Point(220, 342);
            this.qgSwitch6.Name = "qgSwitch6";
            this.qgSwitch6.RightTriggered = false;
            this.qgSwitch6.Size = new System.Drawing.Size(158, 139);
            this.qgSwitch6.TabIndex = 5;
            // 
            // qgSwitch5
            // 
            this.qgSwitch5.ButtonText = new string[] {
        "水平",
        "垂直"};
            this.qgSwitch5.CX_Left = "CX垂直";
            this.qgSwitch5.CX_Right = "CX水平";
            this.qgSwitch5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qgSwitch5.HeaderContent = "位置5气缸";
            this.qgSwitch5.LeftTriggered = false;
            this.qgSwitch5.Location = new System.Drawing.Point(22, 342);
            this.qgSwitch5.Name = "qgSwitch5";
            this.qgSwitch5.RightTriggered = false;
            this.qgSwitch5.Size = new System.Drawing.Size(158, 139);
            this.qgSwitch5.TabIndex = 4;
            // 
            // qgSwitch4
            // 
            this.qgSwitch4.ButtonText = new string[] {
        "水平",
        "垂直"};
            this.qgSwitch4.CX_Left = "CX垂直";
            this.qgSwitch4.CX_Right = "CX水平";
            this.qgSwitch4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qgSwitch4.HeaderContent = "位置4气缸";
            this.qgSwitch4.LeftTriggered = false;
            this.qgSwitch4.Location = new System.Drawing.Point(220, 197);
            this.qgSwitch4.Name = "qgSwitch4";
            this.qgSwitch4.RightTriggered = false;
            this.qgSwitch4.Size = new System.Drawing.Size(158, 139);
            this.qgSwitch4.TabIndex = 3;
            // 
            // qgSwitch3
            // 
            this.qgSwitch3.ButtonText = new string[] {
        "水平",
        "垂直"};
            this.qgSwitch3.CX_Left = "CX垂直";
            this.qgSwitch3.CX_Right = "CX水平";
            this.qgSwitch3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qgSwitch3.HeaderContent = "位置3气缸";
            this.qgSwitch3.LeftTriggered = false;
            this.qgSwitch3.Location = new System.Drawing.Point(22, 197);
            this.qgSwitch3.Name = "qgSwitch3";
            this.qgSwitch3.RightTriggered = false;
            this.qgSwitch3.Size = new System.Drawing.Size(158, 139);
            this.qgSwitch3.TabIndex = 2;
            // 
            // qgSwitch2
            // 
            this.qgSwitch2.ButtonText = new string[] {
        "水平",
        "垂直"};
            this.qgSwitch2.CX_Left = "CX垂直";
            this.qgSwitch2.CX_Right = "CX水平";
            this.qgSwitch2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qgSwitch2.HeaderContent = "位置2气缸";
            this.qgSwitch2.LeftTriggered = false;
            this.qgSwitch2.Location = new System.Drawing.Point(220, 52);
            this.qgSwitch2.Name = "qgSwitch2";
            this.qgSwitch2.RightTriggered = false;
            this.qgSwitch2.Size = new System.Drawing.Size(158, 139);
            this.qgSwitch2.TabIndex = 1;
            // 
            // qgSwitch1
            // 
            this.qgSwitch1.ButtonText = new string[] {
        "水平",
        "垂直"};
            this.qgSwitch1.CX_Left = "CX垂直";
            this.qgSwitch1.CX_Right = "CX水平";
            this.qgSwitch1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qgSwitch1.HeaderContent = "位置1气缸";
            this.qgSwitch1.LeftTriggered = false;
            this.qgSwitch1.Location = new System.Drawing.Point(22, 52);
            this.qgSwitch1.Name = "qgSwitch1";
            this.qgSwitch1.RightTriggered = false;
            this.qgSwitch1.Size = new System.Drawing.Size(158, 139);
            this.qgSwitch1.TabIndex = 0;
            // 
            // skinTabPage11
            // 
            this.skinTabPage11.BackColor = System.Drawing.Color.Transparent;
            this.skinTabPage11.BorderColor = System.Drawing.Color.Black;
            this.skinTabPage11.Controls.Add(this.Motor_Control);
            this.skinTabPage11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage11.Location = new System.Drawing.Point(0, 50);
            this.skinTabPage11.Name = "skinTabPage11";
            this.skinTabPage11.Size = new System.Drawing.Size(1502, 665);
            this.skinTabPage11.TabIndex = 3;
            this.skinTabPage11.TabItemImage = null;
            this.skinTabPage11.Text = "电机";
            // 
            // Motor_Control
            // 
            this.Motor_Control.Font = new System.Drawing.Font("宋体", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Motor_Control.Location = new System.Drawing.Point(19, 29);
            this.Motor_Control.Margin = new System.Windows.Forms.Padding(5);
            this.Motor_Control.Name = "Motor_Control";
            this.Motor_Control.Size = new System.Drawing.Size(976, 594);
            this.Motor_Control.TabIndex = 1;
            // 
            // skinTabPage1
            // 
            this.skinTabPage1.BackColor = System.Drawing.Color.White;
            this.skinTabPage1.BorderColor = System.Drawing.Color.Black;
            this.skinTabPage1.Controls.Add(this.Up_EmotorControl);
            this.skinTabPage1.Controls.Add(this.Up_PositionTG5);
            this.skinTabPage1.Controls.Add(this.Up_PositionTG8);
            this.skinTabPage1.Controls.Add(this.Up_Position5);
            this.skinTabPage1.Controls.Add(this.Up_Position8);
            this.skinTabPage1.Controls.Add(this.Up_PositionTG4);
            this.skinTabPage1.Controls.Add(this.Up_PositionTG3);
            this.skinTabPage1.Controls.Add(this.Up_Position4);
            this.skinTabPage1.Controls.Add(this.Up_Position3);
            this.skinTabPage1.Controls.Add(this.Up_PositionTG7);
            this.skinTabPage1.Controls.Add(this.Up_PositionTG6);
            this.skinTabPage1.Controls.Add(this.Up_Position7);
            this.skinTabPage1.Controls.Add(this.Up_Position6);
            this.skinTabPage1.Controls.Add(this.Up_PositionTG2);
            this.skinTabPage1.Controls.Add(this.Up_PositionTG1);
            this.skinTabPage1.Controls.Add(this.Up_Position2);
            this.skinTabPage1.Controls.Add(this.Up_Position1);
            this.skinTabPage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage1.Location = new System.Drawing.Point(0, 50);
            this.skinTabPage1.Name = "skinTabPage1";
            this.skinTabPage1.Size = new System.Drawing.Size(1502, 665);
            this.skinTabPage1.TabIndex = 4;
            this.skinTabPage1.TabItemImage = null;
            this.skinTabPage1.Text = "上料电缸";
            // 
            // Up_EmotorControl
            // 
            this.Up_EmotorControl.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Up_EmotorControl.Location = new System.Drawing.Point(631, 482);
            this.Up_EmotorControl.Margin = new System.Windows.Forms.Padding(4);
            this.Up_EmotorControl.Name = "Up_EmotorControl";
            this.Up_EmotorControl.Size = new System.Drawing.Size(924, 173);
            this.Up_EmotorControl.TabIndex = 50;
            // 
            // Up_PositionTG5
            // 
            this.Up_PositionTG5.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Up_PositionTG5.Location = new System.Drawing.Point(631, 189);
            this.Up_PositionTG5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Up_PositionTG5.Name = "Up_PositionTG5";
            this.Up_PositionTG5.PositionName = "第五层拖钩";
            this.Up_PositionTG5.Size = new System.Drawing.Size(281, 145);
            this.Up_PositionTG5.TabIndex = 49;
            // 
            // Up_PositionTG8
            // 
            this.Up_PositionTG8.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Up_PositionTG8.Location = new System.Drawing.Point(929, 328);
            this.Up_PositionTG8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Up_PositionTG8.Name = "Up_PositionTG8";
            this.Up_PositionTG8.PositionName = "第八层拖钩";
            this.Up_PositionTG8.Size = new System.Drawing.Size(276, 146);
            this.Up_PositionTG8.TabIndex = 48;
            // 
            // Up_Position5
            // 
            this.Up_Position5.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Up_Position5.Location = new System.Drawing.Point(631, 32);
            this.Up_Position5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Up_Position5.Name = "Up_Position5";
            this.Up_Position5.PositionName = "第五层";
            this.Up_Position5.Size = new System.Drawing.Size(281, 161);
            this.Up_Position5.TabIndex = 47;
            // 
            // Up_Position8
            // 
            this.Up_Position8.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Up_Position8.Location = new System.Drawing.Point(631, 332);
            this.Up_Position8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Up_Position8.Name = "Up_Position8";
            this.Up_Position8.PositionName = "第八层";
            this.Up_Position8.Size = new System.Drawing.Size(313, 142);
            this.Up_Position8.TabIndex = 46;
            // 
            // Up_PositionTG4
            // 
            this.Up_PositionTG4.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Up_PositionTG4.Location = new System.Drawing.Point(325, 490);
            this.Up_PositionTG4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Up_PositionTG4.Name = "Up_PositionTG4";
            this.Up_PositionTG4.PositionName = "第四层拖钩";
            this.Up_PositionTG4.Size = new System.Drawing.Size(285, 154);
            this.Up_PositionTG4.TabIndex = 45;
            // 
            // Up_PositionTG3
            // 
            this.Up_PositionTG3.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Up_PositionTG3.Location = new System.Drawing.Point(325, 332);
            this.Up_PositionTG3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Up_PositionTG3.Name = "Up_PositionTG3";
            this.Up_PositionTG3.PositionName = "第三层拖钩";
            this.Up_PositionTG3.Size = new System.Drawing.Size(285, 162);
            this.Up_PositionTG3.TabIndex = 44;
            // 
            // Up_Position4
            // 
            this.Up_Position4.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Up_Position4.Location = new System.Drawing.Point(24, 490);
            this.Up_Position4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Up_Position4.Name = "Up_Position4";
            this.Up_Position4.PositionName = "第四层";
            this.Up_Position4.Size = new System.Drawing.Size(313, 154);
            this.Up_Position4.TabIndex = 43;
            // 
            // Up_Position3
            // 
            this.Up_Position3.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Up_Position3.Location = new System.Drawing.Point(24, 332);
            this.Up_Position3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Up_Position3.Name = "Up_Position3";
            this.Up_Position3.PositionName = "第三层";
            this.Up_Position3.Size = new System.Drawing.Size(279, 150);
            this.Up_Position3.TabIndex = 42;
            // 
            // Up_PositionTG7
            // 
            this.Up_PositionTG7.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Up_PositionTG7.Location = new System.Drawing.Point(1223, 189);
            this.Up_PositionTG7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Up_PositionTG7.Name = "Up_PositionTG7";
            this.Up_PositionTG7.PositionName = "第七层拖钩";
            this.Up_PositionTG7.Size = new System.Drawing.Size(283, 162);
            this.Up_PositionTG7.TabIndex = 41;
            // 
            // Up_PositionTG6
            // 
            this.Up_PositionTG6.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Up_PositionTG6.Location = new System.Drawing.Point(1223, 32);
            this.Up_PositionTG6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Up_PositionTG6.Name = "Up_PositionTG6";
            this.Up_PositionTG6.PositionName = "第六层拖钩";
            this.Up_PositionTG6.Size = new System.Drawing.Size(279, 153);
            this.Up_PositionTG6.TabIndex = 40;
            // 
            // Up_Position7
            // 
            this.Up_Position7.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Up_Position7.Location = new System.Drawing.Point(929, 188);
            this.Up_Position7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Up_Position7.Name = "Up_Position7";
            this.Up_Position7.PositionName = "第七层";
            this.Up_Position7.Size = new System.Drawing.Size(281, 146);
            this.Up_Position7.TabIndex = 39;
            // 
            // Up_Position6
            // 
            this.Up_Position6.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Up_Position6.Location = new System.Drawing.Point(929, 32);
            this.Up_Position6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Up_Position6.Name = "Up_Position6";
            this.Up_Position6.PositionName = "第六层";
            this.Up_Position6.Size = new System.Drawing.Size(280, 139);
            this.Up_Position6.TabIndex = 38;
            // 
            // Up_PositionTG2
            // 
            this.Up_PositionTG2.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Up_PositionTG2.Location = new System.Drawing.Point(325, 189);
            this.Up_PositionTG2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Up_PositionTG2.Name = "Up_PositionTG2";
            this.Up_PositionTG2.PositionName = "第二层拖钩";
            this.Up_PositionTG2.Size = new System.Drawing.Size(285, 145);
            this.Up_PositionTG2.TabIndex = 37;
            // 
            // Up_PositionTG1
            // 
            this.Up_PositionTG1.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Up_PositionTG1.Location = new System.Drawing.Point(325, 32);
            this.Up_PositionTG1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Up_PositionTG1.Name = "Up_PositionTG1";
            this.Up_PositionTG1.PositionName = "第一层拖钩";
            this.Up_PositionTG1.Size = new System.Drawing.Size(285, 143);
            this.Up_PositionTG1.TabIndex = 36;
            // 
            // Up_Position2
            // 
            this.Up_Position2.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Up_Position2.Location = new System.Drawing.Point(21, 189);
            this.Up_Position2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Up_Position2.Name = "Up_Position2";
            this.Up_Position2.PositionName = "第二层";
            this.Up_Position2.Size = new System.Drawing.Size(291, 145);
            this.Up_Position2.TabIndex = 35;
            // 
            // Up_Position1
            // 
            this.Up_Position1.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Up_Position1.Location = new System.Drawing.Point(21, 32);
            this.Up_Position1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Up_Position1.Name = "Up_Position1";
            this.Up_Position1.PositionName = "第一层";
            this.Up_Position1.Size = new System.Drawing.Size(313, 143);
            this.Up_Position1.TabIndex = 34;
            // 
            // skinTabPage2
            // 
            this.skinTabPage2.BackColor = System.Drawing.Color.White;
            this.skinTabPage2.BorderColor = System.Drawing.Color.Black;
            this.skinTabPage2.Controls.Add(this.Down_EmotorControl);
            this.skinTabPage2.Controls.Add(this.Down_PositionTG4);
            this.skinTabPage2.Controls.Add(this.Down_PositionTG3);
            this.skinTabPage2.Controls.Add(this.Down_Position4);
            this.skinTabPage2.Controls.Add(this.Down_Position3);
            this.skinTabPage2.Controls.Add(this.Down_PositionTG2);
            this.skinTabPage2.Controls.Add(this.Down_PositionTG1);
            this.skinTabPage2.Controls.Add(this.Down_Position2);
            this.skinTabPage2.Controls.Add(this.Down_Position1);
            this.skinTabPage2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage2.Location = new System.Drawing.Point(0, 50);
            this.skinTabPage2.Name = "skinTabPage2";
            this.skinTabPage2.Size = new System.Drawing.Size(1502, 665);
            this.skinTabPage2.TabIndex = 5;
            this.skinTabPage2.TabItemImage = null;
            this.skinTabPage2.Text = "下料电缸";
            // 
            // Down_EmotorControl
            // 
            this.Down_EmotorControl.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Down_EmotorControl.Location = new System.Drawing.Point(26, 375);
            this.Down_EmotorControl.Margin = new System.Windows.Forms.Padding(4);
            this.Down_EmotorControl.Name = "Down_EmotorControl";
            this.Down_EmotorControl.Size = new System.Drawing.Size(904, 199);
            this.Down_EmotorControl.TabIndex = 38;
            // 
            // Down_PositionTG4
            // 
            this.Down_PositionTG4.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Down_PositionTG4.Location = new System.Drawing.Point(956, 178);
            this.Down_PositionTG4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Down_PositionTG4.Name = "Down_PositionTG4";
            this.Down_PositionTG4.PositionName = "第四层拖钩";
            this.Down_PositionTG4.Size = new System.Drawing.Size(302, 143);
            this.Down_PositionTG4.TabIndex = 37;
            // 
            // Down_PositionTG3
            // 
            this.Down_PositionTG3.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Down_PositionTG3.Location = new System.Drawing.Point(956, 35);
            this.Down_PositionTG3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Down_PositionTG3.Name = "Down_PositionTG3";
            this.Down_PositionTG3.PositionName = "第三层拖钩";
            this.Down_PositionTG3.Size = new System.Drawing.Size(283, 153);
            this.Down_PositionTG3.TabIndex = 36;
            // 
            // Down_Position4
            // 
            this.Down_Position4.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Down_Position4.Location = new System.Drawing.Point(654, 178);
            this.Down_Position4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Down_Position4.Name = "Down_Position4";
            this.Down_Position4.PositionName = "第四层";
            this.Down_Position4.Size = new System.Drawing.Size(313, 163);
            this.Down_Position4.TabIndex = 35;
            // 
            // Down_Position3
            // 
            this.Down_Position3.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Down_Position3.Location = new System.Drawing.Point(654, 35);
            this.Down_Position3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Down_Position3.Name = "Down_Position3";
            this.Down_Position3.PositionName = "第三层";
            this.Down_Position3.Size = new System.Drawing.Size(296, 144);
            this.Down_Position3.TabIndex = 34;
            // 
            // Down_PositionTG2
            // 
            this.Down_PositionTG2.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Down_PositionTG2.Location = new System.Drawing.Point(345, 178);
            this.Down_PositionTG2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Down_PositionTG2.Name = "Down_PositionTG2";
            this.Down_PositionTG2.PositionName = "第二层拖钩";
            this.Down_PositionTG2.Size = new System.Drawing.Size(283, 146);
            this.Down_PositionTG2.TabIndex = 33;
            // 
            // Down_PositionTG1
            // 
            this.Down_PositionTG1.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Down_PositionTG1.Location = new System.Drawing.Point(345, 28);
            this.Down_PositionTG1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Down_PositionTG1.Name = "Down_PositionTG1";
            this.Down_PositionTG1.PositionName = "第一层拖钩";
            this.Down_PositionTG1.Size = new System.Drawing.Size(283, 151);
            this.Down_PositionTG1.TabIndex = 32;
            // 
            // Down_Position2
            // 
            this.Down_Position2.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Down_Position2.Location = new System.Drawing.Point(26, 178);
            this.Down_Position2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Down_Position2.Name = "Down_Position2";
            this.Down_Position2.PositionName = "第二层";
            this.Down_Position2.Size = new System.Drawing.Size(283, 146);
            this.Down_Position2.TabIndex = 31;
            // 
            // Down_Position1
            // 
            this.Down_Position1.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Down_Position1.Location = new System.Drawing.Point(26, 28);
            this.Down_Position1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Down_Position1.Name = "Down_Position1";
            this.Down_Position1.PositionName = "第一层";
            this.Down_Position1.Size = new System.Drawing.Size(296, 151);
            this.Down_Position1.TabIndex = 30;
            // 
            // skinTabPage3
            // 
            this.skinTabPage3.BackColor = System.Drawing.Color.White;
            this.skinTabPage3.BorderColor = System.Drawing.Color.Black;
            this.skinTabPage3.Controls.Add(this.Take_Position4);
            this.skinTabPage3.Controls.Add(this.Take_EmotorControl);
            this.skinTabPage3.Controls.Add(this.Take_Position2);
            this.skinTabPage3.Controls.Add(this.Take_Position3);
            this.skinTabPage3.Controls.Add(this.Take_Position1);
            this.skinTabPage3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage3.Location = new System.Drawing.Point(0, 50);
            this.skinTabPage3.Name = "skinTabPage3";
            this.skinTabPage3.Size = new System.Drawing.Size(1502, 665);
            this.skinTabPage3.TabIndex = 6;
            this.skinTabPage3.TabItemImage = null;
            this.skinTabPage3.Text = "取料电缸";
            // 
            // Take_Position4
            // 
            this.Take_Position4.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Take_Position4.Location = new System.Drawing.Point(1002, 37);
            this.Take_Position4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Take_Position4.Name = "Take_Position4";
            this.Take_Position4.PositionName = "取盒位";
            this.Take_Position4.Size = new System.Drawing.Size(289, 149);
            this.Take_Position4.TabIndex = 24;
            // 
            // Take_EmotorControl
            // 
            this.Take_EmotorControl.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Take_EmotorControl.Location = new System.Drawing.Point(50, 235);
            this.Take_EmotorControl.Margin = new System.Windows.Forms.Padding(4);
            this.Take_EmotorControl.Name = "Take_EmotorControl";
            this.Take_EmotorControl.Size = new System.Drawing.Size(1111, 199);
            this.Take_EmotorControl.TabIndex = 23;
            // 
            // Take_Position2
            // 
            this.Take_Position2.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Take_Position2.Location = new System.Drawing.Point(380, 37);
            this.Take_Position2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Take_Position2.Name = "Take_Position2";
            this.Take_Position2.PositionName = "放盒位";
            this.Take_Position2.Size = new System.Drawing.Size(286, 149);
            this.Take_Position2.TabIndex = 22;
            // 
            // Take_Position3
            // 
            this.Take_Position3.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Take_Position3.Location = new System.Drawing.Point(695, 37);
            this.Take_Position3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Take_Position3.Name = "Take_Position3";
            this.Take_Position3.PositionName = "装料位";
            this.Take_Position3.Size = new System.Drawing.Size(279, 149);
            this.Take_Position3.TabIndex = 21;
            // 
            // Take_Position1
            // 
            this.Take_Position1.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Take_Position1.Location = new System.Drawing.Point(50, 37);
            this.Take_Position1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Take_Position1.Name = "Take_Position1";
            this.Take_Position1.PositionName = "旋转位";
            this.Take_Position1.Size = new System.Drawing.Size(313, 164);
            this.Take_Position1.TabIndex = 20;
            // 
            // FrmDebug
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.BackPalace = global::WindowsFormsApp.Properties.Resources.metalBG;
            this.ClientSize = new System.Drawing.Size(1518, 762);
            this.Controls.Add(this.skinTabControl3);
            this.MdiBackColor = System.Drawing.Color.Black;
            this.Name = "FrmDebug";
            this.Text = "FrmDebug";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmDebug_FormClosing);
            this.Load += new System.EventHandler(this.FrmDebug_Load);
            this.skinTabControl3.ResumeLayout(false);
            this.skinTabPage8.ResumeLayout(false);
            this.skinTabPage8.PerformLayout();
            this.skinTabPage9.ResumeLayout(false);
            this.skinGroupBox4.ResumeLayout(false);
            this.skinGroupBox2.ResumeLayout(false);
            this.skinGroupBox3.ResumeLayout(false);
            this.skinGroupBox1.ResumeLayout(false);
            this.skinGroupBox5.ResumeLayout(false);
            this.skinTabPage11.ResumeLayout(false);
            this.skinTabPage1.ResumeLayout(false);
            this.skinTabPage2.ResumeLayout(false);
            this.skinTabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        private CCWin.SkinControl.SkinTabControl skinTabControl3;
        private CCWin.SkinControl.SkinTabPage skinTabPage9;
        private CCWin.SkinControl.SkinGroupBox skinGroupBox3;
        private components.QGUnit.QGSwitch qgSwitch9;
        private CCWin.SkinControl.SkinGroupBox skinGroupBox1;
        private components.QGUnit.QGSwitch qgSwitch10;
        private CCWin.SkinControl.SkinGroupBox skinGroupBox5;
        private components.QGUnit.QGSwitch qgSwitch7;
        private components.QGUnit.QGSwitch qgSwitch6;
        private components.QGUnit.QGSwitch qgSwitch5;
        private components.QGUnit.QGSwitch qgSwitch4;
        private components.QGUnit.QGSwitch qgSwitch3;
        private components.QGUnit.QGSwitch qgSwitch2;
        private components.QGUnit.QGSwitch qgSwitch1;
        private CCWin.SkinControl.SkinTabPage skinTabPage8;
        private components.grip.GripSwitch grip_down;
        private components.grip.GripSwitch grip_up;
        private components.vacuum.VacuumSwitch SecondRobotVacuumSwitch;
        private components.vacuum.VacuumSwitch ThirdRobotVacuumSwitch;
        private components.vacuum.VacuumSwitch FirstRobotVacuumSwitch;
        private CCWin.SkinControl.SkinTabPage skinTabPage11;
        private components.Motor.MotorControl Motor_Control;
        private CCWin.SkinControl.SkinGroupBox skinGroupBox2;
        private components.QGUnit.QGSwitch qgSwitch8;
        private CCWin.SkinControl.SkinTabPage skinTabPage1;
        private components.EMotor.EmotorControl Up_EmotorControl;
        private components.EMotor.PositionBlock Up_PositionTG5;
        private components.EMotor.PositionBlock Up_PositionTG8;
        private components.EMotor.PositionBlock Up_Position5;
        private components.EMotor.PositionBlock Up_Position8;
        private components.EMotor.PositionBlock Up_PositionTG4;
        private components.EMotor.PositionBlock Up_PositionTG3;
        private components.EMotor.PositionBlock Up_Position4;
        private components.EMotor.PositionBlock Up_Position3;
        private components.EMotor.PositionBlock Up_PositionTG7;
        private components.EMotor.PositionBlock Up_PositionTG6;
        private components.EMotor.PositionBlock Up_Position7;
        private components.EMotor.PositionBlock Up_Position6;
        private components.EMotor.PositionBlock Up_PositionTG2;
        private components.EMotor.PositionBlock Up_PositionTG1;
        private components.EMotor.PositionBlock Up_Position2;
        private components.EMotor.PositionBlock Up_Position1;
        private CCWin.SkinControl.SkinTabPage skinTabPage2;
        private components.EMotor.EmotorControl Down_EmotorControl;
        private components.EMotor.PositionBlock Down_PositionTG4;
        private components.EMotor.PositionBlock Down_PositionTG3;
        private components.EMotor.PositionBlock Down_Position4;
        private components.EMotor.PositionBlock Down_Position3;
        private components.EMotor.PositionBlock Down_PositionTG2;
        private components.EMotor.PositionBlock Down_PositionTG1;
        private components.EMotor.PositionBlock Down_Position2;
        private components.EMotor.PositionBlock Down_Position1;
        private CCWin.SkinControl.SkinTabPage skinTabPage3;
        private components.EMotor.EmotorControl Take_EmotorControl;
        private components.EMotor.PositionBlock Take_Position2;
        private components.EMotor.PositionBlock Take_Position3;
        private components.EMotor.PositionBlock Take_Position1;
        private components.robot.RobotOption robotOption1;
        private components.EMotor.PositionBlock Take_Position4;
        private CCWin.SkinControl.SkinGroupBox skinGroupBox4;
        private components.QGUnit.QGSwitch qgSwitch11;

        #endregion
        // private WebKit.WebKitBrowser webBrowser1;
    }
}