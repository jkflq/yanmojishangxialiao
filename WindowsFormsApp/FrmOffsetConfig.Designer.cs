﻿
namespace WindowsFormsApp
{
    partial class FrmOffsetConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmOffsetConfig));
            this.DataGridView_Take = new System.Windows.Forms.DataGridView();
            this.skinButton2 = new CCWin.SkinControl.SkinButton();
            this.skinButton1 = new CCWin.SkinControl.SkinButton();
            this.skinTabControl1 = new CCWin.SkinControl.SkinTabControl();
            this.skinTabPage2 = new CCWin.SkinControl.SkinTabPage();
            this.skinTabPage1 = new CCWin.SkinControl.SkinTabPage();
            this.skinButton4 = new CCWin.SkinControl.SkinButton();
            this.skinButton3 = new CCWin.SkinControl.SkinButton();
            this.DataGridView_Up = new System.Windows.Forms.DataGridView();
            this.skinTabPage3 = new CCWin.SkinControl.SkinTabPage();
            this.skinButton6 = new CCWin.SkinControl.SkinButton();
            this.skinButton5 = new CCWin.SkinControl.SkinButton();
            this.DataGridView_Down = new System.Windows.Forms.DataGridView();
            this.上下料区间 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.上下料X补偿值 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.上下料Y补偿值 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.上下料角度补偿值 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.上料区间 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.上料X补偿值 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.上料Y补偿值 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.上料Z补偿值 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.上料角度补偿值 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.下料区间 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.下料X补偿值 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.下料Y补偿值 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.下料Z补偿值 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.下料角度补偿值 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_Take)).BeginInit();
            this.skinTabControl1.SuspendLayout();
            this.skinTabPage2.SuspendLayout();
            this.skinTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_Up)).BeginInit();
            this.skinTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_Down)).BeginInit();
            this.SuspendLayout();
            // 
            // DataGridView_Take
            // 
            this.DataGridView_Take.AllowUserToAddRows = false;
            this.DataGridView_Take.AllowUserToDeleteRows = false;
            this.DataGridView_Take.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataGridView_Take.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView_Take.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.上下料区间,
            this.上下料X补偿值,
            this.上下料Y补偿值,
            this.上下料角度补偿值});
            this.DataGridView_Take.Dock = System.Windows.Forms.DockStyle.Top;
            this.DataGridView_Take.Location = new System.Drawing.Point(0, 0);
            this.DataGridView_Take.Name = "DataGridView_Take";
            this.DataGridView_Take.RowHeadersWidth = 51;
            this.DataGridView_Take.RowTemplate.Height = 27;
            this.DataGridView_Take.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridView_Take.Size = new System.Drawing.Size(981, 339);
            this.DataGridView_Take.TabIndex = 3;
            // 
            // skinButton2
            // 
            this.skinButton2.BackColor = System.Drawing.Color.Transparent;
            this.skinButton2.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinButton2.DownBack = null;
            this.skinButton2.Location = new System.Drawing.Point(149, 378);
            this.skinButton2.MouseBack = null;
            this.skinButton2.Name = "skinButton2";
            this.skinButton2.NormlBack = null;
            this.skinButton2.Size = new System.Drawing.Size(151, 61);
            this.skinButton2.TabIndex = 5;
            this.skinButton2.Text = "写入参数";
            this.skinButton2.UseVisualStyleBackColor = false;
            this.skinButton2.Click += new System.EventHandler(this.skinButton2_Click);
            // 
            // skinButton1
            // 
            this.skinButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.skinButton1.BackColor = System.Drawing.Color.Transparent;
            this.skinButton1.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinButton1.DownBack = null;
            this.skinButton1.Location = new System.Drawing.Point(674, 378);
            this.skinButton1.MouseBack = null;
            this.skinButton1.Name = "skinButton1";
            this.skinButton1.NormlBack = null;
            this.skinButton1.Size = new System.Drawing.Size(151, 61);
            this.skinButton1.TabIndex = 6;
            this.skinButton1.Text = "关闭";
            this.skinButton1.UseVisualStyleBackColor = false;
            this.skinButton1.Click += new System.EventHandler(this.skinButton1_Click);
            // 
            // skinTabControl1
            // 
            this.skinTabControl1.AnimatorType = CCWin.SkinControl.AnimationType.HorizSlide;
            this.skinTabControl1.CloseRect = new System.Drawing.Rectangle(2, 2, 12, 12);
            this.skinTabControl1.Controls.Add(this.skinTabPage2);
            this.skinTabControl1.Controls.Add(this.skinTabPage1);
            this.skinTabControl1.Controls.Add(this.skinTabPage3);
            this.skinTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabControl1.HeadBack = null;
            this.skinTabControl1.ImgTxtOffset = new System.Drawing.Point(0, 0);
            this.skinTabControl1.ItemSize = new System.Drawing.Size(100, 36);
            this.skinTabControl1.Location = new System.Drawing.Point(8, 39);
            this.skinTabControl1.Name = "skinTabControl1";
            this.skinTabControl1.PageArrowDown = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageArrowDown")));
            this.skinTabControl1.PageArrowHover = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageArrowHover")));
            this.skinTabControl1.PageCloseHover = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageCloseHover")));
            this.skinTabControl1.PageCloseNormal = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageCloseNormal")));
            this.skinTabControl1.PageDown = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageDown")));
            this.skinTabControl1.PageDownTxtColor = System.Drawing.Color.Orange;
            this.skinTabControl1.PageHover = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageHover")));
            this.skinTabControl1.PageImagePosition = CCWin.SkinControl.SkinTabControl.ePageImagePosition.Left;
            this.skinTabControl1.PageNorml = null;
            this.skinTabControl1.SelectedIndex = 1;
            this.skinTabControl1.Size = new System.Drawing.Size(981, 551);
            this.skinTabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.skinTabControl1.TabIndex = 7;
            // 
            // skinTabPage2
            // 
            this.skinTabPage2.BackColor = System.Drawing.Color.White;
            this.skinTabPage2.Controls.Add(this.skinButton1);
            this.skinTabPage2.Controls.Add(this.DataGridView_Take);
            this.skinTabPage2.Controls.Add(this.skinButton2);
            this.skinTabPage2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage2.Location = new System.Drawing.Point(0, 36);
            this.skinTabPage2.Name = "skinTabPage2";
            this.skinTabPage2.Size = new System.Drawing.Size(981, 515);
            this.skinTabPage2.TabIndex = 1;
            this.skinTabPage2.TabItemImage = null;
            this.skinTabPage2.Text = "上下料机械臂";
            // 
            // skinTabPage1
            // 
            this.skinTabPage1.BackColor = System.Drawing.Color.White;
            this.skinTabPage1.Controls.Add(this.skinButton4);
            this.skinTabPage1.Controls.Add(this.skinButton3);
            this.skinTabPage1.Controls.Add(this.DataGridView_Up);
            this.skinTabPage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage1.Location = new System.Drawing.Point(0, 36);
            this.skinTabPage1.Name = "skinTabPage1";
            this.skinTabPage1.Size = new System.Drawing.Size(981, 515);
            this.skinTabPage1.TabIndex = 0;
            this.skinTabPage1.TabItemImage = null;
            this.skinTabPage1.Text = "上料机械臂";
            // 
            // skinButton4
            // 
            this.skinButton4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.skinButton4.BackColor = System.Drawing.Color.Transparent;
            this.skinButton4.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinButton4.DownBack = null;
            this.skinButton4.Location = new System.Drawing.Point(674, 378);
            this.skinButton4.MouseBack = null;
            this.skinButton4.Name = "skinButton4";
            this.skinButton4.NormlBack = null;
            this.skinButton4.Size = new System.Drawing.Size(151, 61);
            this.skinButton4.TabIndex = 7;
            this.skinButton4.Text = "关闭";
            this.skinButton4.UseVisualStyleBackColor = false;
            // 
            // skinButton3
            // 
            this.skinButton3.BackColor = System.Drawing.Color.Transparent;
            this.skinButton3.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinButton3.DownBack = null;
            this.skinButton3.Location = new System.Drawing.Point(149, 378);
            this.skinButton3.MouseBack = null;
            this.skinButton3.Name = "skinButton3";
            this.skinButton3.NormlBack = null;
            this.skinButton3.Size = new System.Drawing.Size(151, 61);
            this.skinButton3.TabIndex = 6;
            this.skinButton3.Text = "写入参数";
            this.skinButton3.UseVisualStyleBackColor = false;
            // 
            // DataGridView_Up
            // 
            this.DataGridView_Up.AllowUserToAddRows = false;
            this.DataGridView_Up.AllowUserToDeleteRows = false;
            this.DataGridView_Up.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataGridView_Up.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView_Up.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.上料区间,
            this.上料X补偿值,
            this.上料Y补偿值,
            this.上料Z补偿值,
            this.上料角度补偿值});
            this.DataGridView_Up.Dock = System.Windows.Forms.DockStyle.Top;
            this.DataGridView_Up.Location = new System.Drawing.Point(0, 0);
            this.DataGridView_Up.Name = "DataGridView_Up";
            this.DataGridView_Up.RowHeadersWidth = 51;
            this.DataGridView_Up.RowTemplate.Height = 27;
            this.DataGridView_Up.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridView_Up.Size = new System.Drawing.Size(981, 339);
            this.DataGridView_Up.TabIndex = 4;
            // 
            // skinTabPage3
            // 
            this.skinTabPage3.BackColor = System.Drawing.Color.White;
            this.skinTabPage3.Controls.Add(this.skinButton6);
            this.skinTabPage3.Controls.Add(this.skinButton5);
            this.skinTabPage3.Controls.Add(this.DataGridView_Down);
            this.skinTabPage3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage3.Location = new System.Drawing.Point(0, 36);
            this.skinTabPage3.Name = "skinTabPage3";
            this.skinTabPage3.Size = new System.Drawing.Size(981, 515);
            this.skinTabPage3.TabIndex = 2;
            this.skinTabPage3.TabItemImage = null;
            this.skinTabPage3.Text = "下料机械臂";
            // 
            // skinButton6
            // 
            this.skinButton6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.skinButton6.BackColor = System.Drawing.Color.Transparent;
            this.skinButton6.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinButton6.DownBack = null;
            this.skinButton6.Location = new System.Drawing.Point(674, 378);
            this.skinButton6.MouseBack = null;
            this.skinButton6.Name = "skinButton6";
            this.skinButton6.NormlBack = null;
            this.skinButton6.Size = new System.Drawing.Size(151, 61);
            this.skinButton6.TabIndex = 8;
            this.skinButton6.Text = "关闭";
            this.skinButton6.UseVisualStyleBackColor = false;
            // 
            // skinButton5
            // 
            this.skinButton5.BackColor = System.Drawing.Color.Transparent;
            this.skinButton5.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinButton5.DownBack = null;
            this.skinButton5.Location = new System.Drawing.Point(149, 378);
            this.skinButton5.MouseBack = null;
            this.skinButton5.Name = "skinButton5";
            this.skinButton5.NormlBack = null;
            this.skinButton5.Size = new System.Drawing.Size(151, 61);
            this.skinButton5.TabIndex = 7;
            this.skinButton5.Text = "写入参数";
            this.skinButton5.UseVisualStyleBackColor = false;
            // 
            // DataGridView_Down
            // 
            this.DataGridView_Down.AllowUserToAddRows = false;
            this.DataGridView_Down.AllowUserToDeleteRows = false;
            this.DataGridView_Down.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataGridView_Down.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView_Down.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.下料区间,
            this.下料X补偿值,
            this.下料Y补偿值,
            this.下料Z补偿值,
            this.下料角度补偿值});
            this.DataGridView_Down.Dock = System.Windows.Forms.DockStyle.Top;
            this.DataGridView_Down.Location = new System.Drawing.Point(0, 0);
            this.DataGridView_Down.Name = "DataGridView_Down";
            this.DataGridView_Down.RowHeadersWidth = 51;
            this.DataGridView_Down.RowTemplate.Height = 27;
            this.DataGridView_Down.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridView_Down.Size = new System.Drawing.Size(981, 339);
            this.DataGridView_Down.TabIndex = 4;
            // 
            // 上下料区间
            // 
            this.上下料区间.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.上下料区间.DataPropertyName = "Section";
            this.上下料区间.HeaderText = "区间";
            this.上下料区间.MinimumWidth = 6;
            this.上下料区间.Name = "上下料区间";
            this.上下料区间.ReadOnly = true;
            // 
            // 上下料X补偿值
            // 
            this.上下料X补偿值.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.上下料X补偿值.DataPropertyName = "Xoffset";
            this.上下料X补偿值.HeaderText = "X补偿值";
            this.上下料X补偿值.MinimumWidth = 6;
            this.上下料X补偿值.Name = "上下料X补偿值";
            // 
            // 上下料Y补偿值
            // 
            this.上下料Y补偿值.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.上下料Y补偿值.DataPropertyName = "Yoffset";
            this.上下料Y补偿值.HeaderText = "Y补偿值";
            this.上下料Y补偿值.MinimumWidth = 6;
            this.上下料Y补偿值.Name = "上下料Y补偿值";
            // 
            // 上下料角度补偿值
            // 
            this.上下料角度补偿值.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.上下料角度补偿值.DataPropertyName = "Angeloffset";
            this.上下料角度补偿值.HeaderText = "角度补偿值";
            this.上下料角度补偿值.MinimumWidth = 6;
            this.上下料角度补偿值.Name = "上下料角度补偿值";
            // 
            // 上料区间
            // 
            this.上料区间.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.上料区间.DataPropertyName = "Section";
            this.上料区间.HeaderText = "区间";
            this.上料区间.MinimumWidth = 6;
            this.上料区间.Name = "上料区间";
            this.上料区间.ReadOnly = true;
            // 
            // 上料X补偿值
            // 
            this.上料X补偿值.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.上料X补偿值.DataPropertyName = "Xoffset";
            this.上料X补偿值.HeaderText = "X补偿值";
            this.上料X补偿值.MinimumWidth = 6;
            this.上料X补偿值.Name = "上料X补偿值";
            // 
            // 上料Y补偿值
            // 
            this.上料Y补偿值.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.上料Y补偿值.DataPropertyName = "Yoffset";
            this.上料Y补偿值.HeaderText = "Y补偿值";
            this.上料Y补偿值.MinimumWidth = 6;
            this.上料Y补偿值.Name = "上料Y补偿值";
            // 
            // 上料Z补偿值
            // 
            this.上料Z补偿值.DataPropertyName = "Zoffset";
            this.上料Z补偿值.HeaderText = "Z补偿值";
            this.上料Z补偿值.MinimumWidth = 6;
            this.上料Z补偿值.Name = "上料Z补偿值";
            // 
            // 上料角度补偿值
            // 
            this.上料角度补偿值.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.上料角度补偿值.DataPropertyName = "Angeloffset";
            this.上料角度补偿值.HeaderText = "角度补偿值";
            this.上料角度补偿值.MinimumWidth = 6;
            this.上料角度补偿值.Name = "上料角度补偿值";
            // 
            // 下料区间
            // 
            this.下料区间.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.下料区间.DataPropertyName = "Section";
            this.下料区间.HeaderText = "区间";
            this.下料区间.MinimumWidth = 6;
            this.下料区间.Name = "下料区间";
            this.下料区间.ReadOnly = true;
            // 
            // 下料X补偿值
            // 
            this.下料X补偿值.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.下料X补偿值.DataPropertyName = "Xoffset";
            this.下料X补偿值.HeaderText = "X补偿值";
            this.下料X补偿值.MinimumWidth = 6;
            this.下料X补偿值.Name = "下料X补偿值";
            // 
            // 下料Y补偿值
            // 
            this.下料Y补偿值.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.下料Y补偿值.DataPropertyName = "Yoffset";
            this.下料Y补偿值.HeaderText = "Y补偿值";
            this.下料Y补偿值.MinimumWidth = 6;
            this.下料Y补偿值.Name = "下料Y补偿值";
            // 
            // 下料Z补偿值
            // 
            this.下料Z补偿值.DataPropertyName = "Zoffset";
            this.下料Z补偿值.HeaderText = "Z补偿值";
            this.下料Z补偿值.MinimumWidth = 6;
            this.下料Z补偿值.Name = "下料Z补偿值";
            // 
            // 下料角度补偿值
            // 
            this.下料角度补偿值.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.下料角度补偿值.DataPropertyName = "Angeloffset";
            this.下料角度补偿值.HeaderText = "角度补偿值";
            this.下料角度补偿值.MinimumWidth = 6;
            this.下料角度补偿值.Name = "下料角度补偿值";
            // 
            // FrmOffsetConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(997, 598);
            this.Controls.Add(this.skinTabControl1);
            this.Name = "FrmOffsetConfig";
            this.Text = "offset设置";
            this.Load += new System.EventHandler(this.FrmOffsetConfig_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_Take)).EndInit();
            this.skinTabControl1.ResumeLayout(false);
            this.skinTabPage2.ResumeLayout(false);
            this.skinTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_Up)).EndInit();
            this.skinTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_Down)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView DataGridView_Take;
        private CCWin.SkinControl.SkinButton skinButton2;
        private CCWin.SkinControl.SkinButton skinButton1;
        private CCWin.SkinControl.SkinTabControl skinTabControl1;
        private CCWin.SkinControl.SkinTabPage skinTabPage1;
        private CCWin.SkinControl.SkinTabPage skinTabPage2;
        private CCWin.SkinControl.SkinTabPage skinTabPage3;
        private System.Windows.Forms.DataGridView DataGridView_Up;
        private System.Windows.Forms.DataGridView DataGridView_Down;
        private CCWin.SkinControl.SkinButton skinButton4;
        private CCWin.SkinControl.SkinButton skinButton3;
        private CCWin.SkinControl.SkinButton skinButton6;
        private CCWin.SkinControl.SkinButton skinButton5;
        private System.Windows.Forms.DataGridViewTextBoxColumn 上下料区间;
        private System.Windows.Forms.DataGridViewTextBoxColumn 上下料X补偿值;
        private System.Windows.Forms.DataGridViewTextBoxColumn 上下料Y补偿值;
        private System.Windows.Forms.DataGridViewTextBoxColumn 上下料角度补偿值;
        private System.Windows.Forms.DataGridViewTextBoxColumn 上料区间;
        private System.Windows.Forms.DataGridViewTextBoxColumn 上料X补偿值;
        private System.Windows.Forms.DataGridViewTextBoxColumn 上料Y补偿值;
        private System.Windows.Forms.DataGridViewTextBoxColumn 上料Z补偿值;
        private System.Windows.Forms.DataGridViewTextBoxColumn 上料角度补偿值;
        private System.Windows.Forms.DataGridViewTextBoxColumn 下料区间;
        private System.Windows.Forms.DataGridViewTextBoxColumn 下料X补偿值;
        private System.Windows.Forms.DataGridViewTextBoxColumn 下料Y补偿值;
        private System.Windows.Forms.DataGridViewTextBoxColumn 下料Z补偿值;
        private System.Windows.Forms.DataGridViewTextBoxColumn 下料角度补偿值;
    }
}