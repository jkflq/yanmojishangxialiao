﻿
namespace WindowsFormsApp.customizeWidget
{
    partial class Down_Disk
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.picb_1B_DOWN = new CCWin.SkinControl.SkinPictureBox();
            this.picb_1A_DOWN = new CCWin.SkinControl.SkinPictureBox();
            this.picb_3A_DOWN = new CCWin.SkinControl.SkinPictureBox();
            this.picb_3B_DOWN = new CCWin.SkinControl.SkinPictureBox();
            this.picb_4A_DOWN = new CCWin.SkinControl.SkinPictureBox();
            this.picb_4B_DOWN = new CCWin.SkinControl.SkinPictureBox();
            this.picb_5A_DOWN = new CCWin.SkinControl.SkinPictureBox();
            this.picb_5B_DOWN = new CCWin.SkinControl.SkinPictureBox();
            this.picb_6A_DOWN = new CCWin.SkinControl.SkinPictureBox();
            this.picb_8B_DOWN = new CCWin.SkinControl.SkinPictureBox();
            this.picb_7A_DOWN = new CCWin.SkinControl.SkinPictureBox();
            this.picb_7B_DOWN = new CCWin.SkinControl.SkinPictureBox();
            this.picb_8A_DOWN = new CCWin.SkinControl.SkinPictureBox();
            this.picb_6B_DOWN = new CCWin.SkinControl.SkinPictureBox();
            this.picb_2A_DOWN = new CCWin.SkinControl.SkinPictureBox();
            this.picb_2B_DOWN = new CCWin.SkinControl.SkinPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picb_1B_DOWN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_1A_DOWN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_3A_DOWN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_3B_DOWN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_4A_DOWN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_4B_DOWN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_5A_DOWN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_5B_DOWN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_6A_DOWN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_8B_DOWN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_7A_DOWN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_7B_DOWN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_8A_DOWN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_6B_DOWN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_2A_DOWN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_2B_DOWN)).BeginInit();
            this.SuspendLayout();
            // 
            // picb_1B_DOWN
            // 
            this.picb_1B_DOWN.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_1B_DOWN.Location = new System.Drawing.Point(169, 26);
            this.picb_1B_DOWN.Name = "picb_1B_DOWN";
            this.picb_1B_DOWN.Size = new System.Drawing.Size(63, 47);
            this.picb_1B_DOWN.TabIndex = 31;
            this.picb_1B_DOWN.TabStop = false;
            // 
            // picb_1A_DOWN
            // 
            this.picb_1A_DOWN.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_1A_DOWN.Location = new System.Drawing.Point(75, 26);
            this.picb_1A_DOWN.Name = "picb_1A_DOWN";
            this.picb_1A_DOWN.Size = new System.Drawing.Size(63, 47);
            this.picb_1A_DOWN.TabIndex = 32;
            this.picb_1A_DOWN.TabStop = false;
            // 
            // picb_3A_DOWN
            // 
            this.picb_3A_DOWN.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_3A_DOWN.Location = new System.Drawing.Point(75, 128);
            this.picb_3A_DOWN.Name = "picb_3A_DOWN";
            this.picb_3A_DOWN.Size = new System.Drawing.Size(63, 47);
            this.picb_3A_DOWN.TabIndex = 36;
            this.picb_3A_DOWN.TabStop = false;
            // 
            // picb_3B_DOWN
            // 
            this.picb_3B_DOWN.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_3B_DOWN.Location = new System.Drawing.Point(169, 128);
            this.picb_3B_DOWN.Name = "picb_3B_DOWN";
            this.picb_3B_DOWN.Size = new System.Drawing.Size(63, 47);
            this.picb_3B_DOWN.TabIndex = 35;
            this.picb_3B_DOWN.TabStop = false;
            // 
            // picb_4A_DOWN
            // 
            this.picb_4A_DOWN.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_4A_DOWN.Location = new System.Drawing.Point(47, 154);
            this.picb_4A_DOWN.Name = "picb_4A_DOWN";
            this.picb_4A_DOWN.Size = new System.Drawing.Size(63, 47);
            this.picb_4A_DOWN.TabIndex = 37;
            this.picb_4A_DOWN.TabStop = false;
            // 
            // picb_4B_DOWN
            // 
            this.picb_4B_DOWN.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_4B_DOWN.Location = new System.Drawing.Point(144, 154);
            this.picb_4B_DOWN.Name = "picb_4B_DOWN";
            this.picb_4B_DOWN.Size = new System.Drawing.Size(63, 47);
            this.picb_4B_DOWN.TabIndex = 38;
            this.picb_4B_DOWN.TabStop = false;
            // 
            // picb_5A_DOWN
            // 
            this.picb_5A_DOWN.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_5A_DOWN.Location = new System.Drawing.Point(75, 230);
            this.picb_5A_DOWN.Name = "picb_5A_DOWN";
            this.picb_5A_DOWN.Size = new System.Drawing.Size(63, 47);
            this.picb_5A_DOWN.TabIndex = 40;
            this.picb_5A_DOWN.TabStop = false;
            // 
            // picb_5B_DOWN
            // 
            this.picb_5B_DOWN.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_5B_DOWN.Location = new System.Drawing.Point(169, 230);
            this.picb_5B_DOWN.Name = "picb_5B_DOWN";
            this.picb_5B_DOWN.Size = new System.Drawing.Size(63, 47);
            this.picb_5B_DOWN.TabIndex = 39;
            this.picb_5B_DOWN.TabStop = false;
            // 
            // picb_6A_DOWN
            // 
            this.picb_6A_DOWN.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_6A_DOWN.Location = new System.Drawing.Point(47, 256);
            this.picb_6A_DOWN.Name = "picb_6A_DOWN";
            this.picb_6A_DOWN.Size = new System.Drawing.Size(63, 47);
            this.picb_6A_DOWN.TabIndex = 41;
            this.picb_6A_DOWN.TabStop = false;
            // 
            // picb_8B_DOWN
            // 
            this.picb_8B_DOWN.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_8B_DOWN.Location = new System.Drawing.Point(144, 358);
            this.picb_8B_DOWN.Name = "picb_8B_DOWN";
            this.picb_8B_DOWN.Size = new System.Drawing.Size(63, 47);
            this.picb_8B_DOWN.TabIndex = 46;
            this.picb_8B_DOWN.TabStop = false;
            // 
            // picb_7A_DOWN
            // 
            this.picb_7A_DOWN.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_7A_DOWN.Location = new System.Drawing.Point(75, 332);
            this.picb_7A_DOWN.Name = "picb_7A_DOWN";
            this.picb_7A_DOWN.Size = new System.Drawing.Size(63, 47);
            this.picb_7A_DOWN.TabIndex = 44;
            this.picb_7A_DOWN.TabStop = false;
            // 
            // picb_7B_DOWN
            // 
            this.picb_7B_DOWN.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_7B_DOWN.Location = new System.Drawing.Point(169, 332);
            this.picb_7B_DOWN.Name = "picb_7B_DOWN";
            this.picb_7B_DOWN.Size = new System.Drawing.Size(63, 47);
            this.picb_7B_DOWN.TabIndex = 43;
            this.picb_7B_DOWN.TabStop = false;
            // 
            // picb_8A_DOWN
            // 
            this.picb_8A_DOWN.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_8A_DOWN.Location = new System.Drawing.Point(47, 358);
            this.picb_8A_DOWN.Name = "picb_8A_DOWN";
            this.picb_8A_DOWN.Size = new System.Drawing.Size(63, 47);
            this.picb_8A_DOWN.TabIndex = 45;
            this.picb_8A_DOWN.TabStop = false;
            // 
            // picb_6B_DOWN
            // 
            this.picb_6B_DOWN.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_6B_DOWN.Location = new System.Drawing.Point(144, 256);
            this.picb_6B_DOWN.Name = "picb_6B_DOWN";
            this.picb_6B_DOWN.Size = new System.Drawing.Size(63, 47);
            this.picb_6B_DOWN.TabIndex = 42;
            this.picb_6B_DOWN.TabStop = false;
            // 
            // picb_2A_DOWN
            // 
            this.picb_2A_DOWN.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_2A_DOWN.Location = new System.Drawing.Point(47, 52);
            this.picb_2A_DOWN.Name = "picb_2A_DOWN";
            this.picb_2A_DOWN.Size = new System.Drawing.Size(63, 47);
            this.picb_2A_DOWN.TabIndex = 33;
            this.picb_2A_DOWN.TabStop = false;
            // 
            // picb_2B_DOWN
            // 
            this.picb_2B_DOWN.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_2B_DOWN.Location = new System.Drawing.Point(144, 52);
            this.picb_2B_DOWN.Name = "picb_2B_DOWN";
            this.picb_2B_DOWN.Size = new System.Drawing.Size(63, 47);
            this.picb_2B_DOWN.TabIndex = 34;
            this.picb_2B_DOWN.TabStop = false;
            // 
            // Down_Disk
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.picb_8B_DOWN);
            this.Controls.Add(this.picb_8A_DOWN);
            this.Controls.Add(this.picb_7B_DOWN);
            this.Controls.Add(this.picb_7A_DOWN);
            this.Controls.Add(this.picb_6B_DOWN);
            this.Controls.Add(this.picb_6A_DOWN);
            this.Controls.Add(this.picb_5B_DOWN);
            this.Controls.Add(this.picb_5A_DOWN);
            this.Controls.Add(this.picb_4B_DOWN);
            this.Controls.Add(this.picb_4A_DOWN);
            this.Controls.Add(this.picb_3B_DOWN);
            this.Controls.Add(this.picb_3A_DOWN);
            this.Controls.Add(this.picb_2B_DOWN);
            this.Controls.Add(this.picb_2A_DOWN);
            this.Controls.Add(this.picb_1B_DOWN);
            this.Controls.Add(this.picb_1A_DOWN);
            this.Name = "Down_Disk";
            this.Size = new System.Drawing.Size(276, 440);
            ((System.ComponentModel.ISupportInitialize)(this.picb_1B_DOWN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_1A_DOWN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_3A_DOWN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_3B_DOWN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_4A_DOWN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_4B_DOWN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_5A_DOWN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_5B_DOWN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_6A_DOWN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_8B_DOWN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_7A_DOWN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_7B_DOWN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_8A_DOWN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_6B_DOWN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_2A_DOWN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_2B_DOWN)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private CCWin.SkinControl.SkinPictureBox picb_1B_DOWN;
        private CCWin.SkinControl.SkinPictureBox picb_1A_DOWN;
        private CCWin.SkinControl.SkinPictureBox picb_3A_DOWN;
        private CCWin.SkinControl.SkinPictureBox picb_3B_DOWN;
        private CCWin.SkinControl.SkinPictureBox picb_4A_DOWN;
        private CCWin.SkinControl.SkinPictureBox picb_4B_DOWN;
        private CCWin.SkinControl.SkinPictureBox picb_5A_DOWN;
        private CCWin.SkinControl.SkinPictureBox picb_5B_DOWN;
        private CCWin.SkinControl.SkinPictureBox picb_6A_DOWN;
        private CCWin.SkinControl.SkinPictureBox picb_8B_DOWN;
        private CCWin.SkinControl.SkinPictureBox picb_7A_DOWN;
        private CCWin.SkinControl.SkinPictureBox picb_7B_DOWN;
        private CCWin.SkinControl.SkinPictureBox picb_8A_DOWN;
        private CCWin.SkinControl.SkinPictureBox picb_6B_DOWN;
        private CCWin.SkinControl.SkinPictureBox picb_2A_DOWN;
        private CCWin.SkinControl.SkinPictureBox picb_2B_DOWN;
    }
}
