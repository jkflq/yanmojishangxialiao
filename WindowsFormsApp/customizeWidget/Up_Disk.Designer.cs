﻿
namespace WindowsFormsApp.customizeWidget
{
    partial class Up_Disk
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.picb_8B_UP = new CCWin.SkinControl.SkinPictureBox();
            this.picb_8A_UP = new CCWin.SkinControl.SkinPictureBox();
            this.picb_7B_UP = new CCWin.SkinControl.SkinPictureBox();
            this.picb_7A_UP = new CCWin.SkinControl.SkinPictureBox();
            this.picb_6B_UP = new CCWin.SkinControl.SkinPictureBox();
            this.picb_6A_UP = new CCWin.SkinControl.SkinPictureBox();
            this.picb_5B_UP = new CCWin.SkinControl.SkinPictureBox();
            this.picb_5A_UP = new CCWin.SkinControl.SkinPictureBox();
            this.picb_4B_UP = new CCWin.SkinControl.SkinPictureBox();
            this.picb_4A_UP = new CCWin.SkinControl.SkinPictureBox();
            this.picb_3B_UP = new CCWin.SkinControl.SkinPictureBox();
            this.picb_3A_UP = new CCWin.SkinControl.SkinPictureBox();
            this.picb_2B_UP = new CCWin.SkinControl.SkinPictureBox();
            this.picb_2A_UP = new CCWin.SkinControl.SkinPictureBox();
            this.picb_1B_UP = new CCWin.SkinControl.SkinPictureBox();
            this.picb_1A_UP = new CCWin.SkinControl.SkinPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picb_8B_UP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_8A_UP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_7B_UP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_7A_UP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_6B_UP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_6A_UP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_5B_UP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_5A_UP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_4B_UP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_4A_UP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_3B_UP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_3A_UP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_2B_UP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_2A_UP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_1B_UP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_1A_UP)).BeginInit();
            this.SuspendLayout();
            // 
            // picb_8B_UP
            // 
            this.picb_8B_UP.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_8B_UP.Location = new System.Drawing.Point(172, 367);
            this.picb_8B_UP.Name = "picb_8B_UP";
            this.picb_8B_UP.Size = new System.Drawing.Size(63, 45);
            this.picb_8B_UP.TabIndex = 31;
            this.picb_8B_UP.TabStop = false;
            // 
            // picb_8A_UP
            // 
            this.picb_8A_UP.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_8A_UP.Location = new System.Drawing.Point(63, 367);
            this.picb_8A_UP.Name = "picb_8A_UP";
            this.picb_8A_UP.Size = new System.Drawing.Size(63, 45);
            this.picb_8A_UP.TabIndex = 30;
            this.picb_8A_UP.TabStop = false;
            // 
            // picb_7B_UP
            // 
            this.picb_7B_UP.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_7B_UP.Location = new System.Drawing.Point(172, 316);
            this.picb_7B_UP.Name = "picb_7B_UP";
            this.picb_7B_UP.Size = new System.Drawing.Size(63, 45);
            this.picb_7B_UP.TabIndex = 29;
            this.picb_7B_UP.TabStop = false;
            // 
            // picb_7A_UP
            // 
            this.picb_7A_UP.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_7A_UP.Location = new System.Drawing.Point(63, 316);
            this.picb_7A_UP.Name = "picb_7A_UP";
            this.picb_7A_UP.Size = new System.Drawing.Size(63, 45);
            this.picb_7A_UP.TabIndex = 28;
            this.picb_7A_UP.TabStop = false;
            // 
            // picb_6B_UP
            // 
            this.picb_6B_UP.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_6B_UP.Location = new System.Drawing.Point(172, 265);
            this.picb_6B_UP.Name = "picb_6B_UP";
            this.picb_6B_UP.Size = new System.Drawing.Size(63, 45);
            this.picb_6B_UP.TabIndex = 27;
            this.picb_6B_UP.TabStop = false;
            // 
            // picb_6A_UP
            // 
            this.picb_6A_UP.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_6A_UP.Location = new System.Drawing.Point(63, 265);
            this.picb_6A_UP.Name = "picb_6A_UP";
            this.picb_6A_UP.Size = new System.Drawing.Size(63, 45);
            this.picb_6A_UP.TabIndex = 26;
            this.picb_6A_UP.TabStop = false;
            // 
            // picb_5B_UP
            // 
            this.picb_5B_UP.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_5B_UP.Location = new System.Drawing.Point(172, 214);
            this.picb_5B_UP.Name = "picb_5B_UP";
            this.picb_5B_UP.Size = new System.Drawing.Size(63, 45);
            this.picb_5B_UP.TabIndex = 25;
            this.picb_5B_UP.TabStop = false;
            // 
            // picb_5A_UP
            // 
            this.picb_5A_UP.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_5A_UP.Location = new System.Drawing.Point(63, 214);
            this.picb_5A_UP.Name = "picb_5A_UP";
            this.picb_5A_UP.Size = new System.Drawing.Size(63, 45);
            this.picb_5A_UP.TabIndex = 24;
            this.picb_5A_UP.TabStop = false;
            // 
            // picb_4B_UP
            // 
            this.picb_4B_UP.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_4B_UP.Location = new System.Drawing.Point(172, 163);
            this.picb_4B_UP.Name = "picb_4B_UP";
            this.picb_4B_UP.Size = new System.Drawing.Size(63, 45);
            this.picb_4B_UP.TabIndex = 23;
            this.picb_4B_UP.TabStop = false;
            // 
            // picb_4A_UP
            // 
            this.picb_4A_UP.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_4A_UP.Location = new System.Drawing.Point(63, 163);
            this.picb_4A_UP.Name = "picb_4A_UP";
            this.picb_4A_UP.Size = new System.Drawing.Size(63, 45);
            this.picb_4A_UP.TabIndex = 22;
            this.picb_4A_UP.TabStop = false;
            // 
            // picb_3B_UP
            // 
            this.picb_3B_UP.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_3B_UP.Location = new System.Drawing.Point(172, 112);
            this.picb_3B_UP.Name = "picb_3B_UP";
            this.picb_3B_UP.Size = new System.Drawing.Size(63, 45);
            this.picb_3B_UP.TabIndex = 21;
            this.picb_3B_UP.TabStop = false;
            // 
            // picb_3A_UP
            // 
            this.picb_3A_UP.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_3A_UP.Location = new System.Drawing.Point(63, 112);
            this.picb_3A_UP.Name = "picb_3A_UP";
            this.picb_3A_UP.Size = new System.Drawing.Size(63, 45);
            this.picb_3A_UP.TabIndex = 20;
            this.picb_3A_UP.TabStop = false;
            // 
            // picb_2B_UP
            // 
            this.picb_2B_UP.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_2B_UP.Location = new System.Drawing.Point(172, 61);
            this.picb_2B_UP.Name = "picb_2B_UP";
            this.picb_2B_UP.Size = new System.Drawing.Size(63, 45);
            this.picb_2B_UP.TabIndex = 19;
            this.picb_2B_UP.TabStop = false;
            // 
            // picb_2A_UP
            // 
            this.picb_2A_UP.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_2A_UP.Location = new System.Drawing.Point(63, 61);
            this.picb_2A_UP.Name = "picb_2A_UP";
            this.picb_2A_UP.Size = new System.Drawing.Size(63, 45);
            this.picb_2A_UP.TabIndex = 18;
            this.picb_2A_UP.TabStop = false;
            // 
            // picb_1B_UP
            // 
            this.picb_1B_UP.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_1B_UP.Location = new System.Drawing.Point(172, 10);
            this.picb_1B_UP.Name = "picb_1B_UP";
            this.picb_1B_UP.Size = new System.Drawing.Size(63, 45);
            this.picb_1B_UP.TabIndex = 17;
            this.picb_1B_UP.TabStop = false;
            // 
            // picb_1A_UP
            // 
            this.picb_1A_UP.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_1A_UP.Location = new System.Drawing.Point(63, 10);
            this.picb_1A_UP.Name = "picb_1A_UP";
            this.picb_1A_UP.Size = new System.Drawing.Size(63, 45);
            this.picb_1A_UP.TabIndex = 16;
            this.picb_1A_UP.TabStop = false;
            // 
            // Up_Disk
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.picb_8B_UP);
            this.Controls.Add(this.picb_8A_UP);
            this.Controls.Add(this.picb_7B_UP);
            this.Controls.Add(this.picb_7A_UP);
            this.Controls.Add(this.picb_6B_UP);
            this.Controls.Add(this.picb_6A_UP);
            this.Controls.Add(this.picb_5B_UP);
            this.Controls.Add(this.picb_5A_UP);
            this.Controls.Add(this.picb_4B_UP);
            this.Controls.Add(this.picb_4A_UP);
            this.Controls.Add(this.picb_3B_UP);
            this.Controls.Add(this.picb_3A_UP);
            this.Controls.Add(this.picb_2B_UP);
            this.Controls.Add(this.picb_2A_UP);
            this.Controls.Add(this.picb_1B_UP);
            this.Controls.Add(this.picb_1A_UP);
            this.Name = "Up_Disk";
            this.Size = new System.Drawing.Size(292, 422);
            ((System.ComponentModel.ISupportInitialize)(this.picb_8B_UP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_8A_UP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_7B_UP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_7A_UP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_6B_UP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_6A_UP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_5B_UP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_5A_UP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_4B_UP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_4A_UP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_3B_UP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_3A_UP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_2B_UP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_2A_UP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_1B_UP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_1A_UP)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private CCWin.SkinControl.SkinPictureBox picb_8B_UP;
        private CCWin.SkinControl.SkinPictureBox picb_8A_UP;
        private CCWin.SkinControl.SkinPictureBox picb_7B_UP;
        private CCWin.SkinControl.SkinPictureBox picb_7A_UP;
        private CCWin.SkinControl.SkinPictureBox picb_6B_UP;
        private CCWin.SkinControl.SkinPictureBox picb_6A_UP;
        private CCWin.SkinControl.SkinPictureBox picb_5B_UP;
        private CCWin.SkinControl.SkinPictureBox picb_5A_UP;
        private CCWin.SkinControl.SkinPictureBox picb_4B_UP;
        private CCWin.SkinControl.SkinPictureBox picb_4A_UP;
        private CCWin.SkinControl.SkinPictureBox picb_3B_UP;
        private CCWin.SkinControl.SkinPictureBox picb_3A_UP;
        private CCWin.SkinControl.SkinPictureBox picb_2B_UP;
        private CCWin.SkinControl.SkinPictureBox picb_2A_UP;
        private CCWin.SkinControl.SkinPictureBox picb_1B_UP;
        private CCWin.SkinControl.SkinPictureBox picb_1A_UP;
    }
}
