﻿using CCWin.SkinControl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp.components;
using WindowsFormsApp.deviceConnect;

namespace WindowsFormsApp.customizeWidget
{
    public partial class Down_Disk : UserControl,IAutoUpdatable
    {
        public Down_Disk()
        {
            InitializeComponent();
            ConnectConfig.updateList.Add(this);
        }

        bool _index1 = false;
        bool _index2 = false;
        bool _index3 = false;
        bool _index4 = false;
        bool _index5 = false;
        bool _index6 = false;
        bool _index7 = false;
        bool _index8 = false;

        public bool Index1
        {
            get
            {
                return _index1;

            }
            set
            {
                _index1 = value;
                if (value)
                {

                    picb_1A_DOWN.BackColor = Color.Lime;
                    picb_1B_DOWN.BackColor = Color.Lime;

                }
                else
                {
                    picb_1A_DOWN.BackColor = SystemColors.ControlDarkDark;
                    picb_1B_DOWN.BackColor = SystemColors.ControlDarkDark;
                }
             
            }
        }
        public bool Index2
        {
            get
            {
                return _index2;

            }
            set
            {
                _index2 = value;
                if (value)
                {

                    picb_2A_DOWN.BackColor = Color.Lime;
                    picb_2B_DOWN.BackColor = Color.Lime;

                }
                else
                {
                    picb_3A_DOWN.BackColor = SystemColors.ControlDarkDark;
                    picb_3B_DOWN.BackColor = SystemColors.ControlDarkDark;
                }

            }
        }
        public bool Index4
        {
            get
            {
                return _index4;

            }
            set
            {
                _index4 = value;
                if (value)
                {

                    picb_4A_DOWN.BackColor = Color.Lime;
                    picb_4B_DOWN.BackColor = Color.Lime;

                }
                else
                {
                    picb_4A_DOWN.BackColor = SystemColors.ControlDarkDark;
                    picb_4B_DOWN.BackColor = SystemColors.ControlDarkDark;
                }

            }
        }
        public bool Index5
        {
            get
            {
                return _index5;

            }
            set
            {
                _index5 = value;
                if (value)
                {

                    picb_5A_DOWN.BackColor = Color.Lime;
                    picb_5B_DOWN.BackColor = Color.Lime;

                }
                else
                {
                    picb_5A_DOWN.BackColor = SystemColors.ControlDarkDark;
                    picb_5B_DOWN.BackColor = SystemColors.ControlDarkDark;
                }

            }
        }
        public bool Index6
        {
            get
            {
                return _index6;

            }
            set
            {
                _index6 = value;
                if (value)
                {

                    picb_6A_DOWN.BackColor = Color.Lime;
                    picb_6B_DOWN.BackColor = Color.Lime;

                }
                else
                {
                    picb_6A_DOWN.BackColor = SystemColors.ControlDarkDark;
                    picb_6B_DOWN.BackColor = SystemColors.ControlDarkDark;
                }

            }
        }
        public bool Index7
        {
            get
            {
                return _index7;

            }
            set
            {
                _index7 = value;
                if (value)
                {

                    picb_7A_DOWN.BackColor = Color.Lime;
                    picb_7B_DOWN.BackColor = Color.Lime;

                }
                else
                {
                    picb_7A_DOWN.BackColor = SystemColors.ControlDarkDark;
                    picb_7B_DOWN.BackColor = SystemColors.ControlDarkDark;
                }

            }
        }
        public bool Index3
        {
            get
            {
                return _index3;

            }
            set
            {
                _index3 = value;
                if (value)
                {

                    picb_3A_DOWN.BackColor = Color.Lime;
                    picb_3B_DOWN.BackColor = Color.Lime;

                }
                else
                {
                    picb_3A_DOWN.BackColor = SystemColors.ControlDarkDark;
                    picb_3B_DOWN.BackColor = SystemColors.ControlDarkDark;
                }

            }
        }
        public bool Index8
        {
            get
            {
                return _index8;

            }
            set
            {
                _index8 = value;
                if (value)
                {

                    picb_8A_DOWN.BackColor = Color.Lime;
                    picb_8B_DOWN.BackColor = Color.Lime;

                }
                else
                {
                    picb_8A_DOWN.BackColor = SystemColors.ControlDarkDark;
                    picb_8B_DOWN.BackColor = SystemColors.ControlDarkDark;
                }
                
            }
        }

        public void ReFreshState()
        {
            Index1 = ConnectConfig.ControlUnit.ReadCoil(ControlerPortConfig.Down_Disk_Finish_1);
            Index2 = ConnectConfig.ControlUnit.ReadCoil(ControlerPortConfig.Down_Disk_Finish_2);
            Index3 = ConnectConfig.ControlUnit.ReadCoil(ControlerPortConfig.Down_Disk_Finish_3);
            Index4 = ConnectConfig.ControlUnit.ReadCoil(ControlerPortConfig.Down_Disk_Finish_4);
            Index5 = ConnectConfig.ControlUnit.ReadCoil(ControlerPortConfig.Down_Disk_Finish_5);
            Index6 = ConnectConfig.ControlUnit.ReadCoil(ControlerPortConfig.Down_Disk_Finish_6);
            Index7 = ConnectConfig.ControlUnit.ReadCoil(ControlerPortConfig.Down_Disk_Finish_7);
            Index8 = ConnectConfig.ControlUnit.ReadCoil(ControlerPortConfig.Down_Disk_Finish_8);
        }

    }
}
