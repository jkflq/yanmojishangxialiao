﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp.components;
using WindowsFormsApp.deviceConnect;

namespace WindowsFormsApp.customizeWidget
{
    public partial class ModbusView : UserControl,IAutoUpdatable
    {
        public ModbusView()
        {
            InitializeComponent();

            for (int i = 1; i < 170; i++)
            {
                dataGridView1.Rows.Add();
            }
            ConnectConfig.updateList.Add(this);
        }
        public string RemPath
        {
            get;
            set;
        }
        public int Page
        {
            get
            {
                return 0;
            }
        }
        private int _isRegister;   //线圈还是寄存器还是机械臂输入

        private Dictionary<string, string> remTables = new Dictionary<string, string>();//用于存储备注信息

        public void LoadRemAuto(string path)
        {
            using (FileStream fs = File.OpenRead(path))
            {
                IWorkbook workbook;
                string suffix = Path.GetExtension(path);
                if (suffix == ".xls")
                {
                    workbook = new HSSFWorkbook(fs);
                }
                else if (suffix == ".xlsx")
                {
                    workbook = new XSSFWorkbook(fs);
                }
                else
                {
                    return;
                }

                ISheet sheet;
                if (IsRegister == 2)
                {
                    sheet = workbook.GetSheet("控制器寄存器");
                }
                else if (IsRegister == 1)
                {
                    sheet = workbook.GetSheet("控制器线圈");
                }
                else if(IsRegister == 3)
                    sheet = workbook.GetSheet("机械臂输入");
                else if(IsRegister == 4)
                    sheet = workbook.GetSheet("机械臂寄存器");
                else 
                    sheet = workbook.GetSheet("机械臂输出");

                remTables = RenderTable(sheet);
                sheet = null;
                workbook = null;
            }
        }


        /// <summary>
        /// 1 控制器线圈  2 控制器寄存器  3. 机械臂输入
        /// 
        /// </summary>
        public int IsRegister
        {
            get
            {
                return _isRegister;
            }
            set
            {
                _isRegister = value;
                //if (InvokeRequired)
                //{
                //    Invoke(new Action(() =>
                //   {
                //        dataGridView1.Columns[1].HeaderText = value ? "寄存器" : "线圈";
                //    }));
                //}
                if(value == 1)
                    dataGridView1.Columns[1].HeaderText =  "控制器线圈";
                else if(value == 2)
                    dataGridView1.Columns[1].HeaderText = "控制器寄存器";
                else if(value == 3)
                    dataGridView1.Columns[1].HeaderText = "机械臂输入";
                else if (value ==4 )
                    dataGridView1.Columns[1].HeaderText = "机械臂寄存器";
                else if (value == 5)
                    dataGridView1.Columns[1].HeaderText = "机械臂输出";
            }
        }
        public void Read_Path()
        {
            string path = "Modbus_Adress.xls";
            LoadRemAuto(path);
        }
        /*private void button1_Click(object sender, EventArgs e)
        {
            string path = "Modbus_Adress.xls";
            switch (openFileDialog1.ShowDialog())
            {
                case DialogResult.OK:
                    path = openFileDialog1.FileName;
                    break;
                default:
                    return;
            }
            LoadRemAuto(path);
        }*/
        private Dictionary<string, string> RenderTable(ISheet sheet)
        {
            Dictionary<string, string> table = new Dictionary<string, string>();
            int rowCount = sheet.LastRowNum;
            for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++)
            {
             
                var row = sheet.GetRow(i);

                if (row.GetCell(0) != null && row.GetCell(1) != null)
                    table.Add((row.GetCell(0).ToString()), row.GetCell(1).ToString());

            }
            return table;
        }
        
        int AddressChange_Coil(string address)
        {
            string[] add = address.Split('X','W','.');

            return 16384 + int.Parse(add[1]) * 8 + int.Parse(add[2]);
        }
        int AddressChange_Reg(string address)
        {
            string[] add = address.Split('X', 'W', '.');

            return 16384 + int.Parse(add[1]);
        }

        void AddressChange_Robot(string address,ref int[] res)
        {
            string[] add = address.Split(',');
            for(int i = 0;i<add.Length;i++)
            {
                res[i] = int.Parse(add[i]);
            }
        }

        public void Write_Data()
        {
            int i = 0;
            foreach (var item in remTables)
            {
                dataGridView1.Rows[i].Cells[0].Value = item.Key;  //线圈或是寄存器或是机械臂输入的地址
                string val;
                if (remTables.TryGetValue(item.Key, out val))
                {
                    dataGridView1.Rows[i].Cells[2].Value = val;
                }
                else
                {
                    dataGridView1.Rows[i].Cells[2].Value = "";
                }
                i++;
            }
        }
        public void UP_Data()
        {
            int i = 0;
            foreach (var item in remTables)
            {
                int num = 0;
                int[] res = new int[2];
                if (IsRegister == 2)
                {
                    num = AddressChange_Reg(item.Key);
                    dataGridView1.Rows[i].Cells[1].Value = ConnectConfig.ControlUnit.ReadDoubleWord($"{num}");
                }
                else if(IsRegister == 1)
                {
                    num = AddressChange_Coil(item.Key);
                    dataGridView1.Rows[i].Cells[1].Value = ConnectConfig.ControlUnit.ReadCoil($"{num}");
                }
                else if(IsRegister == 3)
                {
                    AddressChange_Robot(item.Key,ref res);

                    if (res[0] == 1)
                        dataGridView1.Rows[i].Cells[1].Value = ConnectConfig.FirstRobot.GetDin(res[1]);
                    else if(res[0] == 2)
                        dataGridView1.Rows[i].Cells[1].Value = ConnectConfig.SecondRobot.GetDin(res[1]);
                    else if(res[0] == 3)
                        dataGridView1.Rows[i].Cells[1].Value = ConnectConfig.ThirdRobot.GetDin(res[1]);
                }
                else if (IsRegister == 4)
                {
                    AddressChange_Robot(item.Key,ref res);

                    if (res[0] == 1)
                        dataGridView1.Rows[i].Cells[1].Value = ConnectConfig.FirstRobot.GetR(res[1]);
                    else if (res[0] == 2)
                        dataGridView1.Rows[i].Cells[1].Value = ConnectConfig.SecondRobot.GetR(res[1]);
                    else if (res[0] == 3)
                        dataGridView1.Rows[i].Cells[1].Value = ConnectConfig.ThirdRobot.GetR(res[1]);
                }
                else if (IsRegister == 5)
                {
                    AddressChange_Robot(item.Key, ref res);
                    if (res[0] == 1)
                        dataGridView1.Rows[i].Cells[1].Value = ConnectConfig.FirstRobot.GetDout(res[1]);
                    else if (res[0] == 2)
                        dataGridView1.Rows[i].Cells[1].Value = ConnectConfig.SecondRobot.GetDout(res[1]);
                    else if (res[0] == 3)
                        dataGridView1.Rows[i].Cells[1].Value = ConnectConfig.ThirdRobot.GetDout(res[1]);
                }
                i++;
            }
        }
        public void ReFreshState()
        {
            UP_Data();
        }
    }
}
