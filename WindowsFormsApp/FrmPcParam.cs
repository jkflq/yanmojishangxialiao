﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp.deviceConnect;
using WindowsFormsApp.utils.XML;
using static WindowsFormsApp.deviceConnect.ParamConfig;

namespace WindowsFormsApp
{
    public partial class FrmPcParam : CCWin.Skin_Color
    {
        public FrmPcParam()
        {
            InitializeComponent();
        }

        List<ParamData> oldfirstrobotparam = new List<ParamData>();
        List<ParamData> oldproductionparam = new List<ParamData>();
        List<ParamData> oldsecondrobotparam = new List<ParamData>();
        List<ParamData> oldthirdrobotparam = new List<ParamData>();
        List<ParamData> newfirstrobotparam = new List<ParamData>();
        List<ParamData> newsecondrobotparam = new List<ParamData>();
        List<ParamData> newthirdrobotparam = new List<ParamData>();
        List<ParamData> newproductiondatas = new List<ParamData>();

        private void FrmPcParam_Load(object sender, EventArgs e)
        {
          
            oldfirstrobotparam = myXML.LoadPCParams("FirstRobotParamData");
            this.DataGridView_Take.DataSource = oldfirstrobotparam;

            oldsecondrobotparam = myXML.LoadPCParams("SecondRobotParamData");
            this.DataGridView_Up.DataSource = oldsecondrobotparam;

            oldthirdrobotparam = myXML.LoadPCParams("ThirdRobotParamData");
            this.DataGridView_Down.DataSource = oldthirdrobotparam;

            oldproductionparam = myXML.LoadPCParams("ProductionParamData");
            this.skinDataGridView3.DataSource = oldproductionparam;

      
        }

        private void Btn_Write_Take_Click(object sender, EventArgs e)
        {
            if(!ConnectConfig.FirstRobot.Connected)
            {
                MessageBox.Show("请先连接设备");
                return;
            }
            //上下料机械臂参数
            newfirstrobotparam = DataGridView_Take.DataSource as List<ParamData>;//隐士转换


            //更新参数并加载到文件和寄存器里
            UpdataFirstRobotParam(newfirstrobotparam);

            MessageBox.Show("写入成功");
        }

        /// <summary>
        /// 保存上下料机械臂参数到寄存器和文件
        /// </summary>
        /// <param name="oldData"></param>
        /// <param name="newData"></param>
        private void UpdataFirstRobotParam(List<ParamData> newData)
        {
            bool isChange = false;
            oldfirstrobotparam = myXML.LoadPCParams("FirstRobotParamData");
            for (int i = 0;i< oldfirstrobotparam.Count;i++)
            {
                if(oldfirstrobotparam[i].ParamValue != newData[i].ParamValue)
                {
                    isChange = true;
                    switch (oldfirstrobotparam[i].ParamName)
                    {
                        case "自动运行速度":
                            RobotConnect.SetR(ConnectConfig.FirstRobot, AutoRun_R_REG, newData[i].ParamValue);
                            break;
                        case "下料点下降高度":
                            Hsc3.Comm.GeneralPos curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.XiaLiaoHeight_LR_REG);
                            curpos.vecPos[2] = -newData[i].ParamValue;
                            RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.XiaLiaoHeight_LR_REG);
                            break;
                        case "上料点吸取延时":
                            RobotConnect.SetR(ConnectConfig.FirstRobot, SuckDelay_R_REG, newData[i].ParamValue);
                            break;
                    }
                }    
            }
            if(isChange)
            {
                //如果有改变,则写入到参数文件中
                myXML.SavePCConfig(newData, "FirstRobotParamData");
                //保存LR寄存器值
                RobotConnect.SaveData(ConnectConfig.FirstRobot, "LR");
                isChange = false;
            }

        }
        private void UpdataSecondRobotParam(List<ParamData> newData)
        {
            bool isChange = false;
            oldsecondrobotparam = myXML.LoadPCParams("SecondRobotParamData");
            for (int i = 0; i < oldsecondrobotparam.Count; i++)
            {
                if (oldsecondrobotparam[i].ParamValue != newData[i].ParamValue)
                {
                    isChange = true;
                    switch (oldsecondrobotparam[i].ParamName)
                    {
                        case "自动运行速度":
                            RobotConnect.SetR(ConnectConfig.SecondRobot, AutoRun_R_REG, newData[i].ParamValue);
                            break;
                        default:
                            break;
                    }
                }
            }
            if (isChange)
            {
                //如果有改变,则写入到参数文件中
                myXML.SavePCConfig(newData, "SecondRobotParamData");
                //保存LR寄存器值
                RobotConnect.SaveData(ConnectConfig.SecondRobot, "LR");
                isChange = false;
            }

        }
        private void UpdataThirdRobotParam(List<ParamData> newData)
        {
            bool isChange = false;
            oldthirdrobotparam = myXML.LoadPCParams("ThirdRobotParamData");
            for (int i = 0; i < oldthirdrobotparam.Count; i++)
            {
                if (oldthirdrobotparam[i].ParamValue != newData[i].ParamValue)
                {
                    isChange = true;
                    switch (oldthirdrobotparam[i].ParamName)
                    {
                        case "自动运行速度":
                            RobotConnect.SetR(ConnectConfig.ThirdRobot, AutoRun_R_REG, newData[i].ParamValue);
                            break;
                        default:
                            break;
                    }
                }
            }
            if (isChange)
            {
                //如果有改变,则写入到参数文件中
                myXML.SavePCConfig(newData, "ThirdRobotParamData");
                //保存LR寄存器值
                RobotConnect.SaveData(ConnectConfig.ThirdRobot, "LR");
                isChange = false;
            }

        }


        /// <summary>
        /// 保存产品参数到寄存器和文件
        /// </summary>
        /// <param name="oldData"></param>
        /// <param name="newData"></param>
        private void UpdataProductionParam( List<ParamData> newData)
        {
            bool isChange = false;
            oldproductionparam = myXML.LoadPCParams("ProductionParamData");
            for (int i = 0; i < oldproductionparam.Count(); i++)
            {
                if (oldproductionparam[i].ParamValue != newData[i].ParamValue )
                {
                    isChange = true;
                    Hsc3.Comm.GeneralPos curpos;

                    if (ParamConfig.DiskType == ProductionType.Pro_97R)
                    {
                        switch (newData[i].ParamName)
                        {

                            case "97R下料抓取下降高度":
                                curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", DropHeight_LR_REG);
                                curpos.vecPos[2] = -newData[i].ParamValue;
                                RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, DropHeight_LR_REG);
                                break;
                            case "97R上料点下降高度":

                                curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ShangLiaoHeight_LR_REG);
                                curpos.vecPos[2] = -newData[i].ParamValue;
                                RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ShangLiaoHeight_LR_REG);
                                break;
                            case "97R上料释放下降高度":
                                foreach (var LR in ParamConfig.Offset_Height_LR_REG)
                                {
                                    curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", LR);
                                    curpos.vecPos[2] = -newData[i].ParamValue;
                                    RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, LR);
                                }
                                break;
                            case "97R吸取延时":
                                RobotConnect.SetR(ConnectConfig.FirstRobot, ParamConfig.ProductionSuckDelay_R_REG, newData[i].ParamValue);
                                break;
                            default:
                                break;
                        }
                    }
                    else if (ParamConfig.DiskType == ProductionType.Pro_97U)
                    {
                        switch (newData[i].ParamName)
                        {

                            case "97U下料抓取下降高度":
                                curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", DropHeight_LR_REG);
                                curpos.vecPos[2] = -newData[i].ParamValue;
                                RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, DropHeight_LR_REG);
                                break;
                
                            case "97U上料点下降高度":
                                curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ShangLiaoHeight_LR_REG);
                                curpos.vecPos[2] = -newData[i].ParamValue;
                                RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ShangLiaoHeight_LR_REG);
                                break;
              
                            case "97U上料释放下降高度":

                                foreach (var LR in ParamConfig.Offset_Height_LR_REG)
                                {
                                    curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", LR);
                                    curpos.vecPos[2] = -newData[i].ParamValue;
                                    RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, LR);
                                }
                                break;
           
                            case "97U吸取延时":
                                RobotConnect.SetR(ConnectConfig.FirstRobot, ParamConfig.ProductionSuckDelay_R_REG, newData[i].ParamValue);
                                break;
                            default:
                                break;

                        }
                    }


                }
            }
            if (isChange)
            {
                //如果有改变,则写入到参数文件中
                myXML.SavePCConfig(newData, "ProductionParamData");
                //保存LR寄存器值
                RobotConnect.SaveData(ConnectConfig.FirstRobot, "LR");
                isChange = false;
            }
        }


        private void Btn_Close_Take_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Btn_Write_Up_Click(object sender, EventArgs e)
        {
            if(!ConnectConfig.SecondRobot.Connected)
            {
                MessageBox.Show("请先连接设备");
                return;
            }
          
            //上料机械臂参数
            newsecondrobotparam = DataGridView_Up.DataSource as List<ParamData>;
  
            //更新参数并加载到文件和寄存器里
            UpdataSecondRobotParam(newsecondrobotparam);

            MessageBox.Show("写入成功");
        }

        private void Btn_Write_Down_Click(object sender, EventArgs e)
        {
            if(!ConnectConfig.ThirdRobot.Connected)
            {
                MessageBox.Show("请先连接设备");
                return;
            }

            //下料机械臂参数
            newthirdrobotparam = DataGridView_Down.DataSource as List<ParamData>;


            //更新参数并加载到文件和寄存器里
            UpdataThirdRobotParam(newthirdrobotparam);


            MessageBox.Show("写入成功");
        }

        private void Btn_Write_Prt_Click(object sender, EventArgs e)
        {

            //下料机械臂参数
            newproductiondatas = DataGridView_Down.DataSource as List<ParamData>;


            //更新参数并加载到文件和寄存器里
            UpdataProductionParam(newproductiondatas);

            MessageBox.Show("写入成功");
        }

        private void Btn_Close_Prt_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Btn_Close_Down_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Btn_Close_Up_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
