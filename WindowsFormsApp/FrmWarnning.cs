﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp.deviceConnect;
using WinFormsBase;

namespace WindowsFormsApp
{
    public partial class FrmWarnning : CCWin.Skin_Color
    {
        FrmWorking _frmworking;
        public FrmWarnning( FrmWorking frmWorking)
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | //不擦除背景
            ControlStyles.OptimizedDoubleBuffer | //双缓冲
            ControlStyles.UserPaint,    //使用自定义重绘事件
            true);
            this.UpdateStyles();
            InitializeComponent();
            _frmworking = frmWorking;

        }
        private void ControlerCancelAlarm()
        {
            if (ConnectConfig.ControlUnit.ReadCoil(ControlerPortConfig.UPDiskEMotor_WanningCoil))
            {
                ConnectConfig.ControlUnit.Write(ControlerPortConfig.UPDiskEMotor_Cancel_Wanning, true);
                Thread.Sleep(10);
                ConnectConfig.ControlUnit.Write(ControlerPortConfig.UPDiskEMotor_Cancel_Wanning, false);
            }
            if (ConnectConfig.ControlUnit.ReadCoil(ControlerPortConfig.DOWNDiskEMotor_WanningCoil))
            {
                ConnectConfig.ControlUnit.Write(ControlerPortConfig.DOWNDiskEMotor_Cancel_Wanning, true);
                Thread.Sleep(10);
                ConnectConfig.ControlUnit.Write(ControlerPortConfig.DOWNDiskEMotor_Cancel_Wanning, false);
            }
            if (ConnectConfig.ControlUnit.ReadCoil(ControlerPortConfig.TAKEDiskEMotor_WanningCoil))
            {
                ConnectConfig.ControlUnit.Write(ControlerPortConfig.TAKEDiskEMotor_Cancel_Wanning, true);
                Thread.Sleep(10);
                ConnectConfig.ControlUnit.Write(ControlerPortConfig.TAKEDiskEMotor_Cancel_Wanning, false);
            }
        }
        private void ucBtnExt1_BtnClick(object sender, EventArgs e)
        {

            ParamConfig.WarnFlag = false;
            _frmworking.OldObjectRange = 0;
            //清除文本
            rtfRichTextBox1.Text = " ";
            _frmworking.ucRollText1.Text = " ";
            _frmworking.mysaga.warnMsg.Clear();
           
            //启动按钮灯灭
            RobotConnect.SetDout(ConnectConfig.FirstRobot, 0, false);

            //复位机械臂  
            RobotConnect.Reset(ConnectConfig.FirstRobot);
            RobotConnect.Reset(ConnectConfig.SecondRobot);
            RobotConnect.Reset(ConnectConfig.ThirdRobot);
            //复位设备
            ControlerCancelAlarm();

            //运行指示灯灭
            // RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_RUN_LIGHT, false);

        }

        private void FrmWarnning_FormClosed(object sender, FormClosedEventArgs e)
        {
            _frmworking.ucBtnExt1.Enabled = true;
        }

        private void FrmWarnning_Load(object sender, EventArgs e)
        {
            //显示报警信息
            foreach(var i in _frmworking.mysaga.warnMsg)
            {
                rtfRichTextBox1.Text += i;
                rtfRichTextBox1.Text += "\r\n";
            }
        }
    }
}
