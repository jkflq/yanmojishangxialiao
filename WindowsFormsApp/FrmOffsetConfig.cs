﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using WindowsFormsApp.deviceConnect;
using WindowsFormsApp.utils.XML;
using static WindowsFormsApp.deviceConnect.ParamConfig;

namespace WindowsFormsApp
{
    public partial class FrmOffsetConfig : CCWin.Skin_Color
    {
        public FrmOffsetConfig()
        {
            InitializeComponent();
        }
        //保存原始数据
        List<OffsetData> oldFirstRobotData = new List<OffsetData>();
        List<OffsetData> oldSecondRobotData = new List<OffsetData>();
        List<OffsetData> oldThirdRobotData = new List<OffsetData>();


        private void FrmOffsetConfig_Load(object sender, EventArgs e)
        {
            //加载文件中offset数据
            oldFirstRobotData = myXML.LoadOffsetConfig("FirstOffsetData");
            oldSecondRobotData = myXML.LoadOffsetConfig("SecondOffsetData");
            oldThirdRobotData = myXML.LoadOffsetConfig("ThirdOffsetData");
            //获取原始数据
            DataGridView_Take.DataSource = oldFirstRobotData;
            DataGridView_Up.DataSource = oldSecondRobotData;
            DataGridView_Down.DataSource = oldThirdRobotData;
        }

        private void skinButton2_Click(object sender, EventArgs e)
        {
            if (!ConnectConfig.FirstRobot.Connected)
            {
                MessageBox.Show("请先连接设备");
                return;
            }

            //获取新数据
            List<OffsetData> newDatas = DataGridView_Take.DataSource as List<OffsetData>;

            //更新机械手offset LR寄存器的值


            UpdataRobotData(newDatas);

            //必须先保存文件再加载参数
            //将新数据保存到文件
            //myXML.SaveOffsetConfig(newDatas);

          
            //更新参数值
            //ParamConfig.UpdateOffsetParam();

            MessageBox.Show("写入成功");
        }


        private void UpdataRobotData(List<OffsetData> newData)
        {
            bool isChange = false;
            oldFirstRobotData = myXML.LoadOffsetConfig("FirstOffsetData");
            for (int i = 0;i<oldFirstRobotData.Count();i++)
            {
                if(oldFirstRobotData[i].Xoffset != newData[i].Xoffset || oldFirstRobotData[i].Yoffset != newData[i].Yoffset|| oldFirstRobotData[i].Angeloffset != newData[i].Angeloffset)
                {
                    isChange =  true;
                    Hsc3.Comm.GeneralPos dirpos;
                    if (i<5)
                    {
                        //保存变化的点
                        dirpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.Offset_LR_REG[i]);
                        dirpos.vecPos[0] = newData[i].Xoffset;
                        dirpos.vecPos[1] = newData[i].Yoffset;
                        dirpos.vecPos[3] = newData[i].Angeloffset;
                        RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, dirpos, ParamConfig.Offset_LR_REG[i]);
                        dirpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.Offset_Height_LR_REG[i]);
                        dirpos.vecPos[0] = newData[i].Xoffset;
                        dirpos.vecPos[1] = newData[i].Yoffset;
                        dirpos.vecPos[3] = newData[i].Angeloffset;
                        RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, dirpos, ParamConfig.Offset_Height_LR_REG[i]);
                    }
                    else
                    {
                        dirpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.Offset_LR_REG[i]);
                        dirpos.vecPos[0] += newData[i].Xoffset;
                        dirpos.vecPos[1] += newData[i].Yoffset;
                        dirpos.vecPos[3] += newData[i].Angeloffset;
                        RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, dirpos, ParamConfig.Offset_LR_REG[i]);
                    }
                }
            }

            if(isChange)
            {
                //如果有改变,则写入到参数文件中
                myXML.SaveOffsetConfig(newData);
                //保存LR寄存器值
                RobotConnect.SaveData(ConnectConfig.FirstRobot, "LR");
                isChange = false;
            }
        }



        private void skinButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
