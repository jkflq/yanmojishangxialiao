﻿
using CCWin.SkinControl;
using System.Collections.Generic;

namespace WinFormsBase
{
    partial class FrmWorking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btn_Init = new HZH_Controls.Controls.UCBtnExt();
            this.btn_Start = new HZH_Controls.Controls.UCSwitch();
            this.ucCheckBox1 = new HZH_Controls.Controls.UCCheckBox();
            this.ucCheckBox2 = new HZH_Controls.Controls.UCCheckBox();
            this.ucPanelTitle3 = new HZH_Controls.Controls.UCPanelTitle();
            this.skinButton4 = new CCWin.SkinControl.SkinButton();
            this.skinButton2 = new CCWin.SkinControl.SkinButton();
            this.ucRollText1 = new HZH_Controls.Controls.UCRollText();
            this.ucBtnExt1 = new HZH_Controls.Controls.UCBtnExt();
            this.ucTextBoxEx1 = new HZH_Controls.Controls.UCTextBoxEx();
            this.skinPanel1 = new CCWin.SkinControl.SkinPanel();
            this.skinLabel4 = new CCWin.SkinControl.SkinLabel();
            this.ucCheckBox6 = new HZH_Controls.Controls.UCCheckBox();
            this.skinPanel3 = new CCWin.SkinControl.SkinPanel();
            this.ucCheckBox9 = new HZH_Controls.Controls.UCCheckBox();
            this.skinLabel6 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel5 = new CCWin.SkinControl.SkinLabel();
            this.Alarm_Lamp = new HZH_Controls.Controls.UCAlarmLamp();
            this.ucCheckBox7 = new HZH_Controls.Controls.UCCheckBox();
            this.ucCheckBox8 = new HZH_Controls.Controls.UCCheckBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.UserStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.切换用户ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退出登录ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SetStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_OffsetConfig = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_PCParm = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.初始化ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.设备状态检测ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DebugStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.skinPanel5 = new CCWin.SkinControl.SkinPanel();
            this.skinPanel4 = new CCWin.SkinControl.SkinPanel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lab_FirstRobot = new System.Windows.Forms.ToolStripStatusLabel();
            this.lab_FirstRobotStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lab_SecondRobot = new System.Windows.Forms.ToolStripStatusLabel();
            this.lab_SecondRobotStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lab_ThirdRobot = new System.Windows.Forms.ToolStripStatusLabel();
            this.lab_ThirdRobotStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lab_Controler = new System.Windows.Forms.ToolStripStatusLabel();
            this.lab_ControlerStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel10 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel9 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel11 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.skinGroupBox2 = new CCWin.SkinControl.SkinGroupBox();
            this.skinGroupBox4 = new CCWin.SkinControl.SkinGroupBox();
            this.btn_CountZero = new CCWin.SkinControl.SkinButton();
            this.lab_PutNum = new System.Windows.Forms.Label();
            this.lab_PutNum1 = new System.Windows.Forms.Label();
            this.txt_Log = new System.Windows.Forms.TextBox();
            this.lab_TakeNum = new System.Windows.Forms.Label();
            this.lab_TakeNum1 = new System.Windows.Forms.Label();
            this.skinGroupBox5 = new CCWin.SkinControl.SkinGroupBox();
            this.totalProcessStep = new HZH_Controls.Controls.UCStep();
            this.skinGroupBox3 = new CCWin.SkinControl.SkinGroupBox();
            this.up_Disk1 = new WindowsFormsApp.customizeWidget.Up_Disk();
            this.skinGroupBox1 = new CCWin.SkinControl.SkinGroupBox();
            this.down_Disk2 = new WindowsFormsApp.customizeWidget.Down_Disk();
            this.down_Disk1 = new WindowsFormsApp.customizeWidget.Down_Disk();
            this.button1 = new System.Windows.Forms.Button();
            this.ucPanelTitle3.SuspendLayout();
            this.skinPanel1.SuspendLayout();
            this.skinPanel3.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.skinPanel5.SuspendLayout();
            this.skinPanel4.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.skinGroupBox4.SuspendLayout();
            this.skinGroupBox5.SuspendLayout();
            this.skinGroupBox3.SuspendLayout();
            this.skinGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Init
            // 
            this.btn_Init.BackColor = System.Drawing.Color.White;
            this.btn_Init.BtnBackColor = System.Drawing.Color.White;
            this.btn_Init.BtnFont = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Init.BtnForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_Init.BtnText = "设备连接";
            this.btn_Init.ConerRadius = 20;
            this.btn_Init.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Init.EnabledMouseEffect = false;
            this.btn_Init.FillColor = System.Drawing.Color.Red;
            this.btn_Init.Font = new System.Drawing.Font("微软雅黑", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Init.IsRadius = true;
            this.btn_Init.IsShowRect = true;
            this.btn_Init.IsShowTips = false;
            this.btn_Init.Location = new System.Drawing.Point(295, 86);
            this.btn_Init.Margin = new System.Windows.Forms.Padding(0);
            this.btn_Init.Name = "btn_Init";
            this.btn_Init.RectColor = System.Drawing.Color.Gray;
            this.btn_Init.RectWidth = 1;
            this.btn_Init.Size = new System.Drawing.Size(135, 80);
            this.btn_Init.TabIndex = 42;
            this.btn_Init.TabStop = false;
            this.btn_Init.TipsColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(30)))), ((int)(((byte)(99)))));
            this.btn_Init.TipsText = "";
            this.btn_Init.BtnClick += new System.EventHandler(this.btn_Init_BtnClick);
            // 
            // btn_Start
            // 
            this.btn_Start.AutoSize = true;
            this.btn_Start.BackColor = System.Drawing.Color.Transparent;
            this.btn_Start.Checked = false;
            this.btn_Start.FalseColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(189)))), ((int)(((byte)(189)))));
            this.btn_Start.FalseTextColr = System.Drawing.Color.Black;
            this.btn_Start.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Start.Location = new System.Drawing.Point(8, 94);
            this.btn_Start.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Start.Name = "btn_Start";
            this.btn_Start.Size = new System.Drawing.Size(158, 74);
            this.btn_Start.SwitchType = HZH_Controls.Controls.SwitchType.Ellipse;
            this.btn_Start.TabIndex = 43;
            this.btn_Start.Texts = new string[] {
        "自动",
        "手动"};
            this.btn_Start.TrueColor = System.Drawing.Color.Lime;
            this.btn_Start.TrueTextColr = System.Drawing.Color.Black;
            this.btn_Start.Click += new System.EventHandler(this.btn_Start_Click);
            // 
            // ucCheckBox1
            // 
            this.ucCheckBox1.AutoSize = true;
            this.ucCheckBox1.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.ucCheckBox1.BackColor = System.Drawing.Color.Transparent;
            this.ucCheckBox1.Checked = false;
            this.ucCheckBox1.Font = new System.Drawing.Font("宋体", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ucCheckBox1.Location = new System.Drawing.Point(18, 2);
            this.ucCheckBox1.Margin = new System.Windows.Forms.Padding(2);
            this.ucCheckBox1.Name = "ucCheckBox1";
            this.ucCheckBox1.Padding = new System.Windows.Forms.Padding(1);
            this.ucCheckBox1.Size = new System.Drawing.Size(90, 43);
            this.ucCheckBox1.TabIndex = 68;
            this.ucCheckBox1.TextValue = "首盘";
            this.ucCheckBox1.CheckedChangeEvent += new System.EventHandler(this.ucCheckBox1_CheckedChangeEvent);
            // 
            // ucCheckBox2
            // 
            this.ucCheckBox2.AutoSize = true;
            this.ucCheckBox2.BackColor = System.Drawing.Color.Transparent;
            this.ucCheckBox2.Checked = false;
            this.ucCheckBox2.Font = new System.Drawing.Font("宋体", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ucCheckBox2.Location = new System.Drawing.Point(18, 44);
            this.ucCheckBox2.Margin = new System.Windows.Forms.Padding(2);
            this.ucCheckBox2.Name = "ucCheckBox2";
            this.ucCheckBox2.Padding = new System.Windows.Forms.Padding(1);
            this.ucCheckBox2.Size = new System.Drawing.Size(90, 34);
            this.ucCheckBox2.TabIndex = 69;
            this.ucCheckBox2.TextValue = "末盘";
            this.ucCheckBox2.CheckedChangeEvent += new System.EventHandler(this.ucCheckBox2_CheckedChangeEvent);
            // 
            // ucPanelTitle3
            // 
            this.ucPanelTitle3.BackColor = System.Drawing.Color.Transparent;
            this.ucPanelTitle3.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.ucPanelTitle3.ConerRadius = 10;
            this.ucPanelTitle3.Controls.Add(this.skinButton4);
            this.ucPanelTitle3.Controls.Add(this.skinButton2);
            this.ucPanelTitle3.Enabled = false;
            this.ucPanelTitle3.FillColor = System.Drawing.SystemColors.Info;
            this.ucPanelTitle3.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ucPanelTitle3.IsCanExpand = false;
            this.ucPanelTitle3.IsExpand = false;
            this.ucPanelTitle3.IsRadius = true;
            this.ucPanelTitle3.IsShowRect = true;
            this.ucPanelTitle3.Location = new System.Drawing.Point(20, 95);
            this.ucPanelTitle3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ucPanelTitle3.Name = "ucPanelTitle3";
            this.ucPanelTitle3.Padding = new System.Windows.Forms.Padding(1);
            this.ucPanelTitle3.RectColor = System.Drawing.SystemColors.ActiveCaption;
            this.ucPanelTitle3.RectWidth = 1;
            this.ucPanelTitle3.Size = new System.Drawing.Size(310, 136);
            this.ucPanelTitle3.TabIndex = 75;
            this.ucPanelTitle3.Title = "高度标定";
            // 
            // skinButton4
            // 
            this.skinButton4.AutoSize = true;
            this.skinButton4.BackColor = System.Drawing.Color.Transparent;
            this.skinButton4.BaseColor = System.Drawing.Color.Red;
            this.skinButton4.BorderColor = System.Drawing.Color.Black;
            this.skinButton4.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinButton4.DownBack = null;
            this.skinButton4.Enabled = false;
            this.skinButton4.Font = new System.Drawing.Font("微软雅黑", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinButton4.ForeColor = System.Drawing.Color.SeaShell;
            this.skinButton4.Location = new System.Drawing.Point(200, 52);
            this.skinButton4.MouseBack = null;
            this.skinButton4.Name = "skinButton4";
            this.skinButton4.NormlBack = null;
            this.skinButton4.Size = new System.Drawing.Size(98, 59);
            this.skinButton4.TabIndex = 64;
            this.skinButton4.Text = "校准";
            this.skinButton4.UseVisualStyleBackColor = false;
            this.skinButton4.Click += new System.EventHandler(this.skinButton4_Click);
            // 
            // skinButton2
            // 
            this.skinButton2.AutoSize = true;
            this.skinButton2.BackColor = System.Drawing.Color.Transparent;
            this.skinButton2.BaseColor = System.Drawing.Color.DodgerBlue;
            this.skinButton2.BorderColor = System.Drawing.Color.Black;
            this.skinButton2.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinButton2.DownBack = null;
            this.skinButton2.Font = new System.Drawing.Font("微软雅黑", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinButton2.ForeColor = System.Drawing.Color.Black;
            this.skinButton2.Location = new System.Drawing.Point(19, 52);
            this.skinButton2.MouseBack = null;
            this.skinButton2.Name = "skinButton2";
            this.skinButton2.NormlBack = null;
            this.skinButton2.Size = new System.Drawing.Size(175, 59);
            this.skinButton2.TabIndex = 76;
            this.skinButton2.Text = "移动到校准点";
            this.skinButton2.UseVisualStyleBackColor = false;
            this.skinButton2.Click += new System.EventHandler(this.skinButton2_Click_1);
            // 
            // ucRollText1
            // 
            this.ucRollText1.Font = new System.Drawing.Font("宋体", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ucRollText1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(77)))), ((int)(((byte)(59)))));
            this.ucRollText1.Location = new System.Drawing.Point(41, 175);
            this.ucRollText1.Margin = new System.Windows.Forms.Padding(2);
            this.ucRollText1.MoveSleepTime = 100;
            this.ucRollText1.Name = "ucRollText1";
            this.ucRollText1.RollStyle = HZH_Controls.Controls.RollStyle.LeftToRight;
            this.ucRollText1.Size = new System.Drawing.Size(867, 37);
            this.ucRollText1.TabIndex = 78;
            this.ucRollText1.Text = " ";
            this.ucRollText1.Load += new System.EventHandler(this.ucRollText1_Load);
            // 
            // ucBtnExt1
            // 
            this.ucBtnExt1.AutoSize = true;
            this.ucBtnExt1.BackColor = System.Drawing.Color.White;
            this.ucBtnExt1.BtnBackColor = System.Drawing.Color.White;
            this.ucBtnExt1.BtnFont = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ucBtnExt1.BtnForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ucBtnExt1.BtnText = "报警查看";
            this.ucBtnExt1.ConerRadius = 20;
            this.ucBtnExt1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ucBtnExt1.EnabledMouseEffect = false;
            this.ucBtnExt1.FillColor = System.Drawing.Color.Red;
            this.ucBtnExt1.Font = new System.Drawing.Font("微软雅黑", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ucBtnExt1.IsRadius = true;
            this.ucBtnExt1.IsShowRect = true;
            this.ucBtnExt1.IsShowTips = false;
            this.ucBtnExt1.Location = new System.Drawing.Point(455, 86);
            this.ucBtnExt1.Margin = new System.Windows.Forms.Padding(0);
            this.ucBtnExt1.Name = "ucBtnExt1";
            this.ucBtnExt1.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(77)))), ((int)(((byte)(58)))));
            this.ucBtnExt1.RectWidth = 1;
            this.ucBtnExt1.Size = new System.Drawing.Size(138, 80);
            this.ucBtnExt1.TabIndex = 71;
            this.ucBtnExt1.TabStop = false;
            this.ucBtnExt1.TipsColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(30)))), ((int)(((byte)(99)))));
            this.ucBtnExt1.TipsText = "";
            this.ucBtnExt1.BtnClick += new System.EventHandler(this.ucBtnExt1_BtnClick);
            // 
            // ucTextBoxEx1
            // 
            this.ucTextBoxEx1.AutoSize = true;
            this.ucTextBoxEx1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ucTextBoxEx1.ConerRadius = 5;
            this.ucTextBoxEx1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ucTextBoxEx1.DecLength = 2;
            this.ucTextBoxEx1.FillColor = System.Drawing.Color.Empty;
            this.ucTextBoxEx1.FocusBorderColor = System.Drawing.SystemColors.ActiveBorder;
            this.ucTextBoxEx1.Font = new System.Drawing.Font("微软雅黑", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ucTextBoxEx1.InputText = "";
            this.ucTextBoxEx1.InputType = HZH_Controls.TextInputType.Number;
            this.ucTextBoxEx1.IsFocusColor = true;
            this.ucTextBoxEx1.IsRadius = true;
            this.ucTextBoxEx1.IsShowClearBtn = true;
            this.ucTextBoxEx1.IsShowKeyboard = true;
            this.ucTextBoxEx1.IsShowRect = true;
            this.ucTextBoxEx1.IsShowSearchBtn = false;
            this.ucTextBoxEx1.KeyBoardType = HZH_Controls.Controls.KeyBoardType.UCKeyBorderNum;
            this.ucTextBoxEx1.Location = new System.Drawing.Point(118, 16);
            this.ucTextBoxEx1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ucTextBoxEx1.MaxValue = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.ucTextBoxEx1.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ucTextBoxEx1.Name = "ucTextBoxEx1";
            this.ucTextBoxEx1.Padding = new System.Windows.Forms.Padding(4);
            this.ucTextBoxEx1.PasswordChar = '\0';
            this.ucTextBoxEx1.PromptColor = System.Drawing.Color.Gray;
            this.ucTextBoxEx1.PromptFont = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.ucTextBoxEx1.PromptText = "";
            this.ucTextBoxEx1.RectColor = System.Drawing.Color.Gray;
            this.ucTextBoxEx1.RectWidth = 1;
            this.ucTextBoxEx1.RegexPattern = "";
            this.ucTextBoxEx1.Size = new System.Drawing.Size(138, 61);
            this.ucTextBoxEx1.TabIndex = 80;
            // 
            // skinPanel1
            // 
            this.skinPanel1.BackColor = System.Drawing.Color.Transparent;
            this.skinPanel1.Controls.Add(this.ucTextBoxEx1);
            this.skinPanel1.Controls.Add(this.skinLabel4);
            this.skinPanel1.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel1.DownBack = null;
            this.skinPanel1.Enabled = false;
            this.skinPanel1.Location = new System.Drawing.Point(717, 65);
            this.skinPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.skinPanel1.MouseBack = null;
            this.skinPanel1.Name = "skinPanel1";
            this.skinPanel1.NormlBack = null;
            this.skinPanel1.Size = new System.Drawing.Size(283, 94);
            this.skinPanel1.TabIndex = 81;
            // 
            // skinLabel4
            // 
            this.skinLabel4.AutoSize = true;
            this.skinLabel4.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel4.BorderColor = System.Drawing.Color.White;
            this.skinLabel4.Font = new System.Drawing.Font("微软雅黑", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel4.Location = new System.Drawing.Point(2, 28);
            this.skinLabel4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.skinLabel4.Name = "skinLabel4";
            this.skinLabel4.Size = new System.Drawing.Size(107, 25);
            this.skinLabel4.TabIndex = 81;
            this.skinLabel4.Text = "剩余盘数：";
            // 
            // ucCheckBox6
            // 
            this.ucCheckBox6.AutoSize = true;
            this.ucCheckBox6.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.ucCheckBox6.BackColor = System.Drawing.Color.Transparent;
            this.ucCheckBox6.Checked = false;
            this.ucCheckBox6.Font = new System.Drawing.Font("微软雅黑", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ucCheckBox6.Location = new System.Drawing.Point(27, 237);
            this.ucCheckBox6.Margin = new System.Windows.Forms.Padding(2);
            this.ucCheckBox6.Name = "ucCheckBox6";
            this.ucCheckBox6.Padding = new System.Windows.Forms.Padding(1);
            this.ucCheckBox6.Size = new System.Drawing.Size(266, 35);
            this.ucCheckBox6.TabIndex = 85;
            this.ucCheckBox6.TextValue = "相机高度标定";
            this.ucCheckBox6.CheckedChangeEvent += new System.EventHandler(this.ucCheckBox6_CheckedChangeEvent);
            // 
            // skinPanel3
            // 
            this.skinPanel3.BackColor = System.Drawing.Color.Transparent;
            this.skinPanel3.Controls.Add(this.ucCheckBox9);
            this.skinPanel3.Controls.Add(this.skinLabel6);
            this.skinPanel3.Controls.Add(this.skinLabel5);
            this.skinPanel3.Controls.Add(this.ucCheckBox6);
            this.skinPanel3.Controls.Add(this.ucPanelTitle3);
            this.skinPanel3.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel3.DownBack = null;
            this.skinPanel3.Location = new System.Drawing.Point(3, 218);
            this.skinPanel3.MouseBack = null;
            this.skinPanel3.Name = "skinPanel3";
            this.skinPanel3.NormlBack = null;
            this.skinPanel3.Size = new System.Drawing.Size(649, 277);
            this.skinPanel3.TabIndex = 87;
            // 
            // ucCheckBox9
            // 
            this.ucCheckBox9.AutoSize = true;
            this.ucCheckBox9.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.ucCheckBox9.BackColor = System.Drawing.Color.Transparent;
            this.ucCheckBox9.Checked = false;
            this.ucCheckBox9.Font = new System.Drawing.Font("微软雅黑", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ucCheckBox9.ForeColor = System.Drawing.Color.Red;
            this.ucCheckBox9.Location = new System.Drawing.Point(237, 15);
            this.ucCheckBox9.Margin = new System.Windows.Forms.Padding(2);
            this.ucCheckBox9.Name = "ucCheckBox9";
            this.ucCheckBox9.Padding = new System.Windows.Forms.Padding(1);
            this.ucCheckBox9.Size = new System.Drawing.Size(411, 35);
            this.ucCheckBox9.TabIndex = 91;
            this.ucCheckBox9.TextValue = "如果更换新磨石，请先选中该选项";
            this.ucCheckBox9.Visible = false;
            this.ucCheckBox9.CheckedChangeEvent += new System.EventHandler(this.ucCheckBox9_CheckedChangeEvent);
            // 
            // skinLabel6
            // 
            this.skinLabel6.AutoSize = true;
            this.skinLabel6.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel6.BorderColor = System.Drawing.Color.White;
            this.skinLabel6.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel6.ForeColor = System.Drawing.Color.Red;
            this.skinLabel6.Location = new System.Drawing.Point(21, 15);
            this.skinLabel6.Name = "skinLabel6";
            this.skinLabel6.Size = new System.Drawing.Size(172, 27);
            this.skinLabel6.TabIndex = 90;
            this.skinLabel6.Text = "开班请校准！！！";
            // 
            // skinLabel5
            // 
            this.skinLabel5.AutoSize = true;
            this.skinLabel5.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel5.BorderColor = System.Drawing.Color.White;
            this.skinLabel5.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel5.ForeColor = System.Drawing.Color.Black;
            this.skinLabel5.Location = new System.Drawing.Point(21, 59);
            this.skinLabel5.Name = "skinLabel5";
            this.skinLabel5.Size = new System.Drawing.Size(452, 27);
            this.skinLabel5.TabIndex = 89;
            this.skinLabel5.Text = "步骤：第一步相机高度标定；第二步上料点校准。";
            // 
            // Alarm_Lamp
            // 
            this.Alarm_Lamp.LampColor = new System.Drawing.Color[] {
        System.Drawing.Color.Lime,
        System.Drawing.Color.Red};
            this.Alarm_Lamp.Lampstand = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.Alarm_Lamp.Location = new System.Drawing.Point(778, 277);
            this.Alarm_Lamp.Name = "Alarm_Lamp";
            this.Alarm_Lamp.Size = new System.Drawing.Size(97, 88);
            this.Alarm_Lamp.TabIndex = 92;
            this.Alarm_Lamp.TwinkleSpeed = 0;
            // 
            // ucCheckBox7
            // 
            this.ucCheckBox7.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.ucCheckBox7.BackColor = System.Drawing.Color.Transparent;
            this.ucCheckBox7.Checked = false;
            this.ucCheckBox7.Font = new System.Drawing.Font("宋体", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ucCheckBox7.Location = new System.Drawing.Point(11, 10);
            this.ucCheckBox7.Margin = new System.Windows.Forms.Padding(2);
            this.ucCheckBox7.Name = "ucCheckBox7";
            this.ucCheckBox7.Padding = new System.Windows.Forms.Padding(1);
            this.ucCheckBox7.Size = new System.Drawing.Size(75, 35);
            this.ucCheckBox7.TabIndex = 88;
            this.ucCheckBox7.TextValue = "97U";
            this.ucCheckBox7.CheckedChangeEvent += new System.EventHandler(this.ucCheckBox7_CheckedChangeEvent);
            // 
            // ucCheckBox8
            // 
            this.ucCheckBox8.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.ucCheckBox8.BackColor = System.Drawing.Color.Transparent;
            this.ucCheckBox8.Checked = false;
            this.ucCheckBox8.Font = new System.Drawing.Font("宋体", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ucCheckBox8.Location = new System.Drawing.Point(11, 49);
            this.ucCheckBox8.Margin = new System.Windows.Forms.Padding(2);
            this.ucCheckBox8.Name = "ucCheckBox8";
            this.ucCheckBox8.Padding = new System.Windows.Forms.Padding(1);
            this.ucCheckBox8.Size = new System.Drawing.Size(75, 33);
            this.ucCheckBox8.TabIndex = 89;
            this.ucCheckBox8.TextValue = "97R";
            this.ucCheckBox8.CheckedChangeEvent += new System.EventHandler(this.ucCheckBox8_CheckedChangeEvent);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.UserStripMenuItem,
            this.SetStripMenuItem,
            this.ToolStripMenuItem,
            this.DebugStripMenuItem,
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(8, 39);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1870, 29);
            this.menuStrip1.TabIndex = 90;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // UserStripMenuItem
            // 
            this.UserStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.切换用户ToolStripMenuItem,
            this.退出登录ToolStripMenuItem});
            this.UserStripMenuItem.Font = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.UserStripMenuItem.Name = "UserStripMenuItem";
            this.UserStripMenuItem.Size = new System.Drawing.Size(86, 25);
            this.UserStripMenuItem.Text = "用户管理";
            // 
            // 切换用户ToolStripMenuItem
            // 
            this.切换用户ToolStripMenuItem.Name = "切换用户ToolStripMenuItem";
            this.切换用户ToolStripMenuItem.Size = new System.Drawing.Size(144, 26);
            this.切换用户ToolStripMenuItem.Text = "切换用户";
            // 
            // 退出登录ToolStripMenuItem
            // 
            this.退出登录ToolStripMenuItem.Name = "退出登录ToolStripMenuItem";
            this.退出登录ToolStripMenuItem.Size = new System.Drawing.Size(144, 26);
            this.退出登录ToolStripMenuItem.Text = "退出登录";
            // 
            // SetStripMenuItem
            // 
            this.SetStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_OffsetConfig,
            this.btn_PCParm});
            this.SetStripMenuItem.Font = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.SetStripMenuItem.Name = "SetStripMenuItem";
            this.SetStripMenuItem.Size = new System.Drawing.Size(54, 25);
            this.SetStripMenuItem.Text = "设置";
            // 
            // btn_OffsetConfig
            // 
            this.btn_OffsetConfig.Name = "btn_OffsetConfig";
            this.btn_OffsetConfig.Size = new System.Drawing.Size(188, 26);
            this.btn_OffsetConfig.Text = "offset参数设置";
            this.btn_OffsetConfig.Click += new System.EventHandler(this.btn_OffsetConfig_Click);
            // 
            // btn_PCParm
            // 
            this.btn_PCParm.Name = "btn_PCParm";
            this.btn_PCParm.Size = new System.Drawing.Size(188, 26);
            this.btn_PCParm.Text = "上位机参数";
            this.btn_PCParm.Click += new System.EventHandler(this.btn_PCParm_Click);
            // 
            // ToolStripMenuItem
            // 
            this.ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.初始化ToolStripMenuItem,
            this.设备状态检测ToolStripMenuItem});
            this.ToolStripMenuItem.Font = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ToolStripMenuItem.Name = "ToolStripMenuItem";
            this.ToolStripMenuItem.Size = new System.Drawing.Size(54, 25);
            this.ToolStripMenuItem.Text = "工具";
            // 
            // 初始化ToolStripMenuItem
            // 
            this.初始化ToolStripMenuItem.Name = "初始化ToolStripMenuItem";
            this.初始化ToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.初始化ToolStripMenuItem.Text = "初始化";
            this.初始化ToolStripMenuItem.Click += new System.EventHandler(this.初始化ToolStripMenuItem_Click);
            // 
            // 设备状态检测ToolStripMenuItem
            // 
            this.设备状态检测ToolStripMenuItem.Name = "设备状态检测ToolStripMenuItem";
            this.设备状态检测ToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.设备状态检测ToolStripMenuItem.Text = "设备状态检测";
            this.设备状态检测ToolStripMenuItem.Click += new System.EventHandler(this.设备状态检测ToolStripMenuItem_Click);
            // 
            // DebugStripMenuItem
            // 
            this.DebugStripMenuItem.Font = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.DebugStripMenuItem.Name = "DebugStripMenuItem";
            this.DebugStripMenuItem.Size = new System.Drawing.Size(54, 25);
            this.DebugStripMenuItem.Text = "调试";
            this.DebugStripMenuItem.Click += new System.EventHandler(this.DebugStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(12, 25);
            // 
            // skinPanel5
            // 
            this.skinPanel5.BackColor = System.Drawing.Color.Transparent;
            this.skinPanel5.Controls.Add(this.ucCheckBox7);
            this.skinPanel5.Controls.Add(this.ucCheckBox8);
            this.skinPanel5.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel5.DownBack = null;
            this.skinPanel5.Location = new System.Drawing.Point(596, 77);
            this.skinPanel5.MouseBack = null;
            this.skinPanel5.Name = "skinPanel5";
            this.skinPanel5.NormlBack = null;
            this.skinPanel5.Size = new System.Drawing.Size(110, 87);
            this.skinPanel5.TabIndex = 92;
            // 
            // skinPanel4
            // 
            this.skinPanel4.BackColor = System.Drawing.Color.Transparent;
            this.skinPanel4.Controls.Add(this.ucCheckBox1);
            this.skinPanel4.Controls.Add(this.ucCheckBox2);
            this.skinPanel4.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel4.DownBack = null;
            this.skinPanel4.Location = new System.Drawing.Point(171, 86);
            this.skinPanel4.MouseBack = null;
            this.skinPanel4.Name = "skinPanel4";
            this.skinPanel4.NormlBack = null;
            this.skinPanel4.Size = new System.Drawing.Size(111, 80);
            this.skinPanel4.TabIndex = 94;
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.Color.Transparent;
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lab_FirstRobot,
            this.lab_FirstRobotStatus,
            this.lab_SecondRobot,
            this.lab_SecondRobotStatus,
            this.lab_ThirdRobot,
            this.lab_ThirdRobotStatus,
            this.lab_Controler,
            this.lab_ControlerStatus,
            this.toolStripStatusLabel10,
            this.toolStripStatusLabel9,
            this.toolStripStatusLabel11,
            this.toolStripProgressBar1});
            this.statusStrip1.Location = new System.Drawing.Point(8, 791);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1870, 24);
            this.statusStrip1.TabIndex = 95;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lab_FirstRobot
            // 
            this.lab_FirstRobot.Name = "lab_FirstRobot";
            this.lab_FirstRobot.Size = new System.Drawing.Size(92, 19);
            this.lab_FirstRobot.Text = "上下料机械臂：";
            // 
            // lab_FirstRobotStatus
            // 
            this.lab_FirstRobotStatus.Name = "lab_FirstRobotStatus";
            this.lab_FirstRobotStatus.Size = new System.Drawing.Size(32, 19);
            this.lab_FirstRobotStatus.Text = "离线";
            // 
            // lab_SecondRobot
            // 
            this.lab_SecondRobot.Name = "lab_SecondRobot";
            this.lab_SecondRobot.Size = new System.Drawing.Size(80, 19);
            this.lab_SecondRobot.Text = "上料机械臂：";
            // 
            // lab_SecondRobotStatus
            // 
            this.lab_SecondRobotStatus.Name = "lab_SecondRobotStatus";
            this.lab_SecondRobotStatus.Size = new System.Drawing.Size(32, 19);
            this.lab_SecondRobotStatus.Text = "离线";
            // 
            // lab_ThirdRobot
            // 
            this.lab_ThirdRobot.Name = "lab_ThirdRobot";
            this.lab_ThirdRobot.Size = new System.Drawing.Size(80, 19);
            this.lab_ThirdRobot.Text = "下料机械臂：";
            // 
            // lab_ThirdRobotStatus
            // 
            this.lab_ThirdRobotStatus.Name = "lab_ThirdRobotStatus";
            this.lab_ThirdRobotStatus.Size = new System.Drawing.Size(32, 19);
            this.lab_ThirdRobotStatus.Text = "离线";
            // 
            // lab_Controler
            // 
            this.lab_Controler.Name = "lab_Controler";
            this.lab_Controler.Size = new System.Drawing.Size(56, 19);
            this.lab_Controler.Text = "控制器：";
            // 
            // lab_ControlerStatus
            // 
            this.lab_ControlerStatus.Name = "lab_ControlerStatus";
            this.lab_ControlerStatus.Size = new System.Drawing.Size(32, 19);
            this.lab_ControlerStatus.Text = "离线";
            // 
            // toolStripStatusLabel10
            // 
            this.toolStripStatusLabel10.Name = "toolStripStatusLabel10";
            this.toolStripStatusLabel10.Size = new System.Drawing.Size(0, 19);
            // 
            // toolStripStatusLabel9
            // 
            this.toolStripStatusLabel9.Name = "toolStripStatusLabel9";
            this.toolStripStatusLabel9.Size = new System.Drawing.Size(200, 19);
            this.toolStripStatusLabel9.Text = "                                                ";
            // 
            // toolStripStatusLabel11
            // 
            this.toolStripStatusLabel11.Name = "toolStripStatusLabel11";
            this.toolStripStatusLabel11.Size = new System.Drawing.Size(32, 19);
            this.toolStripStatusLabel11.Text = "进度";
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 18);
            // 
            // skinGroupBox2
            // 
            this.skinGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.skinGroupBox2.BorderColor = System.Drawing.Color.Gray;
            this.skinGroupBox2.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinGroupBox2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.skinGroupBox2.Location = new System.Drawing.Point(8, 593);
            this.skinGroupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.skinGroupBox2.Name = "skinGroupBox2";
            this.skinGroupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.skinGroupBox2.Radius = 8;
            this.skinGroupBox2.RectBackColor = System.Drawing.Color.Transparent;
            this.skinGroupBox2.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinGroupBox2.Size = new System.Drawing.Size(938, 177);
            this.skinGroupBox2.TabIndex = 97;
            this.skinGroupBox2.TabStop = false;
            this.skinGroupBox2.Text = "工作状态";
            this.skinGroupBox2.TitleBorderColor = System.Drawing.Color.Transparent;
            this.skinGroupBox2.TitleRadius = 20;
            this.skinGroupBox2.TitleRectBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(198)))), ((int)(((byte)(203)))));
            this.skinGroupBox2.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // skinGroupBox4
            // 
            this.skinGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.skinGroupBox4.BorderColor = System.Drawing.Color.Gray;
            this.skinGroupBox4.Controls.Add(this.btn_CountZero);
            this.skinGroupBox4.Controls.Add(this.lab_PutNum);
            this.skinGroupBox4.Controls.Add(this.lab_PutNum1);
            this.skinGroupBox4.Controls.Add(this.txt_Log);
            this.skinGroupBox4.Controls.Add(this.lab_TakeNum);
            this.skinGroupBox4.Controls.Add(this.lab_TakeNum1);
            this.skinGroupBox4.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinGroupBox4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.skinGroupBox4.Location = new System.Drawing.Point(1020, 534);
            this.skinGroupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.skinGroupBox4.Name = "skinGroupBox4";
            this.skinGroupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.skinGroupBox4.Radius = 8;
            this.skinGroupBox4.RectBackColor = System.Drawing.Color.Transparent;
            this.skinGroupBox4.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinGroupBox4.Size = new System.Drawing.Size(816, 275);
            this.skinGroupBox4.TabIndex = 99;
            this.skinGroupBox4.TabStop = false;
            this.skinGroupBox4.Text = "工作记录";
            this.skinGroupBox4.TitleBorderColor = System.Drawing.Color.Transparent;
            this.skinGroupBox4.TitleRadius = 20;
            this.skinGroupBox4.TitleRectBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(198)))), ((int)(((byte)(203)))));
            this.skinGroupBox4.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // btn_CountZero
            // 
            this.btn_CountZero.AutoSize = true;
            this.btn_CountZero.BackColor = System.Drawing.Color.Transparent;
            this.btn_CountZero.BaseColor = System.Drawing.Color.DodgerBlue;
            this.btn_CountZero.BorderColor = System.Drawing.SystemColors.InfoText;
            this.btn_CountZero.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_CountZero.DownBack = null;
            this.btn_CountZero.Font = new System.Drawing.Font("微软雅黑", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_CountZero.ForeColor = System.Drawing.Color.Black;
            this.btn_CountZero.Location = new System.Drawing.Point(70, 217);
            this.btn_CountZero.MouseBack = null;
            this.btn_CountZero.Name = "btn_CountZero";
            this.btn_CountZero.NormlBack = null;
            this.btn_CountZero.Size = new System.Drawing.Size(171, 49);
            this.btn_CountZero.TabIndex = 78;
            this.btn_CountZero.Text = "计数清零";
            this.btn_CountZero.UseVisualStyleBackColor = false;
            // 
            // lab_PutNum
            // 
            this.lab_PutNum.AutoSize = true;
            this.lab_PutNum.BackColor = System.Drawing.Color.Transparent;
            this.lab_PutNum.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_PutNum.Location = new System.Drawing.Point(238, 159);
            this.lab_PutNum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lab_PutNum.Name = "lab_PutNum";
            this.lab_PutNum.Size = new System.Drawing.Size(18, 20);
            this.lab_PutNum.TabIndex = 25;
            this.lab_PutNum.Text = "0";
            // 
            // lab_PutNum1
            // 
            this.lab_PutNum1.AutoSize = true;
            this.lab_PutNum1.BackColor = System.Drawing.Color.Transparent;
            this.lab_PutNum1.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_PutNum1.Location = new System.Drawing.Point(65, 159);
            this.lab_PutNum1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lab_PutNum1.Name = "lab_PutNum1";
            this.lab_PutNum1.Size = new System.Drawing.Size(99, 20);
            this.lab_PutNum1.TabIndex = 24;
            this.lab_PutNum1.Text = "放盘片数量：";
            // 
            // txt_Log
            // 
            this.txt_Log.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txt_Log.Location = new System.Drawing.Point(369, 20);
            this.txt_Log.Margin = new System.Windows.Forms.Padding(4);
            this.txt_Log.Multiline = true;
            this.txt_Log.Name = "txt_Log";
            this.txt_Log.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_Log.Size = new System.Drawing.Size(401, 246);
            this.txt_Log.TabIndex = 23;
            // 
            // lab_TakeNum
            // 
            this.lab_TakeNum.AutoSize = true;
            this.lab_TakeNum.BackColor = System.Drawing.Color.Transparent;
            this.lab_TakeNum.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_TakeNum.Location = new System.Drawing.Point(238, 77);
            this.lab_TakeNum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lab_TakeNum.Name = "lab_TakeNum";
            this.lab_TakeNum.Size = new System.Drawing.Size(18, 20);
            this.lab_TakeNum.TabIndex = 22;
            this.lab_TakeNum.Text = "0";
            // 
            // lab_TakeNum1
            // 
            this.lab_TakeNum1.AutoSize = true;
            this.lab_TakeNum1.BackColor = System.Drawing.Color.Transparent;
            this.lab_TakeNum1.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_TakeNum1.Location = new System.Drawing.Point(65, 77);
            this.lab_TakeNum1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lab_TakeNum1.Name = "lab_TakeNum1";
            this.lab_TakeNum1.Size = new System.Drawing.Size(99, 20);
            this.lab_TakeNum1.TabIndex = 21;
            this.lab_TakeNum1.Text = "取盘片数量：";
            // 
            // skinGroupBox5
            // 
            this.skinGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.skinGroupBox5.BorderColor = System.Drawing.Color.Gray;
            this.skinGroupBox5.Controls.Add(this.totalProcessStep);
            this.skinGroupBox5.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinGroupBox5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.skinGroupBox5.Location = new System.Drawing.Point(8, 593);
            this.skinGroupBox5.Margin = new System.Windows.Forms.Padding(4);
            this.skinGroupBox5.Name = "skinGroupBox5";
            this.skinGroupBox5.Padding = new System.Windows.Forms.Padding(4);
            this.skinGroupBox5.Radius = 8;
            this.skinGroupBox5.RectBackColor = System.Drawing.Color.Transparent;
            this.skinGroupBox5.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinGroupBox5.Size = new System.Drawing.Size(938, 177);
            this.skinGroupBox5.TabIndex = 97;
            this.skinGroupBox5.TabStop = false;
            this.skinGroupBox5.Text = "工作状态";
            this.skinGroupBox5.TitleBorderColor = System.Drawing.Color.Transparent;
            this.skinGroupBox5.TitleRadius = 20;
            this.skinGroupBox5.TitleRectBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(198)))), ((int)(((byte)(203)))));
            this.skinGroupBox5.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // totalProcessStep
            // 
            this.totalProcessStep.BackColor = System.Drawing.Color.Transparent;
            this.totalProcessStep.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.totalProcessStep.ImgCompleted = null;
            this.totalProcessStep.LineWidth = 2;
            this.totalProcessStep.Location = new System.Drawing.Point(47, 62);
            this.totalProcessStep.Margin = new System.Windows.Forms.Padding(4);
            this.totalProcessStep.Name = "totalProcessStep";
            this.totalProcessStep.Size = new System.Drawing.Size(853, 79);
            this.totalProcessStep.StepBackColor = System.Drawing.Color.Gray;
            this.totalProcessStep.StepFontColor = System.Drawing.Color.White;
            this.totalProcessStep.StepForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(77)))), ((int)(((byte)(59)))));
            this.totalProcessStep.StepIndex = 0;
            this.totalProcessStep.Steps = new string[] {
        "隔离盘1",
        "隔离盘2",
        "隔离盘3",
        "隔离盘4",
        "隔离盘5"};
            this.totalProcessStep.StepWidth = 35;
            this.totalProcessStep.TabIndex = 7;
            // 
            // skinGroupBox3
            // 
            this.skinGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.skinGroupBox3.BorderColor = System.Drawing.Color.Gray;
            this.skinGroupBox3.Controls.Add(this.up_Disk1);
            this.skinGroupBox3.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinGroupBox3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.skinGroupBox3.Location = new System.Drawing.Point(1424, 43);
            this.skinGroupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.skinGroupBox3.Name = "skinGroupBox3";
            this.skinGroupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.skinGroupBox3.Radius = 8;
            this.skinGroupBox3.RectBackColor = System.Drawing.Color.Transparent;
            this.skinGroupBox3.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinGroupBox3.Size = new System.Drawing.Size(386, 483);
            this.skinGroupBox3.TabIndex = 99;
            this.skinGroupBox3.TabStop = false;
            this.skinGroupBox3.Text = "上料区位置状态";
            this.skinGroupBox3.TitleBorderColor = System.Drawing.Color.Transparent;
            this.skinGroupBox3.TitleRadius = 20;
            this.skinGroupBox3.TitleRectBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(198)))), ((int)(((byte)(203)))));
            this.skinGroupBox3.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // up_Disk1
            // 
            this.up_Disk1.BackColor = System.Drawing.Color.Transparent;
            this.up_Disk1.Index1 = false;
            this.up_Disk1.Index2 = false;
            this.up_Disk1.Index3 = false;
            this.up_Disk1.Index4 = false;
            this.up_Disk1.Index5 = false;
            this.up_Disk1.Index6 = false;
            this.up_Disk1.Index7 = false;
            this.up_Disk1.Index8 = false;
            this.up_Disk1.Location = new System.Drawing.Point(46, 34);
            this.up_Disk1.Name = "up_Disk1";
            this.up_Disk1.Size = new System.Drawing.Size(292, 422);
            this.up_Disk1.TabIndex = 0;
            // 
            // skinGroupBox1
            // 
            this.skinGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.skinGroupBox1.BorderColor = System.Drawing.Color.Gray;
            this.skinGroupBox1.Controls.Add(this.down_Disk2);
            this.skinGroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.skinGroupBox1.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinGroupBox1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.skinGroupBox1.Location = new System.Drawing.Point(1030, 43);
            this.skinGroupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.skinGroupBox1.Name = "skinGroupBox1";
            this.skinGroupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.skinGroupBox1.Radius = 8;
            this.skinGroupBox1.RectBackColor = System.Drawing.Color.Transparent;
            this.skinGroupBox1.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinGroupBox1.Size = new System.Drawing.Size(386, 483);
            this.skinGroupBox1.TabIndex = 100;
            this.skinGroupBox1.TabStop = false;
            this.skinGroupBox1.Text = "下料区位置状态";
            this.skinGroupBox1.TitleBorderColor = System.Drawing.Color.Transparent;
            this.skinGroupBox1.TitleRadius = 20;
            this.skinGroupBox1.TitleRectBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(198)))), ((int)(((byte)(203)))));
            this.skinGroupBox1.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // down_Disk2
            // 
            this.down_Disk2.AutoSize = true;
            this.down_Disk2.BackColor = System.Drawing.Color.Transparent;
            this.down_Disk2.Index1 = false;
            this.down_Disk2.Index2 = false;
            this.down_Disk2.Index3 = false;
            this.down_Disk2.Index4 = false;
            this.down_Disk2.Index5 = false;
            this.down_Disk2.Index6 = false;
            this.down_Disk2.Index7 = false;
            this.down_Disk2.Index8 = false;
            this.down_Disk2.Location = new System.Drawing.Point(60, 34);
            this.down_Disk2.Name = "down_Disk2";
            this.down_Disk2.Size = new System.Drawing.Size(235, 408);
            this.down_Disk2.TabIndex = 0;
            // 
            // down_Disk1
            // 
            this.down_Disk1.AutoSize = true;
            this.down_Disk1.BackColor = System.Drawing.Color.Transparent;
            this.down_Disk1.Index1 = false;
            this.down_Disk1.Index2 = false;
            this.down_Disk1.Index3 = false;
            this.down_Disk1.Index4 = false;
            this.down_Disk1.Index5 = false;
            this.down_Disk1.Index6 = false;
            this.down_Disk1.Index7 = false;
            this.down_Disk1.Index8 = false;
            this.down_Disk1.Location = new System.Drawing.Point(0, 0);
            this.down_Disk1.Name = "down_Disk1";
            this.down_Disk1.Size = new System.Drawing.Size(276, 440);
            this.down_Disk1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(778, 407);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(130, 60);
            this.button1.TabIndex = 101;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FrmWorking
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(201)))), ((int)(((byte)(206)))));
            this.BackPalace = global::WindowsFormsApp.Properties.Resources.metalBG;
            this.ClientSize = new System.Drawing.Size(1886, 823);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.skinGroupBox1);
            this.Controls.Add(this.Alarm_Lamp);
            this.Controls.Add(this.skinGroupBox4);
            this.Controls.Add(this.skinGroupBox3);
            this.Controls.Add(this.skinPanel1);
            this.Controls.Add(this.skinGroupBox5);
            this.Controls.Add(this.skinGroupBox2);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.skinPanel4);
            this.Controls.Add(this.skinPanel3);
            this.Controls.Add(this.skinPanel5);
            this.Controls.Add(this.btn_Init);
            this.Controls.Add(this.ucBtnExt1);
            this.Controls.Add(this.ucRollText1);
            this.Controls.Add(this.btn_Start);
            this.Controls.Add(this.menuStrip1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmWorking";
            this.ShadowColor = System.Drawing.Color.Gray;
            this.Text = "研磨机2.0";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmWorking_FormClosing);
            this.Load += new System.EventHandler(this.FrmWorking_Load);
            this.SizeChanged += new System.EventHandler(this.FrmWorking_SizeChanged);
            this.ucPanelTitle3.ResumeLayout(false);
            this.ucPanelTitle3.PerformLayout();
            this.skinPanel1.ResumeLayout(false);
            this.skinPanel1.PerformLayout();
            this.skinPanel3.ResumeLayout(false);
            this.skinPanel3.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.skinPanel5.ResumeLayout(false);
            this.skinPanel4.ResumeLayout(false);
            this.skinPanel4.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.skinGroupBox4.ResumeLayout(false);
            this.skinGroupBox4.PerformLayout();
            this.skinGroupBox5.ResumeLayout(false);
            this.skinGroupBox3.ResumeLayout(false);
            this.skinGroupBox1.ResumeLayout(false);
            this.skinGroupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private HZH_Controls.Controls.UCSwitch btn_Start;
        private CCWin.SkinControl.SkinLabel lab_Time5;
        private HZH_Controls.Controls.UCCheckBox ucCheckBox1;
        private HZH_Controls.Controls.UCCheckBox ucCheckBox2;
        private HZH_Controls.Controls.UCPanelTitle ucPanelTitle3;
        private CCWin.SkinControl.SkinButton skinButton2;
        private CCWin.SkinControl.SkinButton skinButton4;
        public  HZH_Controls.Controls.UCRollText ucRollText1;
        public HZH_Controls.Controls.UCBtnExt ucBtnExt1;
        private HZH_Controls.Controls.UCTextBoxEx ucTextBoxEx1;
        private CCWin.SkinControl.SkinPanel skinPanel1;
        private CCWin.SkinControl.SkinLabel skinLabel4;
        private HZH_Controls.Controls.UCCheckBox ucCheckBox6;
        private CCWin.SkinControl.SkinPanel skinPanel3;
        private CCWin.SkinControl.SkinLabel skinLabel5;
        private CCWin.SkinControl.SkinLabel skinLabel6;
        private HZH_Controls.Controls.UCCheckBox ucCheckBox7;
        private HZH_Controls.Controls.UCCheckBox ucCheckBox8;
        private HZH_Controls.Controls.UCCheckBox ucCheckBox9;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem SetStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btn_OffsetConfig;
        private System.Windows.Forms.ToolStripMenuItem UserStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 切换用户ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 退出登录ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 初始化ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DebugStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btn_PCParm;
        private CCWin.SkinControl.SkinPanel skinPanel5;
        private CCWin.SkinControl.SkinPanel skinPanel4;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lab_FirstRobot;
        private System.Windows.Forms.ToolStripStatusLabel lab_FirstRobotStatus;
        private System.Windows.Forms.ToolStripStatusLabel lab_SecondRobot;
        private System.Windows.Forms.ToolStripStatusLabel lab_SecondRobotStatus;
        private System.Windows.Forms.ToolStripStatusLabel lab_ThirdRobot;
        private System.Windows.Forms.ToolStripStatusLabel lab_ThirdRobotStatus;
        private System.Windows.Forms.ToolStripStatusLabel lab_Controler;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel10;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel9;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel11;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private CCWin.SkinControl.SkinGroupBox skinGroupBox2;
        private CCWin.SkinControl.SkinGroupBox skinGroupBox4;
        private CCWin.SkinControl.SkinButton btn_CountZero;
        private System.Windows.Forms.Label lab_PutNum;
        private System.Windows.Forms.Label lab_PutNum1;
        private System.Windows.Forms.TextBox txt_Log;
        private System.Windows.Forms.Label lab_TakeNum;
        private System.Windows.Forms.Label lab_TakeNum1;
        private SkinGroupBox skinGroupBox5;
        private HZH_Controls.Controls.UCStep totalProcessStep;
        private HZH_Controls.Controls.UCAlarmLamp Alarm_Lamp;
        public System.Windows.Forms.ToolStripStatusLabel lab_ControlerStatus;
        public HZH_Controls.Controls.UCBtnExt btn_Init;
        private SkinGroupBox skinGroupBox3;
        private SkinGroupBox skinGroupBox1;
        private WindowsFormsApp.customizeWidget.Down_Disk down_Disk1;
        private WindowsFormsApp.customizeWidget.Down_Disk down_Disk2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private WindowsFormsApp.customizeWidget.Up_Disk up_Disk1;
        private System.Windows.Forms.ToolStripMenuItem 设备状态检测ToolStripMenuItem;
        private System.Windows.Forms.Button button1;
    }
}