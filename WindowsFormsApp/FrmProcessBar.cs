﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp.deviceConnect;

namespace WindowsFormsApp
{
    public partial class FrmProcessBar : Form
    {
        public FrmProcessBar()
        {
            InitializeComponent();
        }
        public bool Increase(int nValue)
        {
            if(nValue > 0)
            {
                if(prcBar.Value + nValue < prcBar.Maximum)
                {
                    prcBar.Value += nValue;
                    return true;
                }
                else
                {
                    prcBar.Value = 1;
                    return true;
                }
            }
            return true;

        }

        private void skinButton1_Click(object sender, EventArgs e)
        {
            if (CCWin.MessageBoxEx.Show("是否终止？", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                ConnectConfig.Task_Init_Kill = true;
            }
        }

    }
}
