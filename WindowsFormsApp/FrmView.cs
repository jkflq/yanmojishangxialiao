﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp.deviceConnect;
using WindowsFormsApp.utils.AutoSizeForm;

namespace WindowsFormsApp
{
    public partial class FrmView : CCWin.Skin_Color
    {
        public FrmView()
        {
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true); // 禁止擦除背景.
            SetStyle(ControlStyles.DoubleBuffer, true); // 双缓冲
            InitializeComponent();
        }

        private void FrmView_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }

        private void FrmView_Load(object sender, EventArgs e)
        {
            modbusView1.Read_Path();
            modbusView2.Read_Path();
            modbusView3.Read_Path();
            modbusView4.Read_Path();
            modbusView5.Read_Path();
            modbusView1.Write_Data();
            modbusView2.Write_Data();
            modbusView3.Write_Data();
            modbusView4.Write_Data();
            modbusView5.Write_Data();

        }

        private void button1_Click(object sender, EventArgs e)
        {
           ConnectConfig.StartUpdate();
        }
    }
}
