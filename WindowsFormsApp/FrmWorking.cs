﻿#define FIRST_ROBOT
//#define SECOND_ROBOT
//#define THIRD_ROBOT
#define CONTROLER


using CCWin.SkinControl;
using HZH_Controls.Controls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using System.Xml;
using WindowsFormsApp;
using WindowsFormsApp.components.EMotor;
using WindowsFormsApp.components.grip;
using WindowsFormsApp.components.Motor;
using WindowsFormsApp.components.QGUnit;
using WindowsFormsApp.components.robot;
using WindowsFormsApp.components.tray;
using WindowsFormsApp.components.vacuum;
using WindowsFormsApp.deviceConnect;
using WindowsFormsApp.utils.AutoSizeForm;
using WindowsFormsApp.utils.log;
using WindowsFormsApp.utils.modbus;
using WindowsFormsApp.utils.Saga;
using WindowsFormsApp.utils.XML;

namespace WinFormsBase
{

    /// <summary>
    /// 此窗体为工作主界面
    /// </summary>
    public partial class FrmWorking : CCWin.Skin_Color
    {

        AutoSizeForm autoSize = new AutoSizeForm();
        /// <summary>
        /// 主界面构造函数
        /// </summary>
        public FrmWorking()
        {

            this.SetStyle(ControlStyles.AllPaintingInWmPaint | //不擦除背景
                ControlStyles.OptimizedDoubleBuffer | //双缓冲
                ControlStyles.UserPaint,    //使用自定义重绘事件
                true);
            this.UpdateStyles();
            //页面组件初始化
            InitializeComponent();
            




            // myXML disksnum = new myXML("ParamConfig.xml");

            //ucTextBoxEx1.InputText = disksnum.getNodeValue("PanNumber");
            ucTextBoxEx1.InputText = "5";

        }



        private System.Timers.Timer UItimer;
        private FrmProcessBar myProcessBar = null;
        private delegate bool IncreaseHandle(int nValue);
        private IncreaseHandle myIncrease = null;
        private void ThreadFun()
        {
            MethodInvoker mi = new MethodInvoker(ShowProcessBar);
            this.BeginInvoke(mi);
            Thread.Sleep(1000);
            bool blnIncreased = false;
            object objReturn = null;
            do
            {
                Thread.Sleep(100);
                objReturn = this.Invoke(this.myIncrease, new object[] { 10 });
                blnIncreased = (bool)objReturn;
            }
            while (blnIncreased);
        }
        private void ShowProcessBar()
        {
            myProcessBar = new FrmProcessBar();

            //初始化事件
            myIncrease = new IncreaseHandle(myProcessBar.Increase);
            myProcessBar.ShowDialog();
            myProcessBar = null;

        }

        #region  自定义代码
        //创建接收相机数据的数组
        private byte[] camReceiveBuffer = new byte[2048];


        //定义异步Socket,用于与相机通讯
        private static SocketAsyncEventArgs camReceiveArgs;
        //定义异步Socket,用于与机械臂通讯
        private static SocketAsyncEventArgs robReceiveArgs;


        //用作检测机械臂异常停止的定时器
        private System.Timers.Timer checktimer;
        //用作检测暂停按钮的定时器
        private System.Timers.Timer pausetimer;

        //首盘尾盘正常生产状态选择
        private enum PRODUCTION_STATUS
        {
            FIRST,
            FINAL,
            NORMAL
        }

        private PRODUCTION_STATUS PD_Status = PRODUCTION_STATUS.NORMAL;



        //机械臂程序路径
        string programPath = "\"/usr/codesys/hsc3_app/script\"";

        //要取的物料个数  默认5盘
        public volatile int _pannum = 5;

        public int PanNumber
        {
            get
            {
                return _pannum;
            }
            set
            {
                _pannum = value;

                //改变显示盘数
                ucTextBoxEx1.txtInput.Text = value.ToString();


                //更新程序中的盘数
                //RobotConnect.SetR(ConnectConfig.FirstRobot,ParamConfig.PanNum_R_REG, _pannum);

                ////更新文件中的盘数
                //myXML disksnum = new myXML("ParamConfig.xml");
                //disksnum.setNodeValue("PanNumber", value.ToString());

            }
        }



        //目标区间
        public double NewObjectRange = 0;
        public double OldObjectRange = 0;

        //互斥标记
        public volatile bool MutualFlag = false;




        private enum AdjustGrade
        {
            QUICK,
            SLOW,
            SLIGHT
        }

        //运行状态标志位定义
        private volatile bool _running = false;


        //连接标志位
        private volatile bool _connectflag = false;

        //初始化标志位
        private volatile bool _initflag = false;


        public bool InitFlag
        {
            set
            {

                _initflag = value;
                if(value)
                {
                    MessageBox.Show("初始化完成");

                }

            }
            get
            {
                return _initflag;
            }
        }
        

        //设置或获取系统运行状态
        public bool Running
        {
            get
            {
                return _running;
            }
            set
            {
                _running = value;
                ParamConfig.runflag = value;  //方便其他类中判断自动运行标志位
                //触发事件
                this.Invoke(new Action(() =>
                {

                    if (!value) //系统停止自动运行
                    {

                        //关闭任务
                        if (firstpromise != null)
                            mysaga.Cancel(firstpromise);

                        //设置按钮
                        SetChecked(false, btn_Start);
                        //如果机器人在运动，则停止

                        //停止控制器  停止机械臂

                        //RobotConnect.unload(ConnectConfig.FirstRobot);

                        pausetimer.Stop();

                        LogHelper.loginfo.Info("切换为手动模式");

                        

                    }
                    else//系统开始运行  true
                    {

                        ucPanelTitle3.Enabled = false;


                        SetChecked(true, btn_Start);

                    }

                }));
            }

        }

        //获取初始化状态  true 为初始化完成，false为初始化失败

        public bool Connectflag
        {
            get
            {
                return _connectflag;
            }
            set
            {
                _connectflag = value;
                this.Invoke(new Action(() =>
                {

                    if (!value)
                    {
                        //断开连接
                        ConnectConfig.DisConnect();
                        //断开机械臂报警通信对象
                        //RobotConnect.disconnectIPC(ConnectConfig.FirstRobot.CommSys);
                        //Runing = false
                        if (Running)
                            Running = false;


                    }
                    else//true
                    {
                        //系统信息初始化（IP）读取文件中的信息到上位机中
                        //机械臂实例化，并将机械臂对象进行添加
                       

                        //连接控制器  机械臂
                        ConnectConfig.Connect();//这里是异步的


                    }
                    
                    //设置按钮
                    btn_Init.FillColor = value ? Color.Lime : Color.Red; 
                    btn_Init.BtnText = value ? "断开连接" : "设备连接";

                }));
            }
        }


        /// <summary>
        /// 轮询处理中间件
        /// </summary>
        Dictionary<string, Action<string>> MiddleWares = new Dictionary<string, Action<string>>();


        //创建通信消息类型  用于线程之间的通信
        Msg publicMsg_WAIT = new Msg(MsgType.PUBLIC, MsgContent.WAIT);
        Msg publicMsg_CONTINUE = new Msg(MsgType.PUBLIC, MsgContent.CONTINUE);
        Msg subscribeMsg_WAIT = new Msg(MsgType.SUBSCRIBE, MsgContent.WAIT);
        Msg subscribeMsg_CONTINUE = new Msg(MsgType.SUBSCRIBE, MsgContent.CONTINUE);


#endregion

        private void FrmWorking_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (CCWin.MessageBoxEx.Show("?", "确定退出？", MessageBoxButtons.YesNo) != DialogResult.Yes)
            {
                e.Cancel = true;
            }
            else
            {
                //日志记录退出应用
                LogHelper.logerror.Info("Exit application.？？？？？？？？？？？");
                LogHelper.loginfo.Info("Exit application.？？？？？？？？？？？");
            }

        }



        /// <summary>
        /// 初始化系统参数
        /// </summary>
        /// 
        public void InitAction()

        {
            //初始化机械臂
            RobotConnect.InitRobot(ConnectConfig.FirstRobot);
            RobotConnect.InitRobot(ConnectConfig.SecondRobot);
            RobotConnect.InitRobot(ConnectConfig.ThirdRobot);

            //等待机械臂复位完成或者错误
            while (RobotConnect.GetR(ConnectConfig.FirstRobot, 14) == 0 && !ParamConfig.WarnFlag)// && RobotConnect.GetR(ConnectConfig.ThirdRobot, 14) == 0
            {
                if (ConnectConfig.Task_Init_Kill)
                {
                    return;
                }
            }

            if (ParamConfig.WarnFlag)
            {
                Console.WriteLine("机械臂输初始化失败");
                return;
            }
            //置位初始化标志位
            RobotConnect.SetR(ConnectConfig.FirstRobot, 14, 0);
            RobotConnect.SetR(ConnectConfig.SecondRobot, 14, 0);
            RobotConnect.SetR(ConnectConfig.ThirdRobot, 14, 0);
            Console.WriteLine("机械臂初始化完成");

            //给控制器发送一个初始化信号
            ConnectConfig.ControlUnit.Write(ControlerPortConfig.All_InitCoil, true);
            Thread.Sleep(5000);

            //等待控制器初始化完成
            //while (ConnectConfig.ControlUnit.ReadCoil(ControlerPortConfig.All_Init_Flag) == false && !ParamConfig.WarnFlag)// && RobotConnect.GetR(ConnectConfig.ThirdRobot, 14) == 0
            //{
            //    if (ConnectConfig.Task_Init_Kill)
            //    {
            //        return;
            //    }
            //}

            //if (ParamConfig.WarnFlag)
            //{
            //    Console.WriteLine("控制器初始化失败");
            //    return;
            //}

            LogHelper.loginfo.Info("初始化设备完成");

        }

        /// <summary>
        /// 实例化设备及其初始化设备IP信息
        /// </summary>
        private void SystemInitConfig()
        {

            //从文件中加载参数
            //加载offset值  这里并没有写入机械臂操作   还是上次的值 只要不用示教器改就行 保持上位机中参数与示教器中参数的统一
            ParamConfig.UpdateOffsetParam();

            //加载一号机械臂参数
            ParamConfig.UpdateFirstRobotParam();
            //加载产品参数
            ParamConfig.UpdateProductParam();



            //用于检测暂停的定时器初始化
            pausetimer = new System.Timers.Timer(200);
            pausetimer.Elapsed += CheckPause;
            pausetimer.AutoReset = true;
            pausetimer.Enabled = false;

        }

        private void FrmWorking_Load(object sender, EventArgs e)
        {
            autoSize.controllInitializeSize(this);
            
            Control.CheckForIllegalCrossThreadCalls = false;
            //绑定连接事件
            ConnectConfig.Connecting += ConnectConfig_Connecting;
            ConnectConfig.DisConnecting += ConnectConfig_DisConnecting;


            //if (UItimer == null)
            //{
            //    UItimer = new System.Timers.Timer(1000);
            //    UItimer.AutoReset = true;
            //    UItimer.Elapsed += new System.Timers.ElapsedEventHandler(UItimer_Elapsed);
            //    UItimer.Enabled = false;
            //}
                SystemInitConfig();



            //加载offset值
            //ParamConfig.UpdateOffsetParam();
            ////加载相机参数
            ////ParamConfig.UpdateCameraParam();
            ////加载一号机械臂参数
            //ParamConfig.UpdateFirstRobotParam();
            ////加载产品参数
            //ParamConfig.UpdateProductParam();

            //日志初始化
            LogHelper.SetConfig();

            //初始化Saga
            //InitSaga();


            //初始化UI的状态 以便更改
            InitUIStatus();
        }


        //private void UItimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        //{
        //    CreateControlsByTimer();
        //}

        //private void CreateControlsByTimer()
        //{
        //    //创建一个委托，用于封装一个方法，在这里是封装了 控制更新控件 的方法
        //    Action invokeAction = new Action(CreateControlsByTimer);
        //    //判断操作控件的线程是否创建控件的线程
        //    //调用方调用方位于创建控件所在的线程以外的线程中，如果在其他线程则对控件进行方法调用时必须调用 Invoke 方法
        //    if (this.InvokeRequired)
        //    {
        //        //与调用线程不同的线程上创建（说明您必须通过 Invoke 方法对控件进行调用）
        //        this.Invoke(invokeAction);
        //    }
        //    else
        //    {
        //        //窗体线程，即主线程

        //        //更新UI
        //        int j = 0;
        //        foreach(var i in ControlerPortConfig.UpDisksREG)
        //        {
        //            bool status = ConnectConfig.ControlUnit.ReadCoil(i);
        //            UPDisksPicb[j++].BackColor = status ? Color.Lime : SystemColors.ControlDarkDark;
        //            UPDisksPicb[j++].BackColor = status ? SystemColors.ControlLightLight : SystemColors.ControlText;
        //        }
 
        //    }
        //}


        /// <summary>
        /// 设备通讯连接
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private  void ConnectConfig_Connecting(object sender, EventArgs e)
        {
            ArrayList taskList = new ArrayList();
            try
            {
#if FIRST_ROBOT
                Task task1 = new Task(() => {
                    //连接上下料机械臂运动接口
                    bool flag1 = RobotConnect.connectIPC(ConnectConfig.FirstRobot.CommMot, ConnectConfig.FirstRobot.IP, ConnectConfig.FirstRobot.CommPort);
                    //连接上下料机械臂系统接口
                    //bool flag2 = RobotConnect.connectIPC(ConnectConfig.FirstRobot.CommSys, ConnectConfig.FirstRobot.IP, ConnectConfig.FirstRobot.CommPort);
                    if (flag1)
                    {
                        LogHelper.loginfo.Info("上下料机械臂连接成功");
                    }
                    else
                    {
                        LogHelper.logerror.Error($"上下料机械臂连接失败,机械臂IP：{ConnectConfig.FirstRobotIP}");
                        MessageBox.Show("上下料机械臂连接失败");
                    }
                    RobotStatus(ConnectConfig.FirstRobot, flag1);

                });
                task1.Start();
                taskList.Add(task1);
                //连接机械臂
                //Task.Run(() =>
                //{
                //    //连接上下料机械臂运动接口
                //    bool flag1 = RobotConnect.connectIPC(ConnectConfig.FirstRobot.CommMot, ConnectConfig.FirstRobot.IP, ConnectConfig.FirstRobot.CommPort);
                //    //连接上下料机械臂系统接口
                //    //bool flag2 = RobotConnect.connectIPC(ConnectConfig.FirstRobot.CommSys, ConnectConfig.FirstRobot.IP, ConnectConfig.FirstRobot.CommPort);
                //    if (flag1)
                //    {
                //        LogHelper.loginfo.Info("上下料机械臂连接成功");
                //    }
                //    else
                //    {
                //        LogHelper.logerror.Error($"上下料机械臂连接失败,机械臂IP：{ConnectConfig.FirstRobotIP}");
                //        MessageBox.Show("上下料机械臂连接失败");
                //    }
                //    RobotStatus(ConnectConfig.FirstRobot, flag1);
                //});

#endif

#if SECOND_ROBOT

                //连接机械臂
                //Task.Run(() =>
                //   {
                //       //连接上下料机械臂运动接口
                //       bool flag1 = RobotConnect.connectIPC(ConnectConfig.SecondRobot.CommMot, ConnectConfig.SecondRobot.IP, ConnectConfig.SecondRobot.CommPort);
                //       //连接上下料机械臂系统接口
                //       //bool flag2 = RobotConnect.connectIPC(ConnectConfig.SecondRobot.CommSys, ConnectConfig.SecondRobot.IP, ConnectConfig.SecondRobot.CommPort);
                //       if (flag1)
                //       {
                //           LogHelper.loginfo.Info("上料机械臂连接成功");
                //       }
                //       else
                //       {
                //           LogHelper.logerror.Error($"上料机械臂连接失败,机械臂IP：{ConnectConfig.SecondRobotIP}");
                //           MessageBox.Show("上料机械臂连接失败");
                //       }
                //       RobotStatus(ConnectConfig.SecondRobot, flag1);
                //   });



                Task task2 = new Task(() =>
                {                        //连接上下料机械臂运动接口
                    bool flag1 = RobotConnect.connectIPC(ConnectConfig.SecondRobot.CommMot, ConnectConfig.SecondRobot.IP, ConnectConfig.SecondRobot.CommPort);
                    //连接上下料机械臂系统接口
                    //bool flag2 = RobotConnect.connectIPC(ConnectConfig.SecondRobot.CommSys, ConnectConfig.SecondRobot.IP, ConnectConfig.SecondRobot.CommPort);
                    if (flag1)
                    {
                        LogHelper.loginfo.Info("上料机械臂连接成功");
                    }
                    else
                    {
                        LogHelper.logerror.Error($"上料机械臂连接失败,机械臂IP：{ConnectConfig.SecondRobotIP}");
                        MessageBox.Show("上料机械臂连接失败");
                    }
                    RobotStatus(ConnectConfig.SecondRobot, flag1);

                });
                task2.Start();
                taskList.Add(task2);
#endif

#if THIRD_ROBOT

                //连接机械臂
                //Task.Run(() =>
                //    {
                //        //连接上下料机械臂运动接口
                //        bool flag1 = RobotConnect.connectIPC(ConnectConfig.ThirdRobot.CommMot, ConnectConfig.ThirdRobot.IP, ConnectConfig.ThirdRobot.CommPort);
                //        //连接上下料机械臂系统接口
                //       // bool flag2 = RobotConnect.connectIPC(ConnectConfig.ThirdRobot.CommSys, ConnectConfig.ThirdRobot.IP, ConnectConfig.ThirdRobot.CommPort);
                //        if (flag1 )
                //        {
                //            LogHelper.loginfo.Info("下料机械臂连接成功");
                //        }
                //        else
                //        {
                //            LogHelper.logerror.Error($"下料机械臂连接失败,机械臂IP：{ConnectConfig.ThirdRobotIP}");
                //            MessageBox.Show("下料机械臂连接失败");
                //        }
                //        RobotStatus(ConnectConfig.ThirdRobot, flag1);
                //    });

                Task task3 = new Task(() =>
                {
                    //连接上下料机械臂运动接口
                    bool flag1 = RobotConnect.connectIPC(ConnectConfig.ThirdRobot.CommMot, ConnectConfig.ThirdRobot.IP, ConnectConfig.ThirdRobot.CommPort);
                    //连接上下料机械臂系统接口
                    // bool flag2 = RobotConnect.connectIPC(ConnectConfig.ThirdRobot.CommSys, ConnectConfig.ThirdRobot.IP, ConnectConfig.ThirdRobot.CommPort);
                    if (flag1)
                    {
                        LogHelper.loginfo.Info("下料机械臂连接成功");
                    }
                    else
                    {
                        LogHelper.logerror.Error($"下料机械臂连接失败,机械臂IP：{ConnectConfig.ThirdRobotIP}");
                        MessageBox.Show("下料机械臂连接失败");
                    }
                    RobotStatus(ConnectConfig.ThirdRobot, flag1);
                });
                task3.Start();
                taskList.Add(task3);


#endif

#if CONTROLER
                //控制器连接部分


                Task task4 = new Task(() =>
                {
                    bool flag = ControlerConnect.Connect();
                    if (flag)
                    {
                        ControlerStatus(true);
             
                    }
                    else
                    {
                        ControlerStatus(false);
                        Thread.Sleep(2000);
          
                    }
                });
                task4.Start();
                taskList.Add(task4);

#endif
                Task[] tasks = taskList.ToArray(typeof(Task)) as Task[];
                 var isSuccess = Task.WaitAll(tasks,1500);

                if (isSuccess != false)
                     ConnectConfig.Connected = true;
                else
                {
                    throw new Exception();
                }

            }
            catch
            {
                MessageBox.Show("设备连接异常");
            }



            



        }

        /// <summary>
        /// 断开设备连接
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConnectConfig_DisConnecting(object sender, EventArgs e)
        {
            if(ConnectConfig.Connected)
            {
                //关闭监测报警
                if (checktimer != null)
                {
                    LogHelper.loginfo.Info("监测设备报警定时器关闭");
                    Console.WriteLine("监测设备报警定时器关闭");
                    checktimer.Stop();
                    checktimer?.Dispose();
                    //清除机械臂检查任务
                   // ConnectConfig.robots.Clear();
                }
                //如果机械臂处于连接状态
                if (ConnectConfig.FirstRobot.Connected)
                {
                    //关闭机械臂运动通信代理
                    RobotConnect.disconnectIPC(ConnectConfig.FirstRobot.CommMot);
                    //RobotConnect.disconnectIPC(ConnectConfig.FirstRobot.CommSys);
                    RobotStatus(ConnectConfig.FirstRobot, false);
                    LogHelper.loginfo.Info("上下料机械臂连接成功断开");

                }
                if (ConnectConfig.SecondRobot.Connected)
                {

                    //关闭机械臂运动通信代理
                    RobotConnect.disconnectIPC(ConnectConfig.SecondRobot.CommMot);
                    //RobotConnect.disconnectIPC(ConnectConfig.SecondRobot.CommSys);
                    RobotStatus(ConnectConfig.SecondRobot, false);
                    LogHelper.loginfo.Info("上料机械臂连接成功断开");

                }
                if (ConnectConfig.ThirdRobot.Connected)
                {
                    //关闭机械臂运动通信代理
                    RobotConnect.disconnectIPC(ConnectConfig.ThirdRobot.CommMot);
                   // RobotConnect.disconnectIPC(ConnectConfig.ThirdRobot.CommSys);
                    RobotStatus(ConnectConfig.ThirdRobot, false);
                    LogHelper.loginfo.Info("下料机械臂连接成功断开");

                }

                if (ConnectConfig.ControlerConnected)
                {
                    ControlerConnect.Disconnect();
                    ControlerStatus(false);
                    LogHelper.loginfo.Info("控制器连接成功断开");
                }



                ConnectConfig.Connected = false;
                btn_Init.FillColor =  Color.Red;
                btn_Init.BtnText = "设备连接";
            }


        }




#region saga frame

        //声明一个线程对象，用于主程序的线程
        TaskCompletionSource<string> firstpromise;


        //创建Saga对象，控制任务的进行
        public Saga<string> mysaga = new Saga<string>();

        //建立相机有效信息的正则表达式（X;Y;角度;匹配分数;）
        //Regex msgRegex = new Regex(@"(?<xpos>\d+[.]\d+);(?<ypos>\d+[.]\d+);(?<angle>-?\d+[.]\d+);"); // (?:0\.\d+|[1])
        Regex msgRegex = new Regex(@"(?<xpos>\d+[.]\d+);(?<ypos>\d+[.]\d+);(?<angle>-?\d+[.]\d+);(?<grade>[0][.]\d+| [1]);");
        /// <summary>
        /// 初始化Saga，用于最初的相机坐标信息匹配
        /// </summary>
        private void InitSaga()
        {
            //初始化匹配方式  相机数据只要不是零就匹配成功
            mysaga.IsNeed += EffectMatch;
            //初始化中间件，目前只做了对应saga的部分
            InitMiddleWare();
        }

        /// <summary>
        /// 过滤相机数据
        /// </summary>
        /// <param name="effect"></param>
        /// <param name="str"></param>
        /// <returns></returns>
        private bool EffectMatch(Effect<string> effect, string str)
        {
            if (effect.effectType == EffectType.TAKE && msgRegex.IsMatch(str))
            {
                var match = msgRegex.Match(str);
                if (match.Groups["xpos"].Value != "0.000" || match.Groups["ypos"].Value != "0.000" || match.Groups["angle"].Value != "0.000" || match.Groups["grade"].Value != "0.000")
                {
                    //Console.WriteLine($"{match.Groups["xpos"].Value},,,,{match.Groups["ypos"].Value},,,,{match.Groups["angle"].Value}");
                    return true;
                }

            }
            return false;
        }

        private void InitMiddleWare()
        {
            if (MiddleWares.Count > 0)
            {
                return;
            }
            //添加saga中间件
            MiddleWares.Add("saga", SagaMiddleWare);
        }
        private void SagaMiddleWare(string msg)
        {
            mysaga.channel.DealCarmeraData(msg);
        }






#endregion




        /// <summary>
        /// 一号机械臂执行主函数
        /// </summary>
        /// <returns></returns>
        IEnumerable<Effect<string>> mainProcess()
        {

            //当对使用yield return的函数的返回值进行遍历的时候，
            //不是一次获取所有返回结果，而是一次只返回一个结果，
            //当循环到下一个变量时，从yield return位置重新开始执行代码，
            //到再次yield return返回下一个结果。
            //while (Running)
            //{

                //LogHelper.loginfo.Info($"上下料进行中，开始上下料倒数第{PanNumber}盘 !!!!!!!!!!!!!!!!!!!!!!!!");
                //加载自动程序
                yield return mysaga.Call((resolve, promise) =>
                {
                    //运行机械臂和控制器程序
                    RobotConnect.Start(ConnectConfig.FirstRobot);
                    RobotConnect.Start(ConnectConfig.SecondRobot);
                    RobotConnect.Start(ConnectConfig.ThirdRobot);


                    //开启暂停检测定时器

                    pausetimer.Start();

                    resolve("运行程序成功");
                });
                

                yield return mysaga.Call((resolve, promise) =>{

                    while(Running)
                    {
                        PanNumber = (int)(6-RobotConnect.GetR(ConnectConfig.FirstRobot, ParamConfig.PanNum_R_REG));
                        bool value  = RobotConnect.GetDin(ConnectConfig.FirstRobot, RobotPortConfig.INPUT_SUCCESS);
                        if (value )
                        {
                            Console.WriteLine($"程序结束标志:{value}");
                            Console.WriteLine("程序运行结束");
                            pausetimer.Stop();
                            resolve("等待运行程序完成");
                            return;
                        }
                    }
                });

            yield return mysaga.Call((resolve, promise) => {
                ////上料完成指示灯亮
                RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_SHANGLIAO_SEUUESS_LIGHT, true);
                ////运行指示灯灭
                RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_RUN_LIGHT, false);
                pausetimer.Stop();
                resolve("本次任务完成");
            });

            //    //正常盘
            //    if (PD_Status == PRODUCTION_STATUS.NORMAL)
            //    {
            //        yield return first_saga.Call(FastToOrigin);
            //        yield return first_saga.Call((resolve, promise) =>
            //        {
            //            Hsc3.Comm.GeneralPos dirPos;
            //            dirPos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.QULIAO_LR_REG);
            //            RobotConnect.MoveTo(ConnectConfig.FirstRobot, ref dirPos, (int)ParamConfig.MoveSpeed.ParamValue, true);
            //            resolve("");
            //        });
            //        //等到定位系统到位信号 
            //        yield return first_saga.Call((resolve, promise) =>
            //        {

            //            LogHelper.loginfo.Info("等待定位系统到位");
            //            while (!RobotConnect.GetDin(ConnectConfig.FirstRobot, PortConfig.INPUT_DINGWEI_ARRIVAL)) ;//只有定位系统到位后才抓取
            //            LogHelper.loginfo.Info("定位系统到位");
            //            resolve("");
            //        });
            //        yield return first_saga.Call(TakeDisks);//下料
            //        yield return first_saga.Call(FastToOrigin2);
            //        yield return first_saga.Call(NewDisks);
            //        yield return first_saga.Call(PlaceDisks);

            //    }
            //    //首盘
            //    else if(PD_Status == PRODUCTION_STATUS.FIRST)
            //    {

            //        yield return first_saga.Call(FastToOrigin2);
            //        yield return first_saga.Call((resolve, promise) =>
            //        {
            //            LogHelper.loginfo.Info("等待定位系统到位");
            //            while (!RobotConnect.GetDin(ConnectConfig.FirstRobot, PortConfig.INPUT_DINGWEI_ARRIVAL)) ;//只有定位系统到位后才抓取
            //            LogHelper.loginfo.Info("定位系统到位");
            //            resolve("");
            //        });
            //        yield return first_saga.Call(NewDisks);
            //        //等到定位系统到位信号 
            //        yield return first_saga.Call(PlaceDisks);
            //    }
            //    //末盘
            //    else
            //    {
            //        yield return first_saga.Call(FastToOrigin);
            //        yield return first_saga.Call((resolve, promise) =>
            //        {
            //            Hsc3.Comm.GeneralPos dirPos;
            //            dirPos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.QULIAO_LR_REG);
            //            RobotConnect.MoveTo(ConnectConfig.FirstRobot, ref dirPos, (int)ParamConfig.MoveSpeed.ParamValue, true);
            //            resolve("");
            //        });
            //        yield return first_saga.Call((resolve, promise) =>
            //        {

            //            LogHelper.loginfo.Info("等待定位系统到位");
            //            while (!RobotConnect.GetDin(ConnectConfig.FirstRobot, PortConfig.INPUT_DINGWEI_ARRIVAL)) ;//只有定位系统到位后才抓取
            //            LogHelper.loginfo.Info("定位系统到位");
            //            resolve("");
            //        });
            //        yield return first_saga.Call(TakeDisks);//下料

            //    }


            //    yield return first_saga.Call(UpdataMsg);

            //}//while循环结束

            ////盘数复位
            //PanNumber = 5;
            ////上料完成指示灯亮
            //RobotConnect.SetDout(ConnectConfig.FirstRobot, 2, true);
            ////运行指示灯灭
            //RobotConnect.SetDout(ConnectConfig.FirstRobot, 0, false);

            //Hsc3.Comm.GeneralPos dirpos;

            ////更新LR寄存器高度
            //foreach (var i in ParamConfig.LR_REG)
            //{
            //    dirpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", i);
            //    dirpos.vecPos[2] -= 0.001;
            //    RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, dirpos, i);
            //}


            //foreach (var i in ParamConfig.JR_REG)
            //{
            //    dirpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "JR", i);
            //    dirpos.vecPos[2] -= 0.001; 
            //    dirpos.isJoint = true;
            //    RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, dirpos, i);
            //}

            //RobotConnect.SaveData(ConnectConfig.FirstRobot, "JR");
            //RobotConnect.SaveData(ConnectConfig.FirstRobot, "LR");


            //if (PD_Status == PRODUCTION_STATUS.FIRST)
            //{
            //    PD_Status = PRODUCTION_STATUS.NORMAL;
            //    ucCheckBox1.Checked = false;
            //    ucCheckBox2.Visible = true;
            //}
            //if (PD_Status == PRODUCTION_STATUS.FINAL)
            //{
            //    PD_Status = PRODUCTION_STATUS.NORMAL;
            //    ucCheckBox2.Checked = false;
            //    ucCheckBox1.Visible = true;
            //}
        }

        private void RunTask()
        {
            //判断是否上下料打到自动
            while (ConnectConfig.ControlUnit.ReadCoil(ControlerPortConfig.UPDiskEMotor_AutoButton) == false || ConnectConfig.ControlUnit.ReadCoil(ControlerPortConfig.DOWNDiskEMotor_AutoButton) == false)
            {
                MessageBox.Show("请将上下料旋钮打到自动");
            }

            Task.Run(() =>
            {
                //程序就绪
                RobotConnect.AutoLoadProgram(ConnectConfig.FirstRobot, 0);
                RobotConnect.AutoLoadProgram(ConnectConfig.SecondRobot, 0);
                RobotConnect.AutoLoadProgram(ConnectConfig.ThirdRobot, 0);
                Console.WriteLine("切换为自动模式，等待启动信号");
                LogHelper.loginfo.Info("切换为自动模式，等待启动信号");

                while (Running)
                {
                    //Console.WriteLine("我在等待按钮");

                    //判断启动按钮
                    if (RobotConnect.GetDin(ConnectConfig.FirstRobot, RobotPortConfig.INPUT_START_KEY) && Running)
                    {
                        LogHelper.loginfo.Info($"开启一次上下料任务，剩余齿轮片数:{PanNumber} ！！！！！！！！！！");

                        Console.WriteLine("开始一次任务........................");


                        //给定位系统开始信号
                        //LocationStart();
                        if ((firstpromise == null || firstpromise.Task.IsCompleted))
                        {
                            firstpromise = new TaskCompletionSource<string>();
                            mysaga.AutoRun(mainProcess, firstpromise).ContinueWith((task) =>
                            {
                                //取消任务线程
                                for (int i = mysaga.listTask.Count - 1; i >= 0; i--)
                                {
                                    mysaga.listTask[i]?.TrySetCanceled();
                                }
                                firstpromise?.TrySetCanceled();

                                //如果是过程中切换成手动，则不记录
                                if(Running)
                                {
                                    Console.WriteLine("本次任务完成");
                                    LogHelper.loginfo.Info("本次上下料任务完成！！！！！！！！！！！！！！！！！！！！！！！！进入等待状态");
                                }
                            });

                        }

                    }

                }

                //RobotConnect.unload(ConnectConfig.FirstRobot);
                Console.WriteLine("程序已结束");
            });

        }

        //加载机械臂自动运行程序





#region  一号机械臂流程代码

        public void FastToOrigin(Func<string, bool> resolve, TaskCompletionSource<string> task)
        {

            while (!RobotConnect.GetDin(ConnectConfig.FirstRobot, RobotPortConfig.INPUT_YANMOJI_READY)) ;//就绪后往下进行
            LogHelper.loginfo.Info("研磨机上盘就绪");
            //因取消相机添加
            while (!RobotConnect.GetDin(ConnectConfig.FirstRobot, RobotPortConfig.INPUT_DINGWEI_READY)) ;//只有定位系统在工作状态才工作
            LogHelper.loginfo.Info("研磨机定位系统就绪");

            //获取当前笛卡尔点的数据
            List<double> curLoc = RobotConnect.GetLocData(ConnectConfig.FirstRobot);
            Hsc3.Comm.GeneralPos dirPos;
            //获取圆弧起始点姿态
            Hsc3.Comm.GeneralPos curPos = RobotConnect.GetCurPoint(ConnectConfig.FirstRobot, "LR");
            if (curPos.vecPos[1] < -400)
            {
                dirPos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", 35);
                RobotConnect.MoveTo(ConnectConfig.FirstRobot, ref dirPos, (int)ParamConfig.AutoMoveSpeed.ParamValue, true);
            }
            dirPos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.START_POINT_LR_REG);
            //将当前点笛卡尔坐标数据改变为目的地点的笛卡尔数据
            //直线到点
            RobotConnect.MoveTo(ConnectConfig.FirstRobot, ref dirPos, (int)ParamConfig.AutoMoveSpeed.ParamValue, true);
            //关节到点 改变位姿
            dirPos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "JR", 0);

            RobotConnect.MoveTo(ConnectConfig.FirstRobot, ref dirPos, (int)ParamConfig.AutoMoveSpeed.ParamValue, false);
            //ConnectConfig.FirstRobot.proMot.stopJog(0);
            resolve("运动到初始位置");

        }


        public void FastToOrigin2(Func<string, bool> resolve, TaskCompletionSource<string> task)
        {

            Hsc3.Comm.GeneralPos dirPos;

            Hsc3.Comm.GeneralPos curPos = RobotConnect.GetCurPoint(ConnectConfig.FirstRobot, "LR");
            if (curPos.vecPos[1] < -400)
            {
                dirPos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", 35);
                RobotConnect.MoveTo(ConnectConfig.FirstRobot, ref dirPos, (int)ParamConfig.AutoMoveSpeed.ParamValue, true);
            }
            //获取圆弧起始点姿态
            dirPos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.START_POINT_LR_REG);
            //直线到点
            RobotConnect.MoveTo(ConnectConfig.FirstRobot, ref dirPos, (int)ParamConfig.AutoMoveSpeed.ParamValue, true);

            //关节到点 改变位姿(取料)
            dirPos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "JR", 1);

            RobotConnect.MoveTo(ConnectConfig.FirstRobot, ref dirPos, (int)ParamConfig.AutoMoveSpeed.ParamValue, false);
            //ConnectConfig.FirstRobot.proMot.stopJog(0);
            resolve("运动到初始位置");

        }






        /// <summary>
        /// 吸取盘片
        /// </summary>
        /// <returns></returns>
        IEnumerable<Effect<string>> TakeDisks()
        {
            
 
            yield return mysaga.Call((resolve, promise) =>
            {
                LogHelper.loginfo.Info("开始下料");
                //取磨过的料
                int delay = (int)ParamConfig.Production_97R_AbsorbDelay.ParamValue;
                Console.Write(delay);

                PutTakeDiscs(true, false, 0, ParamConfig.Production_97R_DropHeight.ParamValue,0, delay);
                resolve("");
            });

            //监测下料机是否准备好
            //第一次下料要判断下料盘是否准备好
            while (!RobotConnect.GetDin(ConnectConfig.FirstRobot, RobotPortConfig.INPUT_XIALIAO_READY)) ;//就绪后往下进行
            LogHelper.loginfo.Info("下料机就绪");

            yield return mysaga.Call((resolve, promise) =>
            {
                Hsc3.Comm.GeneralPos dirPos;


                dirPos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", 7);
                RobotConnect.MoveTo(ConnectConfig.FirstRobot, ref dirPos, (int)ParamConfig.AutoMoveSpeed.ParamValue, true);
           
                dirPos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", 6);
                RobotConnect.MoveTo(ConnectConfig.FirstRobot, ref dirPos, (int)ParamConfig.AutoMoveSpeed.ParamValue, true);
                resolve("");
            });
            yield return mysaga.Call((resolve, promise) =>
            {
                PutTakeDiscs(false, false, 0, ParamConfig.XiaLiaoHeight.ParamValue);
                resolve("");
            });


            yield return mysaga.Call((resolve, promise) =>
            {
                //给下料机发送信号准备下一盘
                RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_XIALIAO_SUCCESS, true);
                Thread.Sleep(50);
                RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_XIALIAO_SUCCESS, false);
                LogHelper.loginfo.Info("下料完成");
                resolve("");
            });

        }


        IEnumerable<Effect<string>> NewDisks()
        {
            //第一次要判断新料是否准备好
            //if (PanNumber == 5)
            LogHelper.loginfo.Info("开始上料");
            while (!RobotConnect.GetDin(ConnectConfig.FirstRobot, RobotPortConfig.INPUT_SHANGLIAO_READY)) ;//就绪后往下进行
            LogHelper.loginfo.Info("上料机就绪");
            yield return mysaga.Call((resolve, promise) =>
            {
                Hsc3.Comm.GeneralPos dirPos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", 5);
                RobotConnect.MoveTo(ConnectConfig.FirstRobot, ref dirPos, (int)ParamConfig.AutoMoveSpeed.ParamValue, true);
                resolve("");
            });

            yield return mysaga.Call((resolve, promise) =>
            {
                //PutTakeDiscs(true, false, 0, ParamConfig.ShangLiaoHeight.ParamValue);
                //如果发生真空报警，下次只上料不下料
                //if (ParamConfig.WarnFlag)
                    //ParamConfig.shangliao_erro = true;
                resolve("");
            });
          
            yield return mysaga.Call((resolve, promise) =>
            {
                //给上料机信号
                RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_SHANGLIAO_SUCCESS, true);
                Thread.Sleep(50);
                RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_SHANGLIAO_SUCCESS, false);
                resolve("");
            });


        }




        /// <summary>
        /// 放置盘片
        /// </summary>
        /// <param name="resolve"></param>
        /// <param name="task"></param>

        IEnumerable<Effect<string>> PlaceDisks()
        {
            //给第一个盘加offset
            yield return mysaga.Call((Action<Func<string, bool>, TaskCompletionSource<string>>)((resolve, promise) =>
            {
                Hsc3.Comm.GeneralPos dirpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", 9);
                switch (PanNumber)
                {
                    case 5:
                        dirpos.vecPos[0] += ParamConfig.FirstPanOffset.Xoffset;
                        dirpos.vecPos[1] += ParamConfig.FirstPanOffset.Yoffset;
                        dirpos.vecPos[3] += ParamConfig.FirstPanOffset.Angeloffset;
                        break;
                    case 4:
                        dirpos.vecPos[0] += ParamConfig.SecondPanOffset.Xoffset;
                        dirpos.vecPos[1] += ParamConfig.SecondPanOffset.Yoffset;
                        dirpos.vecPos[3] += ParamConfig.SecondPanOffset.Angeloffset;
                        break;
                    case 3:
                        dirpos.vecPos[0] += ParamConfig.ThirdPanOffset.Xoffset;
                        dirpos.vecPos[1] += ParamConfig.ThirdPanOffset.Yoffset;
                        dirpos.vecPos[3] += ParamConfig.ThirdPanOffset.Angeloffset;
                        break;
                    case 2:
                        dirpos.vecPos[0] += ParamConfig.FourthPanOffset.Xoffset;
                        dirpos.vecPos[1] += ParamConfig.FourthPanOffset.Yoffset;
                        dirpos.vecPos[3] += ParamConfig.FourthPanOffset.Angeloffset;
                        break;
                    case 1:
                        dirpos.vecPos[0] += ParamConfig.FifthPanOffset.Xoffset;
                        dirpos.vecPos[1] += ParamConfig.FifthPanOffset.Yoffset;
                        dirpos.vecPos[3] += ParamConfig.FifthPanOffset.Angeloffset;
                        break;
                }
                //移动到该点
                RobotConnect.MoveTo(ConnectConfig.FirstRobot, ref dirpos, (int)ParamConfig.AutoMoveSpeed.ParamValue, true);


                resolve("");
            }));
            yield return mysaga.Call((resolve, promise) =>
            {
                PutTakeDiscs(false, true, 0, ParamConfig.Production_97R_ReleaseHeight.ParamValue);
                LogHelper.loginfo.Info("上料完成");
                resolve("");
            });

        }
        /// <summary>
        /// 更新盘片数量信息，满足要求则结束任务
        /// </summary>
        /// <param name="resolve"></param>
        /// <param name="task"></param>

        IEnumerable<Effect<string>> UpdataMsg()
        {

            //获取过度点
            Hsc3.Comm.GeneralPos dirPos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", 7);

            //点动研磨机
            if (PanNumber > 1)
            {
                yield return mysaga.Call((resolve, promise) => {
                //因取消相机添加
                GrabCompletted(); //抓取后给定位系统发个信号
                //因取消相机注释
                //DiandongYanmoji();
                    resolve("");
                });

            }
            else
            {
                yield return mysaga.Call((resolve, promise) => {
                    //因取消相机注释
                    //DiandongYanmoji();
                    RobotConnect.MoveTo(ConnectConfig.FirstRobot, ref dirPos, (int)ParamConfig.AutoMoveSpeed.ParamValue, true);   //回等待点
                    //因取消相机添加
                    GrabCompletted(); //抓取后给定位系统发个信号
                    resolve("");
                });

            }


            //原子操作
            LogHelper.loginfo.Info($"第{6-PanNumber}盘齿轮片上下料完成");

            Interlocked.Decrement(ref _pannum);
            //为了修改文件里的值
            PanNumber = _pannum;


        }





#endregion




#region  一号机械臂过程辅助函数

        public void GrabCompletted()
        {
            RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_DINGWEI_ZHUAQUWANCHENG, true);
            Thread.Sleep(500);
            RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_DINGWEI_ZHUAQUWANCHENG, false);
        }

        public void DiandongYanmoji()
        {
            //点动开始
            RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_YANMOJI_DIANDONG, true);
            Thread.Sleep(200);
            RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_YANMOJI_DIANDONG, false);
            //Thread.Sleep(1500);

        }
        /// <summary>
        /// 给定位系统启动信号
        /// </summary>
        public void LocationStart()
        {
            RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_DINGWEI_STARTSYS, true);
            Thread.Sleep(500);
            RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_DINGWEI_STARTSYS, false);
        }


        /// <summary>
        /// 吸取或释放盘片
        /// </summary>
        /// <param name="flag">true 吸取 false 释放</param>
        /// <param name="isturn">true 落地旋转  </param>
        /// <param name="mode">0 固定高度  1 光电检测</param>
        /// <param name="height"> 固定高度  默认为0</param>
        /// <param name="offset">上升的时候的补偿值</param>
        /// <param name="delay">吸取时候的延时，默认500</param>
        /// 
        private void PutTakeDiscs(bool flag, bool isturn, int mode, double height = 0, int offset = 0,int delay = 500)
        {
            if (flag)//吸取
            {
                switch (mode)
                {
                    case 0://固定高度
                        RobotPortConfig.EVC1_STATUS = RobotPortConfig.EVC_STATUS.INSPIRATE;
                        RobotConnect.JointManualIncMove(ConnectConfig.FirstRobot, 2, (int)ParamConfig.ManualMoveSpeed.ParamValue, height, Hsc3.Comm.DirectType.NEGATIVE);
                        //判断真空发生器
                        delay = delay/2;
                        Thread.Sleep(delay);
                        if (!RobotConnect.GetDin(ConnectConfig.FirstRobot, RobotPortConfig.FirstRobot_INPUT_VacuumDegree))
                        {
                            //吹气
                            Thread.Sleep(delay);
                            if (!RobotConnect.GetDin(ConnectConfig.FirstRobot, RobotPortConfig.FirstRobot_INPUT_VacuumDegree))
                            {
                                RobotPortConfig.EVC1_STATUS = RobotPortConfig.EVC_STATUS.EXPIRATE;
                                //提示报警
                                WarnAndShowMsg(mysaga, "吸取失败");
                            }

                        }
                        //Thread.Sleep(1000);
                        RobotConnect.JointManualIncMove(ConnectConfig.FirstRobot, 2, (int)(int)ParamConfig.ManualMoveSpeed.ParamValue, height+offset, Hsc3.Comm.DirectType.POSITIVE);
                        //吸起后再检查
                        if (!RobotConnect.GetDin(ConnectConfig.FirstRobot, RobotPortConfig.FirstRobot_INPUT_VacuumDegree))
                        {
                            RobotConnect.JointManualIncMove(ConnectConfig.FirstRobot, 2, (int)ParamConfig.ManualMoveSpeed.ParamValue, height + offset, Hsc3.Comm.DirectType.NEGATIVE);
                            RobotPortConfig.EVC1_STATUS = RobotPortConfig.EVC_STATUS.EXPIRATE;
                            RobotConnect.JointManualIncMove(ConnectConfig.FirstRobot, 2, (int)ParamConfig.ManualMoveSpeed.ParamValue, height + offset, Hsc3.Comm.DirectType.POSITIVE);

                            //报警并显示信息
                            WarnAndShowMsg(mysaga, "有盘片被拽落");
                        }
                        break;
                    case 1://电涡流检测

                        RobotConnect.JointManualIncMove(ConnectConfig.FirstRobot, 2, (int)ParamConfig.ManualMoveSpeed.ParamValue, 15, Hsc3.Comm.DirectType.NEGATIVE);
                        RobotConnect.JointManualConMove(ConnectConfig.FirstRobot, 2, 1, Hsc3.Comm.DirectType.NEGATIVE);
                        float high;
                        high = RobotConnect.MeasureHeight();
                        while (high >=1.5)
                        {
                            high = RobotConnect.MeasureHeight();
                            Console.WriteLine($"传感器高度：{high}");
                        }
                        RobotConnect.StopRobot(ConnectConfig.FirstRobot);
                        RobotPortConfig.EVC1_STATUS = RobotPortConfig.EVC_STATUS.INSPIRATE;
                        RobotConnect.JointManualIncMove(ConnectConfig.FirstRobot, 2, (int)ParamConfig.ManualMoveSpeed.ParamValue, height, Hsc3.Comm.DirectType.POSITIVE);
                        break;
                }


            }
            else
            {
                switch (mode)
                {
                    case 0://固定高度
                        //下降
                        RobotConnect.JointManualIncMove(ConnectConfig.FirstRobot, 2, (int)ParamConfig.ManualMoveSpeed.ParamValue, height, Hsc3.Comm.DirectType.NEGATIVE);
               
                        if (isturn)
                        {
                            RobotConnect.JointManualIncMove(ConnectConfig.FirstRobot, 3, 100, ParamConfig.RotateAngel.ParamValue, Hsc3.Comm.DirectType.NEGATIVE);
                            RobotConnect.JointManualIncMove(ConnectConfig.FirstRobot, 3, 100, ParamConfig.RotateAngel.ParamValue * 2, Hsc3.Comm.DirectType.POSITIVE);
                            RobotConnect.JointManualIncMove(ConnectConfig.FirstRobot, 3, 100, ParamConfig.RotateAngel.ParamValue, Hsc3.Comm.DirectType.NEGATIVE);
                        }
                        //吸嘴停止吸气
                        RobotPortConfig.EVC1_STATUS = RobotPortConfig.EVC_STATUS.CLOSE;
                        //吸嘴吹气
                        RobotPortConfig.EVC2_STATUS = RobotPortConfig.EVC_STATUS.EXPIRATE;

                        //上升
                        RobotConnect.JointManualIncMove(ConnectConfig.FirstRobot, 2, (int)ParamConfig.ManualMoveSpeed.ParamValue, height, Hsc3.Comm.DirectType.POSITIVE);
                        //吸嘴停止吹气
                        RobotPortConfig.EVC2_STATUS = RobotPortConfig.EVC_STATUS.CLOSE;
                        break;
                }
            }
        }

  

        IEnumerable<Effect<string>> MoveTo_ShangLiao()
        {
            yield return mysaga.Call((resolve, promise) =>
            {   //先判断研磨机是否升起
                while (!RobotConnect.GetDin(ConnectConfig.FirstRobot, RobotPortConfig.INPUT_YANMOJI_READY))
                {
                    MessageBox.Show("请先将研磨机升起");
                }
                resolve("");

            });

            yield return mysaga.Call(FastToOrigin2);
            yield return mysaga.Call((resolve, promise) => {
                //移动到上料点
                Hsc3.Comm.GeneralPos dirPos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", 5);
                RobotConnect.MoveTo(ConnectConfig.FirstRobot, ref dirPos, 30, true);
                MessageBox.Show("到达上料点");
                resolve("");
            });

        }


        /// <summary>
        /// 等待信号，true为高电平有效
        /// </summary>
        public async void WaitSign(int inputio,bool vaild)
        {
            var t1 = Task.Run(() =>
            {
                while (!(RobotConnect.GetDin(ConnectConfig.FirstRobot, inputio) == vaild)) ;
            });
            t1.Start();
            var t = await Task.WhenAny(new Task[] {
                    t1,
                    Task.Delay(15000)
                });
            if (t != t1)
            {
                LogHelper.logerror.Error("等待信号超时");
             
                WarnAndShowMsg(mysaga,"等到信号超时");
                t.Dispose();
            }
            t1.Dispose();
        }


#endregion


#region 报警机制
        /// <summary>
        /// 开启设备监测
        /// </summary>
        private void CheckDevices(Object source, ElapsedEventArgs e)
        {
#if FIRST_ROBOT
            ChooseCheckRobot(ConnectConfig.FirstRobot);
#endif
#if SECOND_ROBOT
            ChooseCheckRobot(ConnectConfig.SecondRobot);
#endif
#if THIRD_ROBOT
            ChooseCheckRobot(ConnectConfig.ThirdRobot);
#endif
#if CONTROLER
            CheckControler();
#endif
        }

        public void CheckControler()
        {
            //查看控制器是否有报警
            if(ConnectConfig.ControlUnit.ReadCoil(ControlerPortConfig.UPDiskEMotor_WanningCoil))
            {
                string str = "未知错误";
                EMotor.ErrCode.TryGetValue(ConnectConfig.ControlUnit.ReadInt16(ControlerPortConfig.UPDiskEMotor_AlalmCode), out str);
                WarnAndShowMsg(mysaga, str);
            }
            if (ConnectConfig.ControlUnit.ReadCoil(ControlerPortConfig.DOWNDiskEMotor_WanningCoil))
            {
                string str = "未知错误";
                EMotor.ErrCode.TryGetValue(ConnectConfig.ControlUnit.ReadInt16(ControlerPortConfig.DOWNDiskEMotor_AlalmCode), out str);
                WarnAndShowMsg(mysaga, str);
            }
            if (ConnectConfig.ControlUnit.ReadCoil(ControlerPortConfig.TAKEDiskEMotor_WanningCoil))
            {
                string str = "未知错误";
                EMotor.ErrCode.TryGetValue(ConnectConfig.ControlUnit.ReadInt16(ControlerPortConfig.TAKEDiskEMotor_AlalmCode), out str);
                WarnAndShowMsg(mysaga, str);
            }

            //显示对应的报警信息


        }
        public void ChooseCheckRobot(Robot robot)
        {
            Hsc3.Comm.ErrLevel errorlevel = new Hsc3.Comm.ErrLevel();
            ulong code = 0;
            string strMsg = "";
            //获取错误信息
            string ret = "";

            //robot.CommMot.execCmd("?group[0].status", ref ret, 0);
            bool isFaults = RobotConnect.GetDout(robot, RobotPortConfig.OUTPUT_ROBOT_FAULTS);
            ulong ErrCode = robot.proSys.getMessage(ref errorlevel, ref code, ref strMsg, 50);
            //if(isFaults)
            //{
            //    WarnAndShowMsg(first_saga, robot.Name + ":" + "错误停止中");
            //    LogHelper.logerror.Error(robot.Name + ":" + "错误停止中");
            //}

            switch (errorlevel)
            {
                case Hsc3.Comm.ErrLevel.ERR_LEVEL_ERROR:
                case Hsc3.Comm.ErrLevel.ERR_LEVEL_FATAL:

                    robot.Error = true;
                    WarnAndShowMsg(mysaga,robot.Name + ":" + strMsg);
                    LogHelper.logerror.Error(robot.Name + ":" + strMsg);
                    break;
                case Hsc3.Comm.ErrLevel.ERR_LEVEL_INFO:
                    LogHelper.loginfo.Info(robot.Name + ":" + strMsg);
                    Console.WriteLine(robot.Name + ":" + strMsg);
                    break;
            }



            ////监测到有系统错误
            //if (isFaults &&( errorlevel == Hsc3.Comm.ErrLevel.ERR_LEVEL_ERROR || errorlevel == Hsc3.Comm.ErrLevel.ERR_LEVEL_FATAL))
            //{
            //    if ((errorlevel == Hsc3.Comm.ErrLevel.ERR_LEVEL_ERROR || errorlevel == Hsc3.Comm.ErrLevel.ERR_LEVEL_FATAL) && ret == "\"6\"")
            //    {
            //        if(robot == ConnectConfig.FirstRobot)
            //        {
            //            WarnAndShowMsg(first_saga, "上下料机械臂：" + strMsg);
            //            LogHelper.logerror.Error("上下料机械臂：" + strMsg);
            //        }
            //        else if(robot == ConnectConfig.SecondRobot)
            //        {
            //            WarnAndShowMsg(first_saga, "上料机械臂：" + strMsg);
            //            LogHelper.logerror.Error("上料机械臂：" + strMsg);
            //        }
            //        else
            //        {
            //            WarnAndShowMsg(first_saga, "下料机械臂：" + strMsg);
            //            LogHelper.logerror.Error("下料机械臂：" + strMsg);
            //        }
            //    }

            //}
            //if (errorlevel == Hsc3.Comm.ErrLevel.ERR_LEVEL_INFO)
            //{
            //    if (robot == ConnectConfig.FirstRobot)
            //    {
            //        LogHelper.loginfo.Info("上下料机械臂：" + strMsg);
            //        Console.WriteLine("上下料机械臂：" + strMsg);
            //    }
            //    else if (robot == ConnectConfig.SecondRobot)
            //    {
            //        LogHelper.loginfo.Info("上料机械臂：" + strMsg);
            //        Console.WriteLine("上料机械臂：" + strMsg);
            //    }
            //    else
            //    {
            //        LogHelper.loginfo.Info("下料机械臂：" + strMsg);
            //        Console.WriteLine("下料机械臂：" + strMsg);
            //    }

            //}
        }

        public void WarnAndShowMsg(Saga<string> saga,string msg)
        {
            //置位报警标志
            ParamConfig.WarnFlag = true;
            if (!saga.warnMsg.Contains(msg))
            {
                //添加日志
                LogHelper.logerror.Error(msg);
                //添加报警信息
                mysaga.warnMsg.Add(msg);
                ucRollText1.Text += msg+"\t";
                Alarm_Lamp.TwinkleSpeed = 500;
            }
        }

        private void CheckDevicesStart()
        {
            checktimer = new System.Timers.Timer(500);
            checktimer.Elapsed += CheckDevices;
            checktimer.AutoReset = true;
            checktimer.Enabled = true;
            checktimer.Start();
            Console.WriteLine("设备检测定时器开启");
            LogHelper.loginfo.Info("设备检测定时器开启");

        }


        private void CheckPause(Object source, ElapsedEventArgs e)
        {
            //如果按键按下
            ConnectConfig.FirstRobot.Pause();
            ConnectConfig.SecondRobot.Pause();
            ConnectConfig.ThirdRobot.Pause();

        }
        

#endregion





#region 提取坐标信息
        /// <summary>
        /// 提取X，Y,角度值
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="XPoint"></param>
        /// <param name="YPoint"></param>
        /// <returns></returns>
        private double ParseCMD(string str, out double XPoint, out double YPoint,out double Grade)
        {

            var match = msgRegex.Match(str);
            XPoint = double.Parse(match.Groups["xpos"].Value);
            YPoint = double.Parse(match.Groups["ypos"].Value);
            Grade = double.Parse(match.Groups["grade"].Value);
            Console.WriteLine("解析完成");
            return double.Parse(match.Groups["angle"].Value);
            

        }
        private PointF ParseCMD(string cmd)
        {
            var match = msgRegex.Match(cmd);
            return new PointF(float.Parse(match.Groups["xpos"].Value), float.Parse(match.Groups["ypos"].Value));
        }


#endregion


#region 设备状态显示

        public void RobotStatus(Robot robot,bool status)
        {
            Task.Run(() => {

                if (robot == ConnectConfig.FirstRobot)
                {
                    if (status)
                    {
                        ConnectConfig.FirstRobot.Connected = true;
                        SetText("在线", lab_FirstRobotStatus);
                        SetColor(Color.Lime, lab_FirstRobotStatus);
                    }
                    else
                    {
                        ConnectConfig.FirstRobot.Connected = false;
                        SetText("离线", lab_FirstRobotStatus);
                        SetColor(Color.Red, lab_FirstRobotStatus);
                    }
                }
                else if (robot == ConnectConfig.SecondRobot)
                {
                    if (status)
                    {
                        ConnectConfig.SecondRobot.Connected = true;
                        SetText("在线", lab_SecondRobotStatus);
                        SetColor(Color.Lime, lab_SecondRobotStatus);
                    }
                    else
                    {
                        ConnectConfig.SecondRobot.Connected = false;
                        SetText("离线", lab_SecondRobotStatus);
                        SetColor(Color.Red, lab_SecondRobotStatus);
                    }
                }
                else
                {
                    if (status)
                    {
                        ConnectConfig.ThirdRobot.Connected = true;
                        SetText("在线", lab_ThirdRobotStatus);
                        SetColor(Color.Lime, lab_ThirdRobotStatus);
                    }
                    else
                    {
                        ConnectConfig.ThirdRobot.Connected = false;
                        SetText("离线", lab_ThirdRobotStatus);
                        SetColor(Color.Red, lab_ThirdRobotStatus);
                    }
                }


            });

        }

        public void ControlerStatus(bool status)
        {
            Task.Run(() =>
            {
                if (status)
                {
                    ConnectConfig.ControlerConnected = true;
                    SetText("在线", lab_ControlerStatus);
                    SetColor(Color.Lime, lab_ControlerStatus);
                }
                else
                {
                    ConnectConfig.ControlerConnected = false;
                    SetText("离线", lab_ControlerStatus);
                    SetColor(Color.Red, lab_ControlerStatus);
                }
            });

        }

        /// <summary>
        /// 设置控件背景色
        /// </summary>
        /// <param name="color"></param>
        /// <param name="ctrl"></param>
        private void SetColorB(Color color, Control ctrl)
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    ctrl.BackColor = color;
                }));
            }
            else
            {
                ctrl.BackColor = color;
            }
        }
        /// <summary>
        /// 设置控件文字
        /// </summary>
        /// <param name="str"></param>
        /// <param name="ctrl"></param>
        private void SetText(string str, Control ctrl)
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    ctrl.Text = str;
                }));
            }
            else
            {
                ctrl.Text = str;
            }
        }


        private void SetText(string str, ToolStripLabel ctrl)
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    ctrl.Text = str;
                }));
            }
            else
            {
                ctrl.Text = str;
            }
        }

        private void SetColor(Color color, ToolStripLabel ctrl)
        {
            if (InvokeRequired && !this.IsDisposed)
            {
                Invoke(new Action(() =>
                {
                    ctrl.ForeColor = color;
                }));
            }
            else
                ctrl.ForeColor = color;
        }

        private void SetChecked(bool chk,UCSwitch ctrl)
        {
            if(InvokeRequired && !this.IsDisposed)
            {
                Invoke(new Action(() =>
                {
                    ctrl.Checked = chk;
                }));
            }
            else
            {
                ctrl.Checked = chk;
            }
        }


#endregion


#region  控件效果
        /// <summary>
        /// 开始按钮  现改为自动手动切换按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Start_Click(object sender, EventArgs e)
        {
            
            Running = !Running; 
            //如果初始化未完成或者启动按钮未选中
            if(!InitFlag || !Running)  //初始化按钮为false或者Runing标志位为false
            {
                //btn_Start.Checked = false;
                //ste_WorkStep.StepIndex = 1;
                //停止任务
                //StopTask();
                //运行指示灯灭
                if (Running != false)
                    Running = false;
                if(!InitFlag)
                {
                    MessageBox.Show("未初始化，请先初始化");
                }
                
         
 
            }
            else//Running==true  InitFlag == true
            {

                //开始运行任务

                //RunTask();
                Console.WriteLine("开始运行任务");


            }
        }




        private async void btn_Init_BtnClick(object sender, EventArgs e)
        {
            ConnectConfig.Error_Connect_Flag = !ConnectConfig.Error_Connect_Flag;
            //判断初始化状态
            if (!Connectflag)//判断初始化标志位  开始是false
            {
                await Task.Run(()=>{

                    Connectflag = true;  //初始化完成 
                                      //Thread.Sleep(1000);
                                      //设备复位
                                      //通讯连接
                                      //await Task.Run(() =>
                                      //{
                                      //    ConnectConfig.Connect();
                                      //});


                    //这里有问题（连接是异步的，所以这块用标志位不行）
                    //连接成功完成后开启检查线程并初始化设备

                });

                Thread.Sleep(100);//给100ms连接时间，若未连接上，则断开连接

                if (ConnectConfig.Connected)
                {
                    //开启设备监测
                    CheckDevicesStart();

                    //初始化标志位归位
                    RobotConnect.SetR(ConnectConfig.FirstRobot, 14, 0);
                    RobotConnect.SetR(ConnectConfig.SecondRobot, 14, 0);
                    RobotConnect.SetR(ConnectConfig.ThirdRobot, 14, 0);
                }
                else
                {
                    Connectflag = false;
                }




            }
            //系统已经为初始化状态，此时应该取消初始化
            else
            {
                //Runing = true
                //断开通讯连接
                //设置初始化按钮状态
                //设置设备链接状态
                Connectflag = false;

                //取消机械臂报警线程
                //有问题 ，取消检查线程后连接不上机械臂了
                //foreach (var i in first_saga.checkRobotTask)
                //{
                //    i.Value.Cancel();
                //}

            }
        }


        private async void MoveTo_ShangLiao_Click(object sender, EventArgs e)
        {
            if (!ConnectConfig.FirstRobot.Connected )
            {
                MessageBox.Show("设备未连接");

                return;
            }

            if (!MutualFlag && !Running)  //互斥标志位为false才可运动
            {
                MutualFlag = true;

                if (!RobotConnect.GetDin(ConnectConfig.FirstRobot, RobotPortConfig.INPUT_YANMOJI_READY))//就绪后往下进行
                {
                    MessageBox.Show("请确保研磨机已经上升到顶端");
                    MutualFlag = false;
                    return;
                }
                TaskCompletionSource<string> task = new TaskCompletionSource<string>();
                ParamConfig.runflag = true;
                await mysaga.AutoRun(MoveTo_ShangLiao, task);
                if (task != null)
                    mysaga.Cancel(task);
                
                ParamConfig.runflag = false;
                MutualFlag = false;
            }
        }



        private void AppendCount(int num)
        {
            TakeCnt = TakeCnt + num;
            PutCnt = PutCnt + num;
        }
        private int TakeCnt
        {
            get
            {
                try
                {
                    return int.Parse(lab_TakeNum.Text);
                }
                catch
                {
                    return 0;
                }
            }
            set
            {
                SetText(value.ToString(), lab_TakeNum);
            }
        }

        private int PutCnt
        {
            get
            {
                try
                {
                    return int.Parse(lab_PutNum.Text);
                }
                catch
                {
                    return 0;
                }
            }
            set
            {
                SetText(value.ToString(), lab_PutNum);
            }
        }

        #endregion

        private void skinButton10_Click(object sender, EventArgs e)
        {
            RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_YANMOJI_DIANDONG, true);
            Thread.Sleep(100);
            RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_YANMOJI_DIANDONG, false);
            Thread.Sleep(2000);
        }



        private void ucCheckBox1_CheckedChangeEvent(object sender, EventArgs e)
        {
            if (ucCheckBox1.Checked)
            {

                if (!ConnectConfig.FirstRobot.Connected)
                {
                    MessageBox.Show("请先连接设备");
                    ucCheckBox1.Checked = false;
                    return;
                }
                PD_Status = PRODUCTION_STATUS.FIRST;
                RobotConnect.SetR(ConnectConfig.FirstRobot, ParamConfig.FirstOrEndPan_R_REG, 1);
                RobotConnect.SetR(ConnectConfig.SecondRobot, ParamConfig.FirstOrEndPan_R_REG, 1);
                RobotConnect.SetR(ConnectConfig.ThirdRobot, ParamConfig.FirstOrEndPan_R_REG, 1);
                ucCheckBox2.Visible = false;

            }    
            else 
            {

                PD_Status = PRODUCTION_STATUS.NORMAL;
                RobotConnect.SetR(ConnectConfig.FirstRobot, ParamConfig.FirstOrEndPan_R_REG, 3);
                RobotConnect.SetR(ConnectConfig.SecondRobot, ParamConfig.FirstOrEndPan_R_REG, 3);
                RobotConnect.SetR(ConnectConfig.ThirdRobot, ParamConfig.FirstOrEndPan_R_REG, 3);
                ucCheckBox2.Visible = true;
            }
            Console.WriteLine(PD_Status);
        }

        private void ucCheckBox2_CheckedChangeEvent(object sender, EventArgs e)
        {
            if (ucCheckBox2.Checked)
            {
                if(!ConnectConfig.Connected)
                {
                    MessageBox.Show("请先连接设备");
                    ucCheckBox2.Checked = false;
                    return;
                }
                PD_Status = PRODUCTION_STATUS.FINAL;
                RobotConnect.SetR(ConnectConfig.FirstRobot, ParamConfig.FirstOrEndPan_R_REG, 2);
                RobotConnect.SetR(ConnectConfig.SecondRobot, ParamConfig.FirstOrEndPan_R_REG, 2);
                RobotConnect.SetR(ConnectConfig.ThirdRobot, ParamConfig.FirstOrEndPan_R_REG, 2);
                ucCheckBox1.Visible = false;
            }
                
            else 
            {
                PD_Status = PRODUCTION_STATUS.NORMAL;
                RobotConnect.SetR(ConnectConfig.FirstRobot, ParamConfig.FirstOrEndPan_R_REG, 3);
                RobotConnect.SetR(ConnectConfig.SecondRobot, ParamConfig.FirstOrEndPan_R_REG, 3);
                RobotConnect.SetR(ConnectConfig.ThirdRobot, ParamConfig.FirstOrEndPan_R_REG, 3);
                ucCheckBox1.Visible = true ;
            }
            Console.WriteLine(PD_Status);
        }



        private void btn_WaitPoint_Click(object sender, EventArgs e)
        {
            if (!ConnectConfig.Connected)
            {
                MessageBox.Show("设备未连接");
                return;
            }


            if (!MutualFlag && !Running)
            {
                MutualFlag = true;
                if (!RobotConnect.GetDin(ConnectConfig.FirstRobot, RobotPortConfig.INPUT_YANMOJI_READY))//就绪后往下进行
                {
                    MessageBox.Show("请确保研磨机已经上升到顶端");
                    MutualFlag = false;
                    return;
                }

                Hsc3.Comm.GeneralPos curpos = RobotConnect.GetCurPoint(ConnectConfig.FirstRobot, "JR");
                if(curpos.vecPos[0] < 0 && curpos.vecPos[1]<0)
                {
                    MessageBox.Show("不可到达，请先移动到原点");
                    MutualFlag = false;
                    return;
                }
                else if(curpos.vecPos[1] > 0)
                {
                    Hsc3.Comm.GeneralPos dirpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", 7);
                    RobotConnect.MoveTo(ConnectConfig.FirstRobot, ref dirpos, (int)ParamConfig.ManualMoveSpeed.ParamValue, true);
                }
                else
                {
                    Hsc3.Comm.GeneralPos dirpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", 7);
                    RobotConnect.MoveTo(ConnectConfig.FirstRobot, ref dirpos, (int)ParamConfig.ManualMoveSpeed.ParamValue, true);
                }
                RobotConnect.SetDout(ConnectConfig.FirstRobot, RobotPortConfig.OUTPUT_ROBOT_IN_YANMOJI,false);
                MessageBox.Show("已移动到等待点");
                MutualFlag = false;
            }

        }

        private void btn_ScanStartPoint_Click(object sender, EventArgs e)
        {
            if (!ConnectConfig.FirstRobot.Connected)
            {
                MessageBox.Show("设备未连接");
                return;
            }
            if (!MutualFlag && !Running)
            {
                MutualFlag = true;
                if (!RobotConnect.GetDin(ConnectConfig.FirstRobot, RobotPortConfig.INPUT_YANMOJI_READY))//就绪后往下进行
                {
                    MessageBox.Show("请确保研磨机已经上升到顶端");
                    MutualFlag = false;
                    return;
                }
                //获取当前笛卡尔点的数据
                List<double> curLoc = RobotConnect.GetLocData(ConnectConfig.FirstRobot);
                Hsc3.Comm.GeneralPos dirPos;
                //获取圆弧起始点姿态
                Hsc3.Comm.GeneralPos curPos = RobotConnect.GetCurPoint(ConnectConfig.FirstRobot, "LR");
                if (curPos.vecPos[1] < -400)
                {                                                                                                                                               
                    dirPos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", 35);
                    RobotConnect.MoveTo(ConnectConfig.FirstRobot, ref dirPos, (int)ParamConfig.ManualMoveSpeed.ParamValue, true);
                }
                dirPos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.START_POINT_LR_REG);

                //直线到点
                RobotConnect.MoveTo(ConnectConfig.FirstRobot, ref dirPos, (int)ParamConfig.ManualMoveSpeed.ParamValue, true);
                //关节到点 改变位姿
                dirPos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "JR", 0);

                RobotConnect.MoveTo(ConnectConfig.FirstRobot, ref dirPos, (int)ParamConfig.ManualMoveSpeed.ParamValue, false);
                MessageBox.Show("已移动到扫描起始点");


                MutualFlag = false;
            }


        }

        private async void skinButton2_Click_1(object sender, EventArgs e)
        {
            if(!ConnectConfig.FirstRobot.Connected )
            {
                MessageBox.Show("设备未连接");
                return;
            }

            if(!MutualFlag && !Running)
            {
                MutualFlag = true;
                if (!RobotConnect.GetDin(ConnectConfig.FirstRobot, RobotPortConfig.INPUT_YANMOJI_READY))//就绪后往下进行
                {
                    MessageBox.Show("请确保研磨机已经上升到顶端");
                    MutualFlag = false;
                    return;
                }

                await Task.Run(() => {
                    Hsc3.Comm.GeneralPos dirPos;
                    //获取圆弧起始点姿态
                    Hsc3.Comm.GeneralPos curPos = RobotConnect.GetCurPoint(ConnectConfig.FirstRobot, "LR");
                    if (curPos.vecPos[1] < -400)
                    {
                        dirPos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", 35);
                        RobotConnect.MoveTo(ConnectConfig.FirstRobot, ref dirPos, (int)ParamConfig.ManualMoveSpeed.ParamValue, true);
                    }
                    dirPos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.START_POINT_LR_REG);


                    //直线到点
                    RobotConnect.MoveTo(ConnectConfig.FirstRobot, ref dirPos, (int)ParamConfig.ManualMoveSpeed.ParamValue, true);
                    //关节到点 改变位姿
                    dirPos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "JR", 0);

                    RobotConnect.MoveTo(ConnectConfig.FirstRobot, ref dirPos, (int)ParamConfig.ManualMoveSpeed.ParamValue, false);

                });



                LogHelper.loginfo.Debug("开始相机高度校准，已移动到扫描起始点");

                MessageBox.Show("移动到扫描起始点完成");

                skinButton4.Enabled = true;

                MutualFlag = false;
            }

        }

        private async void skinButton4_Click(object sender, EventArgs e)
        {
            if(RobotConnect.GetR(ConnectConfig.FirstRobot,205) != 2)
            {
                MessageBox.Show("测高传感器通讯异常，请确保通信正常后再校准");
                
                return;
            }

            if (!MutualFlag && !Running)
            {
                MutualFlag = true;

                await Task.Run(() => {

                    RobotConnect.JointManualIncMove(ConnectConfig.FirstRobot, 2, (int)ParamConfig.ManualMoveSpeed.ParamValue, ParamConfig.HeightCabPreDecDistance.ParamValue, Hsc3.Comm.DirectType.NEGATIVE);

                    float high;
                    //获取此时的传感器数据
                    high = RobotConnect.MeasureHeight();
                    while (high >= 1.5)
                    {
                        RobotConnect.JointManualIncMove(ConnectConfig.FirstRobot, 2, 1, 0.1, Hsc3.Comm.DirectType.NEGATIVE);
                        high = RobotConnect.MeasureHeight();
                        Console.WriteLine($"传感器高度：{high}");
                    }
                    //RobotConnect.StopRobot(ConnectConfig.FirstRobot);

                    Hsc3.Comm.GeneralPos curpos = RobotConnect.GetCurPoint(ConnectConfig.FirstRobot, "LR");
                    curpos.vecPos[2] += ParamConfig.Height.ParamValue;
                    RobotConnect.MoveTo(ConnectConfig.FirstRobot, ref curpos, (int)ParamConfig.ManualMoveSpeed.ParamValue, true);

                    curpos = RobotConnect.GetCurPoint(ConnectConfig.FirstRobot, "LR");

                    Hsc3.Comm.GeneralPos dirpos;

                    //更新LR寄存器高度
                    foreach(var i in ParamConfig.LR_REG)
                    {
                        dirpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", i);
                        dirpos.vecPos[2] = curpos.vecPos[2];
                        RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, dirpos, i);
                    }
                    foreach (var i in ParamConfig.JR_REG)
                    {
                        dirpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "JR", i);
                        dirpos.vecPos[2] = curpos.vecPos[2];
                        dirpos.isJoint = true;
                        RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, dirpos, i);
                    }

                    RobotConnect.SaveData(ConnectConfig.FirstRobot, "JR");
                    RobotConnect.SaveData(ConnectConfig.FirstRobot, "LR");
                    LogHelper.loginfo.Debug($"相机高度标定完成，标定后机械臂三轴的数据为:{curpos.vecPos[2]}");

                });


                MessageBox.Show("相机高度校准完成");
                skinButton4.Enabled = false;
                MutualFlag = false;
            }
        }

        private void ucRollText1_Load(object sender, EventArgs e)
        {
            ucRollText1.Text = " ";
         }

        private void ucBtnExt1_BtnClick(object sender, EventArgs e)
        {
            FrmWarnning frmwarn = new FrmWarnning(this);
            frmwarn.Show();
            ucBtnExt1.Enabled = false;
        }


        private void ucCheckBox6_CheckedChangeEvent(object sender, EventArgs e)
        {
            if (ucCheckBox6.Checked)
            {
                ucPanelTitle3.Enabled = true;
                ucCheckBox9.Visible = true;
            }

            else
            {
                ucPanelTitle3.Enabled = false;
                ucCheckBox9.Visible = false;
            }
               
        }

        private void ucCheckBox8_CheckedChangeEvent(object sender, EventArgs e)
        {
            if(!ConnectConfig.FirstRobot.Connected)
            {
                MessageBox.Show("请先连接设备");
                if(ucCheckBox8.Checked)
                 ucCheckBox8.Checked = false;
                return;
            }

            
            if (ucCheckBox8.Checked)
            {
                
                ucCheckBox7.Enabled = false;
                ParamConfig.DiskType = ParamConfig.ProductionType.Pro_97R;
                //改变五个点上料时 磨盘内下降释放高度
                Hsc3.Comm.GeneralPos curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.FirstPanReleaseHeightOffset_LR_REG);
                curpos.vecPos[2] = -ParamConfig.Production_97R_ReleaseHeight.ParamValue;
                RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.FirstPanReleaseHeightOffset_LR_REG);

                curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.SecondPanReleaseHeightOffset_LR_REG);
                curpos.vecPos[2] = -ParamConfig.Production_97R_ReleaseHeight.ParamValue;
                RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.SecondPanReleaseHeightOffset_LR_REG);


                curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.ThirdPanReleaseHeightOffset_LR_REG);
                curpos.vecPos[2] = -ParamConfig.Production_97R_ReleaseHeight.ParamValue;
                RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.ThirdPanReleaseHeightOffset_LR_REG);


                curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.FourthPanReleaseHeightOffset_LR_REG);
                curpos.vecPos[2] = -ParamConfig.Production_97R_ReleaseHeight.ParamValue;
                RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.FourthPanReleaseHeightOffset_LR_REG);

                curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.FifthPanReleaseHeightOffset_LR_REG);
                curpos.vecPos[2] = -ParamConfig.Production_97R_ReleaseHeight.ParamValue;
                RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.FifthPanReleaseHeightOffset_LR_REG);


                //改变上料点的下降高度
                curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.ShangLiaoHeight_LR_REG);
                curpos.vecPos[2] = -ParamConfig.Production_97R_ShangLiaoDropHeight.ParamValue;
                RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.ShangLiaoHeight_LR_REG);


                //改变 上料时 磨盘内下降高度
                curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.DropHeight_LR_REG);
                curpos.vecPos[2] = -ParamConfig.Production_97R_DropHeight.ParamValue;
                RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.DropHeight_LR_REG);




                //改变吸取延时
                RobotConnect.SetR(ConnectConfig.FirstRobot, ParamConfig.ProductionSuckDelay_R_REG, ParamConfig.Production_97U_AbsorbDelay.ParamValue);

                RobotConnect.SaveData(ConnectConfig.FirstRobot, "LR");

                MessageBox.Show("97U产品相关参数配置成功");
            }
            else
            {
                ucCheckBox7.Enabled = true;
            }
        }

        private void ucCheckBox7_CheckedChangeEvent(object sender, EventArgs e)
        {
            if (!ConnectConfig.FirstRobot.Connected)
            {
                MessageBox.Show("请先连接设备");
                if (ucCheckBox7.Checked)
                    ucCheckBox7.Checked = false;
                return;
            }
            if (ucCheckBox7.Checked)
            {

                ucCheckBox8.Enabled = false;

                ParamConfig.DiskType = ParamConfig.ProductionType.Pro_97U;

                //改变五个点的上料释放高度
                Hsc3.Comm.GeneralPos curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.FirstPanReleaseHeightOffset_LR_REG);
                curpos.vecPos[2] = -ParamConfig.Production_97U_ReleaseHeight.ParamValue;
                RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.FirstPanReleaseHeightOffset_LR_REG);


                curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.SecondPanReleaseHeightOffset_LR_REG);
                curpos.vecPos[2] = -ParamConfig.Production_97U_ReleaseHeight.ParamValue;
                RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.SecondPanReleaseHeightOffset_LR_REG);


                curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.ThirdPanReleaseHeightOffset_LR_REG);
                curpos.vecPos[2] = -ParamConfig.Production_97U_ReleaseHeight.ParamValue;
                RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.ThirdPanReleaseHeightOffset_LR_REG);


                curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.FourthPanReleaseHeightOffset_LR_REG);
                curpos.vecPos[2] = -ParamConfig.Production_97U_ReleaseHeight.ParamValue;
                RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.FourthPanReleaseHeightOffset_LR_REG);

                curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.FifthPanReleaseHeightOffset_LR_REG);
                curpos.vecPos[2] = -ParamConfig.Production_97U_ReleaseHeight.ParamValue;
                RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.FifthPanReleaseHeightOffset_LR_REG);

                //改变上料点的下降高度
                curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.ShangLiaoHeight_LR_REG);
                curpos.vecPos[2] = -ParamConfig.Production_97U_ShangLiaoDropHeight.ParamValue;
                RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.ShangLiaoHeight_LR_REG);

                //改变 下料时 磨盘内下降高度
                curpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.DropHeight_LR_REG);
                curpos.vecPos[2] = -ParamConfig.Production_97U_DropHeight.ParamValue;
                RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, curpos, ParamConfig.DropHeight_LR_REG);
                //改变吸取延时
                RobotConnect.SetR(ConnectConfig.FirstRobot, ParamConfig.ProductionSuckDelay_R_REG, ParamConfig.Production_97U_AbsorbDelay.ParamValue);

                RobotConnect.SaveData(ConnectConfig.FirstRobot, "LR");

                MessageBox.Show("97U产品相关参数配置成功");
            }
            else
            {
                ucCheckBox8.Enabled = true;
            }
            
            
        }




        private void ucCheckBox9_CheckedChangeEvent(object sender, EventArgs e)
        {
            if (ucCheckBox9.Checked)
            {
                Hsc3.Comm.GeneralPos dirpos = RobotConnect.GetStorePoint(ConnectConfig.FirstRobot, "LR", ParamConfig.START_POINT_LR_REG);
                dirpos.vecPos[2] = -5;
                RobotConnect.SaveKnowPoint(ConnectConfig.FirstRobot, dirpos, ParamConfig.START_POINT_LR_REG);
                RobotConnect.SaveData(ConnectConfig.FirstRobot, "LR");
            }


        }


        private FrmOffsetConfig frmOffset = new FrmOffsetConfig(); 
        private void btn_OffsetConfig_Click(object sender, EventArgs e)
        {
            frmOffset.ShowDialog();
        }

        private FrmPcParam frmPCConfig = new FrmPcParam();
        private void btn_PCParm_Click(object sender, EventArgs e)
        {
            frmPCConfig.ShowDialog();
        }

        private void btn_FirstRobotMoveUp_MouseDown(object sender, MouseEventArgs e)
        {
            if (!ConnectConfig.FirstRobot.Connected )
            {
                MessageBox.Show("上下料机械臂未连接");
                return;
            }

            if (!MutualFlag && !Running)
            {
                MutualFlag = true;
                //if (!RobotConnect.GetDin(ConnectConfig.FirstRobot, PortConfig.INPUT_YANMOJI_READY))//就绪后往下进行
                //{
                //    MessageBox.Show("请确保研磨机已经上升到顶端");
                //    MutualFlag = false;
                //    return;
                //}

                RobotConnect.JointManualConMove(ConnectConfig.FirstRobot, 2, 1, Hsc3.Comm.DirectType.POSITIVE);

                MutualFlag = false;
            }
        }

        private void btn_FirstRobotMoveUp_MouseUp(object sender, MouseEventArgs e)
        {
            if (!ConnectConfig.FirstRobot.Connected)
            {
                return;
            }
            ConnectConfig.FirstRobot.proMot.stopJog(0);

        }

        private void btn_FirstRobotMoveDown_MouseDown(object sender, MouseEventArgs e)
        {
            if (!ConnectConfig.FirstRobot.Connected)
            {
                MessageBox.Show("上下料机械臂未连接");
                return;
            }

            if (!MutualFlag && !Running)
            {
                MutualFlag = true;
 
                RobotConnect.JointManualConMove(ConnectConfig.FirstRobot, 2, 1, Hsc3.Comm.DirectType.NEGATIVE);
                MutualFlag = false;
            }
        }

        private void btn_FirstRobotMoveDown_MouseUp(object sender, MouseEventArgs e)
        {
            if (!ConnectConfig.FirstRobot.Connected)
            {
                return;
            }
            ConnectConfig.FirstRobot.proMot.stopJog(0);
        }
    

        private void FrmWorking_SizeChanged(object sender, EventArgs e)
        {
                autoSize.controlAutoSize(this);
        }


        //创建调试界面
         public static FrmDebug frmDebug = new FrmDebug();
       // frmDebug.Owner = this;
        private void DebugStripMenuItem_Click(object sender, EventArgs e)
        {

            if(ConnectConfig.Connected)
            {
                frmDebug.Show();
            }
            else
            {
                MessageBox.Show("请先连接控制器");
            }
        }

        //初始化界面的控件改变显示状态 将其加入订阅
        private void InitUIStatus()
        {
            //记录盘片进度
            CtrlChange.Ctrl_Working.Subscriber.Subscribe(ControlerPortConfig.CurrentPlateIndex, TotalProcess);
            //记录完成盘片数
            CtrlChange.Ctrl_Working.Subscriber.Subscribe(ControlerPortConfig.CurrentFinishPlate, AppendCount);

        }

        //改变工作盘状态

        public void TotalProcess(int step)
        {
            SetIndex(step, totalProcessStep);
        }


        //设置工作盘状态
        private void SetIndex(int index, UCStep ctrl)
        {
            if (InvokeRequired && !this.IsDisposed)
            {
                Invoke(new Action(() =>
                {
                    ctrl.StepIndex = index;
                }));
            }
            else
            {
                ctrl.StepIndex = index;

            }
        }
        public  FrmView frmView = new FrmView();
        private void 设备状态检测ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmView.Show();
        }
       
        private async void 初始化ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(!Connectflag)
            {
                MessageBox.Show("请先连接设备");
                return;
            }
            if (CCWin.MessageBoxEx.Show("？", "是否初始化？", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                //初始化
                Thread thdSub = new Thread(new ThreadStart(ThreadFun));
                thdSub.Start();
                //1.给三个机械臂加载初始化程序
                Task t1 = Task.Run(() =>
                {
                    InitAction();
                });
                var t = await Task.WhenAny(new Task[] {
                    t1,
                    Task.Delay(20000)
                });
                if (t != t1)
                {
                    ConnectConfig.Task_Init_Kill = true;
                    LogHelper.logerror.Error("机械臂未能复位完成");
                    thdSub.Abort();
   
                    InitFlag = false;
                    t.Dispose();
                    MessageBox.Show("初始化失败");
                    myProcessBar.Close();
                    ConnectConfig.Task_Init_Kill = false;
                }
                else
                {
                    if (ConnectConfig.Task_Init_Kill == true)
                    {
                        thdSub.Abort();
                        ConnectConfig.Task_Init_Kill = false;
                        InitFlag = false;
                        myProcessBar.Close();
                    }
                    else {
                        thdSub.Abort();
                        myProcessBar.Close();
                        InitFlag = true;

                    }
                    
                    
                }
                t1.Dispose();

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //ConnectConfig.ControlUnit.Write(ControlerPortConfig.All_InitCoil, true);
            //Thread.Sleep(100);
            //ConnectConfig.ControlUnit.Write(ControlerPortConfig.All_InitCoil, false);
            //Console.WriteLine(ConnectConfig.ControlUnit.ReadCoil(ControlerPortConfig.UPDiskEMotor_AutoButton));
        }
    }
}
