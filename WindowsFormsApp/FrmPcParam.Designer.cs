﻿
namespace WindowsFormsApp
{
    partial class FrmPcParam
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPcParam));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            this.skinPanel1 = new CCWin.SkinControl.SkinPanel();
            this.TabControl = new CCWin.SkinControl.SkinTabControl();
            this.skinTabPage2 = new CCWin.SkinControl.SkinTabPage();
            this.Btn_Close_Take = new CCWin.SkinControl.SkinButton();
            this.DataGridView_Take = new CCWin.SkinControl.SkinDataGridView();
            this.Robot1ParamName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Robot2ParamValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Robot1ParamUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Btn_Write_Take = new CCWin.SkinControl.SkinButton();
            this.skinTabPage1 = new CCWin.SkinControl.SkinTabPage();
            this.Btn_Close_Up = new CCWin.SkinControl.SkinButton();
            this.Btn_Write_Up = new CCWin.SkinControl.SkinButton();
            this.DataGridView_Up = new CCWin.SkinControl.SkinDataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skinTabPage4 = new CCWin.SkinControl.SkinTabPage();
            this.DataGridView_Down = new CCWin.SkinControl.SkinDataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skinTabPage3 = new CCWin.SkinControl.SkinTabPage();
            this.skinDataGridView3 = new CCWin.SkinControl.SkinDataGridView();
            this.ProductionParamName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductionParamValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductionParamUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Btn_Write_Down = new CCWin.SkinControl.SkinButton();
            this.Btn_Write_Prt = new CCWin.SkinControl.SkinButton();
            this.Btn_Close_Prt = new CCWin.SkinControl.SkinButton();
            this.Btn_Close_Down = new CCWin.SkinControl.SkinButton();
            this.skinPanel1.SuspendLayout();
            this.TabControl.SuspendLayout();
            this.skinTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_Take)).BeginInit();
            this.skinTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_Up)).BeginInit();
            this.skinTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_Down)).BeginInit();
            this.skinTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.skinDataGridView3)).BeginInit();
            this.SuspendLayout();
            // 
            // skinPanel1
            // 
            this.skinPanel1.BackColor = System.Drawing.Color.Transparent;
            this.skinPanel1.Controls.Add(this.TabControl);
            this.skinPanel1.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinPanel1.DownBack = null;
            this.skinPanel1.Location = new System.Drawing.Point(8, 39);
            this.skinPanel1.MouseBack = null;
            this.skinPanel1.Name = "skinPanel1";
            this.skinPanel1.NormlBack = null;
            this.skinPanel1.Size = new System.Drawing.Size(1059, 507);
            this.skinPanel1.TabIndex = 0;
            // 
            // TabControl
            // 
            this.TabControl.AnimatorType = CCWin.SkinControl.AnimationType.HorizSlide;
            this.TabControl.CloseRect = new System.Drawing.Rectangle(2, 2, 12, 12);
            this.TabControl.Controls.Add(this.skinTabPage2);
            this.TabControl.Controls.Add(this.skinTabPage1);
            this.TabControl.Controls.Add(this.skinTabPage4);
            this.TabControl.Controls.Add(this.skinTabPage3);
            this.TabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControl.HeadBack = global::WindowsFormsApp.Properties.Resources.metalBG;
            this.TabControl.ImgTxtOffset = new System.Drawing.Point(0, 0);
            this.TabControl.ItemSize = new System.Drawing.Size(90, 36);
            this.TabControl.Location = new System.Drawing.Point(0, 0);
            this.TabControl.Name = "TabControl";
            this.TabControl.PageArrowDown = ((System.Drawing.Image)(resources.GetObject("TabControl.PageArrowDown")));
            this.TabControl.PageArrowHover = ((System.Drawing.Image)(resources.GetObject("TabControl.PageArrowHover")));
            this.TabControl.PageCloseHover = ((System.Drawing.Image)(resources.GetObject("TabControl.PageCloseHover")));
            this.TabControl.PageCloseNormal = ((System.Drawing.Image)(resources.GetObject("TabControl.PageCloseNormal")));
            this.TabControl.PageDown = ((System.Drawing.Image)(resources.GetObject("TabControl.PageDown")));
            this.TabControl.PageHover = ((System.Drawing.Image)(resources.GetObject("TabControl.PageHover")));
            this.TabControl.PageImagePosition = CCWin.SkinControl.SkinTabControl.ePageImagePosition.Left;
            this.TabControl.PageNorml = null;
            this.TabControl.SelectedIndex = 1;
            this.TabControl.Size = new System.Drawing.Size(1059, 507);
            this.TabControl.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.TabControl.TabIndex = 0;
            // 
            // skinTabPage2
            // 
            this.skinTabPage2.BackColor = System.Drawing.Color.White;
            this.skinTabPage2.Controls.Add(this.Btn_Close_Take);
            this.skinTabPage2.Controls.Add(this.DataGridView_Take);
            this.skinTabPage2.Controls.Add(this.Btn_Write_Take);
            this.skinTabPage2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage2.Location = new System.Drawing.Point(0, 36);
            this.skinTabPage2.Name = "skinTabPage2";
            this.skinTabPage2.Size = new System.Drawing.Size(1059, 471);
            this.skinTabPage2.TabIndex = 1;
            this.skinTabPage2.TabItemImage = null;
            this.skinTabPage2.Text = "上下料机械臂";
            // 
            // Btn_Close_Take
            // 
            this.Btn_Close_Take.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Close_Take.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Close_Take.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.Btn_Close_Take.DownBack = null;
            this.Btn_Close_Take.Location = new System.Drawing.Point(700, 381);
            this.Btn_Close_Take.MouseBack = null;
            this.Btn_Close_Take.Name = "Btn_Close_Take";
            this.Btn_Close_Take.NormlBack = null;
            this.Btn_Close_Take.Size = new System.Drawing.Size(138, 58);
            this.Btn_Close_Take.TabIndex = 2;
            this.Btn_Close_Take.Text = "关闭";
            this.Btn_Close_Take.UseVisualStyleBackColor = false;
            this.Btn_Close_Take.Click += new System.EventHandler(this.Btn_Close_Take_Click);
            // 
            // DataGridView_Take
            // 
            this.DataGridView_Take.AllowUserToAddRows = false;
            this.DataGridView_Take.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(246)))), ((int)(((byte)(253)))));
            this.DataGridView_Take.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridView_Take.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataGridView_Take.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DataGridView_Take.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DataGridView_Take.ColumnFont = null;
            this.DataGridView_Take.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(246)))), ((int)(((byte)(239)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridView_Take.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridView_Take.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView_Take.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Robot1ParamName,
            this.Robot2ParamValue,
            this.Robot1ParamUnit});
            this.DataGridView_Take.ColumnSelectForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(188)))), ((int)(((byte)(240)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGridView_Take.DefaultCellStyle = dataGridViewCellStyle3;
            this.DataGridView_Take.Dock = System.Windows.Forms.DockStyle.Top;
            this.DataGridView_Take.EnableHeadersVisualStyles = false;
            this.DataGridView_Take.GridColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.DataGridView_Take.HeadFont = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.DataGridView_Take.HeadSelectForeColor = System.Drawing.SystemColors.HighlightText;
            this.DataGridView_Take.Location = new System.Drawing.Point(0, 0);
            this.DataGridView_Take.Name = "DataGridView_Take";
            this.DataGridView_Take.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.DataGridView_Take.RowHeadersWidth = 51;
            this.DataGridView_Take.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.DataGridView_Take.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.DataGridView_Take.RowTemplate.Height = 23;
            this.DataGridView_Take.Size = new System.Drawing.Size(1059, 357);
            this.DataGridView_Take.TabIndex = 0;
            this.DataGridView_Take.TitleBack = null;
            this.DataGridView_Take.TitleBackColorBegin = System.Drawing.Color.White;
            this.DataGridView_Take.TitleBackColorEnd = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(196)))), ((int)(((byte)(242)))));
            // 
            // Robot1ParamName
            // 
            this.Robot1ParamName.DataPropertyName = "ParamName";
            this.Robot1ParamName.HeaderText = "参数名";
            this.Robot1ParamName.MinimumWidth = 6;
            this.Robot1ParamName.Name = "Robot1ParamName";
            this.Robot1ParamName.ReadOnly = true;
            // 
            // Robot2ParamValue
            // 
            this.Robot2ParamValue.DataPropertyName = "ParamValue";
            this.Robot2ParamValue.HeaderText = "参数值";
            this.Robot2ParamValue.MinimumWidth = 6;
            this.Robot2ParamValue.Name = "Robot2ParamValue";
            // 
            // Robot1ParamUnit
            // 
            this.Robot1ParamUnit.DataPropertyName = "ParamUnit";
            this.Robot1ParamUnit.HeaderText = "单位";
            this.Robot1ParamUnit.MinimumWidth = 6;
            this.Robot1ParamUnit.Name = "Robot1ParamUnit";
            this.Robot1ParamUnit.ReadOnly = true;
            // 
            // Btn_Write_Take
            // 
            this.Btn_Write_Take.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Write_Take.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.Btn_Write_Take.DownBack = null;
            this.Btn_Write_Take.Location = new System.Drawing.Point(163, 381);
            this.Btn_Write_Take.MouseBack = null;
            this.Btn_Write_Take.Name = "Btn_Write_Take";
            this.Btn_Write_Take.NormlBack = null;
            this.Btn_Write_Take.Size = new System.Drawing.Size(138, 58);
            this.Btn_Write_Take.TabIndex = 1;
            this.Btn_Write_Take.Text = "写入参数";
            this.Btn_Write_Take.UseVisualStyleBackColor = false;
            this.Btn_Write_Take.Click += new System.EventHandler(this.Btn_Write_Take_Click);
            // 
            // skinTabPage1
            // 
            this.skinTabPage1.BackColor = System.Drawing.Color.White;
            this.skinTabPage1.Controls.Add(this.Btn_Close_Up);
            this.skinTabPage1.Controls.Add(this.Btn_Write_Up);
            this.skinTabPage1.Controls.Add(this.DataGridView_Up);
            this.skinTabPage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage1.Location = new System.Drawing.Point(0, 36);
            this.skinTabPage1.Name = "skinTabPage1";
            this.skinTabPage1.Size = new System.Drawing.Size(1059, 471);
            this.skinTabPage1.TabIndex = 3;
            this.skinTabPage1.TabItemImage = null;
            this.skinTabPage1.Text = "上料机械臂";
            // 
            // Btn_Close_Up
            // 
            this.Btn_Close_Up.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Close_Up.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Close_Up.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.Btn_Close_Up.DownBack = null;
            this.Btn_Close_Up.Location = new System.Drawing.Point(700, 381);
            this.Btn_Close_Up.MouseBack = null;
            this.Btn_Close_Up.Name = "Btn_Close_Up";
            this.Btn_Close_Up.NormlBack = null;
            this.Btn_Close_Up.Size = new System.Drawing.Size(138, 58);
            this.Btn_Close_Up.TabIndex = 3;
            this.Btn_Close_Up.Text = "关闭";
            this.Btn_Close_Up.UseVisualStyleBackColor = false;
            this.Btn_Close_Up.Click += new System.EventHandler(this.Btn_Close_Up_Click);
            // 
            // Btn_Write_Up
            // 
            this.Btn_Write_Up.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Write_Up.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.Btn_Write_Up.DownBack = null;
            this.Btn_Write_Up.Location = new System.Drawing.Point(163, 381);
            this.Btn_Write_Up.MouseBack = null;
            this.Btn_Write_Up.Name = "Btn_Write_Up";
            this.Btn_Write_Up.NormlBack = null;
            this.Btn_Write_Up.Size = new System.Drawing.Size(138, 58);
            this.Btn_Write_Up.TabIndex = 2;
            this.Btn_Write_Up.Text = "写入参数";
            this.Btn_Write_Up.UseVisualStyleBackColor = false;
            this.Btn_Write_Up.Click += new System.EventHandler(this.Btn_Write_Up_Click);
            // 
            // DataGridView_Up
            // 
            this.DataGridView_Up.AllowUserToAddRows = false;
            this.DataGridView_Up.AllowUserToDeleteRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(246)))), ((int)(((byte)(253)))));
            this.DataGridView_Up.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.DataGridView_Up.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataGridView_Up.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DataGridView_Up.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DataGridView_Up.ColumnFont = null;
            this.DataGridView_Up.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(246)))), ((int)(((byte)(239)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridView_Up.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.DataGridView_Up.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView_Up.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.DataGridView_Up.ColumnSelectForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(188)))), ((int)(((byte)(240)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGridView_Up.DefaultCellStyle = dataGridViewCellStyle7;
            this.DataGridView_Up.Dock = System.Windows.Forms.DockStyle.Top;
            this.DataGridView_Up.EnableHeadersVisualStyles = false;
            this.DataGridView_Up.GridColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.DataGridView_Up.HeadFont = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.DataGridView_Up.HeadSelectForeColor = System.Drawing.SystemColors.HighlightText;
            this.DataGridView_Up.Location = new System.Drawing.Point(0, 0);
            this.DataGridView_Up.Name = "DataGridView_Up";
            this.DataGridView_Up.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.DataGridView_Up.RowHeadersWidth = 51;
            this.DataGridView_Up.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.DataGridView_Up.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.DataGridView_Up.RowTemplate.Height = 23;
            this.DataGridView_Up.Size = new System.Drawing.Size(1059, 357);
            this.DataGridView_Up.TabIndex = 1;
            this.DataGridView_Up.TitleBack = null;
            this.DataGridView_Up.TitleBackColorBegin = System.Drawing.Color.White;
            this.DataGridView_Up.TitleBackColorEnd = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(196)))), ((int)(((byte)(242)))));
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ParamName";
            this.dataGridViewTextBoxColumn1.HeaderText = "参数名";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "ParamValue";
            this.dataGridViewTextBoxColumn2.HeaderText = "参数值";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "ParamUnit";
            this.dataGridViewTextBoxColumn3.HeaderText = "单位";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // skinTabPage4
            // 
            this.skinTabPage4.BackColor = System.Drawing.Color.White;
            this.skinTabPage4.Controls.Add(this.Btn_Close_Down);
            this.skinTabPage4.Controls.Add(this.Btn_Write_Down);
            this.skinTabPage4.Controls.Add(this.DataGridView_Down);
            this.skinTabPage4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage4.Location = new System.Drawing.Point(0, 36);
            this.skinTabPage4.Name = "skinTabPage4";
            this.skinTabPage4.Size = new System.Drawing.Size(1059, 471);
            this.skinTabPage4.TabIndex = 4;
            this.skinTabPage4.TabItemImage = null;
            this.skinTabPage4.Text = "下料机械臂";
            // 
            // DataGridView_Down
            // 
            this.DataGridView_Down.AllowUserToAddRows = false;
            this.DataGridView_Down.AllowUserToDeleteRows = false;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(246)))), ((int)(((byte)(253)))));
            this.DataGridView_Down.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.DataGridView_Down.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataGridView_Down.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DataGridView_Down.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DataGridView_Down.ColumnFont = null;
            this.DataGridView_Down.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(246)))), ((int)(((byte)(239)))));
            dataGridViewCellStyle10.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridView_Down.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.DataGridView_Down.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView_Down.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6});
            this.DataGridView_Down.ColumnSelectForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(188)))), ((int)(((byte)(240)))));
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGridView_Down.DefaultCellStyle = dataGridViewCellStyle11;
            this.DataGridView_Down.Dock = System.Windows.Forms.DockStyle.Top;
            this.DataGridView_Down.EnableHeadersVisualStyles = false;
            this.DataGridView_Down.GridColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.DataGridView_Down.HeadFont = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.DataGridView_Down.HeadSelectForeColor = System.Drawing.SystemColors.HighlightText;
            this.DataGridView_Down.Location = new System.Drawing.Point(0, 0);
            this.DataGridView_Down.Name = "DataGridView_Down";
            this.DataGridView_Down.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.DataGridView_Down.RowHeadersWidth = 51;
            this.DataGridView_Down.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.DataGridView_Down.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.DataGridView_Down.RowTemplate.Height = 23;
            this.DataGridView_Down.Size = new System.Drawing.Size(1059, 357);
            this.DataGridView_Down.TabIndex = 2;
            this.DataGridView_Down.TitleBack = null;
            this.DataGridView_Down.TitleBackColorBegin = System.Drawing.Color.White;
            this.DataGridView_Down.TitleBackColorEnd = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(196)))), ((int)(((byte)(242)))));
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "ParamName";
            this.dataGridViewTextBoxColumn4.HeaderText = "参数名";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "ParamValue";
            this.dataGridViewTextBoxColumn5.HeaderText = "参数值";
            this.dataGridViewTextBoxColumn5.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "ParamUnit";
            this.dataGridViewTextBoxColumn6.HeaderText = "单位";
            this.dataGridViewTextBoxColumn6.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // skinTabPage3
            // 
            this.skinTabPage3.BackColor = System.Drawing.Color.White;
            this.skinTabPage3.Controls.Add(this.Btn_Close_Prt);
            this.skinTabPage3.Controls.Add(this.Btn_Write_Prt);
            this.skinTabPage3.Controls.Add(this.skinDataGridView3);
            this.skinTabPage3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage3.Location = new System.Drawing.Point(0, 36);
            this.skinTabPage3.Name = "skinTabPage3";
            this.skinTabPage3.Size = new System.Drawing.Size(1059, 471);
            this.skinTabPage3.TabIndex = 2;
            this.skinTabPage3.TabItemImage = null;
            this.skinTabPage3.Text = "产品参数";
            // 
            // skinDataGridView3
            // 
            this.skinDataGridView3.AllowUserToAddRows = false;
            this.skinDataGridView3.AllowUserToDeleteRows = false;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(246)))), ((int)(((byte)(253)))));
            this.skinDataGridView3.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle13;
            this.skinDataGridView3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.skinDataGridView3.BackgroundColor = System.Drawing.SystemColors.Window;
            this.skinDataGridView3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.skinDataGridView3.ColumnFont = null;
            this.skinDataGridView3.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(246)))), ((int)(((byte)(239)))));
            dataGridViewCellStyle14.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.skinDataGridView3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.skinDataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.skinDataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProductionParamName,
            this.ProductionParamValue,
            this.ProductionParamUnit});
            this.skinDataGridView3.ColumnSelectForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(188)))), ((int)(((byte)(240)))));
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.skinDataGridView3.DefaultCellStyle = dataGridViewCellStyle15;
            this.skinDataGridView3.Dock = System.Windows.Forms.DockStyle.Top;
            this.skinDataGridView3.EnableHeadersVisualStyles = false;
            this.skinDataGridView3.GridColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.skinDataGridView3.HeadFont = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinDataGridView3.HeadSelectForeColor = System.Drawing.SystemColors.HighlightText;
            this.skinDataGridView3.Location = new System.Drawing.Point(0, 0);
            this.skinDataGridView3.Name = "skinDataGridView3";
            this.skinDataGridView3.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.skinDataGridView3.RowHeadersWidth = 51;
            this.skinDataGridView3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.skinDataGridView3.RowsDefaultCellStyle = dataGridViewCellStyle16;
            this.skinDataGridView3.RowTemplate.Height = 23;
            this.skinDataGridView3.Size = new System.Drawing.Size(1059, 357);
            this.skinDataGridView3.TabIndex = 0;
            this.skinDataGridView3.TitleBack = null;
            this.skinDataGridView3.TitleBackColorBegin = System.Drawing.Color.White;
            this.skinDataGridView3.TitleBackColorEnd = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(196)))), ((int)(((byte)(242)))));
            // 
            // ProductionParamName
            // 
            this.ProductionParamName.DataPropertyName = "ParamName";
            this.ProductionParamName.HeaderText = "参数名";
            this.ProductionParamName.MinimumWidth = 6;
            this.ProductionParamName.Name = "ProductionParamName";
            this.ProductionParamName.ReadOnly = true;
            // 
            // ProductionParamValue
            // 
            this.ProductionParamValue.DataPropertyName = "ParamValue";
            this.ProductionParamValue.HeaderText = "参数值";
            this.ProductionParamValue.MinimumWidth = 6;
            this.ProductionParamValue.Name = "ProductionParamValue";
            // 
            // ProductionParamUnit
            // 
            this.ProductionParamUnit.DataPropertyName = "ParamUnit";
            this.ProductionParamUnit.HeaderText = "单位";
            this.ProductionParamUnit.MinimumWidth = 6;
            this.ProductionParamUnit.Name = "ProductionParamUnit";
            this.ProductionParamUnit.ReadOnly = true;
            // 
            // Btn_Write_Down
            // 
            this.Btn_Write_Down.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Write_Down.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.Btn_Write_Down.DownBack = null;
            this.Btn_Write_Down.Location = new System.Drawing.Point(163, 381);
            this.Btn_Write_Down.MouseBack = null;
            this.Btn_Write_Down.Name = "Btn_Write_Down";
            this.Btn_Write_Down.NormlBack = null;
            this.Btn_Write_Down.Size = new System.Drawing.Size(138, 58);
            this.Btn_Write_Down.TabIndex = 3;
            this.Btn_Write_Down.Text = "写入参数";
            this.Btn_Write_Down.UseVisualStyleBackColor = false;
            this.Btn_Write_Down.Click += new System.EventHandler(this.Btn_Write_Down_Click);
            // 
            // Btn_Write_Prt
            // 
            this.Btn_Write_Prt.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Write_Prt.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.Btn_Write_Prt.DownBack = null;
            this.Btn_Write_Prt.Location = new System.Drawing.Point(163, 381);
            this.Btn_Write_Prt.MouseBack = null;
            this.Btn_Write_Prt.Name = "Btn_Write_Prt";
            this.Btn_Write_Prt.NormlBack = null;
            this.Btn_Write_Prt.Size = new System.Drawing.Size(138, 58);
            this.Btn_Write_Prt.TabIndex = 2;
            this.Btn_Write_Prt.Text = "写入参数";
            this.Btn_Write_Prt.UseVisualStyleBackColor = false;
            this.Btn_Write_Prt.Click += new System.EventHandler(this.Btn_Write_Prt_Click);
            // 
            // Btn_Close_Prt
            // 
            this.Btn_Close_Prt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Close_Prt.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Close_Prt.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.Btn_Close_Prt.DownBack = null;
            this.Btn_Close_Prt.Location = new System.Drawing.Point(700, 381);
            this.Btn_Close_Prt.MouseBack = null;
            this.Btn_Close_Prt.Name = "Btn_Close_Prt";
            this.Btn_Close_Prt.NormlBack = null;
            this.Btn_Close_Prt.Size = new System.Drawing.Size(138, 58);
            this.Btn_Close_Prt.TabIndex = 3;
            this.Btn_Close_Prt.Text = "关闭";
            this.Btn_Close_Prt.UseVisualStyleBackColor = false;
            this.Btn_Close_Prt.Click += new System.EventHandler(this.Btn_Close_Prt_Click);
            // 
            // Btn_Close_Down
            // 
            this.Btn_Close_Down.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Close_Down.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Close_Down.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.Btn_Close_Down.DownBack = null;
            this.Btn_Close_Down.Location = new System.Drawing.Point(700, 381);
            this.Btn_Close_Down.MouseBack = null;
            this.Btn_Close_Down.Name = "Btn_Close_Down";
            this.Btn_Close_Down.NormlBack = null;
            this.Btn_Close_Down.Size = new System.Drawing.Size(138, 58);
            this.Btn_Close_Down.TabIndex = 4;
            this.Btn_Close_Down.Text = "关闭";
            this.Btn_Close_Down.UseVisualStyleBackColor = false;
            this.Btn_Close_Down.Click += new System.EventHandler(this.Btn_Close_Down_Click);
            // 
            // FrmPcParam
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1075, 554);
            this.Controls.Add(this.skinPanel1);
            this.Name = "FrmPcParam";
            this.Text = " ";
            this.Load += new System.EventHandler(this.FrmPcParam_Load);
            this.skinPanel1.ResumeLayout(false);
            this.TabControl.ResumeLayout(false);
            this.skinTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_Take)).EndInit();
            this.skinTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_Up)).EndInit();
            this.skinTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_Down)).EndInit();
            this.skinTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.skinDataGridView3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private CCWin.SkinControl.SkinPanel skinPanel1;
        private CCWin.SkinControl.SkinTabControl TabControl;
        private CCWin.SkinControl.SkinTabPage skinTabPage2;
        private CCWin.SkinControl.SkinDataGridView DataGridView_Take;
        private System.Windows.Forms.DataGridViewTextBoxColumn Robot1ParamName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Robot2ParamValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn Robot1ParamUnit;
        private CCWin.SkinControl.SkinTabPage skinTabPage3;
        private CCWin.SkinControl.SkinDataGridView skinDataGridView3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductionParamName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductionParamValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductionParamUnit;
        private CCWin.SkinControl.SkinButton Btn_Write_Take;
        private CCWin.SkinControl.SkinButton Btn_Close_Take;
        private CCWin.SkinControl.SkinTabPage skinTabPage1;
        private CCWin.SkinControl.SkinDataGridView DataGridView_Up;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private CCWin.SkinControl.SkinTabPage skinTabPage4;
        private CCWin.SkinControl.SkinDataGridView DataGridView_Down;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private CCWin.SkinControl.SkinButton Btn_Close_Up;
        private CCWin.SkinControl.SkinButton Btn_Write_Up;
        private CCWin.SkinControl.SkinButton Btn_Close_Down;
        private CCWin.SkinControl.SkinButton Btn_Write_Down;
        private CCWin.SkinControl.SkinButton Btn_Close_Prt;
        private CCWin.SkinControl.SkinButton Btn_Write_Prt;
    }
}