﻿
namespace WindowsFormsApp
{
    partial class FrmProcessBar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.prcBar = new CCWin.SkinControl.SkinProgressBar();
            this.skinButton1 = new CCWin.SkinControl.SkinButton();
            this.SuspendLayout();
            // 
            // prcBar
            // 
            this.prcBar.Back = null;
            this.prcBar.BackColor = System.Drawing.Color.Transparent;
            this.prcBar.BarBack = null;
            this.prcBar.BarRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.prcBar.ForeColor = System.Drawing.Color.Red;
            this.prcBar.Location = new System.Drawing.Point(-2, 45);
            this.prcBar.Name = "prcBar";
            this.prcBar.Radius = 1;
            this.prcBar.RadiusStyle = CCWin.SkinClass.RoundStyle.None;
            this.prcBar.Size = new System.Drawing.Size(408, 49);
            this.prcBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.prcBar.TabIndex = 0;
            this.prcBar.TextFormat = CCWin.SkinControl.SkinProgressBar.TxtFormat.None;
            this.prcBar.TrackBack = System.Drawing.Color.Gray;
            this.prcBar.TrackFore = System.Drawing.Color.Fuchsia;
            // 
            // skinButton1
            // 
            this.skinButton1.BackColor = System.Drawing.Color.Transparent;
            this.skinButton1.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinButton1.DownBack = null;
            this.skinButton1.Location = new System.Drawing.Point(145, 126);
            this.skinButton1.MouseBack = null;
            this.skinButton1.Name = "skinButton1";
            this.skinButton1.NormlBack = null;
            this.skinButton1.Size = new System.Drawing.Size(125, 54);
            this.skinButton1.TabIndex = 1;
            this.skinButton1.Text = "中止初始化";
            this.skinButton1.UseVisualStyleBackColor = false;
            this.skinButton1.Click += new System.EventHandler(this.skinButton1_Click);
            // 
            // FrmProcessBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 202);
            this.Controls.Add(this.skinButton1);
            this.Controls.Add(this.prcBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmProcessBar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmProcessBar";
            this.ResumeLayout(false);

        }

        #endregion

        private CCWin.SkinControl.SkinProgressBar prcBar;
        private CCWin.SkinControl.SkinButton skinButton1;
    }
}