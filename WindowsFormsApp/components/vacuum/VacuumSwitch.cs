﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp.deviceConnect;

namespace WindowsFormsApp.components.vacuum
{
    public partial class VacuumSwitch : UserControl,IAutoUpdatable
    {
        public VacuumSwitch()
        {
            InitializeComponent();
            ConnectConfig.updateList.Add(this);
        }


       public Vacuum vacuum;



        public string HeaderContent
        {
            get
            {
                return lab_Header.Text;
            }
            set
            {
                lab_Header.Text = value;
            }
        }

        private void ucSwitch2_CheckedChanged(object sender, EventArgs e)
        {
            if(ucSwitch2.Checked)
            {
                ucSwitch1.Checked = false;
               
                vacuum?.Inspiratory();//吸气

            }
            else
            {
                vacuum?.Close();
            }
        }

        private void ucSwitch1_CheckedChanged(object sender, EventArgs e)
        {
            if (ucSwitch1.Checked)
            {
                ucSwitch2.Checked = false; //先停止吸气
            
                vacuum?.Expiratory();//吹气
            }
            else
            {
                vacuum?.Close();
            }
        }

        public void ReFreshState()
        {
            if (vacuum != null && vacuum.VacuumStatus)
                picb_VacuumStatus.BackColor = Color.Lime;
            else
                picb_VacuumStatus.BackColor = SystemColors.ControlDarkDark;
        }


    }
}
