﻿using System.Threading;
using WindowsFormsApp.components.robot;
using WindowsFormsApp.deviceConnect;

namespace WindowsFormsApp.components.vacuum
{
    //真空发生器对象
    public class Vacuum : IAutoUpdatable
    {

        public Robot robot;

        public int InspiratoryTriggerIO; //吸气控制线圈
        public int ExpiratoryTriggerIO;  //吹气控制线圈
        public int isVacuumDegreeIO;//真空度是否达到IO

        public bool VacuumStatus;


        public Vacuum() {

            ConnectConfig.updateList.Add(this);
        }
        /// <summary>
        /// 吸气
        /// </summary>

        public void Inspiratory()
        {
            RobotConnect.SetDout(robot, InspiratoryTriggerIO, true);
            RobotConnect.SetDout(robot, ExpiratoryTriggerIO, false);
            Thread.Sleep(10);
        }
        /// <summary>
        /// 吹气
        /// </summary>
        public void Expiratory()
        {
            RobotConnect.SetDout(robot, InspiratoryTriggerIO, false);
            RobotConnect.SetDout(robot, ExpiratoryTriggerIO, true);
            Thread.Sleep(10);
        }
        /// <summary>
        /// 关闭吸气吹气
        /// </summary>
        public void Close()
        {
            RobotConnect.SetDout(robot, InspiratoryTriggerIO, false);
            RobotConnect.SetDout(robot, ExpiratoryTriggerIO, false);
            Thread.Sleep(10);

        }
        /// <summary>
        /// 真空度是否达到
        /// </summary>
        /// <returns></returns>

        public bool isVacuumDegree()
        {
            return robot.GetDin(isVacuumDegreeIO);
        }


        public void ReFreshState()
        {
            VacuumStatus = isVacuumDegree();

        }



    }
}
