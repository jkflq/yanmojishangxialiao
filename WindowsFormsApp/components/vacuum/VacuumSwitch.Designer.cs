﻿
namespace WindowsFormsApp.components.vacuum
{
    partial class VacuumSwitch
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.lab_Header = new CCWin.SkinControl.SkinLabel();
            this.skinLabel2 = new CCWin.SkinControl.SkinLabel();
            this.ucSwitch1 = new HZH_Controls.Controls.UCSwitch();
            this.ucSwitch2 = new HZH_Controls.Controls.UCSwitch();
            this.skinPanel1 = new CCWin.SkinControl.SkinPanel();
            this.picb_VacuumStatus = new CCWin.SkinControl.SkinPictureBox();
            this.skinPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picb_VacuumStatus)).BeginInit();
            this.SuspendLayout();
            // 
            // lab_Header
            // 
            this.lab_Header.AutoSize = true;
            this.lab_Header.BackColor = System.Drawing.Color.Transparent;
            this.lab_Header.BorderColor = System.Drawing.Color.White;
            this.lab_Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.lab_Header.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_Header.Location = new System.Drawing.Point(0, 0);
            this.lab_Header.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lab_Header.Name = "lab_Header";
            this.lab_Header.Size = new System.Drawing.Size(111, 27);
            this.lab_Header.TabIndex = 0;
            this.lab_Header.Text = "skinLabel1";
            // 
            // skinLabel2
            // 
            this.skinLabel2.AutoSize = true;
            this.skinLabel2.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel2.BorderColor = System.Drawing.Color.White;
            this.skinLabel2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel2.Location = new System.Drawing.Point(16, 15);
            this.skinLabel2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.skinLabel2.Name = "skinLabel2";
            this.skinLabel2.Size = new System.Drawing.Size(72, 27);
            this.skinLabel2.TabIndex = 2;
            this.skinLabel2.Text = "真空度";
            // 
            // ucSwitch1
            // 
            this.ucSwitch1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ucSwitch1.Checked = false;
            this.ucSwitch1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucSwitch1.FalseColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ucSwitch1.FalseTextColr = System.Drawing.Color.Black;
            this.ucSwitch1.Location = new System.Drawing.Point(0, 27);
            this.ucSwitch1.Margin = new System.Windows.Forms.Padding(4);
            this.ucSwitch1.Name = "ucSwitch1";
            this.ucSwitch1.Size = new System.Drawing.Size(288, 55);
            this.ucSwitch1.SwitchType = HZH_Controls.Controls.SwitchType.Quadrilateral;
            this.ucSwitch1.TabIndex = 4;
            this.ucSwitch1.Texts = new string[] {
        "关闭",
        "吹气"};
            this.ucSwitch1.TrueColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ucSwitch1.TrueTextColr = System.Drawing.Color.Black;
            this.ucSwitch1.CheckedChanged += new System.EventHandler(this.ucSwitch1_CheckedChanged);
            // 
            // ucSwitch2
            // 
            this.ucSwitch2.BackColor = System.Drawing.Color.Gray;
            this.ucSwitch2.Checked = false;
            this.ucSwitch2.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucSwitch2.FalseColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ucSwitch2.FalseTextColr = System.Drawing.Color.Black;
            this.ucSwitch2.Location = new System.Drawing.Point(0, 82);
            this.ucSwitch2.Margin = new System.Windows.Forms.Padding(4);
            this.ucSwitch2.Name = "ucSwitch2";
            this.ucSwitch2.Size = new System.Drawing.Size(288, 55);
            this.ucSwitch2.SwitchType = HZH_Controls.Controls.SwitchType.Quadrilateral;
            this.ucSwitch2.TabIndex = 5;
            this.ucSwitch2.Texts = new string[] {
        "关闭",
        "吸气"};
            this.ucSwitch2.TrueColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ucSwitch2.TrueTextColr = System.Drawing.Color.Black;
            this.ucSwitch2.CheckedChanged += new System.EventHandler(this.ucSwitch2_CheckedChanged);
            // 
            // skinPanel1
            // 
            this.skinPanel1.BackColor = System.Drawing.Color.Transparent;
            this.skinPanel1.Controls.Add(this.picb_VacuumStatus);
            this.skinPanel1.Controls.Add(this.skinLabel2);
            this.skinPanel1.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.skinPanel1.DownBack = null;
            this.skinPanel1.Location = new System.Drawing.Point(0, 161);
            this.skinPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.skinPanel1.MouseBack = null;
            this.skinPanel1.Name = "skinPanel1";
            this.skinPanel1.NormlBack = null;
            this.skinPanel1.Size = new System.Drawing.Size(288, 64);
            this.skinPanel1.TabIndex = 6;
            // 
            // picb_VacuumStatus
            // 
            this.picb_VacuumStatus.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_VacuumStatus.Location = new System.Drawing.Point(136, 13);
            this.picb_VacuumStatus.Margin = new System.Windows.Forms.Padding(4);
            this.picb_VacuumStatus.Name = "picb_VacuumStatus";
            this.picb_VacuumStatus.Size = new System.Drawing.Size(102, 45);
            this.picb_VacuumStatus.TabIndex = 3;
            this.picb_VacuumStatus.TabStop = false;
            // 
            // VacuumSwitch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.skinPanel1);
            this.Controls.Add(this.ucSwitch2);
            this.Controls.Add(this.ucSwitch1);
            this.Controls.Add(this.lab_Header);
            this.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "VacuumSwitch";
            this.Size = new System.Drawing.Size(288, 225);
            this.skinPanel1.ResumeLayout(false);
            this.skinPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picb_VacuumStatus)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CCWin.SkinControl.SkinLabel lab_Header;
        private CCWin.SkinControl.SkinLabel skinLabel2;
        private HZH_Controls.Controls.UCSwitch ucSwitch1;
        private HZH_Controls.Controls.UCSwitch ucSwitch2;
        private CCWin.SkinControl.SkinPanel skinPanel1;
        private CCWin.SkinControl.SkinPictureBox picb_VacuumStatus;
    }
}
