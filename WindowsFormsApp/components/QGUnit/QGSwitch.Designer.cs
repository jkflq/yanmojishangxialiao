﻿
namespace WindowsFormsApp.components.QGUnit
{
    partial class QGSwitch
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.lab_Header = new CCWin.SkinControl.SkinLabel();
            this.btn_Status = new HZH_Controls.Controls.UCSwitch();
            this.lab_CXLeft = new CCWin.SkinControl.SkinLabel();
            this.lab_CXRight = new CCWin.SkinControl.SkinLabel();
            this.picb_CXRight = new CCWin.SkinControl.SkinPictureBox();
            this.picb_CXLeft = new CCWin.SkinControl.SkinPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picb_CXRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_CXLeft)).BeginInit();
            this.SuspendLayout();
            // 
            // lab_Header
            // 
            this.lab_Header.AutoSize = true;
            this.lab_Header.BackColor = System.Drawing.Color.Transparent;
            this.lab_Header.BorderColor = System.Drawing.Color.White;
            this.lab_Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.lab_Header.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_Header.Location = new System.Drawing.Point(0, 0);
            this.lab_Header.Name = "lab_Header";
            this.lab_Header.Size = new System.Drawing.Size(32, 20);
            this.lab_Header.TabIndex = 0;
            this.lab_Header.Text = "QG";
            // 
            // btn_Status
            // 
            this.btn_Status.BackColor = System.Drawing.Color.DimGray;
            this.btn_Status.Checked = false;
            this.btn_Status.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_Status.FalseColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btn_Status.FalseTextColr = System.Drawing.Color.Black;
            this.btn_Status.Location = new System.Drawing.Point(0, 20);
            this.btn_Status.Name = "btn_Status";
            this.btn_Status.Size = new System.Drawing.Size(158, 41);
            this.btn_Status.SwitchType = HZH_Controls.Controls.SwitchType.Quadrilateral;
            this.btn_Status.TabIndex = 5;
            this.btn_Status.Texts = new string[] {
        "缩",
        "伸"};
            this.btn_Status.TrueColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btn_Status.TrueTextColr = System.Drawing.Color.Black;
            this.btn_Status.CheckedChanged += new System.EventHandler(this.btn_Status_CheckedChanged);
            // 
            // lab_CXLeft
            // 
            this.lab_CXLeft.AutoSize = true;
            this.lab_CXLeft.BackColor = System.Drawing.Color.Transparent;
            this.lab_CXLeft.BorderColor = System.Drawing.Color.White;
            this.lab_CXLeft.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_CXLeft.Location = new System.Drawing.Point(3, 64);
            this.lab_CXLeft.Name = "lab_CXLeft";
            this.lab_CXLeft.Size = new System.Drawing.Size(38, 20);
            this.lab_CXLeft.TabIndex = 7;
            this.lab_CXLeft.Text = "CX1";
            // 
            // lab_CXRight
            // 
            this.lab_CXRight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lab_CXRight.AutoSize = true;
            this.lab_CXRight.BackColor = System.Drawing.Color.Transparent;
            this.lab_CXRight.BorderColor = System.Drawing.Color.White;
            this.lab_CXRight.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_CXRight.Location = new System.Drawing.Point(93, 64);
            this.lab_CXRight.Name = "lab_CXRight";
            this.lab_CXRight.Size = new System.Drawing.Size(38, 20);
            this.lab_CXRight.TabIndex = 8;
            this.lab_CXRight.Text = "CX2";
            // 
            // picb_CXRight
            // 
            this.picb_CXRight.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_CXRight.Location = new System.Drawing.Point(0, 87);
            this.picb_CXRight.Name = "picb_CXRight";
            this.picb_CXRight.Size = new System.Drawing.Size(60, 39);
            this.picb_CXRight.TabIndex = 10;
            this.picb_CXRight.TabStop = false;
            // 
            // picb_CXLeft
            // 
            this.picb_CXLeft.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picb_CXLeft.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picb_CXLeft.Location = new System.Drawing.Point(98, 87);
            this.picb_CXLeft.Name = "picb_CXLeft";
            this.picb_CXLeft.Size = new System.Drawing.Size(60, 39);
            this.picb_CXLeft.TabIndex = 9;
            this.picb_CXLeft.TabStop = false;
            // 
            // QGSwitch
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.lab_CXRight);
            this.Controls.Add(this.picb_CXRight);
            this.Controls.Add(this.picb_CXLeft);
            this.Controls.Add(this.lab_CXLeft);
            this.Controls.Add(this.btn_Status);
            this.Controls.Add(this.lab_Header);
            this.Name = "QGSwitch";
            this.Size = new System.Drawing.Size(158, 146);
            this.Load += new System.EventHandler(this.QGSwitch_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picb_CXRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_CXLeft)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CCWin.SkinControl.SkinLabel lab_Header;
        private HZH_Controls.Controls.UCSwitch btn_Status;
        private CCWin.SkinControl.SkinLabel lab_CXLeft;
        private CCWin.SkinControl.SkinLabel lab_CXRight;
        private CCWin.SkinControl.SkinPictureBox picb_CXLeft;
        private CCWin.SkinControl.SkinPictureBox picb_CXRight;
    }
}
