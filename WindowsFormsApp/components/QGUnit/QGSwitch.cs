﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp.deviceConnect;
using WindowsFormsApp.utils.modbus;

namespace WindowsFormsApp.components.QGUnit
{
    public partial class QGSwitch : UserControl,IAutoUpdatable
    {
        public QGSwitch()
        {
            InitializeComponent();
            ConnectConfig.updateList.Add(this);
            this.SetStyle(ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);

        }
       

        public ModbusListener busTcpClient;

        public QG qg;

        public string HeaderContent
        {
            get
            {
                return lab_Header.Text;
            }
            set
            {
                lab_Header.Text = value;

              

            }
        }




        public string CX_Left
        {
            get
            {
                return lab_CXLeft.Text;
            }
            set
            {
                lab_CXLeft.Text = value;

            }
        }

        public string CX_Right
        {
            get
            {
                return lab_CXRight.Text;
            }
            set
            {
                lab_CXRight.Text = value;

            }
        }

        public string[] ButtonText
        {
            get
            {
                return btn_Status.Texts;
            }
            set
            {
                btn_Status.Texts = value;
            }
        }


        //左磁性开关
        private bool _lefttriggered = false;
        //右磁性开关
        private bool _righttriggered = false;
        public bool LeftTriggered
        {
            get
            {
                return _lefttriggered;
            }
            set
            {
                if (value == _lefttriggered) return;
                _lefttriggered = value;

                picb_CXLeft.BackColor = value ? Color.Lime : SystemColors.ControlDarkDark;
                picb_CXLeft.ForeColor = value ? SystemColors.ControlLightLight : SystemColors.ControlText;
                if(qg.CX1Status)
                {
                    btn_Status.Checked = true;
                }


            }
        }

        public bool RightTriggered
        {
            get
            {
                return _righttriggered;
            }
            set
            {
                if (value == _righttriggered) return;
                _righttriggered = value;
                picb_CXRight.BackColor = value ? Color.Lime : SystemColors.ControlDarkDark;
                picb_CXRight.ForeColor = value ? SystemColors.ControlLightLight : SystemColors.ControlText;
                if (qg.CX2Status)
                {
                    btn_Status.Checked = false;
                }

            }
        }



        public void  ReFreshState()
        {
            LeftTriggered = qg.CX1Status;
            RightTriggered = qg.CX2Status;
        }

        
        private void btn_Status_CheckedChanged(object sender, EventArgs e)
        {
           if(btn_Status.Checked) //水平的状态 或伸出状态
            {
                if (qg.qgType == QGType.QG_Line)
                    qg.sendBack();
                else
                    qg.Vertical();   
            }
            else
            {
                if (qg.qgType == QGType.QG_Line)
                    qg.sendOut();
                else
                    //垂直状态 或缩回状态
                    qg.Level();
            }

        }

        private void QGSwitch_Load(object sender, EventArgs e)
        {
            //if(qg.CX1Status == true)
            //{
            //    btn_Status.Checked = true;
            //}
        }
    }
}