﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WindowsFormsApp.deviceConnect;
using WindowsFormsApp.utils.modbus;

namespace WindowsFormsApp.components.QGUnit
{

    public enum QGType
    {
        QG_90,  //90度气缸
        QG_180, //180度气缸
        QG_Line  //直线气缸
    }

    public class QG:IAutoUpdatable
    {
        public ModbusListener busTcpClient;


        public string BendingStatusCoil;      //弯曲状态线圈
        public string SendTriggerCoil;        //弯曲控制线圈

        public string StraightStatuCoil;    //伸直状态线圈
        public string BackTriggerCoil;  //伸直控制线圈



        public string LevelStatusCoil;        //吸气状态线圈

        public string LevelTriggerCoil; //吸气控制线圈

        public string VerticalStatusCoil;         //吹气状态线圈

        public string VerticalTriggerCoil;  //吹气控制线圈

        public QGType qgType;

        public bool CX1Status = false;          //伸直/90/180 水平状态

        public bool CX2Status = false;          // 弯曲/0 垂直状态

        public QG()
        {
            ConnectConfig.updateList.Add(this);
        }
        /// <summary>
        /// 气缸伸出
        /// </summary>
        public void sendOut()
        {
            busTcpClient.Write(BackTriggerCoil, true);
            busTcpClient.Write(BendingStatusCoil, false);
            Thread.Sleep(10);
            busTcpClient.Write(BackTriggerCoil, false);
        }

        /// <summary>
        /// 气缸缩回
        /// </summary>
        public void sendBack()
        {
            busTcpClient.Write(SendTriggerCoil, true);
            busTcpClient.Write(BackTriggerCoil, false);
            Thread.Sleep(10);
            busTcpClient.Write(SendTriggerCoil, false);
        }


        /// <summary>
        /// 气缸垂直
        /// </summary>
        public void Vertical()
        {
            
            busTcpClient.Write(VerticalTriggerCoil, true);
            busTcpClient.Write(LevelTriggerCoil, false);
            Thread.Sleep(10);
            busTcpClient.Write(VerticalTriggerCoil, false);
        }
        /// <summary>
        /// 气缸水平
        /// </summary>
        public void Level()
        {
            busTcpClient.Write(LevelTriggerCoil, true);
            busTcpClient.Write(VerticalTriggerCoil, false);
            Thread.Sleep(10);
            busTcpClient.Write(LevelTriggerCoil, false);
        }


        public void ReFreshState()
        {
            switch(qgType)
            {
                case QGType.QG_Line:
                    CX1Status = busTcpClient.ReadCoil(StraightStatuCoil);
                    CX2Status = busTcpClient.ReadCoil(BendingStatusCoil);
                    break;
                case QGType.QG_90:
                case QGType.QG_180:
                    CX1Status = busTcpClient.ReadCoil(LevelStatusCoil);
                    CX2Status = busTcpClient.ReadCoil(VerticalStatusCoil);
                    break;
            }
        }
    }
}
