﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp.components.robot;
using WindowsFormsApp.deviceConnect;

namespace WindowsFormsApp.components.tray
{
    /// <summary>
    /// 用于托盘上位置的计算
    /// </summary>
    public class Tray
   {
        //固定参数
        private static double Gripper = 96.8;//机械臂末端中心到吸嘴中心的距离
        private static double Rotate_Angle = 42.33; //每个位置的旋转角度
        private static double Rotate_Angle2 = 95.34;
        private static double CenDis_Around = 105.46; //周围盘片的中心距
        private static double CenDis_Center = 102.4; //中心盘片的中心距
        private static double Center_TO_Around = 136.36;
        private static double Offset = 30;  //吸取位置偏移
        private static double Height = 1.2;  //托盘的高度
        private static double Pad_Height = 8;



        public List<double> Left_Up;
        public List<double> Right_Up;
        public List<double> Left_Down;
        public List<double> Right_Down;
        public List<double> Center;


        public Tray()
        {
            //数据格式：X,Y,Z,A,B,C,夹爪
            Left_Up = new List<double> { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
            Left_Down = new List<double> { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
            Right_Down = new List<double> { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
            Right_Up = new List<double> { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
            Center = new List<double> { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
        }

        /// <summary>
        /// 计算其余四个位置的坐标
        /// </summary>
        public List<List<double>> CalPos(List<double> center, bool dir)
        {
            if (dir == true)//机械臂在托盘的右边
            {
                //将标定点移到中心
                center[1] += Gripper;
                
                double Y_Displace = (Center_TO_Around - Offset - Gripper) * Math.Cos(Rotate_Angle*Math.PI/180);
                double X_Displace = (Center_TO_Around - Offset - Gripper) * Math.Sin(Rotate_Angle * Math.PI / 180);

                List<List<double>> PosGather = new List<List<double>> { };
                Left_Up[0] = center[0] - X_Displace;
                Left_Up[1] = center[1] + Y_Displace;
                Left_Up[2] = center[2];
                Left_Up[3] = center[3]+ Rotate_Angle;
                
                for (int i = 4; i < 6; i++)
                {
                    Left_Up[i] = center[i];
                }
                PosGather.Add(Left_Up);



                Right_Up[0] = center[0] + X_Displace;
                Right_Up[1] = center[1] + Y_Displace;
                Right_Up[2] = center[2];
                Right_Up[3] = center[3] - Rotate_Angle;
                
                for (int i = 4; i < 6; i++)
                {
                    Right_Up[i] = center[i];
                }
                PosGather.Add(Right_Up);


                Left_Down[0] = center[0] - X_Displace;
                Left_Down[1] = center[1] - Y_Displace;
                Left_Down[2] = center[2];
                Left_Down[3] = center[3] +Rotate_Angle2+ Rotate_Angle;
                if (Left_Down[3] > 180)
                    Left_Down[3] -= 360;
                for (int i = 4; i < 6; i++)
                {
                    Left_Down[i] = center[i];
                }
                PosGather.Add(Left_Down);


                Right_Down[0] = center[0] + X_Displace;
                Right_Down[1] = center[1] - Y_Displace;
                Right_Down[2] = center[2];
                Right_Down[3] = center[3] - Rotate_Angle - Rotate_Angle2;

                for (int i = 4; i < 6; i++)
                {
                    Right_Down[i] = center[i];
                }
                PosGather.Add(Right_Down);

                Center[0] = center[0];
                Center[1] = center[1] - Offset - Gripper;
                Center[2] = center[2];
                Center[3] = center[3];
                for (int i = 4; i < 6; i++)
                {
                    Center[i] = center[i];
                }
                PosGather.Add(Center);


                return PosGather;

            }
            else {
                //将标定点移到中心
                center[1] -= 96.8;

                double Y_Displace = (Center_TO_Around - Offset - Gripper) * Math.Cos(Rotate_Angle);
                double X_Displace = (Center_TO_Around - Offset - Gripper) * Math.Sin(Rotate_Angle);
                List<List<double>> PosGather = new List<List<double>> { };
                Left_Up[0] = center[0] + X_Displace;
                Left_Up[1] = center[1] - Y_Displace;
                Left_Up[2] = center[2];
                Left_Up[3] = center[3] + Rotate_Angle;
                for (int i = 4; i < 6; i++)
                {
                    Left_Up[i] = center[i];
                }
                PosGather.Add(Left_Up);



                Right_Up[0] = center[0] - X_Displace;
                Right_Up[1] = center[1] - Y_Displace;
                Right_Up[2] = center[2];
                Right_Up[3] = center[3] - Rotate_Angle;
                for (int i = 4; i < 6; i++)
                {
                    Right_Up[i] = center[i];
                }
                PosGather.Add(Right_Up);


                Left_Down[0] = center[0] + X_Displace;
                Left_Down[1] = center[1] + Y_Displace;
                Left_Down[2] = center[2];
                Left_Down[3] = center[3] + 2 * Rotate_Angle;
                for (int i = 4; i < 6; i++)
                {
                    Left_Down[i] = center[i];
                }
                PosGather.Add(Left_Down);


                Right_Down[0] = center[0] - X_Displace;
                Right_Down[1] = center[1] + Y_Displace;
                Right_Down[2] = center[2];
                Right_Down[3] = center[3] - 2 * Rotate_Angle;
                for (int i = 4; i < 6; i++)
                {
                    Right_Down[i] = center[i];
                }
                PosGather.Add(Left_Up);

                return PosGather;

            }



        }

        public void WritePos(Robot robot, bool dir)
        {
            List<List<double>> result;
            Hsc3.Comm.GeneralPos pos = RobotConnect.GetStorePoint(robot, "LR", ParamConfig.CALIBRATE_POS_IO);
            
            result = CalPos(pos.vecPos, dir);
            pos.vecPos = result[0];
            RobotConnect.SaveKnowPoint(robot,pos,ParamConfig.LEFT_UP_POS_IO);//左上
            pos.vecPos = result[1];
            RobotConnect.SaveKnowPoint(robot, pos, ParamConfig.RIGHT_UP_POS_IO);//右上
            pos.vecPos = result[2];
            RobotConnect.SaveKnowPoint(robot, pos, ParamConfig.LEFT_DOWN_POS_IO);//左下
            pos.vecPos = result[3];
            RobotConnect.SaveKnowPoint(robot, pos, ParamConfig.RIGHT_DOWN_POS_IO);//右下
            pos.vecPos = result[4];
            RobotConnect.SaveKnowPoint(robot, pos, ParamConfig.CENTER_POS_IO); //中心



        }




    }
}
