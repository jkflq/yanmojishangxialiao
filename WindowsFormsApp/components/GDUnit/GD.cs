﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp.deviceConnect;
using WindowsFormsApp.utils.modbus;

namespace WindowsFormsApp.components.GDUnit
{
    /// <summary>
    /// 光电开关类
    /// </summary>
    public class GD:IAutoUpdatable
    {

        public ModbusListener busTcpClient;
        public GD()
        {
            //ConnectConfig.updateList.Add(this);
        }

        public void ReFreshState()
        {
            Triggered = busTcpClient.ReadCoil(Address);
        }

        public string Address = "0";
        public bool Triggered = false;

    }
}
