﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp.deviceConnect;

namespace WindowsFormsApp.components.robot
{
    public partial class RobotOption : UserControl
    {
        Robot robot;
        private System.Timers.Timer timer_robot;

        public RobotOption()
        {
            InitializeComponent();
            this.SetStyle(
            ControlStyles.UserPaint |
            ControlStyles.AllPaintingInWmPaint |
            ControlStyles.OptimizedDoubleBuffer, true);
        }

        private void cbx_RobotSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                switch (cbx_RobotSelect.SelectedIndex)
                {
                    case 0:
                        if (ConnectConfig.FirstRobot != null&&ConnectConfig.FirstRobot.Connected)
                        {
                            timer_robot.Stop();
                            robot = ConnectConfig.FirstRobot;
                            timer_robot.Start();
                        }
                           
                        else
                            MessageBox.Show("上下料机械臂未连接");
                        break;
                    case 1:
                        if (ConnectConfig.SecondRobot != null && ConnectConfig.SecondRobot.Connected)
                        {
                            timer_robot.Stop();
                            robot = ConnectConfig.SecondRobot;
                            timer_robot.Start();

                        }
                        else
                            MessageBox.Show("卸料机械臂未连接");
                        break;
                    case 2:
                        if (ConnectConfig.ThirdRobot != null && ConnectConfig.ThirdRobot.Connected)
                        {
                            timer_robot.Stop();
                            robot = ConnectConfig.ThirdRobot;
                            timer_robot.Start();
                        }
                            
                        else
                            MessageBox.Show("装料机械臂未连接");
                        break;

                }
            }
            catch
            {
                MessageBox.Show("设备未连接"); 
            }

        }

        private void RobotOption_Load(object sender, EventArgs e)
        {
            skinComboBox2.SelectedIndex = 0;
            skinComboBox1.SelectedIndex = 0;
            ucTextBoxEx1.InputText = "0";

            pan_D.Visible = true;
            pan_J.Visible = false;

            this.timer_robot = new System.Timers.Timer(500);
            this.timer_robot.Elapsed += Timer_robot_Elapsed;
        }

        private void Timer_robot_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (skinComboBox2.SelectedIndex == 0)
            {
                List<double> datas = robot.GetLocData();
                tet_Xvalue.Text = datas[0].ToString();
                tet_Yvalue.Text = datas[1].ToString();
                tet_Zvalue.Text = datas[2].ToString();
                tet_Avalue.Text = datas[3].ToString();

            }
            else
            {
                List<double> datas = robot.GetJntData();
                tet_J1value.Text = datas[0].ToString();
                tet_J2value.Text = datas[1].ToString();
                tet_J3value.Text = datas[2].ToString();
                tet_J4value.Text = datas[3].ToString();
                tet_J5value.Text = datas[4].ToString();
                tet_J6value.Text = datas[5].ToString();
            }
        }

        private void skinComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (skinComboBox1.SelectedIndex == 0) //增量模式
                skinPanel3.Visible = true;
            else
                skinPanel3.Visible = false;
        }

        private void skinComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(skinComboBox2.SelectedIndex == 0) //笛卡尔
            {
                pan_D.Visible = true;
                pan_J.Visible = false;
                skinPanel2.Visible = false;
                skinPanel4.Visible = true;
                if(robot == ConnectConfig.FirstRobot)
                {
                    btn_BUP.Visible = false;
                    btn_BDOWN.Visible = false;
                    btn_CUP.Visible = false;
                    btn_CDOWN.Visible = false;

                }
                else
                {
                    btn_BUP.Visible = true;
                    btn_BDOWN.Visible = true;
                    btn_CUP.Visible = true;
                    btn_CDOWN.Visible = true;

                }
            }
            else//关节模式
            {
                pan_D.Visible = false;
                pan_J.Visible = true;
                skinPanel2.Visible = true;
                skinPanel4.Visible = false;
                if (robot == ConnectConfig.FirstRobot)
                {
                    btn_J5UP.Visible = false;
                    btn_J5DOWN.Visible = false;
                    btn_J6UP.Visible = false;
                    btn_J6DOWN.Visible = false;

                }
                else
                {
                    btn_J5UP.Visible = true;
                    btn_J5DOWN.Visible = true;
                    btn_J6UP.Visible = true;
                    btn_J6DOWN.Visible = true;
                }
            }
        }

        private void skinTrackBar1_ValueChanged(object sender, EventArgs e)
        {
            skinTextBox7.Text = skinTrackBar1.Value.ToString();
        }


        private void btn_J1UP_MouseDown(object sender, MouseEventArgs e)
        {
            if (robot == null )
            {
                MessageBox.Show("请选择机械臂对象");
                return;
            }
            if(skinTrackBar1.Value == 0)
            {
                MessageBox.Show("请输入速度参数");
                return;
            }
            if (skinComboBox1.SelectedIndex == 0) //增量模式
            {
                if (ucTextBoxEx1.InputText == "0")
                {
                    MessageBox.Show("请输入增量距离");
                    return;
                }    
                RobotConnect.JointManualIncMove(robot, 0, skinTrackBar1.Value, double.Parse(ucTextBoxEx1.InputText),Hsc3.Comm.DirectType.POSITIVE);
            }
            else
            {
                RobotConnect.JointManualConMove(robot, 0, skinTrackBar1.Value, Hsc3.Comm.DirectType.POSITIVE);
            }
        }

        private void btn_J1DOWN_MouseDown(object sender, MouseEventArgs e)
        {
            if (robot == null)
            {
                MessageBox.Show("请选择机械臂对象");
                return;
            }
            if (skinTrackBar1.Value == 0)
            {
                MessageBox.Show("请输入速度参数");
                return;
            }
            if (skinComboBox1.SelectedIndex == 0) //增量模式
            {
                if (ucTextBoxEx1.InputText == "0")
                {
                    MessageBox.Show("请输入增量距离");
                    return;
                }
                RobotConnect.JointManualIncMove(robot, 0, skinTrackBar1.Value, double.Parse(ucTextBoxEx1.InputText), Hsc3.Comm.DirectType.NEGATIVE);
            }
            else
            {
                RobotConnect.JointManualConMove(robot, 0, skinTrackBar1.Value, Hsc3.Comm.DirectType.NEGATIVE);
            }
        }

        private void btn_J2UP_MouseDown(object sender, MouseEventArgs e)
        {
            if (robot == null)
            {
                MessageBox.Show("请选择机械臂对象");
                return;
            }
            if (skinTrackBar1.Value == 0)
            {
                MessageBox.Show("请输入速度参数");
                return;
            }
            if (skinComboBox1.SelectedIndex == 0) //增量模式
            {
                if (ucTextBoxEx1.InputText == "0")
                {
                    MessageBox.Show("请输入增量距离");
                    return;
                }
                RobotConnect.JointManualIncMove(robot, 1, skinTrackBar1.Value, double.Parse(ucTextBoxEx1.InputText), Hsc3.Comm.DirectType.POSITIVE);
            }
            else
            {
                RobotConnect.JointManualConMove(robot, 1, skinTrackBar1.Value, Hsc3.Comm.DirectType.POSITIVE);
            }
        }

        private void btn_J2DOWN_MouseDown(object sender, MouseEventArgs e)
        {
            if (robot == null)
            {
                MessageBox.Show("请选择机械臂对象");
                return;
            }
            if (skinTrackBar1.Value == 0)
            {
                MessageBox.Show("请输入速度参数");
                return;
            }
            if (skinComboBox1.SelectedIndex == 0) //增量模式
            {
                if (ucTextBoxEx1.InputText == "0")
                {
                    MessageBox.Show("请输入增量距离");
                    return;
                }
                RobotConnect.JointManualIncMove(robot, 1, skinTrackBar1.Value, double.Parse(ucTextBoxEx1.InputText), Hsc3.Comm.DirectType.NEGATIVE);
            }
            else
            {
                RobotConnect.JointManualConMove(robot, 1, skinTrackBar1.Value, Hsc3.Comm.DirectType.NEGATIVE);
            }
        }

        private void btn_J3UP_MouseDown(object sender, MouseEventArgs e)
        {
            if (robot == null)
            {
                MessageBox.Show("请选择机械臂对象");
                return;
            }
            if (skinTrackBar1.Value == 0)
            {
                MessageBox.Show("请输入速度参数");
                return;
            }
            if (skinComboBox1.SelectedIndex == 0) //增量模式
            {
                if (ucTextBoxEx1.InputText == "0")
                {
                    MessageBox.Show("请输入增量距离");
                    return;
                }

                RobotConnect.JointManualIncMove(robot, 2, skinTrackBar1.Value, double.Parse(ucTextBoxEx1.InputText), Hsc3.Comm.DirectType.POSITIVE);
            }
            else
            {
                RobotConnect.JointManualConMove(robot, 2, skinTrackBar1.Value, Hsc3.Comm.DirectType.POSITIVE);
            }
        }


        private void btn_J3DOWN_MouseDown(object sender, MouseEventArgs e)
        {
            if (robot == null)
            {
                MessageBox.Show("请选择机械臂对象");
                return;
            }
            if (skinTrackBar1.Value == 0)
            {
                MessageBox.Show("请输入速度参数");
                return;
            }
            if (skinComboBox1.SelectedIndex == 0) //增量模式
            {
                if (ucTextBoxEx1.InputText == "0")
                {
                    MessageBox.Show("请输入增量距离");
                    return;
                }
                RobotConnect.JointManualIncMove(robot, 2, skinTrackBar1.Value, double.Parse(ucTextBoxEx1.InputText), Hsc3.Comm.DirectType.NEGATIVE);
            }
            else
            {
                RobotConnect.JointManualConMove(robot, 2, skinTrackBar1.Value, Hsc3.Comm.DirectType.NEGATIVE);
            }
        }

        private void btn_J4UP_MouseDown(object sender, MouseEventArgs e)
        {
            if (robot == null)
            {
                MessageBox.Show("请选择机械臂对象");
                return;
            }
            if (skinTrackBar1.Value == 0)
            {
                MessageBox.Show("请输入速度参数");
                return;
            }
            if (skinComboBox1.SelectedIndex == 0) //增量模式
            {
                if (ucTextBoxEx1.InputText == "0")
                {
                    MessageBox.Show("请输入增量距离");
                    return;
                }
                RobotConnect.JointManualIncMove(robot, 3, skinTrackBar1.Value, double.Parse(ucTextBoxEx1.InputText), Hsc3.Comm.DirectType.POSITIVE);
            }
            else
            {
                RobotConnect.JointManualConMove(robot, 3, skinTrackBar1.Value, Hsc3.Comm.DirectType.POSITIVE);
            }
        }

       

        private void btn_J4DOWN_MouseDown(object sender, MouseEventArgs e)
        {
            if (robot == null)
            {
                MessageBox.Show("请选择机械臂对象");
                return;
            }
            if (skinTrackBar1.Value == 0)
            {
                MessageBox.Show("请输入速度参数");
                return;
            }
            if (skinComboBox1.SelectedIndex == 0) //增量模式
            {
                if (ucTextBoxEx1.InputText == "0")
                {
                    MessageBox.Show("请输入增量距离");
                    return;
                }
                RobotConnect.JointManualIncMove(robot, 3, skinTrackBar1.Value, double.Parse(ucTextBoxEx1.InputText), Hsc3.Comm.DirectType.NEGATIVE);
            }
            else
            {
                RobotConnect.JointManualConMove(robot, 3, skinTrackBar1.Value, Hsc3.Comm.DirectType.NEGATIVE);
            }
        }

        private void btn_J5UP_MouseDown(object sender, MouseEventArgs e)
        {
            if (robot == null)
            {
                MessageBox.Show("请选择机械臂对象");
                return;
            }
            if(robot == ConnectConfig.FirstRobot)
            {
                MessageBox.Show("超越当前机械臂对象的自由度，请重新选择机械臂对象");
                return;
            }
            if (skinTrackBar1.Value == 0)
            {
                MessageBox.Show("请输入速度参数");
                return;
            }
            if (skinComboBox1.SelectedIndex == 0) //增量模式
            {
                if (ucTextBoxEx1.InputText == "0")
                {
                    MessageBox.Show("请输入增量距离");
                    return;
                }
                RobotConnect.JointManualIncMove(robot, 4, skinTrackBar1.Value, double.Parse(ucTextBoxEx1.InputText), Hsc3.Comm.DirectType.POSITIVE);
            }
            else
            {
                RobotConnect.JointManualConMove(robot, 4, skinTrackBar1.Value, Hsc3.Comm.DirectType.POSITIVE);
            }
        }

        private void btn_J5DOWN_MouseDown(object sender, MouseEventArgs e)
        {
            if (robot == null)
            {
                MessageBox.Show("请选择机械臂对象");
                return;
            }
            if (robot == ConnectConfig.FirstRobot)
            {
                MessageBox.Show("超越当前机械臂对象的自由度，请重新选择机械臂对象");
                return;
            }
            if (skinTrackBar1.Value == 0)
            {
                MessageBox.Show("请输入速度参数");
                return;
            }
            if (skinComboBox1.SelectedIndex == 0) //增量模式
            {
                if (ucTextBoxEx1.InputText == "0")
                {
                    MessageBox.Show("请输入增量距离");
                    return;
                }
                RobotConnect.JointManualIncMove(robot, 4, skinTrackBar1.Value, double.Parse(ucTextBoxEx1.InputText), Hsc3.Comm.DirectType.NEGATIVE);
            }
            else
            {
                RobotConnect.JointManualConMove(robot, 4, skinTrackBar1.Value, Hsc3.Comm.DirectType.NEGATIVE);
            }
        }

        private void btn_J6UP_MouseDown(object sender, MouseEventArgs e)
        {
            if (robot == null)
            {
                MessageBox.Show("请选择机械臂对象");
                return;
            }
            if (robot == ConnectConfig.FirstRobot)
            {
                MessageBox.Show("超越当前机械臂对象的自由度，请重新选择机械臂对象");
                return;
            }
            if (skinTrackBar1.Value == 0)
            {
                MessageBox.Show("请输入速度参数");
                return;
            }
            if (skinComboBox1.SelectedIndex == 0) //增量模式
            {
                if (ucTextBoxEx1.InputText == "0")
                {
                    MessageBox.Show("请输入增量距离");
                    return;
                }
                RobotConnect.JointManualIncMove(robot, 5, skinTrackBar1.Value, double.Parse(ucTextBoxEx1.InputText), Hsc3.Comm.DirectType.POSITIVE);
            }
            else
            {
                RobotConnect.JointManualConMove(robot, 5, skinTrackBar1.Value, Hsc3.Comm.DirectType.POSITIVE);
            }
        }

        private void btn_J6DOWN_MouseDown(object sender, MouseEventArgs e)
        {
            if (robot == null)
            {
                MessageBox.Show("请选择机械臂对象");
                return;
            }
            if (robot == ConnectConfig.FirstRobot)
            {
                MessageBox.Show("超越当前机械臂对象的自由度，请重新选择机械臂对象");
                return;
            }
            if (skinTrackBar1.Value == 0)
            {
                MessageBox.Show("请输入速度参数");
                return;
            }
            if (skinComboBox1.SelectedIndex == 0) //增量模式
            {
                if (ucTextBoxEx1.InputText == "0")
                {
                    MessageBox.Show("请输入增量距离");
                    return;
                }
                RobotConnect.JointManualIncMove(robot, 5, skinTrackBar1.Value, double.Parse(ucTextBoxEx1.InputText), Hsc3.Comm.DirectType.NEGATIVE);
            }
            else
            {
                RobotConnect.JointManualConMove(robot, 5, skinTrackBar1.Value, Hsc3.Comm.DirectType.NEGATIVE);
            }
        }
        private void btn_J3DOWN_MouseUp(object sender, MouseEventArgs e)
        {
            if (skinComboBox1.SelectedIndex == 1)
                robot.proMot.stopJog(0);
        }

        private void btn_J4UP_MouseUp(object sender, MouseEventArgs e)
        {
            if (skinComboBox1.SelectedIndex == 1)
                robot.proMot.stopJog(0);
        }
        private void btn_J6DOWN_MouseUp(object sender, MouseEventArgs e)
        {
            if (skinComboBox1.SelectedIndex == 1)
                robot.proMot.stopJog(0);
        }

        private void btn_J6UP_MouseUp(object sender, MouseEventArgs e)
        {
            if (skinComboBox1.SelectedIndex == 1)
                robot.proMot.stopJog(0);
        }

        private void btn_J5DOWN_MouseUp(object sender, MouseEventArgs e)
        {
            if (skinComboBox1.SelectedIndex == 1)
                robot.proMot.stopJog(0);
        }

        private void btn_J5UP_MouseUp(object sender, MouseEventArgs e)
        {
            if (skinComboBox1.SelectedIndex == 1)
                robot.proMot.stopJog(0);
        }

        private void btn_J4DOWN_MouseUp(object sender, MouseEventArgs e)
        {
            if (skinComboBox1.SelectedIndex == 1)
                robot.proMot.stopJog(0);
        }
        private void btn_J3UP_MouseUp(object sender, MouseEventArgs e)
        {
            if (skinComboBox1.SelectedIndex == 1)
                robot.proMot.stopJog(0);
        }
        private void btn_J2DOWN_MouseUp(object sender, MouseEventArgs e)
        {
            if (skinComboBox1.SelectedIndex == 1)
                robot.proMot.stopJog(0);
        }
        private void btn_J2UP_MouseUp(object sender, MouseEventArgs e)
        {
            if (skinComboBox1.SelectedIndex == 1)
                robot.proMot.stopJog(0);
        }
        private void btn_J1UP_MouseUp(object sender, MouseEventArgs e)
        {
            if (skinComboBox1.SelectedIndex == 1)
                robot.proMot.stopJog(0);
        }
        private void btn_J1DOWN_MouseUp(object sender, MouseEventArgs e)
        {
            if (skinComboBox1.SelectedIndex == 1)
                robot.proMot.stopJog(0);
        }

        private void btn_XUP_MouseDown(object sender, MouseEventArgs e)
        {
            if (robot == null)
            {
                MessageBox.Show("请选择机械臂对象");
                return;
            }
            if (skinTrackBar1.Value == 0)
            {
                MessageBox.Show("请输入速度参数");
                return;
            }
            if (skinComboBox1.SelectedIndex == 0) //增量模式
            {
                if (ucTextBoxEx1.InputText == "0")
                {
                    MessageBox.Show("请输入增量距离");
                    return;
                }
                RobotConnect.DescartManualIncMove(robot, 0, skinTrackBar1.Value, double.Parse(ucTextBoxEx1.InputText), Hsc3.Comm.DirectType.POSITIVE);
            }
            else
            {
                RobotConnect.DescartManualConMove(robot, 0, skinTrackBar1.Value, Hsc3.Comm.DirectType.POSITIVE);
            }
        }

        private void btn_XDOWN_MouseDown(object sender, MouseEventArgs e)
        {
            if (robot == null)
            {
                MessageBox.Show("请选择机械臂对象");
                return;
            }
            if (skinTrackBar1.Value == 0)
            {
                MessageBox.Show("请输入速度参数");
                return;
            }
            if (skinComboBox1.SelectedIndex == 0) //增量模式
            {
                if (ucTextBoxEx1.InputText == "0")
                {
                    MessageBox.Show("请输入增量距离");
                    return;
                }
                RobotConnect.DescartManualIncMove(robot, 0, skinTrackBar1.Value, double.Parse(ucTextBoxEx1.InputText), Hsc3.Comm.DirectType.NEGATIVE);
            }
            else
            {
                RobotConnect.DescartManualConMove(robot, 0, skinTrackBar1.Value, Hsc3.Comm.DirectType.NEGATIVE);
            }
        }

        private void btn_YUP_MouseDown(object sender, MouseEventArgs e)
        {
            if (robot == null)
            {
                MessageBox.Show("请选择机械臂对象");
                return;
            }
            if (skinTrackBar1.Value == 0)
            {
                MessageBox.Show("请输入速度参数");
                return;
            }
            if (skinComboBox1.SelectedIndex == 0) //增量模式
            {
                if (ucTextBoxEx1.InputText == "0")
                {
                    MessageBox.Show("请输入增量距离");
                    return;
                }
                RobotConnect.DescartManualIncMove(robot, 1, skinTrackBar1.Value, double.Parse(ucTextBoxEx1.InputText), Hsc3.Comm.DirectType.POSITIVE);
            }
            else
            {
                RobotConnect.DescartManualConMove(robot, 1, skinTrackBar1.Value, Hsc3.Comm.DirectType.POSITIVE);
            }
        }

        private void btn_YDOWN_MouseDown(object sender, MouseEventArgs e)
        {
            if (robot == null)
            {
                MessageBox.Show("请选择机械臂对象");
                return;
            }
            if (skinTrackBar1.Value == 0)
            {
                MessageBox.Show("请输入速度参数");
                return;
            }
            if (skinComboBox1.SelectedIndex == 0) //增量模式
            {
                if (ucTextBoxEx1.InputText == "0")
                {
                    MessageBox.Show("请输入增量距离");
                    return;
                }
                RobotConnect.DescartManualIncMove(robot, 1, skinTrackBar1.Value, double.Parse(ucTextBoxEx1.InputText), Hsc3.Comm.DirectType.NEGATIVE);
            }
            else
            {
                RobotConnect.DescartManualConMove(robot, 1, skinTrackBar1.Value, Hsc3.Comm.DirectType.NEGATIVE);
            }
        }

        private void btn_ZUP_MouseDown(object sender, MouseEventArgs e)
        {
            if (robot == null)
            {
                MessageBox.Show("请选择机械臂对象");
                return;
            }
            if (skinTrackBar1.Value == 0)
            {
                MessageBox.Show("请输入速度参数");
                return;
            }
            if (skinComboBox1.SelectedIndex == 0) //增量模式
            {
                if (ucTextBoxEx1.InputText == "0")
                {
                    MessageBox.Show("请输入增量距离");
                    return;
                }
                RobotConnect.DescartManualIncMove(robot, 2, skinTrackBar1.Value, double.Parse(ucTextBoxEx1.InputText), Hsc3.Comm.DirectType.POSITIVE);
            }
            else
            {
                RobotConnect.DescartManualConMove(robot, 2, skinTrackBar1.Value, Hsc3.Comm.DirectType.POSITIVE);
            }
        }

        private void btn_ZDOWN_MouseDown(object sender, MouseEventArgs e)
        {
            if (robot == null)
            {
                MessageBox.Show("请选择机械臂对象");
                return;
            }
            if (skinTrackBar1.Value == 0)
            {
                MessageBox.Show("请输入速度参数");
                return;
            }
            if (skinComboBox1.SelectedIndex == 0) //增量模式
            {
                if (ucTextBoxEx1.InputText == "0")
                {
                    MessageBox.Show("请输入增量距离");
                    return;
                }
                RobotConnect.DescartManualIncMove(robot, 2, skinTrackBar1.Value, double.Parse(ucTextBoxEx1.InputText), Hsc3.Comm.DirectType.NEGATIVE);
            }
            else
            {
                RobotConnect.DescartManualConMove(robot, 2, skinTrackBar1.Value, Hsc3.Comm.DirectType.NEGATIVE);
            }
        }

        private void btn_AUP_MouseDown(object sender, MouseEventArgs e)
        {
            if (robot == null)
            {
                MessageBox.Show("请选择机械臂对象");
                return;
            }
            if (skinTrackBar1.Value == 0)
            {
                MessageBox.Show("请输入速度参数");
                return;
            }
            if (skinComboBox1.SelectedIndex == 0) //增量模式
            {
                if (ucTextBoxEx1.InputText == "0")
                {
                    MessageBox.Show("请输入增量距离");
                    return;
                }
                RobotConnect.DescartManualIncMove(robot, 3, skinTrackBar1.Value, double.Parse(ucTextBoxEx1.InputText), Hsc3.Comm.DirectType.POSITIVE);
            }
            else
            {
                RobotConnect.DescartManualConMove(robot, 3, skinTrackBar1.Value, Hsc3.Comm.DirectType.POSITIVE);
            }
        }

        private void btn_ADOWN_MouseDown(object sender, MouseEventArgs e)
        {
            if (robot == null)
            {
                MessageBox.Show("请选择机械臂对象");
                return;
            }
            if (skinTrackBar1.Value == 0)
            {
                MessageBox.Show("请输入速度参数");
                return;
            }
            if (skinComboBox1.SelectedIndex == 0) //增量模式
            {
                if (ucTextBoxEx1.InputText == "0")
                {
                    MessageBox.Show("请输入增量距离");
                    return;
                }
                RobotConnect.DescartManualIncMove(robot, 3, skinTrackBar1.Value, double.Parse(ucTextBoxEx1.InputText), Hsc3.Comm.DirectType.NEGATIVE);
            }
            else
            {
                RobotConnect.DescartManualConMove(robot, 3, skinTrackBar1.Value, Hsc3.Comm.DirectType.NEGATIVE);
            }
        }

        private void btn_BUP_MouseDown(object sender, MouseEventArgs e)
        {
            if (robot == null)
            {
                MessageBox.Show("请选择机械臂对象");
                return;
            }
            if (robot == ConnectConfig.FirstRobot)
            {
                MessageBox.Show("超越当前机械臂对象的自由度，请重新选择机械臂对象");
                return;
            }
            if (skinTrackBar1.Value == 0)
            {
                MessageBox.Show("请输入速度参数");
                return;
            }
            if (skinComboBox1.SelectedIndex == 0) //增量模式
            {
                if (ucTextBoxEx1.InputText == "0")
                {
                    MessageBox.Show("请输入增量距离");
                    return;
                }
                RobotConnect.DescartManualIncMove(robot, 4, skinTrackBar1.Value, double.Parse(ucTextBoxEx1.InputText), Hsc3.Comm.DirectType.POSITIVE);
            }
            else
            {
                RobotConnect.DescartManualConMove(robot, 4, skinTrackBar1.Value, Hsc3.Comm.DirectType.POSITIVE);
            }
        }

        private void btn_BDOWN_MouseDown(object sender, MouseEventArgs e)
        {
            if (robot == null)
            {
                MessageBox.Show("请选择机械臂对象");
                return;
            }
            if (robot == ConnectConfig.FirstRobot)
            {
                MessageBox.Show("超越当前机械臂对象的自由度，请重新选择机械臂对象");
                return;
            }
            if (skinTrackBar1.Value == 0)
            {
                MessageBox.Show("请输入速度参数");
                return;
            }
            if (skinComboBox1.SelectedIndex == 0) //增量模式
            {
                if (ucTextBoxEx1.InputText == "0")
                {
                    MessageBox.Show("请输入增量距离");
                    return;
                }
                RobotConnect.DescartManualIncMove(robot, 4, skinTrackBar1.Value, double.Parse(ucTextBoxEx1.InputText), Hsc3.Comm.DirectType.NEGATIVE);
            }
            else
            {
                RobotConnect.DescartManualConMove(robot, 4, skinTrackBar1.Value, Hsc3.Comm.DirectType.NEGATIVE);
            }
        }

        private void btn_CUP_MouseDown(object sender, MouseEventArgs e)
        {
            if (robot == null)
            {
                MessageBox.Show("请选择机械臂对象");
                return;
            }
            if (robot == ConnectConfig.FirstRobot)
            {
                MessageBox.Show("超越当前机械臂对象的自由度，请重新选择机械臂对象");
                return;
            }
            if (skinTrackBar1.Value == 0)
            {
                MessageBox.Show("请输入速度参数");
                return;
            }
            if (skinComboBox1.SelectedIndex == 0) //增量模式
            {
                if (ucTextBoxEx1.InputText == "0")
                {
                    MessageBox.Show("请输入增量距离");
                    return;
                }
                RobotConnect.DescartManualIncMove(robot, 5, skinTrackBar1.Value, double.Parse(ucTextBoxEx1.InputText), Hsc3.Comm.DirectType.POSITIVE);
            }
            else
            {
                RobotConnect.DescartManualConMove(robot, 5, skinTrackBar1.Value, Hsc3.Comm.DirectType.POSITIVE);
            }
        }

        private void btn_CDOWN_MouseDown(object sender, MouseEventArgs e)
        {
            if (robot == null)
            {
                MessageBox.Show("请选择机械臂对象");
                return;
            }
            if (robot == ConnectConfig.FirstRobot)
            {
                MessageBox.Show("超越当前机械臂对象的自由度，请重新选择机械臂对象");
                return;
            }
            if (skinTrackBar1.Value == 0)
            {
                MessageBox.Show("请输入速度参数");
                return;
            }
            if (skinComboBox1.SelectedIndex == 0) //增量模式
            {
                if (ucTextBoxEx1.InputText == "0")
                {
                    MessageBox.Show("请输入增量距离");
                    return;
                }
                RobotConnect.DescartManualIncMove(robot, 5, skinTrackBar1.Value, double.Parse(ucTextBoxEx1.InputText), Hsc3.Comm.DirectType.NEGATIVE);
            }
            else
            {
                RobotConnect.DescartManualConMove(robot, 5, skinTrackBar1.Value, Hsc3.Comm.DirectType.NEGATIVE);
            }
        }

        private void btn_XUP_MouseUp(object sender, MouseEventArgs e)
        {
            if (skinComboBox1.SelectedIndex == 1)
                robot.proMot.stopJog(0);
        }

        private void btn_XDOWN_MouseUp(object sender, MouseEventArgs e)
        {
            if (skinComboBox1.SelectedIndex == 1)
                robot.proMot.stopJog(0);
        }

        private void btn_YUP_MouseUp(object sender, MouseEventArgs e)
        {
            if (skinComboBox1.SelectedIndex == 1)
                robot.proMot.stopJog(0);
        }

        private void btn_YDOWN_MouseUp(object sender, MouseEventArgs e)
        {
            if (skinComboBox1.SelectedIndex == 1)
                robot.proMot.stopJog(0);
        }

        private void btn_ZUP_MouseUp(object sender, MouseEventArgs e)
        {
            if (skinComboBox1.SelectedIndex == 1)
                robot.proMot.stopJog(0);
        }

        private void btn_ZDOWN_MouseUp(object sender, MouseEventArgs e)
        {
            if (skinComboBox1.SelectedIndex == 1)
                robot.proMot.stopJog(0);
        }

        private void btn_AUP_MouseUp(object sender, MouseEventArgs e)
        {
            if (skinComboBox1.SelectedIndex == 1)
                robot.proMot.stopJog(0);
        }

        private void btn_ADOWN_MouseUp(object sender, MouseEventArgs e)
        {
            if (skinComboBox1.SelectedIndex == 1)
                robot.proMot.stopJog(0);
        }

        private void btn_BUP_MouseUp(object sender, MouseEventArgs e)
        {
            if (skinComboBox1.SelectedIndex == 1)
                robot.proMot.stopJog(0);
        }

        private void btn_BDOWN_MouseUp(object sender, MouseEventArgs e)
        {
            if (skinComboBox1.SelectedIndex == 1) 
                robot.proMot.stopJog(0);
        }

        private void btn_CUP_MouseUp(object sender, MouseEventArgs e)
        {
            if (skinComboBox1.SelectedIndex == 1)
                robot.proMot.stopJog(0);
        }

        private void btn_CDOWN_MouseUp(object sender, MouseEventArgs e)
        {
            if (skinComboBox1.SelectedIndex == 1)
                robot.proMot.stopJog(0);
        }


    }
}
