﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WindowsFormsApp.deviceConnect;

namespace WindowsFormsApp.components.robot
{

    public enum MotorPattern
    {
        T1 = 0,
        T2,
        AUTO,
        EXT,

    }
        


    /// <summary>
    /// 此类用于单个机械臂的参数制定
    /// </summary>
    public class Robot :IAutoUpdatable
    {
        //构造机械臂运动通信客户端代理。
        public Hsc3.Comm.CommApi CommMot;
        //机械臂变量操作代理
        public Hsc3.Proxy.ProxyVar proVar;
        //机械臂运动功能代理
        public Hsc3.Proxy.ProxyMotion proMot;
        //机械臂程序运行代理
        public Hsc3.Comm.ProxyVm proVm;
        //机械臂IO操作代理
        public Hsc3.Proxy.ProxyIO proIO;

        //构建机械臂错误轮询通信客户端代理
        public Hsc3.Comm.CommApi CommSys;
        //机械臂的系统功能代理
        public Hsc3.Proxy.ProxySys proSys;

        public MotorPattern Pattern;

        public int ID;
        public string Name;

        public List<double> jntData;
        public List<double> locData;

        //用于存机械臂输入IO状态
        public bool[] dins;
        public bool[] douts;
        public double[] regs;


        public bool Connected = false;

        public bool Buys = false;
        public bool Error = false;
        public bool Paused = false;

        public string IP;
        public int SocketPort;
        public ushort CommPort;

        //事件
        public delegate void  RobotEveHandler();
        public event RobotEveHandler onError;
        public event RobotEveHandler onPause;
        public event RobotEveHandler onStop;



        public enum AXIS
        {
            J1 = 0,
            J2 = 1,
            J3,
            J4,
            J5,
            J6,
        };

        public Robot()
        {

            ConnectConfig.updateList.Add(this);
        }
        public Robot(string ip,int port1,ushort port2)
        {
            //初始化
            CommMot = new Hsc3.Comm.CommApi();
            CommSys = new Hsc3.Comm.CommApi();
            proVar = new Hsc3.Proxy.ProxyVar(CommMot);
            proMot = new Hsc3.Proxy.ProxyMotion(CommMot);
            proVm = new Hsc3.Comm.ProxyVm(CommMot);
            proIO = new Hsc3.Proxy.ProxyIO(CommMot);
            proSys = new Hsc3.Proxy.ProxySys(CommMot);


            jntData = new List<double>() { 0.0, 0.0, 0.0, 0.0,0.0,0.0 };
            locData = new List<double>() { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

            this.IP = ip;
            this.CommPort = port2;
            this.SocketPort = port1;

        }

        public Robot(string ip, ushort port,int id,string name)
        {
            //初始化
            CommMot = new Hsc3.Comm.CommApi();
            //CommSys = new Hsc3.Comm.CommApi();
            proVar = new Hsc3.Proxy.ProxyVar(CommMot);
            proMot = new Hsc3.Proxy.ProxyMotion(CommMot);
            proVm = new Hsc3.Comm.ProxyVm(CommMot);
            proIO = new Hsc3.Proxy.ProxyIO(CommMot);
            proSys = new Hsc3.Proxy.ProxySys(CommMot);

            Name = name;

            jntData = new List<double>() { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
            locData = new List<double>() { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
            dins = new bool[32];
            regs = new double[32];

            this.IP = ip;
            this.CommPort = port;

            ConnectConfig.updateList.Add(this);
        }

        //获取机械臂笛卡尔坐标位置
        public List<double> GetLocData()
        {
            ulong success = this.proMot.getLocData(0, ref locData);
            //执行成功
            if (success == 0)
                return locData;
            else
            {
                Console.WriteLine("读取关节坐标失败");
                return new List<double>() { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
            }

        }
        //获取输入数组中存的IO状态
        public bool GetDin(int index)
        {
            if (index >= 32)
                return false ; 
            return dins[index];
        }

        //获取输出数组中存的IO状态
        public bool GetDout(int index)
        {
            if (index >= 32)
                return false;
            return douts[index];
        }


        public double GetR(int index)
        {
            if (index > 32)
                return 0;
            return regs[index];
            
           
        }


        //获取当前轴坐标位置
        public List<double> GetJntData()
        {
            ulong success = this.proMot.getJntData(0, ref jntData);
            //执行成功
            if (success == 0)
                return jntData;
            else
            {
                Console.WriteLine("读取关节坐标失败");
                return new List<double>() { 0.0, 0.0, 0.0, 0.0 ,0.0,0.0};
            }

        }



        public void Pause()
        {
            this.onPause?.Invoke();
        }


        public void ReFreshState()
        {
            dins = RobotConnect.GetDinGrp(this);
            regs = RobotConnect.GetRGrp(this);
            douts = RobotConnect.GetDoutGrp(this);
        }



    }
}
