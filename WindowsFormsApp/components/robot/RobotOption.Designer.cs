﻿
namespace WindowsFormsApp.components.robot
{
    partial class RobotOption
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cbx_RobotSelect = new CCWin.SkinControl.SkinComboBox();
            this.lab_RobotChoose = new CCWin.SkinControl.SkinLabel();
            this.skinLabel1 = new CCWin.SkinControl.SkinLabel();
            this.skinComboBox1 = new CCWin.SkinControl.SkinComboBox();
            this.btn_J1UP = new CCWin.SkinControl.SkinButton();
            this.skinPanel1 = new CCWin.SkinControl.SkinPanel();
            this.skinTextBox7 = new CCWin.SkinControl.SkinTextBox();
            this.skinTrackBar1 = new CCWin.SkinControl.SkinTrackBar();
            this.skinLabel16 = new CCWin.SkinControl.SkinLabel();
            this.skinPanel3 = new CCWin.SkinControl.SkinPanel();
            this.skinLabel3 = new CCWin.SkinControl.SkinLabel();
            this.ucTextBoxEx1 = new HZH_Controls.Controls.UCTextBoxEx();
            this.skinLabel2 = new CCWin.SkinControl.SkinLabel();
            this.skinComboBox2 = new CCWin.SkinControl.SkinComboBox();
            this.pan_J = new CCWin.SkinControl.SkinPanel();
            this.btn_J6DOWN = new CCWin.SkinControl.SkinButton();
            this.btn_J6UP = new CCWin.SkinControl.SkinButton();
            this.btn_J5DOWN = new CCWin.SkinControl.SkinButton();
            this.btn_J5UP = new CCWin.SkinControl.SkinButton();
            this.btn_J4DOWN = new CCWin.SkinControl.SkinButton();
            this.btn_J3DOWN = new CCWin.SkinControl.SkinButton();
            this.btn_J2DOWN = new CCWin.SkinControl.SkinButton();
            this.btn_J1DOWN = new CCWin.SkinControl.SkinButton();
            this.btn_J4UP = new CCWin.SkinControl.SkinButton();
            this.btn_J3UP = new CCWin.SkinControl.SkinButton();
            this.btn_J2UP = new CCWin.SkinControl.SkinButton();
            this.pan_D = new CCWin.SkinControl.SkinPanel();
            this.btn_CDOWN = new CCWin.SkinControl.SkinButton();
            this.btn_CUP = new CCWin.SkinControl.SkinButton();
            this.btn_BDOWN = new CCWin.SkinControl.SkinButton();
            this.btn_BUP = new CCWin.SkinControl.SkinButton();
            this.btn_ADOWN = new CCWin.SkinControl.SkinButton();
            this.btn_AUP = new CCWin.SkinControl.SkinButton();
            this.btn_ZDOWN = new CCWin.SkinControl.SkinButton();
            this.btn_XUP = new CCWin.SkinControl.SkinButton();
            this.btn_ZUP = new CCWin.SkinControl.SkinButton();
            this.btn_YUP = new CCWin.SkinControl.SkinButton();
            this.btn_YDOWN = new CCWin.SkinControl.SkinButton();
            this.btn_XDOWN = new CCWin.SkinControl.SkinButton();
            this.skinGroupBox1 = new CCWin.SkinControl.SkinGroupBox();
            this.skinPanel2 = new CCWin.SkinControl.SkinPanel();
            this.skinLabel14 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel15 = new CCWin.SkinControl.SkinLabel();
            this.tet_J6value = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel12 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel10 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel13 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel8 = new CCWin.SkinControl.SkinLabel();
            this.tet_J5value = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel11 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel6 = new CCWin.SkinControl.SkinLabel();
            this.tet_J4value = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel9 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel7 = new CCWin.SkinControl.SkinLabel();
            this.tet_J3value = new CCWin.SkinControl.SkinTextBox();
            this.tet_J2value = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel5 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel4 = new CCWin.SkinControl.SkinLabel();
            this.tet_J1value = new CCWin.SkinControl.SkinTextBox();
            this.skinPanel4 = new CCWin.SkinControl.SkinPanel();
            this.skinLabel19 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel21 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel22 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel23 = new CCWin.SkinControl.SkinLabel();
            this.tet_Avalue = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel24 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel25 = new CCWin.SkinControl.SkinLabel();
            this.tet_Zvalue = new CCWin.SkinControl.SkinTextBox();
            this.tet_Yvalue = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel26 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel27 = new CCWin.SkinControl.SkinLabel();
            this.tet_Xvalue = new CCWin.SkinControl.SkinTextBox();
            this.skinPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.skinTrackBar1)).BeginInit();
            this.skinPanel3.SuspendLayout();
            this.pan_J.SuspendLayout();
            this.pan_D.SuspendLayout();
            this.skinGroupBox1.SuspendLayout();
            this.skinPanel2.SuspendLayout();
            this.skinPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbx_RobotSelect
            // 
            this.cbx_RobotSelect.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.cbx_RobotSelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbx_RobotSelect.FormattingEnabled = true;
            this.cbx_RobotSelect.ItemBorderColor = System.Drawing.Color.Transparent;
            this.cbx_RobotSelect.ItemHoverForeColor = System.Drawing.Color.Black;
            this.cbx_RobotSelect.Items.AddRange(new object[] {
            "上下料机械臂",
            "上料机械臂",
            "下料机械臂"});
            this.cbx_RobotSelect.Location = new System.Drawing.Point(119, 10);
            this.cbx_RobotSelect.MouseColor = System.Drawing.Color.Transparent;
            this.cbx_RobotSelect.MouseGradientColor = System.Drawing.Color.Transparent;
            this.cbx_RobotSelect.Name = "cbx_RobotSelect";
            this.cbx_RobotSelect.Size = new System.Drawing.Size(149, 22);
            this.cbx_RobotSelect.TabIndex = 0;
            this.cbx_RobotSelect.WaterText = "";
            this.cbx_RobotSelect.SelectedIndexChanged += new System.EventHandler(this.cbx_RobotSelect_SelectedIndexChanged);
            // 
            // lab_RobotChoose
            // 
            this.lab_RobotChoose.AutoSize = true;
            this.lab_RobotChoose.BackColor = System.Drawing.Color.Transparent;
            this.lab_RobotChoose.BorderColor = System.Drawing.Color.White;
            this.lab_RobotChoose.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_RobotChoose.Location = new System.Drawing.Point(3, 9);
            this.lab_RobotChoose.Name = "lab_RobotChoose";
            this.lab_RobotChoose.Size = new System.Drawing.Size(84, 20);
            this.lab_RobotChoose.TabIndex = 1;
            this.lab_RobotChoose.Text = "机械臂对象";
            // 
            // skinLabel1
            // 
            this.skinLabel1.AutoSize = true;
            this.skinLabel1.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel1.BorderColor = System.Drawing.Color.White;
            this.skinLabel1.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel1.Location = new System.Drawing.Point(3, 43);
            this.skinLabel1.Name = "skinLabel1";
            this.skinLabel1.Size = new System.Drawing.Size(69, 20);
            this.skinLabel1.TabIndex = 2;
            this.skinLabel1.Text = "运动类型";
            // 
            // skinComboBox1
            // 
            this.skinComboBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.skinComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.skinComboBox1.FormattingEnabled = true;
            this.skinComboBox1.ItemBorderColor = System.Drawing.Color.Transparent;
            this.skinComboBox1.ItemHoverForeColor = System.Drawing.Color.Black;
            this.skinComboBox1.Items.AddRange(new object[] {
            "增量模式",
            "持续模式"});
            this.skinComboBox1.Location = new System.Drawing.Point(119, 44);
            this.skinComboBox1.MouseColor = System.Drawing.Color.Transparent;
            this.skinComboBox1.MouseGradientColor = System.Drawing.Color.Transparent;
            this.skinComboBox1.Name = "skinComboBox1";
            this.skinComboBox1.Size = new System.Drawing.Size(149, 22);
            this.skinComboBox1.TabIndex = 3;
            this.skinComboBox1.WaterText = "";
            this.skinComboBox1.SelectedIndexChanged += new System.EventHandler(this.skinComboBox1_SelectedIndexChanged);
            // 
            // btn_J1UP
            // 
            this.btn_J1UP.BackColor = System.Drawing.Color.Transparent;
            this.btn_J1UP.BaseColor = System.Drawing.Color.DodgerBlue;
            this.btn_J1UP.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_J1UP.DownBack = null;
            this.btn_J1UP.Location = new System.Drawing.Point(27, 18);
            this.btn_J1UP.MouseBack = null;
            this.btn_J1UP.Name = "btn_J1UP";
            this.btn_J1UP.NormlBack = null;
            this.btn_J1UP.Size = new System.Drawing.Size(74, 45);
            this.btn_J1UP.TabIndex = 4;
            this.btn_J1UP.Text = "J1+";
            this.btn_J1UP.UseVisualStyleBackColor = false;
            this.btn_J1UP.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_J1UP_MouseDown);
            this.btn_J1UP.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_J1UP_MouseUp);
            // 
            // skinPanel1
            // 
            this.skinPanel1.BackColor = System.Drawing.Color.Transparent;
            this.skinPanel1.Controls.Add(this.skinTextBox7);
            this.skinPanel1.Controls.Add(this.skinTrackBar1);
            this.skinPanel1.Controls.Add(this.skinLabel16);
            this.skinPanel1.Controls.Add(this.skinPanel3);
            this.skinPanel1.Controls.Add(this.skinLabel2);
            this.skinPanel1.Controls.Add(this.skinComboBox2);
            this.skinPanel1.Controls.Add(this.lab_RobotChoose);
            this.skinPanel1.Controls.Add(this.cbx_RobotSelect);
            this.skinPanel1.Controls.Add(this.skinComboBox1);
            this.skinPanel1.Controls.Add(this.skinLabel1);
            this.skinPanel1.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel1.DownBack = null;
            this.skinPanel1.Location = new System.Drawing.Point(28, 3);
            this.skinPanel1.MouseBack = null;
            this.skinPanel1.Name = "skinPanel1";
            this.skinPanel1.NormlBack = null;
            this.skinPanel1.Size = new System.Drawing.Size(602, 164);
            this.skinPanel1.TabIndex = 5;
            // 
            // skinTextBox7
            // 
            this.skinTextBox7.BackColor = System.Drawing.Color.Transparent;
            this.skinTextBox7.DownBack = null;
            this.skinTextBox7.Icon = null;
            this.skinTextBox7.IconIsButton = false;
            this.skinTextBox7.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.skinTextBox7.IsPasswordChat = '\0';
            this.skinTextBox7.IsSystemPasswordChar = false;
            this.skinTextBox7.Lines = new string[0];
            this.skinTextBox7.Location = new System.Drawing.Point(274, 108);
            this.skinTextBox7.Margin = new System.Windows.Forms.Padding(0);
            this.skinTextBox7.MaxLength = 32767;
            this.skinTextBox7.MinimumSize = new System.Drawing.Size(28, 28);
            this.skinTextBox7.MouseBack = null;
            this.skinTextBox7.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.skinTextBox7.Multiline = true;
            this.skinTextBox7.Name = "skinTextBox7";
            this.skinTextBox7.NormlBack = null;
            this.skinTextBox7.Padding = new System.Windows.Forms.Padding(5);
            this.skinTextBox7.ReadOnly = false;
            this.skinTextBox7.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.skinTextBox7.Size = new System.Drawing.Size(49, 28);
            // 
            // 
            // 
            this.skinTextBox7.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.skinTextBox7.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTextBox7.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.skinTextBox7.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.skinTextBox7.SkinTxt.Margin = new System.Windows.Forms.Padding(4);
            this.skinTextBox7.SkinTxt.Multiline = true;
            this.skinTextBox7.SkinTxt.Name = "BaseText";
            this.skinTextBox7.SkinTxt.Size = new System.Drawing.Size(39, 18);
            this.skinTextBox7.SkinTxt.TabIndex = 0;
            this.skinTextBox7.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.skinTextBox7.SkinTxt.WaterText = "";
            this.skinTextBox7.TabIndex = 2;
            this.skinTextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.skinTextBox7.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.skinTextBox7.WaterText = "";
            this.skinTextBox7.WordWrap = true;
            // 
            // skinTrackBar1
            // 
            this.skinTrackBar1.BackColor = System.Drawing.Color.Transparent;
            this.skinTrackBar1.Bar = null;
            this.skinTrackBar1.BarStyle = CCWin.SkinControl.HSLTrackBarStyle.Opacity;
            this.skinTrackBar1.Location = new System.Drawing.Point(119, 105);
            this.skinTrackBar1.Name = "skinTrackBar1";
            this.skinTrackBar1.Size = new System.Drawing.Size(139, 45);
            this.skinTrackBar1.TabIndex = 16;
            this.skinTrackBar1.Track = null;
            this.skinTrackBar1.ValueChanged += new System.EventHandler(this.skinTrackBar1_ValueChanged);
            // 
            // skinLabel16
            // 
            this.skinLabel16.AutoSize = true;
            this.skinLabel16.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel16.BorderColor = System.Drawing.Color.White;
            this.skinLabel16.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel16.Location = new System.Drawing.Point(3, 120);
            this.skinLabel16.Name = "skinLabel16";
            this.skinLabel16.Size = new System.Drawing.Size(69, 20);
            this.skinLabel16.TabIndex = 11;
            this.skinLabel16.Text = "运动速度";
            // 
            // skinPanel3
            // 
            this.skinPanel3.BackColor = System.Drawing.Color.Transparent;
            this.skinPanel3.Controls.Add(this.skinLabel3);
            this.skinPanel3.Controls.Add(this.ucTextBoxEx1);
            this.skinPanel3.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel3.DownBack = null;
            this.skinPanel3.Location = new System.Drawing.Point(274, 22);
            this.skinPanel3.MouseBack = null;
            this.skinPanel3.Name = "skinPanel3";
            this.skinPanel3.NormlBack = null;
            this.skinPanel3.Size = new System.Drawing.Size(251, 70);
            this.skinPanel3.TabIndex = 10;
            // 
            // skinLabel3
            // 
            this.skinLabel3.AutoSize = true;
            this.skinLabel3.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel3.BorderColor = System.Drawing.Color.White;
            this.skinLabel3.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel3.Location = new System.Drawing.Point(3, 21);
            this.skinLabel3.Name = "skinLabel3";
            this.skinLabel3.Size = new System.Drawing.Size(69, 20);
            this.skinLabel3.TabIndex = 6;
            this.skinLabel3.Text = "寸动距离";
            // 
            // ucTextBoxEx1
            // 
            this.ucTextBoxEx1.BackColor = System.Drawing.Color.Transparent;
            this.ucTextBoxEx1.ConerRadius = 5;
            this.ucTextBoxEx1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ucTextBoxEx1.DecLength = 2;
            this.ucTextBoxEx1.FillColor = System.Drawing.Color.Empty;
            this.ucTextBoxEx1.FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(77)))), ((int)(((byte)(59)))));
            this.ucTextBoxEx1.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.ucTextBoxEx1.InputText = "";
            this.ucTextBoxEx1.InputType = HZH_Controls.TextInputType.NotControl;
            this.ucTextBoxEx1.IsFocusColor = true;
            this.ucTextBoxEx1.IsRadius = true;
            this.ucTextBoxEx1.IsShowClearBtn = true;
            this.ucTextBoxEx1.IsShowKeyboard = false;
            this.ucTextBoxEx1.IsShowRect = true;
            this.ucTextBoxEx1.IsShowSearchBtn = false;
            this.ucTextBoxEx1.KeyBoardType = HZH_Controls.Controls.KeyBoardType.UCKeyBorderAll_EN;
            this.ucTextBoxEx1.Location = new System.Drawing.Point(102, 18);
            this.ucTextBoxEx1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ucTextBoxEx1.MaxValue = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.ucTextBoxEx1.MinValue = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.ucTextBoxEx1.Name = "ucTextBoxEx1";
            this.ucTextBoxEx1.Padding = new System.Windows.Forms.Padding(5);
            this.ucTextBoxEx1.PasswordChar = '\0';
            this.ucTextBoxEx1.PromptColor = System.Drawing.Color.Gray;
            this.ucTextBoxEx1.PromptFont = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.ucTextBoxEx1.PromptText = "";
            this.ucTextBoxEx1.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.ucTextBoxEx1.RectWidth = 1;
            this.ucTextBoxEx1.RegexPattern = "";
            this.ucTextBoxEx1.Size = new System.Drawing.Size(128, 42);
            this.ucTextBoxEx1.TabIndex = 0;
            // 
            // skinLabel2
            // 
            this.skinLabel2.AutoSize = true;
            this.skinLabel2.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel2.BorderColor = System.Drawing.Color.White;
            this.skinLabel2.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel2.Location = new System.Drawing.Point(3, 76);
            this.skinLabel2.Name = "skinLabel2";
            this.skinLabel2.Size = new System.Drawing.Size(69, 20);
            this.skinLabel2.TabIndex = 5;
            this.skinLabel2.Text = "运动模式";
            // 
            // skinComboBox2
            // 
            this.skinComboBox2.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.skinComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.skinComboBox2.FormattingEnabled = true;
            this.skinComboBox2.ItemBorderColor = System.Drawing.Color.Transparent;
            this.skinComboBox2.ItemHoverForeColor = System.Drawing.Color.Black;
            this.skinComboBox2.Items.AddRange(new object[] {
            "笛卡尔",
            "关节"});
            this.skinComboBox2.Location = new System.Drawing.Point(119, 76);
            this.skinComboBox2.MouseColor = System.Drawing.Color.Transparent;
            this.skinComboBox2.MouseGradientColor = System.Drawing.Color.Transparent;
            this.skinComboBox2.Name = "skinComboBox2";
            this.skinComboBox2.Size = new System.Drawing.Size(149, 22);
            this.skinComboBox2.TabIndex = 4;
            this.skinComboBox2.WaterText = "";
            this.skinComboBox2.SelectedIndexChanged += new System.EventHandler(this.skinComboBox2_SelectedIndexChanged);
            // 
            // pan_J
            // 
            this.pan_J.BackColor = System.Drawing.Color.Transparent;
            this.pan_J.Controls.Add(this.btn_J6DOWN);
            this.pan_J.Controls.Add(this.btn_J6UP);
            this.pan_J.Controls.Add(this.btn_J5DOWN);
            this.pan_J.Controls.Add(this.btn_J5UP);
            this.pan_J.Controls.Add(this.btn_J4DOWN);
            this.pan_J.Controls.Add(this.btn_J3DOWN);
            this.pan_J.Controls.Add(this.btn_J2DOWN);
            this.pan_J.Controls.Add(this.btn_J1DOWN);
            this.pan_J.Controls.Add(this.btn_J4UP);
            this.pan_J.Controls.Add(this.btn_J3UP);
            this.pan_J.Controls.Add(this.btn_J2UP);
            this.pan_J.Controls.Add(this.btn_J1UP);
            this.pan_J.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.pan_J.DownBack = null;
            this.pan_J.Location = new System.Drawing.Point(50, 20);
            this.pan_J.MouseBack = null;
            this.pan_J.Name = "pan_J";
            this.pan_J.NormlBack = null;
            this.pan_J.Size = new System.Drawing.Size(429, 207);
            this.pan_J.TabIndex = 6;
            // 
            // btn_J6DOWN
            // 
            this.btn_J6DOWN.BackColor = System.Drawing.Color.Transparent;
            this.btn_J6DOWN.BaseColor = System.Drawing.Color.DodgerBlue;
            this.btn_J6DOWN.BorderColor = System.Drawing.Color.DodgerBlue;
            this.btn_J6DOWN.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_J6DOWN.DownBack = null;
            this.btn_J6DOWN.Location = new System.Drawing.Point(330, 146);
            this.btn_J6DOWN.MouseBack = null;
            this.btn_J6DOWN.Name = "btn_J6DOWN";
            this.btn_J6DOWN.NormlBack = null;
            this.btn_J6DOWN.Size = new System.Drawing.Size(71, 45);
            this.btn_J6DOWN.TabIndex = 15;
            this.btn_J6DOWN.Text = "J6-";
            this.btn_J6DOWN.UseVisualStyleBackColor = false;
            this.btn_J6DOWN.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_J6DOWN_MouseDown);
            this.btn_J6DOWN.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_J6DOWN_MouseUp);
            // 
            // btn_J6UP
            // 
            this.btn_J6UP.BackColor = System.Drawing.Color.Transparent;
            this.btn_J6UP.BaseColor = System.Drawing.Color.DodgerBlue;
            this.btn_J6UP.BorderColor = System.Drawing.Color.DodgerBlue;
            this.btn_J6UP.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_J6UP.DownBack = null;
            this.btn_J6UP.Location = new System.Drawing.Point(232, 146);
            this.btn_J6UP.MouseBack = null;
            this.btn_J6UP.Name = "btn_J6UP";
            this.btn_J6UP.NormlBack = null;
            this.btn_J6UP.Size = new System.Drawing.Size(67, 45);
            this.btn_J6UP.TabIndex = 14;
            this.btn_J6UP.Text = "J6+";
            this.btn_J6UP.UseVisualStyleBackColor = false;
            this.btn_J6UP.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_J6UP_MouseDown);
            this.btn_J6UP.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_J6UP_MouseUp);
            // 
            // btn_J5DOWN
            // 
            this.btn_J5DOWN.BackColor = System.Drawing.Color.Transparent;
            this.btn_J5DOWN.BaseColor = System.Drawing.Color.DodgerBlue;
            this.btn_J5DOWN.BorderColor = System.Drawing.Color.DodgerBlue;
            this.btn_J5DOWN.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_J5DOWN.DownBack = null;
            this.btn_J5DOWN.Location = new System.Drawing.Point(132, 146);
            this.btn_J5DOWN.MouseBack = null;
            this.btn_J5DOWN.Name = "btn_J5DOWN";
            this.btn_J5DOWN.NormlBack = null;
            this.btn_J5DOWN.Size = new System.Drawing.Size(69, 45);
            this.btn_J5DOWN.TabIndex = 13;
            this.btn_J5DOWN.Text = "J5-";
            this.btn_J5DOWN.UseVisualStyleBackColor = false;
            this.btn_J5DOWN.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_J5DOWN_MouseDown);
            this.btn_J5DOWN.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_J5DOWN_MouseUp);
            // 
            // btn_J5UP
            // 
            this.btn_J5UP.BackColor = System.Drawing.Color.Transparent;
            this.btn_J5UP.BaseColor = System.Drawing.Color.DodgerBlue;
            this.btn_J5UP.BorderColor = System.Drawing.Color.DodgerBlue;
            this.btn_J5UP.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_J5UP.DownBack = null;
            this.btn_J5UP.Location = new System.Drawing.Point(27, 146);
            this.btn_J5UP.MouseBack = null;
            this.btn_J5UP.Name = "btn_J5UP";
            this.btn_J5UP.NormlBack = null;
            this.btn_J5UP.Size = new System.Drawing.Size(74, 45);
            this.btn_J5UP.TabIndex = 12;
            this.btn_J5UP.Text = "J5+";
            this.btn_J5UP.UseVisualStyleBackColor = false;
            this.btn_J5UP.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_J5UP_MouseDown);
            this.btn_J5UP.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_J5UP_MouseUp);
            // 
            // btn_J4DOWN
            // 
            this.btn_J4DOWN.BackColor = System.Drawing.Color.Transparent;
            this.btn_J4DOWN.BaseColor = System.Drawing.Color.DodgerBlue;
            this.btn_J4DOWN.BorderColor = System.Drawing.Color.DodgerBlue;
            this.btn_J4DOWN.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_J4DOWN.DownBack = null;
            this.btn_J4DOWN.Location = new System.Drawing.Point(330, 77);
            this.btn_J4DOWN.MouseBack = null;
            this.btn_J4DOWN.Name = "btn_J4DOWN";
            this.btn_J4DOWN.NormlBack = null;
            this.btn_J4DOWN.Size = new System.Drawing.Size(71, 48);
            this.btn_J4DOWN.TabIndex = 11;
            this.btn_J4DOWN.Text = "J4-";
            this.btn_J4DOWN.UseVisualStyleBackColor = false;
            this.btn_J4DOWN.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_J4DOWN_MouseDown);
            this.btn_J4DOWN.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_J4DOWN_MouseUp);
            // 
            // btn_J3DOWN
            // 
            this.btn_J3DOWN.BackColor = System.Drawing.Color.Transparent;
            this.btn_J3DOWN.BaseColor = System.Drawing.Color.DodgerBlue;
            this.btn_J3DOWN.BorderColor = System.Drawing.Color.DodgerBlue;
            this.btn_J3DOWN.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_J3DOWN.DownBack = null;
            this.btn_J3DOWN.Location = new System.Drawing.Point(132, 77);
            this.btn_J3DOWN.MouseBack = null;
            this.btn_J3DOWN.Name = "btn_J3DOWN";
            this.btn_J3DOWN.NormlBack = null;
            this.btn_J3DOWN.Size = new System.Drawing.Size(69, 48);
            this.btn_J3DOWN.TabIndex = 10;
            this.btn_J3DOWN.Text = "J3-";
            this.btn_J3DOWN.UseVisualStyleBackColor = false;
            this.btn_J3DOWN.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_J3DOWN_MouseDown);
            this.btn_J3DOWN.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_J3DOWN_MouseUp);
            // 
            // btn_J2DOWN
            // 
            this.btn_J2DOWN.BackColor = System.Drawing.Color.Transparent;
            this.btn_J2DOWN.BaseColor = System.Drawing.Color.DodgerBlue;
            this.btn_J2DOWN.BorderColor = System.Drawing.Color.DodgerBlue;
            this.btn_J2DOWN.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_J2DOWN.DownBack = null;
            this.btn_J2DOWN.Location = new System.Drawing.Point(330, 18);
            this.btn_J2DOWN.MouseBack = null;
            this.btn_J2DOWN.Name = "btn_J2DOWN";
            this.btn_J2DOWN.NormlBack = null;
            this.btn_J2DOWN.Size = new System.Drawing.Size(71, 45);
            this.btn_J2DOWN.TabIndex = 9;
            this.btn_J2DOWN.Text = "J2-";
            this.btn_J2DOWN.UseVisualStyleBackColor = false;
            this.btn_J2DOWN.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_J2DOWN_MouseDown);
            this.btn_J2DOWN.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_J2DOWN_MouseUp);
            // 
            // btn_J1DOWN
            // 
            this.btn_J1DOWN.BackColor = System.Drawing.Color.Transparent;
            this.btn_J1DOWN.BaseColor = System.Drawing.Color.DodgerBlue;
            this.btn_J1DOWN.BorderColor = System.Drawing.Color.DodgerBlue;
            this.btn_J1DOWN.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_J1DOWN.DownBack = null;
            this.btn_J1DOWN.Location = new System.Drawing.Point(132, 17);
            this.btn_J1DOWN.MouseBack = null;
            this.btn_J1DOWN.Name = "btn_J1DOWN";
            this.btn_J1DOWN.NormlBack = null;
            this.btn_J1DOWN.Size = new System.Drawing.Size(69, 45);
            this.btn_J1DOWN.TabIndex = 8;
            this.btn_J1DOWN.Text = "J1-";
            this.btn_J1DOWN.UseVisualStyleBackColor = false;
            this.btn_J1DOWN.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_J1DOWN_MouseDown);
            this.btn_J1DOWN.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_J1DOWN_MouseUp);
            // 
            // btn_J4UP
            // 
            this.btn_J4UP.BackColor = System.Drawing.Color.Transparent;
            this.btn_J4UP.BaseColor = System.Drawing.Color.DodgerBlue;
            this.btn_J4UP.BorderColor = System.Drawing.Color.DodgerBlue;
            this.btn_J4UP.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_J4UP.DownBack = null;
            this.btn_J4UP.Location = new System.Drawing.Point(232, 77);
            this.btn_J4UP.MouseBack = null;
            this.btn_J4UP.Name = "btn_J4UP";
            this.btn_J4UP.NormlBack = null;
            this.btn_J4UP.Size = new System.Drawing.Size(67, 48);
            this.btn_J4UP.TabIndex = 7;
            this.btn_J4UP.Text = "J4+";
            this.btn_J4UP.UseVisualStyleBackColor = false;
            this.btn_J4UP.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_J4UP_MouseDown);
            this.btn_J4UP.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_J4UP_MouseUp);
            // 
            // btn_J3UP
            // 
            this.btn_J3UP.BackColor = System.Drawing.Color.Transparent;
            this.btn_J3UP.BaseColor = System.Drawing.Color.DodgerBlue;
            this.btn_J3UP.BorderColor = System.Drawing.Color.DodgerBlue;
            this.btn_J3UP.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_J3UP.DownBack = null;
            this.btn_J3UP.Location = new System.Drawing.Point(27, 77);
            this.btn_J3UP.MouseBack = null;
            this.btn_J3UP.Name = "btn_J3UP";
            this.btn_J3UP.NormlBack = null;
            this.btn_J3UP.Size = new System.Drawing.Size(74, 48);
            this.btn_J3UP.TabIndex = 6;
            this.btn_J3UP.Text = "J3+";
            this.btn_J3UP.UseVisualStyleBackColor = false;
            this.btn_J3UP.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_J3UP_MouseDown);
            this.btn_J3UP.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_J3UP_MouseUp);
            // 
            // btn_J2UP
            // 
            this.btn_J2UP.BackColor = System.Drawing.Color.Transparent;
            this.btn_J2UP.BaseColor = System.Drawing.Color.DodgerBlue;
            this.btn_J2UP.BorderColor = System.Drawing.Color.DodgerBlue;
            this.btn_J2UP.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_J2UP.DownBack = null;
            this.btn_J2UP.Location = new System.Drawing.Point(232, 18);
            this.btn_J2UP.MouseBack = null;
            this.btn_J2UP.Name = "btn_J2UP";
            this.btn_J2UP.NormlBack = null;
            this.btn_J2UP.Size = new System.Drawing.Size(67, 45);
            this.btn_J2UP.TabIndex = 5;
            this.btn_J2UP.Text = "J2+";
            this.btn_J2UP.UseVisualStyleBackColor = false;
            this.btn_J2UP.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_J2UP_MouseDown);
            this.btn_J2UP.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_J2UP_MouseUp);
            // 
            // pan_D
            // 
            this.pan_D.BackColor = System.Drawing.Color.Transparent;
            this.pan_D.Controls.Add(this.btn_CDOWN);
            this.pan_D.Controls.Add(this.btn_CUP);
            this.pan_D.Controls.Add(this.btn_BDOWN);
            this.pan_D.Controls.Add(this.btn_BUP);
            this.pan_D.Controls.Add(this.btn_ADOWN);
            this.pan_D.Controls.Add(this.btn_AUP);
            this.pan_D.Controls.Add(this.btn_ZDOWN);
            this.pan_D.Controls.Add(this.btn_XUP);
            this.pan_D.Controls.Add(this.btn_ZUP);
            this.pan_D.Controls.Add(this.btn_YUP);
            this.pan_D.Controls.Add(this.btn_YDOWN);
            this.pan_D.Controls.Add(this.btn_XDOWN);
            this.pan_D.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.pan_D.DownBack = null;
            this.pan_D.Location = new System.Drawing.Point(32, 33);
            this.pan_D.MouseBack = null;
            this.pan_D.Name = "pan_D";
            this.pan_D.NormlBack = null;
            this.pan_D.Size = new System.Drawing.Size(420, 204);
            this.pan_D.TabIndex = 7;
            // 
            // btn_CDOWN
            // 
            this.btn_CDOWN.BackColor = System.Drawing.Color.Transparent;
            this.btn_CDOWN.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_CDOWN.DownBack = null;
            this.btn_CDOWN.Location = new System.Drawing.Point(301, 150);
            this.btn_CDOWN.MouseBack = null;
            this.btn_CDOWN.Name = "btn_CDOWN";
            this.btn_CDOWN.NormlBack = null;
            this.btn_CDOWN.Size = new System.Drawing.Size(70, 44);
            this.btn_CDOWN.TabIndex = 16;
            this.btn_CDOWN.Text = "C-";
            this.btn_CDOWN.UseVisualStyleBackColor = false;
            this.btn_CDOWN.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_CDOWN_MouseDown);
            this.btn_CDOWN.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_CDOWN_MouseUp);
            // 
            // btn_CUP
            // 
            this.btn_CUP.BackColor = System.Drawing.Color.Transparent;
            this.btn_CUP.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_CUP.DownBack = null;
            this.btn_CUP.Location = new System.Drawing.Point(209, 150);
            this.btn_CUP.MouseBack = null;
            this.btn_CUP.Name = "btn_CUP";
            this.btn_CUP.NormlBack = null;
            this.btn_CUP.Size = new System.Drawing.Size(70, 44);
            this.btn_CUP.TabIndex = 15;
            this.btn_CUP.Text = "C+";
            this.btn_CUP.UseVisualStyleBackColor = false;
            this.btn_CUP.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_CUP_MouseDown);
            this.btn_CUP.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_CUP_MouseUp);
            // 
            // btn_BDOWN
            // 
            this.btn_BDOWN.BackColor = System.Drawing.Color.Transparent;
            this.btn_BDOWN.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_BDOWN.DownBack = null;
            this.btn_BDOWN.Location = new System.Drawing.Point(112, 150);
            this.btn_BDOWN.MouseBack = null;
            this.btn_BDOWN.Name = "btn_BDOWN";
            this.btn_BDOWN.NormlBack = null;
            this.btn_BDOWN.Size = new System.Drawing.Size(65, 44);
            this.btn_BDOWN.TabIndex = 14;
            this.btn_BDOWN.Text = "B-";
            this.btn_BDOWN.UseVisualStyleBackColor = false;
            this.btn_BDOWN.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_BDOWN_MouseDown);
            this.btn_BDOWN.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_BDOWN_MouseUp);
            // 
            // btn_BUP
            // 
            this.btn_BUP.BackColor = System.Drawing.Color.Transparent;
            this.btn_BUP.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_BUP.DownBack = null;
            this.btn_BUP.Location = new System.Drawing.Point(18, 150);
            this.btn_BUP.MouseBack = null;
            this.btn_BUP.Name = "btn_BUP";
            this.btn_BUP.NormlBack = null;
            this.btn_BUP.Size = new System.Drawing.Size(70, 44);
            this.btn_BUP.TabIndex = 13;
            this.btn_BUP.Text = "B+";
            this.btn_BUP.UseVisualStyleBackColor = false;
            this.btn_BUP.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_BUP_MouseDown);
            this.btn_BUP.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_BUP_MouseUp);
            // 
            // btn_ADOWN
            // 
            this.btn_ADOWN.BackColor = System.Drawing.Color.Transparent;
            this.btn_ADOWN.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_ADOWN.DownBack = null;
            this.btn_ADOWN.Location = new System.Drawing.Point(301, 93);
            this.btn_ADOWN.MouseBack = null;
            this.btn_ADOWN.Name = "btn_ADOWN";
            this.btn_ADOWN.NormlBack = null;
            this.btn_ADOWN.Size = new System.Drawing.Size(70, 44);
            this.btn_ADOWN.TabIndex = 12;
            this.btn_ADOWN.Text = "A-";
            this.btn_ADOWN.UseVisualStyleBackColor = false;
            this.btn_ADOWN.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_ADOWN_MouseDown);
            this.btn_ADOWN.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_ADOWN_MouseUp);
            // 
            // btn_AUP
            // 
            this.btn_AUP.BackColor = System.Drawing.Color.Transparent;
            this.btn_AUP.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_AUP.DownBack = null;
            this.btn_AUP.Location = new System.Drawing.Point(209, 93);
            this.btn_AUP.MouseBack = null;
            this.btn_AUP.Name = "btn_AUP";
            this.btn_AUP.NormlBack = null;
            this.btn_AUP.Size = new System.Drawing.Size(70, 44);
            this.btn_AUP.TabIndex = 11;
            this.btn_AUP.Text = "A+";
            this.btn_AUP.UseVisualStyleBackColor = false;
            this.btn_AUP.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_AUP_MouseDown);
            this.btn_AUP.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_AUP_MouseUp);
            // 
            // btn_ZDOWN
            // 
            this.btn_ZDOWN.BackColor = System.Drawing.Color.Transparent;
            this.btn_ZDOWN.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_ZDOWN.DownBack = null;
            this.btn_ZDOWN.Location = new System.Drawing.Point(112, 93);
            this.btn_ZDOWN.MouseBack = null;
            this.btn_ZDOWN.Name = "btn_ZDOWN";
            this.btn_ZDOWN.NormlBack = null;
            this.btn_ZDOWN.Size = new System.Drawing.Size(65, 44);
            this.btn_ZDOWN.TabIndex = 10;
            this.btn_ZDOWN.Text = "Z-";
            this.btn_ZDOWN.UseVisualStyleBackColor = false;
            this.btn_ZDOWN.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_ZDOWN_MouseDown);
            this.btn_ZDOWN.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_ZDOWN_MouseUp);
            // 
            // btn_XUP
            // 
            this.btn_XUP.BackColor = System.Drawing.Color.Transparent;
            this.btn_XUP.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_XUP.DownBack = null;
            this.btn_XUP.Location = new System.Drawing.Point(18, 22);
            this.btn_XUP.MouseBack = null;
            this.btn_XUP.Name = "btn_XUP";
            this.btn_XUP.NormlBack = null;
            this.btn_XUP.Size = new System.Drawing.Size(70, 45);
            this.btn_XUP.TabIndex = 5;
            this.btn_XUP.Text = "X+";
            this.btn_XUP.UseVisualStyleBackColor = false;
            this.btn_XUP.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_XUP_MouseDown);
            this.btn_XUP.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_XUP_MouseUp);
            // 
            // btn_ZUP
            // 
            this.btn_ZUP.BackColor = System.Drawing.Color.Transparent;
            this.btn_ZUP.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_ZUP.DownBack = null;
            this.btn_ZUP.Location = new System.Drawing.Point(18, 93);
            this.btn_ZUP.MouseBack = null;
            this.btn_ZUP.Name = "btn_ZUP";
            this.btn_ZUP.NormlBack = null;
            this.btn_ZUP.Size = new System.Drawing.Size(70, 44);
            this.btn_ZUP.TabIndex = 9;
            this.btn_ZUP.Text = "Z+";
            this.btn_ZUP.UseVisualStyleBackColor = false;
            this.btn_ZUP.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_ZUP_MouseDown);
            this.btn_ZUP.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_ZUP_MouseUp);
            // 
            // btn_YUP
            // 
            this.btn_YUP.BackColor = System.Drawing.Color.Transparent;
            this.btn_YUP.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_YUP.DownBack = null;
            this.btn_YUP.Location = new System.Drawing.Point(209, 22);
            this.btn_YUP.MouseBack = null;
            this.btn_YUP.Name = "btn_YUP";
            this.btn_YUP.NormlBack = null;
            this.btn_YUP.Size = new System.Drawing.Size(67, 45);
            this.btn_YUP.TabIndex = 8;
            this.btn_YUP.Text = "Y-";
            this.btn_YUP.UseVisualStyleBackColor = false;
            this.btn_YUP.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_YUP_MouseDown);
            this.btn_YUP.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_YUP_MouseUp);
            // 
            // btn_YDOWN
            // 
            this.btn_YDOWN.BackColor = System.Drawing.Color.Transparent;
            this.btn_YDOWN.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_YDOWN.DownBack = null;
            this.btn_YDOWN.Location = new System.Drawing.Point(301, 22);
            this.btn_YDOWN.MouseBack = null;
            this.btn_YDOWN.Name = "btn_YDOWN";
            this.btn_YDOWN.NormlBack = null;
            this.btn_YDOWN.Size = new System.Drawing.Size(70, 45);
            this.btn_YDOWN.TabIndex = 7;
            this.btn_YDOWN.Text = "Y+";
            this.btn_YDOWN.UseVisualStyleBackColor = false;
            this.btn_YDOWN.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_YDOWN_MouseDown);
            this.btn_YDOWN.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_YDOWN_MouseUp);
            // 
            // btn_XDOWN
            // 
            this.btn_XDOWN.BackColor = System.Drawing.Color.Transparent;
            this.btn_XDOWN.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_XDOWN.DownBack = null;
            this.btn_XDOWN.Location = new System.Drawing.Point(112, 22);
            this.btn_XDOWN.MouseBack = null;
            this.btn_XDOWN.Name = "btn_XDOWN";
            this.btn_XDOWN.NormlBack = null;
            this.btn_XDOWN.Size = new System.Drawing.Size(65, 45);
            this.btn_XDOWN.TabIndex = 6;
            this.btn_XDOWN.Text = "X-";
            this.btn_XDOWN.UseVisualStyleBackColor = false;
            this.btn_XDOWN.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_XDOWN_MouseDown);
            this.btn_XDOWN.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_XDOWN_MouseUp);
            // 
            // skinGroupBox1
            // 
            this.skinGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.skinGroupBox1.BorderColor = System.Drawing.Color.Red;
            this.skinGroupBox1.Controls.Add(this.pan_J);
            this.skinGroupBox1.Controls.Add(this.pan_D);
            this.skinGroupBox1.ForeColor = System.Drawing.Color.Blue;
            this.skinGroupBox1.Location = new System.Drawing.Point(310, 193);
            this.skinGroupBox1.Name = "skinGroupBox1";
            this.skinGroupBox1.RectBackColor = System.Drawing.Color.White;
            this.skinGroupBox1.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinGroupBox1.Size = new System.Drawing.Size(510, 313);
            this.skinGroupBox1.TabIndex = 8;
            this.skinGroupBox1.TabStop = false;
            this.skinGroupBox1.Text = "操作按钮";
            this.skinGroupBox1.TitleBorderColor = System.Drawing.Color.Red;
            this.skinGroupBox1.TitleRectBackColor = System.Drawing.Color.White;
            this.skinGroupBox1.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // skinPanel2
            // 
            this.skinPanel2.BackColor = System.Drawing.Color.Transparent;
            this.skinPanel2.Controls.Add(this.skinLabel14);
            this.skinPanel2.Controls.Add(this.skinLabel15);
            this.skinPanel2.Controls.Add(this.tet_J6value);
            this.skinPanel2.Controls.Add(this.skinLabel12);
            this.skinPanel2.Controls.Add(this.skinLabel10);
            this.skinPanel2.Controls.Add(this.skinLabel13);
            this.skinPanel2.Controls.Add(this.skinLabel8);
            this.skinPanel2.Controls.Add(this.tet_J5value);
            this.skinPanel2.Controls.Add(this.skinLabel11);
            this.skinPanel2.Controls.Add(this.skinLabel6);
            this.skinPanel2.Controls.Add(this.tet_J4value);
            this.skinPanel2.Controls.Add(this.skinLabel9);
            this.skinPanel2.Controls.Add(this.skinLabel7);
            this.skinPanel2.Controls.Add(this.tet_J3value);
            this.skinPanel2.Controls.Add(this.tet_J2value);
            this.skinPanel2.Controls.Add(this.skinLabel5);
            this.skinPanel2.Controls.Add(this.skinLabel4);
            this.skinPanel2.Controls.Add(this.tet_J1value);
            this.skinPanel2.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel2.DownBack = null;
            this.skinPanel2.Location = new System.Drawing.Point(28, 173);
            this.skinPanel2.MouseBack = null;
            this.skinPanel2.Name = "skinPanel2";
            this.skinPanel2.NormlBack = null;
            this.skinPanel2.Size = new System.Drawing.Size(258, 346);
            this.skinPanel2.TabIndex = 9;
            // 
            // skinLabel14
            // 
            this.skinLabel14.AutoSize = true;
            this.skinLabel14.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel14.BorderColor = System.Drawing.Color.White;
            this.skinLabel14.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel14.Location = new System.Drawing.Point(201, 296);
            this.skinLabel14.Name = "skinLabel14";
            this.skinLabel14.Size = new System.Drawing.Size(13, 17);
            this.skinLabel14.TabIndex = 14;
            this.skinLabel14.Text = "°";
            // 
            // skinLabel15
            // 
            this.skinLabel15.AutoSize = true;
            this.skinLabel15.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel15.BorderColor = System.Drawing.Color.White;
            this.skinLabel15.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel15.Location = new System.Drawing.Point(29, 305);
            this.skinLabel15.Name = "skinLabel15";
            this.skinLabel15.Size = new System.Drawing.Size(20, 17);
            this.skinLabel15.TabIndex = 13;
            this.skinLabel15.Text = "J6";
            // 
            // tet_J6value
            // 
            this.tet_J6value.BackColor = System.Drawing.Color.Transparent;
            this.tet_J6value.DownBack = null;
            this.tet_J6value.Icon = null;
            this.tet_J6value.IconIsButton = false;
            this.tet_J6value.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tet_J6value.IsPasswordChat = '\0';
            this.tet_J6value.IsSystemPasswordChar = false;
            this.tet_J6value.Lines = new string[0];
            this.tet_J6value.Location = new System.Drawing.Point(77, 296);
            this.tet_J6value.Margin = new System.Windows.Forms.Padding(0);
            this.tet_J6value.MaxLength = 32767;
            this.tet_J6value.MinimumSize = new System.Drawing.Size(28, 28);
            this.tet_J6value.MouseBack = null;
            this.tet_J6value.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tet_J6value.Multiline = true;
            this.tet_J6value.Name = "tet_J6value";
            this.tet_J6value.NormlBack = null;
            this.tet_J6value.Padding = new System.Windows.Forms.Padding(5);
            this.tet_J6value.ReadOnly = true;
            this.tet_J6value.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tet_J6value.Size = new System.Drawing.Size(121, 35);
            // 
            // 
            // 
            this.tet_J6value.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tet_J6value.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tet_J6value.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tet_J6value.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tet_J6value.SkinTxt.Margin = new System.Windows.Forms.Padding(4);
            this.tet_J6value.SkinTxt.Multiline = true;
            this.tet_J6value.SkinTxt.Name = "BaseText";
            this.tet_J6value.SkinTxt.ReadOnly = true;
            this.tet_J6value.SkinTxt.Size = new System.Drawing.Size(111, 25);
            this.tet_J6value.SkinTxt.TabIndex = 0;
            this.tet_J6value.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tet_J6value.SkinTxt.WaterText = "";
            this.tet_J6value.TabIndex = 12;
            this.tet_J6value.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tet_J6value.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tet_J6value.WaterText = "";
            this.tet_J6value.WordWrap = true;
            // 
            // skinLabel12
            // 
            this.skinLabel12.AutoSize = true;
            this.skinLabel12.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel12.BorderColor = System.Drawing.Color.White;
            this.skinLabel12.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel12.Location = new System.Drawing.Point(201, 243);
            this.skinLabel12.Name = "skinLabel12";
            this.skinLabel12.Size = new System.Drawing.Size(13, 17);
            this.skinLabel12.TabIndex = 11;
            this.skinLabel12.Text = "°";
            // 
            // skinLabel10
            // 
            this.skinLabel10.AutoSize = true;
            this.skinLabel10.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel10.BorderColor = System.Drawing.Color.White;
            this.skinLabel10.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel10.Location = new System.Drawing.Point(201, 194);
            this.skinLabel10.Name = "skinLabel10";
            this.skinLabel10.Size = new System.Drawing.Size(13, 17);
            this.skinLabel10.TabIndex = 11;
            this.skinLabel10.Text = "°";
            // 
            // skinLabel13
            // 
            this.skinLabel13.AutoSize = true;
            this.skinLabel13.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel13.BorderColor = System.Drawing.Color.White;
            this.skinLabel13.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel13.Location = new System.Drawing.Point(29, 252);
            this.skinLabel13.Name = "skinLabel13";
            this.skinLabel13.Size = new System.Drawing.Size(20, 17);
            this.skinLabel13.TabIndex = 10;
            this.skinLabel13.Text = "J5";
            // 
            // skinLabel8
            // 
            this.skinLabel8.AutoSize = true;
            this.skinLabel8.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel8.BorderColor = System.Drawing.Color.White;
            this.skinLabel8.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel8.Location = new System.Drawing.Point(201, 148);
            this.skinLabel8.Name = "skinLabel8";
            this.skinLabel8.Size = new System.Drawing.Size(13, 17);
            this.skinLabel8.TabIndex = 11;
            this.skinLabel8.Text = "°";
            // 
            // tet_J5value
            // 
            this.tet_J5value.BackColor = System.Drawing.Color.Transparent;
            this.tet_J5value.DownBack = null;
            this.tet_J5value.Icon = null;
            this.tet_J5value.IconIsButton = false;
            this.tet_J5value.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tet_J5value.IsPasswordChat = '\0';
            this.tet_J5value.IsSystemPasswordChar = false;
            this.tet_J5value.Lines = new string[0];
            this.tet_J5value.Location = new System.Drawing.Point(77, 243);
            this.tet_J5value.Margin = new System.Windows.Forms.Padding(0);
            this.tet_J5value.MaxLength = 32767;
            this.tet_J5value.MinimumSize = new System.Drawing.Size(28, 28);
            this.tet_J5value.MouseBack = null;
            this.tet_J5value.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tet_J5value.Multiline = true;
            this.tet_J5value.Name = "tet_J5value";
            this.tet_J5value.NormlBack = null;
            this.tet_J5value.Padding = new System.Windows.Forms.Padding(5);
            this.tet_J5value.ReadOnly = true;
            this.tet_J5value.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tet_J5value.Size = new System.Drawing.Size(121, 35);
            // 
            // 
            // 
            this.tet_J5value.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tet_J5value.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tet_J5value.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tet_J5value.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tet_J5value.SkinTxt.Margin = new System.Windows.Forms.Padding(4);
            this.tet_J5value.SkinTxt.Multiline = true;
            this.tet_J5value.SkinTxt.Name = "BaseText";
            this.tet_J5value.SkinTxt.ReadOnly = true;
            this.tet_J5value.SkinTxt.Size = new System.Drawing.Size(111, 25);
            this.tet_J5value.SkinTxt.TabIndex = 0;
            this.tet_J5value.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tet_J5value.SkinTxt.WaterText = "";
            this.tet_J5value.TabIndex = 9;
            this.tet_J5value.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tet_J5value.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tet_J5value.WaterText = "";
            this.tet_J5value.WordWrap = true;
            // 
            // skinLabel11
            // 
            this.skinLabel11.AutoSize = true;
            this.skinLabel11.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel11.BorderColor = System.Drawing.Color.White;
            this.skinLabel11.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel11.Location = new System.Drawing.Point(29, 203);
            this.skinLabel11.Name = "skinLabel11";
            this.skinLabel11.Size = new System.Drawing.Size(20, 17);
            this.skinLabel11.TabIndex = 10;
            this.skinLabel11.Text = "J4";
            // 
            // skinLabel6
            // 
            this.skinLabel6.AutoSize = true;
            this.skinLabel6.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel6.BorderColor = System.Drawing.Color.White;
            this.skinLabel6.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel6.Location = new System.Drawing.Point(201, 92);
            this.skinLabel6.Name = "skinLabel6";
            this.skinLabel6.Size = new System.Drawing.Size(13, 17);
            this.skinLabel6.TabIndex = 11;
            this.skinLabel6.Text = "°";
            // 
            // tet_J4value
            // 
            this.tet_J4value.BackColor = System.Drawing.Color.Transparent;
            this.tet_J4value.DownBack = null;
            this.tet_J4value.Icon = null;
            this.tet_J4value.IconIsButton = false;
            this.tet_J4value.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tet_J4value.IsPasswordChat = '\0';
            this.tet_J4value.IsSystemPasswordChar = false;
            this.tet_J4value.Lines = new string[0];
            this.tet_J4value.Location = new System.Drawing.Point(77, 194);
            this.tet_J4value.Margin = new System.Windows.Forms.Padding(0);
            this.tet_J4value.MaxLength = 32767;
            this.tet_J4value.MinimumSize = new System.Drawing.Size(28, 28);
            this.tet_J4value.MouseBack = null;
            this.tet_J4value.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tet_J4value.Multiline = true;
            this.tet_J4value.Name = "tet_J4value";
            this.tet_J4value.NormlBack = null;
            this.tet_J4value.Padding = new System.Windows.Forms.Padding(5);
            this.tet_J4value.ReadOnly = true;
            this.tet_J4value.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tet_J4value.Size = new System.Drawing.Size(121, 35);
            // 
            // 
            // 
            this.tet_J4value.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tet_J4value.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tet_J4value.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tet_J4value.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tet_J4value.SkinTxt.Margin = new System.Windows.Forms.Padding(4);
            this.tet_J4value.SkinTxt.Multiline = true;
            this.tet_J4value.SkinTxt.Name = "BaseText";
            this.tet_J4value.SkinTxt.ReadOnly = true;
            this.tet_J4value.SkinTxt.Size = new System.Drawing.Size(111, 25);
            this.tet_J4value.SkinTxt.TabIndex = 0;
            this.tet_J4value.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tet_J4value.SkinTxt.WaterText = "";
            this.tet_J4value.TabIndex = 9;
            this.tet_J4value.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tet_J4value.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tet_J4value.WaterText = "";
            this.tet_J4value.WordWrap = true;
            // 
            // skinLabel9
            // 
            this.skinLabel9.AutoSize = true;
            this.skinLabel9.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel9.BorderColor = System.Drawing.Color.White;
            this.skinLabel9.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel9.Location = new System.Drawing.Point(29, 157);
            this.skinLabel9.Name = "skinLabel9";
            this.skinLabel9.Size = new System.Drawing.Size(20, 17);
            this.skinLabel9.TabIndex = 10;
            this.skinLabel9.Text = "J3";
            // 
            // skinLabel7
            // 
            this.skinLabel7.AutoSize = true;
            this.skinLabel7.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel7.BorderColor = System.Drawing.Color.White;
            this.skinLabel7.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel7.Location = new System.Drawing.Point(29, 101);
            this.skinLabel7.Name = "skinLabel7";
            this.skinLabel7.Size = new System.Drawing.Size(20, 17);
            this.skinLabel7.TabIndex = 10;
            this.skinLabel7.Text = "J2";
            // 
            // tet_J3value
            // 
            this.tet_J3value.BackColor = System.Drawing.Color.Transparent;
            this.tet_J3value.DownBack = null;
            this.tet_J3value.Icon = null;
            this.tet_J3value.IconIsButton = false;
            this.tet_J3value.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tet_J3value.IsPasswordChat = '\0';
            this.tet_J3value.IsSystemPasswordChar = false;
            this.tet_J3value.Lines = new string[0];
            this.tet_J3value.Location = new System.Drawing.Point(77, 148);
            this.tet_J3value.Margin = new System.Windows.Forms.Padding(0);
            this.tet_J3value.MaxLength = 32767;
            this.tet_J3value.MinimumSize = new System.Drawing.Size(28, 28);
            this.tet_J3value.MouseBack = null;
            this.tet_J3value.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tet_J3value.Multiline = true;
            this.tet_J3value.Name = "tet_J3value";
            this.tet_J3value.NormlBack = null;
            this.tet_J3value.Padding = new System.Windows.Forms.Padding(5);
            this.tet_J3value.ReadOnly = true;
            this.tet_J3value.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tet_J3value.Size = new System.Drawing.Size(121, 35);
            // 
            // 
            // 
            this.tet_J3value.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tet_J3value.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tet_J3value.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tet_J3value.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tet_J3value.SkinTxt.Margin = new System.Windows.Forms.Padding(4);
            this.tet_J3value.SkinTxt.Multiline = true;
            this.tet_J3value.SkinTxt.Name = "BaseText";
            this.tet_J3value.SkinTxt.ReadOnly = true;
            this.tet_J3value.SkinTxt.Size = new System.Drawing.Size(111, 25);
            this.tet_J3value.SkinTxt.TabIndex = 0;
            this.tet_J3value.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tet_J3value.SkinTxt.WaterText = "";
            this.tet_J3value.TabIndex = 9;
            this.tet_J3value.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tet_J3value.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tet_J3value.WaterText = "";
            this.tet_J3value.WordWrap = true;
            // 
            // tet_J2value
            // 
            this.tet_J2value.BackColor = System.Drawing.Color.Transparent;
            this.tet_J2value.DownBack = null;
            this.tet_J2value.Icon = null;
            this.tet_J2value.IconIsButton = false;
            this.tet_J2value.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tet_J2value.IsPasswordChat = '\0';
            this.tet_J2value.IsSystemPasswordChar = false;
            this.tet_J2value.Lines = new string[0];
            this.tet_J2value.Location = new System.Drawing.Point(77, 92);
            this.tet_J2value.Margin = new System.Windows.Forms.Padding(0);
            this.tet_J2value.MaxLength = 32767;
            this.tet_J2value.MinimumSize = new System.Drawing.Size(28, 28);
            this.tet_J2value.MouseBack = null;
            this.tet_J2value.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tet_J2value.Multiline = true;
            this.tet_J2value.Name = "tet_J2value";
            this.tet_J2value.NormlBack = null;
            this.tet_J2value.Padding = new System.Windows.Forms.Padding(5);
            this.tet_J2value.ReadOnly = true;
            this.tet_J2value.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tet_J2value.Size = new System.Drawing.Size(121, 35);
            // 
            // 
            // 
            this.tet_J2value.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tet_J2value.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tet_J2value.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tet_J2value.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tet_J2value.SkinTxt.Margin = new System.Windows.Forms.Padding(4);
            this.tet_J2value.SkinTxt.Multiline = true;
            this.tet_J2value.SkinTxt.Name = "BaseText";
            this.tet_J2value.SkinTxt.ReadOnly = true;
            this.tet_J2value.SkinTxt.Size = new System.Drawing.Size(111, 25);
            this.tet_J2value.SkinTxt.TabIndex = 0;
            this.tet_J2value.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tet_J2value.SkinTxt.WaterText = "";
            this.tet_J2value.TabIndex = 9;
            this.tet_J2value.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tet_J2value.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tet_J2value.WaterText = "";
            this.tet_J2value.WordWrap = true;
            // 
            // skinLabel5
            // 
            this.skinLabel5.AutoSize = true;
            this.skinLabel5.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel5.BorderColor = System.Drawing.Color.White;
            this.skinLabel5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel5.Location = new System.Drawing.Point(201, 38);
            this.skinLabel5.Name = "skinLabel5";
            this.skinLabel5.Size = new System.Drawing.Size(13, 17);
            this.skinLabel5.TabIndex = 8;
            this.skinLabel5.Text = "°";
            // 
            // skinLabel4
            // 
            this.skinLabel4.AutoSize = true;
            this.skinLabel4.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel4.BorderColor = System.Drawing.Color.White;
            this.skinLabel4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel4.Location = new System.Drawing.Point(29, 47);
            this.skinLabel4.Name = "skinLabel4";
            this.skinLabel4.Size = new System.Drawing.Size(20, 17);
            this.skinLabel4.TabIndex = 7;
            this.skinLabel4.Text = "J1";
            // 
            // tet_J1value
            // 
            this.tet_J1value.BackColor = System.Drawing.Color.Transparent;
            this.tet_J1value.DownBack = null;
            this.tet_J1value.Icon = null;
            this.tet_J1value.IconIsButton = false;
            this.tet_J1value.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tet_J1value.IsPasswordChat = '\0';
            this.tet_J1value.IsSystemPasswordChar = false;
            this.tet_J1value.Lines = new string[0];
            this.tet_J1value.Location = new System.Drawing.Point(77, 38);
            this.tet_J1value.Margin = new System.Windows.Forms.Padding(0);
            this.tet_J1value.MaxLength = 32767;
            this.tet_J1value.MinimumSize = new System.Drawing.Size(28, 28);
            this.tet_J1value.MouseBack = null;
            this.tet_J1value.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tet_J1value.Multiline = true;
            this.tet_J1value.Name = "tet_J1value";
            this.tet_J1value.NormlBack = null;
            this.tet_J1value.Padding = new System.Windows.Forms.Padding(5);
            this.tet_J1value.ReadOnly = true;
            this.tet_J1value.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tet_J1value.Size = new System.Drawing.Size(121, 35);
            // 
            // 
            // 
            this.tet_J1value.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tet_J1value.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tet_J1value.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tet_J1value.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tet_J1value.SkinTxt.Margin = new System.Windows.Forms.Padding(4);
            this.tet_J1value.SkinTxt.Multiline = true;
            this.tet_J1value.SkinTxt.Name = "BaseText";
            this.tet_J1value.SkinTxt.ReadOnly = true;
            this.tet_J1value.SkinTxt.Size = new System.Drawing.Size(111, 25);
            this.tet_J1value.SkinTxt.TabIndex = 0;
            this.tet_J1value.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tet_J1value.SkinTxt.WaterText = "";
            this.tet_J1value.TabIndex = 1;
            this.tet_J1value.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tet_J1value.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tet_J1value.WaterText = "";
            this.tet_J1value.WordWrap = true;
            // 
            // skinPanel4
            // 
            this.skinPanel4.BackColor = System.Drawing.Color.Transparent;
            this.skinPanel4.Controls.Add(this.skinLabel19);
            this.skinPanel4.Controls.Add(this.skinLabel21);
            this.skinPanel4.Controls.Add(this.skinLabel22);
            this.skinPanel4.Controls.Add(this.skinLabel23);
            this.skinPanel4.Controls.Add(this.tet_Avalue);
            this.skinPanel4.Controls.Add(this.skinLabel24);
            this.skinPanel4.Controls.Add(this.skinLabel25);
            this.skinPanel4.Controls.Add(this.tet_Zvalue);
            this.skinPanel4.Controls.Add(this.tet_Yvalue);
            this.skinPanel4.Controls.Add(this.skinLabel26);
            this.skinPanel4.Controls.Add(this.skinLabel27);
            this.skinPanel4.Controls.Add(this.tet_Xvalue);
            this.skinPanel4.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel4.DownBack = null;
            this.skinPanel4.Location = new System.Drawing.Point(40, 170);
            this.skinPanel4.MouseBack = null;
            this.skinPanel4.Name = "skinPanel4";
            this.skinPanel4.NormlBack = null;
            this.skinPanel4.Size = new System.Drawing.Size(246, 336);
            this.skinPanel4.TabIndex = 15;
            // 
            // skinLabel19
            // 
            this.skinLabel19.AutoSize = true;
            this.skinLabel19.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel19.BorderColor = System.Drawing.Color.White;
            this.skinLabel19.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel19.Location = new System.Drawing.Point(201, 201);
            this.skinLabel19.Name = "skinLabel19";
            this.skinLabel19.Size = new System.Drawing.Size(30, 17);
            this.skinLabel19.TabIndex = 11;
            this.skinLabel19.Text = "mm";
            // 
            // skinLabel21
            // 
            this.skinLabel21.AutoSize = true;
            this.skinLabel21.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel21.BorderColor = System.Drawing.Color.White;
            this.skinLabel21.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel21.Location = new System.Drawing.Point(201, 155);
            this.skinLabel21.Name = "skinLabel21";
            this.skinLabel21.Size = new System.Drawing.Size(30, 17);
            this.skinLabel21.TabIndex = 11;
            this.skinLabel21.Text = "mm";
            // 
            // skinLabel22
            // 
            this.skinLabel22.AutoSize = true;
            this.skinLabel22.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel22.BorderColor = System.Drawing.Color.White;
            this.skinLabel22.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel22.Location = new System.Drawing.Point(29, 203);
            this.skinLabel22.Name = "skinLabel22";
            this.skinLabel22.Size = new System.Drawing.Size(16, 17);
            this.skinLabel22.TabIndex = 10;
            this.skinLabel22.Text = "A";
            // 
            // skinLabel23
            // 
            this.skinLabel23.AutoSize = true;
            this.skinLabel23.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel23.BorderColor = System.Drawing.Color.White;
            this.skinLabel23.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel23.Location = new System.Drawing.Point(201, 99);
            this.skinLabel23.Name = "skinLabel23";
            this.skinLabel23.Size = new System.Drawing.Size(30, 17);
            this.skinLabel23.TabIndex = 11;
            this.skinLabel23.Text = "mm";
            // 
            // tet_Avalue
            // 
            this.tet_Avalue.BackColor = System.Drawing.Color.Transparent;
            this.tet_Avalue.DownBack = null;
            this.tet_Avalue.Icon = null;
            this.tet_Avalue.IconIsButton = false;
            this.tet_Avalue.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tet_Avalue.IsPasswordChat = '\0';
            this.tet_Avalue.IsSystemPasswordChar = false;
            this.tet_Avalue.Lines = new string[0];
            this.tet_Avalue.Location = new System.Drawing.Point(77, 195);
            this.tet_Avalue.Margin = new System.Windows.Forms.Padding(0);
            this.tet_Avalue.MaxLength = 32767;
            this.tet_Avalue.MinimumSize = new System.Drawing.Size(28, 28);
            this.tet_Avalue.MouseBack = null;
            this.tet_Avalue.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tet_Avalue.Multiline = true;
            this.tet_Avalue.Name = "tet_Avalue";
            this.tet_Avalue.NormlBack = null;
            this.tet_Avalue.Padding = new System.Windows.Forms.Padding(5);
            this.tet_Avalue.ReadOnly = true;
            this.tet_Avalue.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tet_Avalue.Size = new System.Drawing.Size(121, 35);
            // 
            // 
            // 
            this.tet_Avalue.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tet_Avalue.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tet_Avalue.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tet_Avalue.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tet_Avalue.SkinTxt.Margin = new System.Windows.Forms.Padding(4);
            this.tet_Avalue.SkinTxt.Multiline = true;
            this.tet_Avalue.SkinTxt.Name = "BaseText";
            this.tet_Avalue.SkinTxt.ReadOnly = true;
            this.tet_Avalue.SkinTxt.Size = new System.Drawing.Size(111, 25);
            this.tet_Avalue.SkinTxt.TabIndex = 0;
            this.tet_Avalue.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tet_Avalue.SkinTxt.WaterText = "";
            this.tet_Avalue.TabIndex = 9;
            this.tet_Avalue.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tet_Avalue.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tet_Avalue.WaterText = "";
            this.tet_Avalue.WordWrap = true;
            // 
            // skinLabel24
            // 
            this.skinLabel24.AutoSize = true;
            this.skinLabel24.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel24.BorderColor = System.Drawing.Color.White;
            this.skinLabel24.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel24.Location = new System.Drawing.Point(29, 157);
            this.skinLabel24.Name = "skinLabel24";
            this.skinLabel24.Size = new System.Drawing.Size(15, 17);
            this.skinLabel24.TabIndex = 10;
            this.skinLabel24.Text = "Z";
            // 
            // skinLabel25
            // 
            this.skinLabel25.AutoSize = true;
            this.skinLabel25.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel25.BorderColor = System.Drawing.Color.White;
            this.skinLabel25.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel25.Location = new System.Drawing.Point(29, 101);
            this.skinLabel25.Name = "skinLabel25";
            this.skinLabel25.Size = new System.Drawing.Size(15, 17);
            this.skinLabel25.TabIndex = 10;
            this.skinLabel25.Text = "Y";
            // 
            // tet_Zvalue
            // 
            this.tet_Zvalue.BackColor = System.Drawing.Color.Transparent;
            this.tet_Zvalue.DownBack = null;
            this.tet_Zvalue.Icon = null;
            this.tet_Zvalue.IconIsButton = false;
            this.tet_Zvalue.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tet_Zvalue.IsPasswordChat = '\0';
            this.tet_Zvalue.IsSystemPasswordChar = false;
            this.tet_Zvalue.Lines = new string[0];
            this.tet_Zvalue.Location = new System.Drawing.Point(77, 148);
            this.tet_Zvalue.Margin = new System.Windows.Forms.Padding(0);
            this.tet_Zvalue.MaxLength = 32767;
            this.tet_Zvalue.MinimumSize = new System.Drawing.Size(28, 28);
            this.tet_Zvalue.MouseBack = null;
            this.tet_Zvalue.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tet_Zvalue.Multiline = true;
            this.tet_Zvalue.Name = "tet_Zvalue";
            this.tet_Zvalue.NormlBack = null;
            this.tet_Zvalue.Padding = new System.Windows.Forms.Padding(5);
            this.tet_Zvalue.ReadOnly = true;
            this.tet_Zvalue.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tet_Zvalue.Size = new System.Drawing.Size(121, 35);
            // 
            // 
            // 
            this.tet_Zvalue.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tet_Zvalue.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tet_Zvalue.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tet_Zvalue.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tet_Zvalue.SkinTxt.Margin = new System.Windows.Forms.Padding(4);
            this.tet_Zvalue.SkinTxt.Multiline = true;
            this.tet_Zvalue.SkinTxt.Name = "BaseText";
            this.tet_Zvalue.SkinTxt.ReadOnly = true;
            this.tet_Zvalue.SkinTxt.Size = new System.Drawing.Size(111, 25);
            this.tet_Zvalue.SkinTxt.TabIndex = 0;
            this.tet_Zvalue.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tet_Zvalue.SkinTxt.WaterText = "";
            this.tet_Zvalue.TabIndex = 9;
            this.tet_Zvalue.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tet_Zvalue.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tet_Zvalue.WaterText = "";
            this.tet_Zvalue.WordWrap = true;
            // 
            // tet_Yvalue
            // 
            this.tet_Yvalue.BackColor = System.Drawing.Color.Transparent;
            this.tet_Yvalue.DownBack = null;
            this.tet_Yvalue.Icon = null;
            this.tet_Yvalue.IconIsButton = false;
            this.tet_Yvalue.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tet_Yvalue.IsPasswordChat = '\0';
            this.tet_Yvalue.IsSystemPasswordChar = false;
            this.tet_Yvalue.Lines = new string[0];
            this.tet_Yvalue.Location = new System.Drawing.Point(77, 92);
            this.tet_Yvalue.Margin = new System.Windows.Forms.Padding(0);
            this.tet_Yvalue.MaxLength = 32767;
            this.tet_Yvalue.MinimumSize = new System.Drawing.Size(28, 28);
            this.tet_Yvalue.MouseBack = null;
            this.tet_Yvalue.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tet_Yvalue.Multiline = true;
            this.tet_Yvalue.Name = "tet_Yvalue";
            this.tet_Yvalue.NormlBack = null;
            this.tet_Yvalue.Padding = new System.Windows.Forms.Padding(5);
            this.tet_Yvalue.ReadOnly = true;
            this.tet_Yvalue.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tet_Yvalue.Size = new System.Drawing.Size(121, 35);
            // 
            // 
            // 
            this.tet_Yvalue.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tet_Yvalue.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tet_Yvalue.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tet_Yvalue.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tet_Yvalue.SkinTxt.Margin = new System.Windows.Forms.Padding(4);
            this.tet_Yvalue.SkinTxt.Multiline = true;
            this.tet_Yvalue.SkinTxt.Name = "BaseText";
            this.tet_Yvalue.SkinTxt.ReadOnly = true;
            this.tet_Yvalue.SkinTxt.Size = new System.Drawing.Size(111, 25);
            this.tet_Yvalue.SkinTxt.TabIndex = 0;
            this.tet_Yvalue.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tet_Yvalue.SkinTxt.WaterText = "";
            this.tet_Yvalue.TabIndex = 9;
            this.tet_Yvalue.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tet_Yvalue.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tet_Yvalue.WaterText = "";
            this.tet_Yvalue.WordWrap = true;
            // 
            // skinLabel26
            // 
            this.skinLabel26.AutoSize = true;
            this.skinLabel26.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel26.BorderColor = System.Drawing.Color.White;
            this.skinLabel26.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel26.Location = new System.Drawing.Point(201, 43);
            this.skinLabel26.Name = "skinLabel26";
            this.skinLabel26.Size = new System.Drawing.Size(30, 17);
            this.skinLabel26.TabIndex = 8;
            this.skinLabel26.Text = "mm";
            // 
            // skinLabel27
            // 
            this.skinLabel27.AutoSize = true;
            this.skinLabel27.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel27.BorderColor = System.Drawing.Color.White;
            this.skinLabel27.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel27.Location = new System.Drawing.Point(29, 47);
            this.skinLabel27.Name = "skinLabel27";
            this.skinLabel27.Size = new System.Drawing.Size(16, 17);
            this.skinLabel27.TabIndex = 7;
            this.skinLabel27.Text = "X";
            // 
            // tet_Xvalue
            // 
            this.tet_Xvalue.BackColor = System.Drawing.Color.Transparent;
            this.tet_Xvalue.DownBack = null;
            this.tet_Xvalue.Icon = null;
            this.tet_Xvalue.IconIsButton = false;
            this.tet_Xvalue.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tet_Xvalue.IsPasswordChat = '\0';
            this.tet_Xvalue.IsSystemPasswordChar = false;
            this.tet_Xvalue.Lines = new string[0];
            this.tet_Xvalue.Location = new System.Drawing.Point(77, 38);
            this.tet_Xvalue.Margin = new System.Windows.Forms.Padding(0);
            this.tet_Xvalue.MaxLength = 32767;
            this.tet_Xvalue.MinimumSize = new System.Drawing.Size(28, 28);
            this.tet_Xvalue.MouseBack = null;
            this.tet_Xvalue.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tet_Xvalue.Multiline = true;
            this.tet_Xvalue.Name = "tet_Xvalue";
            this.tet_Xvalue.NormlBack = null;
            this.tet_Xvalue.Padding = new System.Windows.Forms.Padding(5);
            this.tet_Xvalue.ReadOnly = true;
            this.tet_Xvalue.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tet_Xvalue.Size = new System.Drawing.Size(121, 35);
            // 
            // 
            // 
            this.tet_Xvalue.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tet_Xvalue.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tet_Xvalue.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tet_Xvalue.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tet_Xvalue.SkinTxt.Margin = new System.Windows.Forms.Padding(4);
            this.tet_Xvalue.SkinTxt.Multiline = true;
            this.tet_Xvalue.SkinTxt.Name = "BaseText";
            this.tet_Xvalue.SkinTxt.ReadOnly = true;
            this.tet_Xvalue.SkinTxt.Size = new System.Drawing.Size(111, 25);
            this.tet_Xvalue.SkinTxt.TabIndex = 0;
            this.tet_Xvalue.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tet_Xvalue.SkinTxt.WaterText = "";
            this.tet_Xvalue.TabIndex = 1;
            this.tet_Xvalue.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tet_Xvalue.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tet_Xvalue.WaterText = "";
            this.tet_Xvalue.WordWrap = true;
            // 
            // RobotOption
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.skinPanel4);
            this.Controls.Add(this.skinPanel2);
            this.Controls.Add(this.skinGroupBox1);
            this.Controls.Add(this.skinPanel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "RobotOption";
            this.Size = new System.Drawing.Size(836, 549);
            this.Load += new System.EventHandler(this.RobotOption_Load);
            this.skinPanel1.ResumeLayout(false);
            this.skinPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.skinTrackBar1)).EndInit();
            this.skinPanel3.ResumeLayout(false);
            this.skinPanel3.PerformLayout();
            this.pan_J.ResumeLayout(false);
            this.pan_D.ResumeLayout(false);
            this.skinGroupBox1.ResumeLayout(false);
            this.skinPanel2.ResumeLayout(false);
            this.skinPanel2.PerformLayout();
            this.skinPanel4.ResumeLayout(false);
            this.skinPanel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private CCWin.SkinControl.SkinComboBox cbx_RobotSelect;
        private CCWin.SkinControl.SkinLabel lab_RobotChoose;
        private CCWin.SkinControl.SkinLabel skinLabel1;
        private CCWin.SkinControl.SkinComboBox skinComboBox1;
        private CCWin.SkinControl.SkinButton btn_J1UP;
        private CCWin.SkinControl.SkinPanel skinPanel1;
        private CCWin.SkinControl.SkinComboBox skinComboBox2;
        private CCWin.SkinControl.SkinPanel pan_J;
        private CCWin.SkinControl.SkinButton btn_J6DOWN;
        private CCWin.SkinControl.SkinButton btn_J6UP;
        private CCWin.SkinControl.SkinButton btn_J5DOWN;
        private CCWin.SkinControl.SkinButton btn_J5UP;
        private CCWin.SkinControl.SkinButton btn_J4DOWN;
        private CCWin.SkinControl.SkinButton btn_J3DOWN;
        private CCWin.SkinControl.SkinButton btn_J2DOWN;
        private CCWin.SkinControl.SkinButton btn_J1DOWN;
        private CCWin.SkinControl.SkinButton btn_J4UP;
        private CCWin.SkinControl.SkinButton btn_J3UP;
        private CCWin.SkinControl.SkinButton btn_J2UP;
        private CCWin.SkinControl.SkinPanel pan_D;
        private CCWin.SkinControl.SkinButton btn_ZDOWN;
        private CCWin.SkinControl.SkinButton btn_ZUP;
        private CCWin.SkinControl.SkinButton btn_YUP;
        private CCWin.SkinControl.SkinButton btn_YDOWN;
        private CCWin.SkinControl.SkinButton btn_XDOWN;
        private CCWin.SkinControl.SkinButton btn_XUP;
        private CCWin.SkinControl.SkinLabel skinLabel2;
        private CCWin.SkinControl.SkinGroupBox skinGroupBox1;
        private CCWin.SkinControl.SkinButton btn_ADOWN;
        private CCWin.SkinControl.SkinButton btn_AUP;
        private CCWin.SkinControl.SkinLabel skinLabel3;
        private CCWin.SkinControl.SkinPanel skinPanel2;
        private HZH_Controls.Controls.UCTextBoxEx ucTextBoxEx1;
        private CCWin.SkinControl.SkinPanel skinPanel3;
        private CCWin.SkinControl.SkinLabel skinLabel14;
        private CCWin.SkinControl.SkinLabel skinLabel15;
        private CCWin.SkinControl.SkinTextBox tet_J6value;
        private CCWin.SkinControl.SkinLabel skinLabel12;
        private CCWin.SkinControl.SkinLabel skinLabel10;
        private CCWin.SkinControl.SkinLabel skinLabel13;
        private CCWin.SkinControl.SkinLabel skinLabel8;
        private CCWin.SkinControl.SkinTextBox tet_J5value;
        private CCWin.SkinControl.SkinLabel skinLabel11;
        private CCWin.SkinControl.SkinLabel skinLabel6;
        private CCWin.SkinControl.SkinTextBox tet_J4value;
        private CCWin.SkinControl.SkinLabel skinLabel9;
        private CCWin.SkinControl.SkinLabel skinLabel7;
        private CCWin.SkinControl.SkinTextBox tet_J3value;
        private CCWin.SkinControl.SkinTextBox tet_J2value;
        private CCWin.SkinControl.SkinLabel skinLabel5;
        private CCWin.SkinControl.SkinLabel skinLabel4;
        private CCWin.SkinControl.SkinTextBox tet_J1value;
        private CCWin.SkinControl.SkinPanel skinPanel4;
        private CCWin.SkinControl.SkinLabel skinLabel19;
        private CCWin.SkinControl.SkinLabel skinLabel21;
        private CCWin.SkinControl.SkinLabel skinLabel22;
        private CCWin.SkinControl.SkinLabel skinLabel23;
        private CCWin.SkinControl.SkinTextBox tet_Avalue;
        private CCWin.SkinControl.SkinLabel skinLabel24;
        private CCWin.SkinControl.SkinLabel skinLabel25;
        private CCWin.SkinControl.SkinTextBox tet_Zvalue;
        private CCWin.SkinControl.SkinTextBox tet_Yvalue;
        private CCWin.SkinControl.SkinLabel skinLabel26;
        private CCWin.SkinControl.SkinLabel skinLabel27;
        private CCWin.SkinControl.SkinTextBox tet_Xvalue;
        private CCWin.SkinControl.SkinLabel skinLabel16;
        private CCWin.SkinControl.SkinTrackBar skinTrackBar1;
        private CCWin.SkinControl.SkinTextBox skinTextBox7;
        private CCWin.SkinControl.SkinButton btn_CDOWN;
        private CCWin.SkinControl.SkinButton btn_CUP;
        private CCWin.SkinControl.SkinButton btn_BDOWN;
        private CCWin.SkinControl.SkinButton btn_BUP;
    }
}
