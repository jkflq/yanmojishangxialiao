﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp.deviceConnect;

namespace WindowsFormsApp.components.Motor
{
    public partial class MotorControl : UserControl,IAutoUpdatable
    {

        public Motor motor;
        public MotorControl()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);
            ConnectConfig.updateList.Add(this);
        }

        private void textbox_MotorPosition_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(e.KeyChar > '0' && e.KeyChar < '9' || e.KeyChar == 8))
                e.Handled = true;
        }
        public bool value = false;
        //电机使能
        private void Button_MotorEnable_Click(object sender, EventArgs e)
        {
            if (!motor.busTcpClient.ReadCoil(motor.EnableCoil))
            {
                value = true;
                motor.busTcpClient.Write(motor.EnableCoil, true);

            }
            else
            {
                value = false;
                motor.busTcpClient.Write(motor.EnableCoil, false);

            }
            Button_MotorEnable.BaseColor = value ? Color.Lime : Color.Red;
            Button_MotorEnable.Text = value ? "电机失能" : "电机使能";
        }
        //电机移动到原点
        private void Button_MoveOrigion_Click(object sender, EventArgs e)
        {
            motor.busTcpClient.Write(motor.Move_OriginCoil, true);
            Thread.Sleep(10);
            motor.busTcpClient.Write(motor.Move_OriginCoil, false);
        }
        //取消报警
        private void Button_OffWarning_Click(object sender, EventArgs e)
        {
            motor.busTcpClient.Write(motor.WarningCoil, false);
        }

        private void Button_Motor_WriteAcc_Click(object sender, EventArgs e)
        {
            motor.busTcpClient.Write(motor.AccReg, int.Parse(Text_Motor_Acc.InputText));
        }

        private void Button_Motor_WriteSpeed_Click(object sender, EventArgs e)
        {
            motor.busTcpClient.Write(motor.SpeedReg, int.Parse(Text_Motor_Speed.InputText));
        }

        private void Button_Motor_WriteAngle_Click(object sender, EventArgs e)
        {
            motor.busTcpClient.WriteDoubleWord(motor.Target_AngleReg, (int)(double.Parse(Text_Motor_Angle.InputText)));
            motor.busTcpClient.Write(motor.Run, true);
            Thread.Sleep(10);
            motor.busTcpClient.Write(motor.Run, false);
        }
        private string Transform_TO_String(int a)
        {
            double b = a;
            b = b / 100;
            return b.ToString();

        }
        public void ReFreshState()
        {
            Text_Current_Angle.Text = Transform_TO_String(motor.busTcpClient.ReadDoubleWord(motor.Current_PositionReg));

        }
        public void read()
        {
            Text_Motor_Acc.InputText = motor.busTcpClient.ReadDoubleWord(motor.AccReg).ToString();
            Text_Motor_Speed.InputText = motor.busTcpClient.ReadDoubleWord(motor.SpeedReg).ToString();
        }
    }
}
