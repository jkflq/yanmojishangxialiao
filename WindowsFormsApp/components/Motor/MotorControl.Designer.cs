﻿
namespace WindowsFormsApp.components.Motor
{
    partial class MotorControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Button_MotorEnable = new CCWin.SkinControl.SkinButton();
            this.Emotor_AlarmLamp = new HZH_Controls.Controls.UCAlarmLamp();
            this.Button_MoveOrigion = new CCWin.SkinControl.SkinButton();
            this.Lab_MotorAcc = new CCWin.SkinControl.SkinLabel();
            this.Lab_MotroSpeed = new CCWin.SkinControl.SkinLabel();
            this.skinLabel1 = new CCWin.SkinControl.SkinLabel();
            this.Button_Motor_WriteAcc = new CCWin.SkinControl.SkinButton();
            this.Button_Motor_WriteSpeed = new CCWin.SkinControl.SkinButton();
            this.Button_Motor_WriteAngle = new CCWin.SkinControl.SkinButton();
            this.Button_OffWarning = new CCWin.SkinControl.SkinButton();
            this.Text_Motor_Acc = new HZH_Controls.Controls.UCTextBoxEx();
            this.Text_Motor_Speed = new HZH_Controls.Controls.UCTextBoxEx();
            this.Text_Motor_Angle = new HZH_Controls.Controls.UCTextBoxEx();
            this.skinLabel2 = new CCWin.SkinControl.SkinLabel();
            this.Text_Current_Angle = new CCWin.SkinControl.SkinTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Button_MotorEnable
            // 
            this.Button_MotorEnable.BackColor = System.Drawing.Color.Transparent;
            this.Button_MotorEnable.BaseColor = System.Drawing.Color.Red;
            this.Button_MotorEnable.BorderColor = System.Drawing.Color.DodgerBlue;
            this.Button_MotorEnable.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.Button_MotorEnable.DownBack = null;
            this.Button_MotorEnable.Location = new System.Drawing.Point(29, 347);
            this.Button_MotorEnable.Margin = new System.Windows.Forms.Padding(4);
            this.Button_MotorEnable.MouseBack = null;
            this.Button_MotorEnable.Name = "Button_MotorEnable";
            this.Button_MotorEnable.NormlBack = null;
            this.Button_MotorEnable.Size = new System.Drawing.Size(124, 68);
            this.Button_MotorEnable.TabIndex = 0;
            this.Button_MotorEnable.Text = "电机伺服";
            this.Button_MotorEnable.UseVisualStyleBackColor = false;
            this.Button_MotorEnable.Click += new System.EventHandler(this.Button_MotorEnable_Click);
            // 
            // Emotor_AlarmLamp
            // 
            this.Emotor_AlarmLamp.LampColor = new System.Drawing.Color[] {
        System.Drawing.Color.Lime,
        System.Drawing.Color.Red};
            this.Emotor_AlarmLamp.Lampstand = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.Emotor_AlarmLamp.Location = new System.Drawing.Point(521, 245);
            this.Emotor_AlarmLamp.Name = "Emotor_AlarmLamp";
            this.Emotor_AlarmLamp.Size = new System.Drawing.Size(66, 67);
            this.Emotor_AlarmLamp.TabIndex = 3;
            this.Emotor_AlarmLamp.TwinkleSpeed = 0;
            // 
            // Button_MoveOrigion
            // 
            this.Button_MoveOrigion.BackColor = System.Drawing.Color.Transparent;
            this.Button_MoveOrigion.BaseColor = System.Drawing.Color.DodgerBlue;
            this.Button_MoveOrigion.BorderColor = System.Drawing.Color.DodgerBlue;
            this.Button_MoveOrigion.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.Button_MoveOrigion.DownBack = null;
            this.Button_MoveOrigion.Location = new System.Drawing.Point(188, 347);
            this.Button_MoveOrigion.Margin = new System.Windows.Forms.Padding(4);
            this.Button_MoveOrigion.MouseBack = null;
            this.Button_MoveOrigion.Name = "Button_MoveOrigion";
            this.Button_MoveOrigion.NormlBack = null;
            this.Button_MoveOrigion.Size = new System.Drawing.Size(124, 68);
            this.Button_MoveOrigion.TabIndex = 4;
            this.Button_MoveOrigion.Text = "回归原位";
            this.Button_MoveOrigion.UseVisualStyleBackColor = false;
            this.Button_MoveOrigion.Click += new System.EventHandler(this.Button_MoveOrigion_Click);
            // 
            // Lab_MotorAcc
            // 
            this.Lab_MotorAcc.AutoSize = true;
            this.Lab_MotorAcc.BackColor = System.Drawing.Color.Transparent;
            this.Lab_MotorAcc.BorderColor = System.Drawing.Color.White;
            this.Lab_MotorAcc.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Lab_MotorAcc.Location = new System.Drawing.Point(24, 27);
            this.Lab_MotorAcc.Name = "Lab_MotorAcc";
            this.Lab_MotorAcc.Size = new System.Drawing.Size(90, 21);
            this.Lab_MotorAcc.TabIndex = 9;
            this.Lab_MotorAcc.Text = "旋转加速度";
            // 
            // Lab_MotroSpeed
            // 
            this.Lab_MotroSpeed.AutoSize = true;
            this.Lab_MotroSpeed.BackColor = System.Drawing.Color.Transparent;
            this.Lab_MotroSpeed.BorderColor = System.Drawing.Color.White;
            this.Lab_MotroSpeed.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Lab_MotroSpeed.Location = new System.Drawing.Point(24, 101);
            this.Lab_MotroSpeed.Name = "Lab_MotroSpeed";
            this.Lab_MotroSpeed.Size = new System.Drawing.Size(74, 21);
            this.Lab_MotroSpeed.TabIndex = 10;
            this.Lab_MotroSpeed.Text = "旋转速度";
            // 
            // skinLabel1
            // 
            this.skinLabel1.AutoSize = true;
            this.skinLabel1.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel1.BorderColor = System.Drawing.Color.White;
            this.skinLabel1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel1.Location = new System.Drawing.Point(24, 176);
            this.skinLabel1.Name = "skinLabel1";
            this.skinLabel1.Size = new System.Drawing.Size(74, 21);
            this.skinLabel1.TabIndex = 12;
            this.skinLabel1.Text = "目标角度";
            // 
            // Button_Motor_WriteAcc
            // 
            this.Button_Motor_WriteAcc.BackColor = System.Drawing.Color.Transparent;
            this.Button_Motor_WriteAcc.BaseColor = System.Drawing.Color.DodgerBlue;
            this.Button_Motor_WriteAcc.BorderColor = System.Drawing.Color.DodgerBlue;
            this.Button_Motor_WriteAcc.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.Button_Motor_WriteAcc.DownBack = null;
            this.Button_Motor_WriteAcc.Location = new System.Drawing.Point(396, 20);
            this.Button_Motor_WriteAcc.Margin = new System.Windows.Forms.Padding(4);
            this.Button_Motor_WriteAcc.MouseBack = null;
            this.Button_Motor_WriteAcc.Name = "Button_Motor_WriteAcc";
            this.Button_Motor_WriteAcc.NormlBack = null;
            this.Button_Motor_WriteAcc.Size = new System.Drawing.Size(63, 50);
            this.Button_Motor_WriteAcc.TabIndex = 17;
            this.Button_Motor_WriteAcc.Text = "写入";
            this.Button_Motor_WriteAcc.UseVisualStyleBackColor = false;
            this.Button_Motor_WriteAcc.Click += new System.EventHandler(this.Button_Motor_WriteAcc_Click);
            // 
            // Button_Motor_WriteSpeed
            // 
            this.Button_Motor_WriteSpeed.BackColor = System.Drawing.Color.Transparent;
            this.Button_Motor_WriteSpeed.BaseColor = System.Drawing.Color.DodgerBlue;
            this.Button_Motor_WriteSpeed.BorderColor = System.Drawing.Color.DodgerBlue;
            this.Button_Motor_WriteSpeed.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.Button_Motor_WriteSpeed.DownBack = null;
            this.Button_Motor_WriteSpeed.Location = new System.Drawing.Point(396, 94);
            this.Button_Motor_WriteSpeed.Margin = new System.Windows.Forms.Padding(4);
            this.Button_Motor_WriteSpeed.MouseBack = null;
            this.Button_Motor_WriteSpeed.Name = "Button_Motor_WriteSpeed";
            this.Button_Motor_WriteSpeed.NormlBack = null;
            this.Button_Motor_WriteSpeed.Size = new System.Drawing.Size(63, 50);
            this.Button_Motor_WriteSpeed.TabIndex = 18;
            this.Button_Motor_WriteSpeed.Text = "写入";
            this.Button_Motor_WriteSpeed.UseVisualStyleBackColor = false;
            this.Button_Motor_WriteSpeed.Click += new System.EventHandler(this.Button_Motor_WriteSpeed_Click);
            // 
            // Button_Motor_WriteAngle
            // 
            this.Button_Motor_WriteAngle.BackColor = System.Drawing.Color.Transparent;
            this.Button_Motor_WriteAngle.BaseColor = System.Drawing.Color.DodgerBlue;
            this.Button_Motor_WriteAngle.BorderColor = System.Drawing.Color.DodgerBlue;
            this.Button_Motor_WriteAngle.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.Button_Motor_WriteAngle.DownBack = null;
            this.Button_Motor_WriteAngle.Location = new System.Drawing.Point(335, 159);
            this.Button_Motor_WriteAngle.Margin = new System.Windows.Forms.Padding(4);
            this.Button_Motor_WriteAngle.MouseBack = null;
            this.Button_Motor_WriteAngle.Name = "Button_Motor_WriteAngle";
            this.Button_Motor_WriteAngle.NormlBack = null;
            this.Button_Motor_WriteAngle.Size = new System.Drawing.Size(124, 63);
            this.Button_Motor_WriteAngle.TabIndex = 19;
            this.Button_Motor_WriteAngle.Text = "运动到位";
            this.Button_Motor_WriteAngle.UseVisualStyleBackColor = false;
            this.Button_Motor_WriteAngle.Click += new System.EventHandler(this.Button_Motor_WriteAngle_Click);
            // 
            // Button_OffWarning
            // 
            this.Button_OffWarning.BackColor = System.Drawing.Color.Transparent;
            this.Button_OffWarning.BaseColor = System.Drawing.Color.DodgerBlue;
            this.Button_OffWarning.BorderColor = System.Drawing.Color.DodgerBlue;
            this.Button_OffWarning.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.Button_OffWarning.DownBack = null;
            this.Button_OffWarning.Location = new System.Drawing.Point(335, 347);
            this.Button_OffWarning.Margin = new System.Windows.Forms.Padding(4);
            this.Button_OffWarning.MouseBack = null;
            this.Button_OffWarning.Name = "Button_OffWarning";
            this.Button_OffWarning.NormlBack = null;
            this.Button_OffWarning.Size = new System.Drawing.Size(124, 68);
            this.Button_OffWarning.TabIndex = 20;
            this.Button_OffWarning.Text = "取消报警";
            this.Button_OffWarning.UseVisualStyleBackColor = false;
            this.Button_OffWarning.Click += new System.EventHandler(this.Button_OffWarning_Click);
            // 
            // Text_Motor_Acc
            // 
            this.Text_Motor_Acc.BackColor = System.Drawing.Color.Transparent;
            this.Text_Motor_Acc.ConerRadius = 20;
            this.Text_Motor_Acc.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Text_Motor_Acc.DecLength = 2;
            this.Text_Motor_Acc.FillColor = System.Drawing.Color.Empty;
            this.Text_Motor_Acc.FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.Text_Motor_Acc.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.Text_Motor_Acc.InputText = "";
            this.Text_Motor_Acc.InputType = HZH_Controls.TextInputType.Number;
            this.Text_Motor_Acc.IsFocusColor = true;
            this.Text_Motor_Acc.IsRadius = true;
            this.Text_Motor_Acc.IsShowClearBtn = true;
            this.Text_Motor_Acc.IsShowKeyboard = true;
            this.Text_Motor_Acc.IsShowRect = true;
            this.Text_Motor_Acc.IsShowSearchBtn = false;
            this.Text_Motor_Acc.KeyBoardType = HZH_Controls.Controls.KeyBoardType.UCKeyBorderNum;
            this.Text_Motor_Acc.Location = new System.Drawing.Point(148, 27);
            this.Text_Motor_Acc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Text_Motor_Acc.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.Text_Motor_Acc.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.Text_Motor_Acc.Name = "Text_Motor_Acc";
            this.Text_Motor_Acc.Padding = new System.Windows.Forms.Padding(5);
            this.Text_Motor_Acc.PasswordChar = '\0';
            this.Text_Motor_Acc.PromptColor = System.Drawing.Color.Gray;
            this.Text_Motor_Acc.PromptFont = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.Text_Motor_Acc.PromptText = "";
            this.Text_Motor_Acc.RectColor = System.Drawing.Color.Black;
            this.Text_Motor_Acc.RectWidth = 5;
            this.Text_Motor_Acc.RegexPattern = "";
            this.Text_Motor_Acc.Size = new System.Drawing.Size(151, 43);
            this.Text_Motor_Acc.TabIndex = 21;
            // 
            // Text_Motor_Speed
            // 
            this.Text_Motor_Speed.BackColor = System.Drawing.Color.Transparent;
            this.Text_Motor_Speed.ConerRadius = 20;
            this.Text_Motor_Speed.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Text_Motor_Speed.DecLength = 2;
            this.Text_Motor_Speed.FillColor = System.Drawing.Color.Empty;
            this.Text_Motor_Speed.FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.Text_Motor_Speed.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.Text_Motor_Speed.InputText = "";
            this.Text_Motor_Speed.InputType = HZH_Controls.TextInputType.Number;
            this.Text_Motor_Speed.IsFocusColor = true;
            this.Text_Motor_Speed.IsRadius = true;
            this.Text_Motor_Speed.IsShowClearBtn = true;
            this.Text_Motor_Speed.IsShowKeyboard = true;
            this.Text_Motor_Speed.IsShowRect = true;
            this.Text_Motor_Speed.IsShowSearchBtn = false;
            this.Text_Motor_Speed.KeyBoardType = HZH_Controls.Controls.KeyBoardType.UCKeyBorderNum;
            this.Text_Motor_Speed.Location = new System.Drawing.Point(148, 101);
            this.Text_Motor_Speed.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Text_Motor_Speed.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.Text_Motor_Speed.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.Text_Motor_Speed.Name = "Text_Motor_Speed";
            this.Text_Motor_Speed.Padding = new System.Windows.Forms.Padding(5);
            this.Text_Motor_Speed.PasswordChar = '\0';
            this.Text_Motor_Speed.PromptColor = System.Drawing.Color.Gray;
            this.Text_Motor_Speed.PromptFont = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.Text_Motor_Speed.PromptText = "";
            this.Text_Motor_Speed.RectColor = System.Drawing.Color.Black;
            this.Text_Motor_Speed.RectWidth = 5;
            this.Text_Motor_Speed.RegexPattern = "";
            this.Text_Motor_Speed.Size = new System.Drawing.Size(151, 43);
            this.Text_Motor_Speed.TabIndex = 22;
            // 
            // Text_Motor_Angle
            // 
            this.Text_Motor_Angle.BackColor = System.Drawing.Color.Transparent;
            this.Text_Motor_Angle.ConerRadius = 20;
            this.Text_Motor_Angle.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Text_Motor_Angle.DecLength = 2;
            this.Text_Motor_Angle.FillColor = System.Drawing.Color.Empty;
            this.Text_Motor_Angle.FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.Text_Motor_Angle.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.Text_Motor_Angle.InputText = "";
            this.Text_Motor_Angle.InputType = HZH_Controls.TextInputType.Number;
            this.Text_Motor_Angle.IsFocusColor = true;
            this.Text_Motor_Angle.IsRadius = true;
            this.Text_Motor_Angle.IsShowClearBtn = true;
            this.Text_Motor_Angle.IsShowKeyboard = true;
            this.Text_Motor_Angle.IsShowRect = true;
            this.Text_Motor_Angle.IsShowSearchBtn = false;
            this.Text_Motor_Angle.KeyBoardType = HZH_Controls.Controls.KeyBoardType.UCKeyBorderNum;
            this.Text_Motor_Angle.Location = new System.Drawing.Point(148, 176);
            this.Text_Motor_Angle.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Text_Motor_Angle.MaxValue = new decimal(new int[] {
            720,
            0,
            0,
            0});
            this.Text_Motor_Angle.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.Text_Motor_Angle.Name = "Text_Motor_Angle";
            this.Text_Motor_Angle.Padding = new System.Windows.Forms.Padding(5);
            this.Text_Motor_Angle.PasswordChar = '\0';
            this.Text_Motor_Angle.PromptColor = System.Drawing.Color.Gray;
            this.Text_Motor_Angle.PromptFont = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.Text_Motor_Angle.PromptText = "";
            this.Text_Motor_Angle.RectColor = System.Drawing.Color.Black;
            this.Text_Motor_Angle.RectWidth = 5;
            this.Text_Motor_Angle.RegexPattern = "";
            this.Text_Motor_Angle.Size = new System.Drawing.Size(151, 43);
            this.Text_Motor_Angle.TabIndex = 22;
            // 
            // skinLabel2
            // 
            this.skinLabel2.AutoSize = true;
            this.skinLabel2.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel2.BorderColor = System.Drawing.Color.White;
            this.skinLabel2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel2.Location = new System.Drawing.Point(24, 256);
            this.skinLabel2.Name = "skinLabel2";
            this.skinLabel2.Size = new System.Drawing.Size(74, 21);
            this.skinLabel2.TabIndex = 23;
            this.skinLabel2.Text = "当前角度";
            // 
            // Text_Current_Angle
            // 
            this.Text_Current_Angle.BackColor = System.Drawing.Color.Transparent;
            this.Text_Current_Angle.DownBack = null;
            this.Text_Current_Angle.Icon = null;
            this.Text_Current_Angle.IconIsButton = false;
            this.Text_Current_Angle.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.Text_Current_Angle.IsPasswordChat = '\0';
            this.Text_Current_Angle.IsSystemPasswordChar = false;
            this.Text_Current_Angle.Lines = new string[0];
            this.Text_Current_Angle.Location = new System.Drawing.Point(150, 256);
            this.Text_Current_Angle.Margin = new System.Windows.Forms.Padding(0);
            this.Text_Current_Angle.MaxLength = 32767;
            this.Text_Current_Angle.MinimumSize = new System.Drawing.Size(28, 28);
            this.Text_Current_Angle.MouseBack = null;
            this.Text_Current_Angle.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.Text_Current_Angle.Multiline = true;
            this.Text_Current_Angle.Name = "Text_Current_Angle";
            this.Text_Current_Angle.NormlBack = null;
            this.Text_Current_Angle.Padding = new System.Windows.Forms.Padding(5);
            this.Text_Current_Angle.ReadOnly = true;
            this.Text_Current_Angle.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Text_Current_Angle.Size = new System.Drawing.Size(149, 40);
            // 
            // 
            // 
            this.Text_Current_Angle.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Text_Current_Angle.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Text_Current_Angle.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.Text_Current_Angle.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.Text_Current_Angle.SkinTxt.Multiline = true;
            this.Text_Current_Angle.SkinTxt.Name = "BaseText";
            this.Text_Current_Angle.SkinTxt.ReadOnly = true;
            this.Text_Current_Angle.SkinTxt.Size = new System.Drawing.Size(139, 30);
            this.Text_Current_Angle.SkinTxt.TabIndex = 0;
            this.Text_Current_Angle.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.Text_Current_Angle.SkinTxt.WaterText = "";
            this.Text_Current_Angle.TabIndex = 29;
            this.Text_Current_Angle.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Text_Current_Angle.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.Text_Current_Angle.WaterText = "";
            this.Text_Current_Angle.WordWrap = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(306, 109);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 16);
            this.label1.TabIndex = 30;
            this.label1.Text = "mm/s";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(306, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 16);
            this.label2.TabIndex = 31;
            this.label2.Text = "mm/s²";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(302, 256);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 16);
            this.label3.TabIndex = 32;
            this.label3.Text = "°";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(302, 176);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 16);
            this.label4.TabIndex = 33;
            this.label4.Text = "°";
            // 
            // MotorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Text_Current_Angle);
            this.Controls.Add(this.skinLabel2);
            this.Controls.Add(this.Text_Motor_Angle);
            this.Controls.Add(this.Text_Motor_Speed);
            this.Controls.Add(this.Text_Motor_Acc);
            this.Controls.Add(this.Button_OffWarning);
            this.Controls.Add(this.Button_Motor_WriteAngle);
            this.Controls.Add(this.Button_Motor_WriteSpeed);
            this.Controls.Add(this.Button_Motor_WriteAcc);
            this.Controls.Add(this.skinLabel1);
            this.Controls.Add(this.Lab_MotroSpeed);
            this.Controls.Add(this.Lab_MotorAcc);
            this.Controls.Add(this.Button_MoveOrigion);
            this.Controls.Add(this.Emotor_AlarmLamp);
            this.Controls.Add(this.Button_MotorEnable);
            this.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MotorControl";
            this.Size = new System.Drawing.Size(628, 465);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CCWin.SkinControl.SkinButton Button_MotorEnable;
        private HZH_Controls.Controls.UCAlarmLamp Emotor_AlarmLamp;
        private CCWin.SkinControl.SkinButton Button_MoveOrigion;
        private CCWin.SkinControl.SkinLabel Lab_MotorAcc;
        private CCWin.SkinControl.SkinLabel Lab_MotroSpeed;
        private CCWin.SkinControl.SkinLabel skinLabel1;
        private CCWin.SkinControl.SkinButton Button_Motor_WriteAcc;
        private CCWin.SkinControl.SkinButton Button_Motor_WriteSpeed;
        private CCWin.SkinControl.SkinButton Button_Motor_WriteAngle;
        private CCWin.SkinControl.SkinButton Button_OffWarning;
        private HZH_Controls.Controls.UCTextBoxEx Text_Motor_Acc;
        private HZH_Controls.Controls.UCTextBoxEx Text_Motor_Speed;
        private HZH_Controls.Controls.UCTextBoxEx Text_Motor_Angle;
        private CCWin.SkinControl.SkinLabel skinLabel2;
        private CCWin.SkinControl.SkinTextBox Text_Current_Angle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}
