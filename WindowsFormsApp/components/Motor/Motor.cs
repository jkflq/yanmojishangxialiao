﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp.deviceConnect;
using WindowsFormsApp.utils.modbus;

namespace WindowsFormsApp.components.Motor
{
    public class Motor:IAutoUpdatable
    {
        public ModbusListener busTcpClient;
        public int position;

        //错误代码
        public static Dictionary<int, string> ErrCode = new Dictionary<int, string> {
            { 1,"错误停止"},
            { 2,"未使能"},
        };

        public Motor()
        {
           // ConnectConfig.updateList.Add(this);
        }
        public string Current_PositionReg;
        public string WarningCoil;
        public string Move_OriginCoil;
        public string SpeedReg;
        public string Target_AngleReg;
        public string AccReg;
        public string EnableCoil;
        public string Run;


        public void ReFreshState()
        {
            position = busTcpClient.ReadInt16(Current_PositionReg);
        }

    }
}
