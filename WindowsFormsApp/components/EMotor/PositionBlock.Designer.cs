﻿
namespace WindowsFormsApp.components.EMotor
{
    partial class PositionBlock
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Write_Btn = new CCWin.SkinControl.SkinButton();
            this.textbox_Position = new HZH_Controls.Controls.UCTextBoxEx();
            this.label1 = new System.Windows.Forms.Label();
            this.GPBOX_Position = new CCWin.SkinControl.SkinGroupBox();
            this.Read_Btn = new CCWin.SkinControl.SkinButton();
            this.GPBOX_Position.SuspendLayout();
            this.SuspendLayout();
            // 
            // Write_Btn
            // 
            this.Write_Btn.BackColor = System.Drawing.Color.Transparent;
            this.Write_Btn.BaseColor = System.Drawing.Color.DodgerBlue;
            this.Write_Btn.BorderColor = System.Drawing.Color.Transparent;
            this.Write_Btn.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.Write_Btn.DownBack = null;
            this.Write_Btn.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Write_Btn.Location = new System.Drawing.Point(122, 78);
            this.Write_Btn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Write_Btn.MouseBack = null;
            this.Write_Btn.Name = "Write_Btn";
            this.Write_Btn.NormlBack = null;
            this.Write_Btn.Size = new System.Drawing.Size(92, 49);
            this.Write_Btn.TabIndex = 6;
            this.Write_Btn.Text = "写入";
            this.Write_Btn.UseVisualStyleBackColor = false;
            this.Write_Btn.Click += new System.EventHandler(this.Write_Btn_Click);
            // 
            // textbox_Position
            // 
            this.textbox_Position.BackColor = System.Drawing.Color.Transparent;
            this.textbox_Position.ConerRadius = 20;
            this.textbox_Position.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textbox_Position.DecLength = 2;
            this.textbox_Position.FillColor = System.Drawing.Color.Empty;
            this.textbox_Position.FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.textbox_Position.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.textbox_Position.InputText = "";
            this.textbox_Position.InputType = HZH_Controls.TextInputType.Number;
            this.textbox_Position.IsFocusColor = true;
            this.textbox_Position.IsRadius = true;
            this.textbox_Position.IsShowClearBtn = true;
            this.textbox_Position.IsShowKeyboard = true;
            this.textbox_Position.IsShowRect = true;
            this.textbox_Position.IsShowSearchBtn = false;
            this.textbox_Position.KeyBoardType = HZH_Controls.Controls.KeyBoardType.UCKeyBorderAll_Num;
            this.textbox_Position.Location = new System.Drawing.Point(8, 29);
            this.textbox_Position.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textbox_Position.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.textbox_Position.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.textbox_Position.Name = "textbox_Position";
            this.textbox_Position.Padding = new System.Windows.Forms.Padding(5);
            this.textbox_Position.PasswordChar = '\0';
            this.textbox_Position.PromptColor = System.Drawing.Color.Gray;
            this.textbox_Position.PromptFont = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.textbox_Position.PromptText = "";
            this.textbox_Position.RectColor = System.Drawing.Color.Black;
            this.textbox_Position.RectWidth = 5;
            this.textbox_Position.RegexPattern = "";
            this.textbox_Position.Size = new System.Drawing.Size(127, 40);
            this.textbox_Position.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(142, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 20);
            this.label1.TabIndex = 23;
            this.label1.Text = "mm";
            // 
            // GPBOX_Position
            // 
            this.GPBOX_Position.BackColor = System.Drawing.Color.Transparent;
            this.GPBOX_Position.BorderColor = System.Drawing.Color.Gray;
            this.GPBOX_Position.Controls.Add(this.textbox_Position);
            this.GPBOX_Position.Controls.Add(this.label1);
            this.GPBOX_Position.Controls.Add(this.Read_Btn);
            this.GPBOX_Position.Controls.Add(this.Write_Btn);
            this.GPBOX_Position.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.GPBOX_Position.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.GPBOX_Position.Location = new System.Drawing.Point(4, 4);
            this.GPBOX_Position.Margin = new System.Windows.Forms.Padding(4);
            this.GPBOX_Position.Name = "GPBOX_Position";
            this.GPBOX_Position.Padding = new System.Windows.Forms.Padding(4);
            this.GPBOX_Position.Radius = 8;
            this.GPBOX_Position.RectBackColor = System.Drawing.Color.Transparent;
            this.GPBOX_Position.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.GPBOX_Position.Size = new System.Drawing.Size(240, 135);
            this.GPBOX_Position.TabIndex = 104;
            this.GPBOX_Position.TabStop = false;
            this.GPBOX_Position.Text = "位置信息";
            this.GPBOX_Position.TitleBorderColor = System.Drawing.Color.Transparent;
            this.GPBOX_Position.TitleRadius = 20;
            this.GPBOX_Position.TitleRectBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(198)))), ((int)(((byte)(203)))));
            this.GPBOX_Position.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // Read_Btn
            // 
            this.Read_Btn.BackColor = System.Drawing.Color.Transparent;
            this.Read_Btn.BaseColor = System.Drawing.Color.DodgerBlue;
            this.Read_Btn.BorderColor = System.Drawing.Color.Transparent;
            this.Read_Btn.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.Read_Btn.DownBack = null;
            this.Read_Btn.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Read_Btn.Location = new System.Drawing.Point(7, 78);
            this.Read_Btn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Read_Btn.MouseBack = null;
            this.Read_Btn.Name = "Read_Btn";
            this.Read_Btn.NormlBack = null;
            this.Read_Btn.Size = new System.Drawing.Size(91, 49);
            this.Read_Btn.TabIndex = 5;
            this.Read_Btn.Text = "读取数值";
            this.Read_Btn.UseVisualStyleBackColor = false;
            this.Read_Btn.Click += new System.EventHandler(this.Read_Btn_Click);
            // 
            // PositionBlock
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.GPBOX_Position);
            this.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "PositionBlock";
            this.Size = new System.Drawing.Size(255, 144);
            this.Load += new System.EventHandler(this.PositionBlock_Load);
            this.GPBOX_Position.ResumeLayout(false);
            this.GPBOX_Position.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private CCWin.SkinControl.SkinButton Write_Btn;
        private HZH_Controls.Controls.UCTextBoxEx textbox_Position;
        private System.Windows.Forms.Label label1;
        private CCWin.SkinControl.SkinGroupBox GPBOX_Position;
        private CCWin.SkinControl.SkinButton Read_Btn;
    }
}
