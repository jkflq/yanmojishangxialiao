﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp.deviceConnect;
using WindowsFormsApp.utils.modbus;

namespace WindowsFormsApp.components.EMotor
{
    /// <summary>
    /// 电缸类
    /// </summary>
    public class EMotor:IAutoUpdatable
    {
        public ModbusListener busTcpClient;

        public int positon;

        public static Dictionary<int, string> ErrCode = new Dictionary<int, string> {
            { 1,"错误停止"},
            { 2,"未使能"},
            { 128,"紧急停止"}
        }; 

        public int ID;

        public EMotor()
        {
            ConnectConfig.updateList.Add(this);
           
        }
        
        public string FirstPositionReg;      //第一个位置位置信息
        public string FirstTGPositionReg;    //第一个位置脱钩位信息

        public string SecondPositionReg;
        public string SecondTGPositionReg;

        public string ThirdPositionReg;
        public string ThirdTGPositionReg;

        public string FourthPositionReg;
        public string FourthTGPositionReg;

        public string FifthPositionReg;
        public string FifthTGPositionReg;

        public string SixthPositionReg;
        public string SixthTGPositionReg;

        public string SeventhPositionReg;
        public string SeventhTGPositionReg;

        public string EighthPositionReg;
        public string EighthTGPositionReg;
 
        public string Move_OriginCoil;
        public string Move_UpCoil;
        public string Move_DownCoil;
        public string CurrentPositionReg;
        public string TargetPositionReg;
        public string SpeedReg;
        public string AccReg;
        public string EnableCoil;       //电缸使能
        public string Moving_Flag;
        public string Wanning_Flag;     //报警
        public string CancelWanningCoil;  //取消报警
        public string AlalmCodeReg;
        public string Running;          //启动





        public void MoveToPosition(string position)
        {
            busTcpClient.Write(position, true);
        }
        public void ModifyPosition(string position,int value)
        {
            busTcpClient.Write(position, value);
        }
        public int GetCurrentPosition(string position)
        {
            return busTcpClient.ReadInt16(position);
        }
     

        //将所需要读取的状态的值重写
        public void ReFreshState()
        {
            
        }
    }
}
