﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp.deviceConnect;

namespace WindowsFormsApp.components.EMotor
{
    public partial class EmotorControl : UserControl,IAutoUpdatable
    {
        public EmotorControl()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);
            ConnectConfig.updateList.Add(this);
        }
        public EMotor emotor;
        public bool value = false;

        //启动时读取位置和速度
        private string Transform_TO_String(int a)
        {
            double b = a;
            b = b / 100;
            return b.ToString();

        }
        private void Button_Speed_Click(object sender, EventArgs e)
        {
            emotor.busTcpClient.Write(emotor.SpeedReg, int.Parse(text_speed.InputText));
        }
        public void ReFreshState()
        {
            text_position.Text = Transform_TO_String(emotor.busTcpClient.ReadDoubleWord(emotor.CurrentPositionReg));
        }
        private void Button_Acc_Click(object sender, EventArgs e)
        {
            emotor.busTcpClient.Write(emotor.AccReg, int.Parse(text_Acc.InputText));
        }

        private void Button_ReadSpeed_Click(object sender, EventArgs e)
        {
            text_speed.InputText =  emotor.busTcpClient.ReadInt16(emotor.SpeedReg).ToString();
        }

        private void Button_ReadAcc_Click(object sender, EventArgs e)
        {
            text_Acc.InputText = emotor.busTcpClient.ReadInt16(emotor.AccReg).ToString();
        }

        private void Button_Enable_Click(object sender, EventArgs e)
        {
            if (!emotor.busTcpClient.ReadCoil(emotor.EnableCoil))
            {
                value = true;
                emotor.busTcpClient.Write(emotor.EnableCoil, true);
                
            }
            else
            {
                value = false;
                emotor.busTcpClient.Write(emotor.EnableCoil, false);

            }
            Button_Enable.BaseColor = value ? Color.Lime : Color.Red;
            Button_Enable.Text = value ? "电缸失能" : "电缸使能";
        }


        /// <summary>
        /// 如果报警点按钮取消报警
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Off_Wanning_Click(object sender, EventArgs e)
        {
            if (emotor.busTcpClient.ReadCoil(emotor.Wanning_Flag))
            {
                emotor.busTcpClient.Write(emotor.CancelWanningCoil, true);
                Thread.Sleep(10);
                emotor.busTcpClient.Write(emotor.CancelWanningCoil, false);
            }
        }


        private void ButtonMove_Click(object sender, EventArgs e)
        {

           if ((float.Parse(ucTextBoxEx1.InputText)) < 25)
              {
                     MessageBox.Show("输入范围超限");
                        return;
              }
           
                emotor.busTcpClient.WriteDoubleWord(emotor.TargetPositionReg, (int)(double.Parse(ucTextBoxEx1.InputText)*100));
            emotor.busTcpClient.Write(emotor.Running, true);
            Thread.Sleep(10);
            emotor.busTcpClient.Write(emotor.Running, false);
        }

        private void EmotorControl_Load(object sender, EventArgs e)
        {
            if (ConnectConfig.Connected)
            {
                text_Acc.InputText = emotor.busTcpClient.ReadInt16(emotor.AccReg).ToString();
                text_speed.InputText = emotor.busTcpClient.ReadInt16(emotor.SpeedReg).ToString();
            }
        }
    }
}
