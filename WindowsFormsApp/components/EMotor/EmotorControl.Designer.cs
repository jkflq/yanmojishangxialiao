﻿
namespace WindowsFormsApp.components.EMotor
{
    partial class EmotorControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.skinLabel1 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel2 = new CCWin.SkinControl.SkinLabel();
            this.Button_Speed = new CCWin.SkinControl.SkinButton();
            this.skinLabel3 = new CCWin.SkinControl.SkinLabel();
            this.Button_Acc = new CCWin.SkinControl.SkinButton();
            this.text_speed = new HZH_Controls.Controls.UCTextBoxEx();
            this.text_Acc = new HZH_Controls.Controls.UCTextBoxEx();
            this.Button_ReadSpeed = new CCWin.SkinControl.SkinButton();
            this.Button_ReadAcc = new CCWin.SkinControl.SkinButton();
            this.text_position = new CCWin.SkinControl.SkinTextBox();
            this.Button_Enable = new CCWin.SkinControl.SkinButton();
            this.Button_Off_Wanning = new CCWin.SkinControl.SkinButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ucTextBoxEx1 = new HZH_Controls.Controls.UCTextBoxEx();
            this.Button_Move = new CCWin.SkinControl.SkinButton();
            this.skinLabel4 = new CCWin.SkinControl.SkinLabel();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // skinLabel1
            // 
            this.skinLabel1.AutoSize = true;
            this.skinLabel1.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel1.BorderColor = System.Drawing.Color.White;
            this.skinLabel1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel1.Location = new System.Drawing.Point(303, 15);
            this.skinLabel1.Name = "skinLabel1";
            this.skinLabel1.Size = new System.Drawing.Size(74, 21);
            this.skinLabel1.TabIndex = 3;
            this.skinLabel1.Text = "手动速度";
            // 
            // skinLabel2
            // 
            this.skinLabel2.AutoSize = true;
            this.skinLabel2.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel2.BorderColor = System.Drawing.Color.White;
            this.skinLabel2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel2.Location = new System.Drawing.Point(481, 130);
            this.skinLabel2.Name = "skinLabel2";
            this.skinLabel2.Size = new System.Drawing.Size(74, 21);
            this.skinLabel2.TabIndex = 4;
            this.skinLabel2.Text = "升降位置";
            // 
            // Button_Speed
            // 
            this.Button_Speed.BackColor = System.Drawing.Color.Transparent;
            this.Button_Speed.BaseColor = System.Drawing.Color.DodgerBlue;
            this.Button_Speed.BorderColor = System.Drawing.Color.DodgerBlue;
            this.Button_Speed.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.Button_Speed.DownBack = null;
            this.Button_Speed.Location = new System.Drawing.Point(624, 10);
            this.Button_Speed.Margin = new System.Windows.Forms.Padding(4);
            this.Button_Speed.MouseBack = null;
            this.Button_Speed.Name = "Button_Speed";
            this.Button_Speed.NormlBack = null;
            this.Button_Speed.Size = new System.Drawing.Size(63, 37);
            this.Button_Speed.TabIndex = 7;
            this.Button_Speed.Text = "写入";
            this.Button_Speed.UseVisualStyleBackColor = false;
            this.Button_Speed.Click += new System.EventHandler(this.Button_Speed_Click);
            // 
            // skinLabel3
            // 
            this.skinLabel3.AutoSize = true;
            this.skinLabel3.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel3.BorderColor = System.Drawing.Color.White;
            this.skinLabel3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel3.Location = new System.Drawing.Point(303, 72);
            this.skinLabel3.Name = "skinLabel3";
            this.skinLabel3.Size = new System.Drawing.Size(58, 21);
            this.skinLabel3.TabIndex = 11;
            this.skinLabel3.Text = "加速度";
            // 
            // Button_Acc
            // 
            this.Button_Acc.BackColor = System.Drawing.Color.Transparent;
            this.Button_Acc.BaseColor = System.Drawing.Color.DodgerBlue;
            this.Button_Acc.BorderColor = System.Drawing.Color.DodgerBlue;
            this.Button_Acc.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.Button_Acc.DownBack = null;
            this.Button_Acc.Location = new System.Drawing.Point(624, 72);
            this.Button_Acc.Margin = new System.Windows.Forms.Padding(4);
            this.Button_Acc.MouseBack = null;
            this.Button_Acc.Name = "Button_Acc";
            this.Button_Acc.NormlBack = null;
            this.Button_Acc.Size = new System.Drawing.Size(63, 37);
            this.Button_Acc.TabIndex = 12;
            this.Button_Acc.Text = "写入";
            this.Button_Acc.UseVisualStyleBackColor = false;
            this.Button_Acc.Click += new System.EventHandler(this.Button_Acc_Click);
            // 
            // text_speed
            // 
            this.text_speed.BackColor = System.Drawing.Color.Transparent;
            this.text_speed.ConerRadius = 20;
            this.text_speed.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.text_speed.DecLength = 2;
            this.text_speed.FillColor = System.Drawing.Color.Empty;
            this.text_speed.FocusBorderColor = System.Drawing.Color.Black;
            this.text_speed.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.text_speed.InputText = "";
            this.text_speed.InputType = HZH_Controls.TextInputType.Integer;
            this.text_speed.IsFocusColor = true;
            this.text_speed.IsRadius = true;
            this.text_speed.IsShowClearBtn = true;
            this.text_speed.IsShowKeyboard = true;
            this.text_speed.IsShowRect = true;
            this.text_speed.IsShowSearchBtn = false;
            this.text_speed.KeyBoardType = HZH_Controls.Controls.KeyBoardType.UCKeyBorderNum;
            this.text_speed.Location = new System.Drawing.Point(402, 7);
            this.text_speed.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.text_speed.MaxValue = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.text_speed.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.text_speed.Name = "text_speed";
            this.text_speed.Padding = new System.Windows.Forms.Padding(5);
            this.text_speed.PasswordChar = '\0';
            this.text_speed.PromptColor = System.Drawing.Color.Gray;
            this.text_speed.PromptFont = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.text_speed.PromptText = "";
            this.text_speed.RectColor = System.Drawing.Color.Black;
            this.text_speed.RectWidth = 5;
            this.text_speed.RegexPattern = "";
            this.text_speed.Size = new System.Drawing.Size(149, 40);
            this.text_speed.TabIndex = 23;
            // 
            // text_Acc
            // 
            this.text_Acc.BackColor = System.Drawing.Color.Transparent;
            this.text_Acc.ConerRadius = 20;
            this.text_Acc.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.text_Acc.DecLength = 2;
            this.text_Acc.FillColor = System.Drawing.Color.Empty;
            this.text_Acc.FocusBorderColor = System.Drawing.Color.Black;
            this.text_Acc.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.text_Acc.InputText = "";
            this.text_Acc.InputType = HZH_Controls.TextInputType.Integer;
            this.text_Acc.IsFocusColor = true;
            this.text_Acc.IsRadius = true;
            this.text_Acc.IsShowClearBtn = true;
            this.text_Acc.IsShowKeyboard = true;
            this.text_Acc.IsShowRect = true;
            this.text_Acc.IsShowSearchBtn = false;
            this.text_Acc.KeyBoardType = HZH_Controls.Controls.KeyBoardType.UCKeyBorderNum;
            this.text_Acc.Location = new System.Drawing.Point(402, 65);
            this.text_Acc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.text_Acc.MaxValue = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.text_Acc.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.text_Acc.Name = "text_Acc";
            this.text_Acc.Padding = new System.Windows.Forms.Padding(5);
            this.text_Acc.PasswordChar = '\0';
            this.text_Acc.PromptColor = System.Drawing.Color.Gray;
            this.text_Acc.PromptFont = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.text_Acc.PromptText = "";
            this.text_Acc.RectColor = System.Drawing.Color.Black;
            this.text_Acc.RectWidth = 5;
            this.text_Acc.RegexPattern = "";
            this.text_Acc.Size = new System.Drawing.Size(149, 40);
            this.text_Acc.TabIndex = 24;
            // 
            // Button_ReadSpeed
            // 
            this.Button_ReadSpeed.BackColor = System.Drawing.Color.Transparent;
            this.Button_ReadSpeed.BaseColor = System.Drawing.Color.DodgerBlue;
            this.Button_ReadSpeed.BorderColor = System.Drawing.Color.DodgerBlue;
            this.Button_ReadSpeed.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.Button_ReadSpeed.DownBack = null;
            this.Button_ReadSpeed.Location = new System.Drawing.Point(709, 11);
            this.Button_ReadSpeed.Margin = new System.Windows.Forms.Padding(4);
            this.Button_ReadSpeed.MouseBack = null;
            this.Button_ReadSpeed.Name = "Button_ReadSpeed";
            this.Button_ReadSpeed.NormlBack = null;
            this.Button_ReadSpeed.Size = new System.Drawing.Size(63, 37);
            this.Button_ReadSpeed.TabIndex = 26;
            this.Button_ReadSpeed.Text = "读取";
            this.Button_ReadSpeed.UseVisualStyleBackColor = false;
            this.Button_ReadSpeed.Click += new System.EventHandler(this.Button_ReadSpeed_Click);
            // 
            // Button_ReadAcc
            // 
            this.Button_ReadAcc.BackColor = System.Drawing.Color.Transparent;
            this.Button_ReadAcc.BaseColor = System.Drawing.Color.DodgerBlue;
            this.Button_ReadAcc.BorderColor = System.Drawing.Color.DodgerBlue;
            this.Button_ReadAcc.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.Button_ReadAcc.DownBack = null;
            this.Button_ReadAcc.Location = new System.Drawing.Point(709, 72);
            this.Button_ReadAcc.Margin = new System.Windows.Forms.Padding(4);
            this.Button_ReadAcc.MouseBack = null;
            this.Button_ReadAcc.Name = "Button_ReadAcc";
            this.Button_ReadAcc.NormlBack = null;
            this.Button_ReadAcc.Size = new System.Drawing.Size(63, 37);
            this.Button_ReadAcc.TabIndex = 27;
            this.Button_ReadAcc.Text = "读取";
            this.Button_ReadAcc.UseVisualStyleBackColor = false;
            this.Button_ReadAcc.Click += new System.EventHandler(this.Button_ReadAcc_Click);
            // 
            // text_position
            // 
            this.text_position.BackColor = System.Drawing.Color.Transparent;
            this.text_position.DownBack = null;
            this.text_position.Icon = null;
            this.text_position.IconIsButton = false;
            this.text_position.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.text_position.IsPasswordChat = '\0';
            this.text_position.IsSystemPasswordChar = false;
            this.text_position.Lines = new string[0];
            this.text_position.Location = new System.Drawing.Point(591, 131);
            this.text_position.Margin = new System.Windows.Forms.Padding(0);
            this.text_position.MaxLength = 32767;
            this.text_position.MinimumSize = new System.Drawing.Size(28, 28);
            this.text_position.MouseBack = null;
            this.text_position.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.text_position.Multiline = true;
            this.text_position.Name = "text_position";
            this.text_position.NormlBack = null;
            this.text_position.Padding = new System.Windows.Forms.Padding(5);
            this.text_position.ReadOnly = true;
            this.text_position.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.text_position.Size = new System.Drawing.Size(149, 30);
            // 
            // 
            // 
            this.text_position.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.text_position.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.text_position.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.text_position.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.text_position.SkinTxt.Multiline = true;
            this.text_position.SkinTxt.Name = "BaseText";
            this.text_position.SkinTxt.ReadOnly = true;
            this.text_position.SkinTxt.Size = new System.Drawing.Size(139, 20);
            this.text_position.SkinTxt.TabIndex = 0;
            this.text_position.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.text_position.SkinTxt.WaterText = "";
            this.text_position.TabIndex = 28;
            this.text_position.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.text_position.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.text_position.WaterText = "";
            this.text_position.WordWrap = true;
            // 
            // Button_Enable
            // 
            this.Button_Enable.BackColor = System.Drawing.Color.Transparent;
            this.Button_Enable.BaseColor = System.Drawing.Color.Red;
            this.Button_Enable.BorderColor = System.Drawing.Color.DodgerBlue;
            this.Button_Enable.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.Button_Enable.DownBack = null;
            this.Button_Enable.Location = new System.Drawing.Point(14, 19);
            this.Button_Enable.Margin = new System.Windows.Forms.Padding(4);
            this.Button_Enable.MouseBack = null;
            this.Button_Enable.Name = "Button_Enable";
            this.Button_Enable.NormlBack = null;
            this.Button_Enable.Size = new System.Drawing.Size(120, 73);
            this.Button_Enable.TabIndex = 29;
            this.Button_Enable.Text = "电缸使能";
            this.Button_Enable.UseVisualStyleBackColor = false;
            this.Button_Enable.Click += new System.EventHandler(this.Button_Enable_Click);
            // 
            // Button_Off_Wanning
            // 
            this.Button_Off_Wanning.BackColor = System.Drawing.Color.Transparent;
            this.Button_Off_Wanning.BaseColor = System.Drawing.Color.DodgerBlue;
            this.Button_Off_Wanning.BorderColor = System.Drawing.Color.DodgerBlue;
            this.Button_Off_Wanning.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.Button_Off_Wanning.DownBack = null;
            this.Button_Off_Wanning.Location = new System.Drawing.Point(158, 19);
            this.Button_Off_Wanning.Margin = new System.Windows.Forms.Padding(4);
            this.Button_Off_Wanning.MouseBack = null;
            this.Button_Off_Wanning.Name = "Button_Off_Wanning";
            this.Button_Off_Wanning.NormlBack = null;
            this.Button_Off_Wanning.Size = new System.Drawing.Size(121, 73);
            this.Button_Off_Wanning.TabIndex = 30;
            this.Button_Off_Wanning.Text = "取消报警";
            this.Button_Off_Wanning.UseVisualStyleBackColor = false;
            this.Button_Off_Wanning.Click += new System.EventHandler(this.Button_Off_Wanning_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(558, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 16);
            this.label1.TabIndex = 31;
            this.label1.Text = "mm/s";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(558, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 16);
            this.label2.TabIndex = 32;
            this.label2.Text = "mm/s²";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(743, 134);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 16);
            this.label3.TabIndex = 33;
            this.label3.Text = "mm";
            // 
            // ucTextBoxEx1
            // 
            this.ucTextBoxEx1.BackColor = System.Drawing.Color.Transparent;
            this.ucTextBoxEx1.ConerRadius = 20;
            this.ucTextBoxEx1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ucTextBoxEx1.DecLength = 2;
            this.ucTextBoxEx1.FillColor = System.Drawing.Color.Empty;
            this.ucTextBoxEx1.FocusBorderColor = System.Drawing.Color.Black;
            this.ucTextBoxEx1.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.ucTextBoxEx1.InputText = "";
            this.ucTextBoxEx1.InputType = HZH_Controls.TextInputType.Number;
            this.ucTextBoxEx1.IsFocusColor = true;
            this.ucTextBoxEx1.IsRadius = true;
            this.ucTextBoxEx1.IsShowClearBtn = true;
            this.ucTextBoxEx1.IsShowKeyboard = true;
            this.ucTextBoxEx1.IsShowRect = true;
            this.ucTextBoxEx1.IsShowSearchBtn = false;
            this.ucTextBoxEx1.KeyBoardType = HZH_Controls.Controls.KeyBoardType.UCKeyBorderNum;
            this.ucTextBoxEx1.Location = new System.Drawing.Point(108, 123);
            this.ucTextBoxEx1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ucTextBoxEx1.MaxValue = new decimal(new int[] {
            1300,
            0,
            0,
            0});
            this.ucTextBoxEx1.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ucTextBoxEx1.Name = "ucTextBoxEx1";
            this.ucTextBoxEx1.Padding = new System.Windows.Forms.Padding(5);
            this.ucTextBoxEx1.PasswordChar = '\0';
            this.ucTextBoxEx1.PromptColor = System.Drawing.Color.Gray;
            this.ucTextBoxEx1.PromptFont = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.ucTextBoxEx1.PromptText = "";
            this.ucTextBoxEx1.RectColor = System.Drawing.Color.Black;
            this.ucTextBoxEx1.RectWidth = 5;
            this.ucTextBoxEx1.RegexPattern = "";
            this.ucTextBoxEx1.Size = new System.Drawing.Size(149, 40);
            this.ucTextBoxEx1.TabIndex = 25;
            // 
            // Button_Move
            // 
            this.Button_Move.BackColor = System.Drawing.Color.Transparent;
            this.Button_Move.BaseColor = System.Drawing.Color.DodgerBlue;
            this.Button_Move.BorderColor = System.Drawing.Color.DodgerBlue;
            this.Button_Move.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.Button_Move.DownBack = null;
            this.Button_Move.Location = new System.Drawing.Point(337, 124);
            this.Button_Move.Margin = new System.Windows.Forms.Padding(4);
            this.Button_Move.MouseBack = null;
            this.Button_Move.Name = "Button_Move";
            this.Button_Move.NormlBack = null;
            this.Button_Move.Size = new System.Drawing.Size(124, 40);
            this.Button_Move.TabIndex = 37;
            this.Button_Move.Text = "运动到位";
            this.Button_Move.UseVisualStyleBackColor = false;
            this.Button_Move.Click += new System.EventHandler(this.ButtonMove_Click);
            // 
            // skinLabel4
            // 
            this.skinLabel4.AutoSize = true;
            this.skinLabel4.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel4.BorderColor = System.Drawing.Color.White;
            this.skinLabel4.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel4.Location = new System.Drawing.Point(9, 129);
            this.skinLabel4.Name = "skinLabel4";
            this.skinLabel4.Size = new System.Drawing.Size(74, 21);
            this.skinLabel4.TabIndex = 38;
            this.skinLabel4.Text = "目标位置";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(264, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 16);
            this.label4.TabIndex = 39;
            this.label4.Text = "mm";
            // 
            // EmotorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.skinLabel4);
            this.Controls.Add(this.Button_Move);
            this.Controls.Add(this.ucTextBoxEx1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Button_Off_Wanning);
            this.Controls.Add(this.Button_Enable);
            this.Controls.Add(this.text_position);
            this.Controls.Add(this.Button_ReadAcc);
            this.Controls.Add(this.Button_ReadSpeed);
            this.Controls.Add(this.text_Acc);
            this.Controls.Add(this.text_speed);
            this.Controls.Add(this.Button_Acc);
            this.Controls.Add(this.skinLabel3);
            this.Controls.Add(this.Button_Speed);
            this.Controls.Add(this.skinLabel2);
            this.Controls.Add(this.skinLabel1);
            this.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "EmotorControl";
            this.Size = new System.Drawing.Size(778, 175);
            this.Load += new System.EventHandler(this.EmotorControl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private CCWin.SkinControl.SkinLabel skinLabel1;
        private CCWin.SkinControl.SkinLabel skinLabel2;
        private CCWin.SkinControl.SkinButton Button_Speed;
        private CCWin.SkinControl.SkinLabel skinLabel3;
        private CCWin.SkinControl.SkinButton Button_Acc;
        private HZH_Controls.Controls.UCTextBoxEx text_speed;
        private HZH_Controls.Controls.UCTextBoxEx text_Acc;
        private CCWin.SkinControl.SkinButton Button_ReadSpeed;
        private CCWin.SkinControl.SkinButton Button_ReadAcc;
        private CCWin.SkinControl.SkinTextBox text_position;
        private CCWin.SkinControl.SkinButton Button_Enable;
        private CCWin.SkinControl.SkinButton Button_Off_Wanning;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private HZH_Controls.Controls.UCTextBoxEx ucTextBoxEx1;
        private CCWin.SkinControl.SkinButton Button_Move;
        private CCWin.SkinControl.SkinLabel skinLabel4;
        private System.Windows.Forms.Label label4;
    }
}
