﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp.deviceConnect;

namespace WindowsFormsApp.components.EMotor
{
    public partial class PositionBlock : UserControl
    {
        public PositionBlock()
        {
            InitializeComponent();
            this.SetStyle( ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);
        }
        public EMotor emotor;

        public int PositionIdex;

        public string PositionName
        {
            get
            {
                return GPBOX_Position.Text;
            }
            set
            {
                GPBOX_Position.Text = value;
            }
        }
        public void Read()
        {
            switch (PositionIdex)
            {
                case 1:
                    textbox_Position.InputText = Transform_TO_String(emotor.busTcpClient.ReadDoubleWord(emotor.FirstPositionReg));
                    break;
                case 2:
                    textbox_Position.InputText = Transform_TO_String(emotor.busTcpClient.ReadDoubleWord(emotor.SecondPositionReg));
                    break;
                case 3:
                    textbox_Position.InputText = Transform_TO_String(emotor.busTcpClient.ReadDoubleWord(emotor.ThirdPositionReg));
                    break;
                case 4:
                    textbox_Position.InputText = Transform_TO_String(emotor.busTcpClient.ReadDoubleWord(emotor.FourthPositionReg));
                    break;
                case 5:
                    textbox_Position.InputText = Transform_TO_String(emotor.busTcpClient.ReadDoubleWord(emotor.FifthPositionReg));
                    break;
                case 6:
                    textbox_Position.InputText = Transform_TO_String(emotor.busTcpClient.ReadDoubleWord(emotor.SixthPositionReg));
                    break;
                case 7:
                    textbox_Position.InputText = Transform_TO_String(emotor.busTcpClient.ReadDoubleWord(emotor.SeventhPositionReg));
                    break;
                case 8:
                    textbox_Position.InputText = Transform_TO_String(emotor.busTcpClient.ReadDoubleWord(emotor.EighthPositionReg));
                    break;
                case 11:
                    textbox_Position.InputText = Transform_TO_String(emotor.busTcpClient.ReadDoubleWord(emotor.FirstTGPositionReg));
                    break;
                case 12:
                    textbox_Position.InputText = Transform_TO_String(emotor.busTcpClient.ReadDoubleWord(emotor.SecondTGPositionReg));
                    break;
                case 13:
                    textbox_Position.InputText = Transform_TO_String(emotor.busTcpClient.ReadDoubleWord(emotor.ThirdTGPositionReg));
                    break;
                case 14:
                    textbox_Position.InputText = Transform_TO_String(emotor.busTcpClient.ReadDoubleWord(emotor.FourthTGPositionReg));
                    break;
                case 15:
                    textbox_Position.InputText = Transform_TO_String(emotor.busTcpClient.ReadDoubleWord(emotor.FifthTGPositionReg));
                    break;
                case 16:
                    textbox_Position.InputText = Transform_TO_String(emotor.busTcpClient.ReadDoubleWord(emotor.SixthTGPositionReg));
                    break;
                case 17:
                    textbox_Position.InputText = Transform_TO_String(emotor.busTcpClient.ReadDoubleWord(emotor.SeventhTGPositionReg));
                    break;
                case 18:
                    textbox_Position.InputText = Transform_TO_String(emotor.busTcpClient.ReadDoubleWord(emotor.EighthTGPositionReg));
                    break;
                default:
                    break;
            }
        }

        private void Read_Btn_Click(object sender, EventArgs e)
        {
            //读取寄存器里的每层的位置并写入Text_Box中
            Read();
        }
        private int Transform_TO_Int(string str)
        {
            float a = (float.Parse(str));
            return (int)a*100;
        }
        private string Transform_TO_String(int a)
        {
            double b = a;
            b = b / 100;
            return b.ToString();

        }
        private void Write_Btn_Click(object sender, EventArgs e)
        {
            if (emotor.ID == 0)
            {
                if ((float.Parse(textbox_Position.InputText)) < 25)
                {
                    MessageBox.Show("输入范围超限");
                    return;
                }
            }
            if (emotor.ID == 1)
            {
                if ((float.Parse(textbox_Position.InputText)) < 500)
                {
                    MessageBox.Show("输入范围超限");
                    return;
                }
            }
            if (emotor.ID == 2)
            {
                if ((float.Parse(textbox_Position.InputText)) < 144)
                {
                    MessageBox.Show("输入范围超限");
                    return;
                }
            }
            //将text_box里的内容写入到寄存器中
            switch (PositionIdex)
            {
                case 1:
                    emotor.busTcpClient.Write(emotor.FirstPositionReg, Transform_TO_Int(textbox_Position.InputText));
                    break;
                case 2:
                    emotor.busTcpClient.Write(emotor.SecondPositionReg, Transform_TO_Int(textbox_Position.InputText));
                    break;
                case 3:
                    emotor.busTcpClient.Write(emotor.ThirdPositionReg, Transform_TO_Int(textbox_Position.InputText));
                    break;
                case 4:
                    emotor.busTcpClient.Write(emotor.FourthPositionReg, Transform_TO_Int(textbox_Position.InputText));
                    break;
                case 5:
                    emotor.busTcpClient.Write(emotor.FifthPositionReg, Transform_TO_Int(textbox_Position.InputText));
                    break;
                case 6:
                    emotor.busTcpClient.Write(emotor.SixthPositionReg, Transform_TO_Int(textbox_Position.InputText));
                    break;
                case 7:
                    emotor.busTcpClient.Write(emotor.SeventhPositionReg, Transform_TO_Int(textbox_Position.InputText));
                    break;
                case 8:
                    emotor.busTcpClient.Write(emotor.EighthPositionReg, Transform_TO_Int(textbox_Position.InputText));
                    break;
                case 11:
                    emotor.busTcpClient.Write(emotor.FifthTGPositionReg, Transform_TO_Int(textbox_Position.InputText));
                    break;
                case 12:
                    emotor.busTcpClient.Write(emotor.SecondTGPositionReg, Transform_TO_Int(textbox_Position.InputText));
                    break;
                case 13:
                    emotor.busTcpClient.Write(emotor.ThirdTGPositionReg, Transform_TO_Int(textbox_Position.InputText));
                    break;
                case 14:
                    emotor.busTcpClient.Write(emotor.FourthTGPositionReg, Transform_TO_Int(textbox_Position.InputText));
                    break;
                case 15:
                    emotor.busTcpClient.Write(emotor.FifthTGPositionReg, Transform_TO_Int(textbox_Position.InputText));
                    break;
                case 16:
                    emotor.busTcpClient.Write(emotor.SixthTGPositionReg, Transform_TO_Int(textbox_Position.InputText));
                    break;
                case 17:
                    emotor.busTcpClient.Write(emotor.SeventhTGPositionReg, Transform_TO_Int(textbox_Position.InputText));
                    break;
                case 18:
                    emotor.busTcpClient.Write(emotor.EighthTGPositionReg, Transform_TO_Int(textbox_Position.InputText));
                    break;
                default:
                    break;
            }
        }
        //启动时读取对应的位置
        private void PositionBlock_Load(object sender, EventArgs e)
        {
            if (emotor?.ID == 0)
            {
                textbox_Position.MaxValue = 1030;
            }
            else if (emotor?.ID == 1)
            {
                textbox_Position.MaxValue = 952;
            }
            else if (emotor?.ID == 2)
            {
                textbox_Position.MaxValue = 1290;
            }
        }
    }

}
