﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp.deviceConnect;

namespace WindowsFormsApp.components.grip
{
    public partial class GripSwitch : UserControl
    {
        public GripSwitch()
        {
            InitializeComponent();
        }
        public Grip grip;

        public string HeaderContent
        {
            get
            {
                return lab_Header.Text;
            }
            set
            {
                lab_Header.Text = value;
            }
        }

        private void Btn_MoveFirst_Click(object sender, EventArgs e)
        {
            RobotConnect.SetDout(grip.robot, grip.Third_move, true);
            Thread.Sleep(100);
            RobotConnect.SetDout(grip.robot, grip.Third_move, false);
            Thread.Sleep(100);
            RobotConnect.SetDout(grip.robot, grip.First_move,true);
            Thread.Sleep(100);
            RobotConnect.SetDout(grip.robot, grip.First_move, false);
        }

        private void Btn_MoveSecond_Click(object sender, EventArgs e)
        {
            RobotConnect.SetDout(grip.robot, grip.Third_move, true);
            Thread.Sleep(100);
            RobotConnect.SetDout(grip.robot, grip.Third_move, false);
            Thread.Sleep(100);
            RobotConnect.SetDout(grip.robot, grip.Second_move, true);
            Thread.Sleep(100);
            RobotConnect.SetDout(grip.robot, grip.Second_move, false);
        }

        private void Btn_MoveThird_Click(object sender, EventArgs e)
        {
            RobotConnect.SetDout(grip.robot, grip.Third_move, true);
            Thread.Sleep(100);
            RobotConnect.SetDout(grip.robot, grip.Third_move, false);
        }
    }



}
