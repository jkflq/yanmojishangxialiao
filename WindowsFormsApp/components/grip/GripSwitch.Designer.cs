﻿
namespace WindowsFormsApp.components.grip
{
    partial class GripSwitch
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lab_Header = new CCWin.SkinControl.SkinLabel();
            this.Btn_MoveFirst = new CCWin.SkinControl.SkinButton();
            this.Btn_MoveSecond = new CCWin.SkinControl.SkinButton();
            this.Btn_MoveThird = new CCWin.SkinControl.SkinButton();
            this.SuspendLayout();
            // 
            // lab_Header
            // 
            this.lab_Header.AutoSize = true;
            this.lab_Header.BackColor = System.Drawing.Color.Transparent;
            this.lab_Header.BorderColor = System.Drawing.Color.White;
            this.lab_Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.lab_Header.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_Header.Location = new System.Drawing.Point(0, 0);
            this.lab_Header.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lab_Header.Name = "lab_Header";
            this.lab_Header.Size = new System.Drawing.Size(89, 21);
            this.lab_Header.TabIndex = 1;
            this.lab_Header.Text = "skinLabel1";
            // 
            // Btn_MoveFirst
            // 
            this.Btn_MoveFirst.BackColor = System.Drawing.Color.Transparent;
            this.Btn_MoveFirst.BaseColor = System.Drawing.Color.DodgerBlue;
            this.Btn_MoveFirst.BorderColor = System.Drawing.Color.DodgerBlue;
            this.Btn_MoveFirst.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.Btn_MoveFirst.DownBack = null;
            this.Btn_MoveFirst.Location = new System.Drawing.Point(0, 63);
            this.Btn_MoveFirst.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Btn_MoveFirst.MouseBack = null;
            this.Btn_MoveFirst.Name = "Btn_MoveFirst";
            this.Btn_MoveFirst.NormlBack = null;
            this.Btn_MoveFirst.Size = new System.Drawing.Size(71, 62);
            this.Btn_MoveFirst.TabIndex = 2;
            this.Btn_MoveFirst.Text = "行程一";
            this.Btn_MoveFirst.UseVisualStyleBackColor = false;
            this.Btn_MoveFirst.Click += new System.EventHandler(this.Btn_MoveFirst_Click);
            // 
            // Btn_MoveSecond
            // 
            this.Btn_MoveSecond.BackColor = System.Drawing.Color.Transparent;
            this.Btn_MoveSecond.BaseColor = System.Drawing.Color.DodgerBlue;
            this.Btn_MoveSecond.BorderColor = System.Drawing.Color.DodgerBlue;
            this.Btn_MoveSecond.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.Btn_MoveSecond.DownBack = null;
            this.Btn_MoveSecond.Location = new System.Drawing.Point(78, 63);
            this.Btn_MoveSecond.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Btn_MoveSecond.MouseBack = null;
            this.Btn_MoveSecond.Name = "Btn_MoveSecond";
            this.Btn_MoveSecond.NormlBack = null;
            this.Btn_MoveSecond.Size = new System.Drawing.Size(71, 62);
            this.Btn_MoveSecond.TabIndex = 3;
            this.Btn_MoveSecond.Text = "行程二";
            this.Btn_MoveSecond.UseVisualStyleBackColor = false;
            this.Btn_MoveSecond.Click += new System.EventHandler(this.Btn_MoveSecond_Click);
            // 
            // Btn_MoveThird
            // 
            this.Btn_MoveThird.BackColor = System.Drawing.Color.Transparent;
            this.Btn_MoveThird.BaseColor = System.Drawing.Color.DodgerBlue;
            this.Btn_MoveThird.BorderColor = System.Drawing.Color.DodgerBlue;
            this.Btn_MoveThird.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.Btn_MoveThird.DownBack = null;
            this.Btn_MoveThird.Location = new System.Drawing.Point(156, 63);
            this.Btn_MoveThird.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Btn_MoveThird.MouseBack = null;
            this.Btn_MoveThird.Name = "Btn_MoveThird";
            this.Btn_MoveThird.NormlBack = null;
            this.Btn_MoveThird.Size = new System.Drawing.Size(71, 62);
            this.Btn_MoveThird.TabIndex = 4;
            this.Btn_MoveThird.Text = "复位";
            this.Btn_MoveThird.UseVisualStyleBackColor = false;
            this.Btn_MoveThird.Click += new System.EventHandler(this.Btn_MoveThird_Click);
            // 
            // GripSwitch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.Btn_MoveThird);
            this.Controls.Add(this.Btn_MoveSecond);
            this.Controls.Add(this.Btn_MoveFirst);
            this.Controls.Add(this.lab_Header);
            this.Font = new System.Drawing.Font("宋体", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "GripSwitch";
            this.Size = new System.Drawing.Size(230, 129);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private CCWin.SkinControl.SkinLabel lab_Header;
        private CCWin.SkinControl.SkinButton Btn_MoveFirst;
        private CCWin.SkinControl.SkinButton Btn_MoveSecond;
        private CCWin.SkinControl.SkinButton Btn_MoveThird;
    }
}
